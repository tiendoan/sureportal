//
//  DetailWorkTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import BubblePictures

class DetailWorkTableViewCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var descLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var mandaysLabel: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var tableView: UITableView!
  
  private var bubblePictures: BubblePictures!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    
    let configFiles = getConfigFiles()
    let layoutConfigurator = BPLayoutConfigurator(
      backgroundColorForTruncatedBubble: UIColor.gray,
      fontForBubbleTitles: UIFont(name: "HelveticaNeue-Bold", size: 16.0)!,
      colorForBubbleBorders: UIColor.white,
      colorForBubbleTitles: UIColor.white,
      maxCharactersForBubbleTitles: 1,
      maxNumberOfBubbles: 3,
      widthForBubbleBorders: 0,
      distanceInterBubbles: -5,
      //direction: .leftToRight,
      alignment: .right)
    
    bubblePictures = BubblePictures(collectionView: collectionView, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
    bubblePictures.delegate = self
    
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 200
    tableView.register(withClass: DetailWotkItemTableViewCell.self)
    tableView.tableFooterView = UIView()
    // Initialization code
  }
  
  override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
    var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
    size.height += tableView.contentSize.height - 100
    return size
  }
  
  func getConfigFiles() -> [BPCellConfigFile] {
    return [
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_1")),
        title: ""),
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_2")),
        title: ""),
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_3")),
        title: ""),
    ]
  }

}

extension DetailWorkTableViewCell: UITableViewDataSource{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withClass: DetailWotkItemTableViewCell.self, for: indexPath)
    cell.avatarImageView.image = UIImage(named: "img_avatar_\(arc4random_uniform(3)+1)")
    cell.nameLabel.text = "Demo 2"
    cell.titleLabel.text = "Giám đốc"
    cell.descLabel.text = "Nguyên cứu và xây dựng danh mục vị trí công việc và bô tiêu chí đánh giá năng lực. Tổ chức lấy ý kiến hoàn toàn khách quan từ các thanh viên trong nhóm."
    cell.timeLabel.text = "6-16, Th6, 2017"
    return cell
  }
}

extension DetailWorkTableViewCell: BPDelegate {
  func didSelectTruncatedBubble() {
    print("Selected truncated bubble")
  }
  
  func didSelectBubble(at index: Int) {
    print(index)
  }
}

