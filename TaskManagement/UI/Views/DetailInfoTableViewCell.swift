//
//  DetailInfoTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class DetailInfoTableViewCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
