//
//  DetailHeaderTableView.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class DetailHeaderTableView: UIView {
  
  @IBOutlet weak var titleLabel: UILabel!

  class func instanceFromNib() -> DetailHeaderTableView {
    return UINib(nibName: "DetailHeaderTableView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DetailHeaderTableView
  }

}
