//
//  TaskTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/3/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SwipeCellKit
import BubblePictures

class TaskTableViewCell: SwipeTableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var statusLabel: UILabel!
  
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var moreButton: UIButton!
  
  private var bubblePictures: BubblePictures!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    moreButton.setImage(#imageLiteral(resourceName: "ic_more_vert_white").withRenderingMode(.alwaysTemplate), for: .normal)
    let configFiles = getConfigFiles()
    let layoutConfigurator = BPLayoutConfigurator(
      backgroundColorForTruncatedBubble: UIColor.gray,
      fontForBubbleTitles: UIFont(name: "HelveticaNeue-Bold", size: 16.0)!,
      colorForBubbleBorders: UIColor.white,
      colorForBubbleTitles: UIColor.white,
      maxCharactersForBubbleTitles: 1,
      maxNumberOfBubbles: 4,
      widthForBubbleBorders: 0,
      distanceInterBubbles: -5,
      //direction: .leftToRight,
      alignment: .right)
    
    bubblePictures = BubblePictures(collectionView: collectionView, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
    bubblePictures.delegate = self
  }
  
  func getConfigFiles() -> [BPCellConfigFile] {
    return [
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_1")),
        title: ""),
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_2")),
        title: ""),
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_3")),
        title: ""),
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_4")),
        title: ""),
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_1")),
        title: ""),
      BPCellConfigFile(
        imageType: BPImageType.image(#imageLiteral(resourceName: "img_avatar_2")),
        title: "")
    ]
  }

}

extension TaskTableViewCell: BPDelegate {
  func didSelectTruncatedBubble() {
    print("Selected truncated bubble")
  }
  
  func didSelectBubble(at index: Int) {
    print(index)
  }
}


