//
//  MainViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/2/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import KYDrawerController

class MainViewController: KYDrawerController{
  
  override var preferredStatusBarStyle: UIStatusBarStyle{
    return .lightContent
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.screenEdgePanGestureEnabled = false
    // Do any additional setup after loading the view.
  }
  
  func toggleMenu() {
    if self.drawerState == .closed {
      self.setDrawerState(.opened, animated: true)
    }
    else{
      self.setDrawerState(.closed, animated: true)
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

extension UIViewController {
  func main() -> MainViewController? {
    var viewController: UIViewController? = self
    while viewController != nil {
      if viewController is KYDrawerController {
        return viewController as? MainViewController
      }
      viewController = viewController?.parent
    }
    return nil;
  }
  
  func showBack(){
    let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(closePage(_:)))
    self.navigationItem.leftBarButtonItem = backButton
  }
  
  //Action func
  func closePage(_ sender: Any) {
    if let navigationController = navigationController, navigationController.viewControllers.first != self {
      navigationController.popViewController(animated: true)
    } else {
      dismiss(animated: true, completion: nil)
    }
  }
}
