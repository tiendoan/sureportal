//
//  Transfer.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 10/17/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class Transfer: Mappable {
  var cancelTime: Date?
  var departmentID: String?
  var departmentLevel: Int?
  var departmentName: String?
  var distributeDate: Date?
  var readTime: Date?
  var reason: String?
  var receiverLevel: Int?
  var receiverLoginJobTitle: String?
  var receiverLoginName: String?
  var receiverLoginPicture: String?
  var receiverName: String?
  var senderLoginName: String?
  var senderName: String?
  var status: String?
  var transferID: String?
  var transferTime: Date?
  var transferType: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    cancelTime <- map["CancelTime"]
    departmentID <- map["DepartmentID"]
    departmentLevel <- map["DepartmentLevel"]
    departmentName <- map["DepartmentName"]
    distributeDate <- (map["DistributeDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    readTime <- (map["ReadTime"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    reason <- map["Reason"]
    receiverLevel <- map["ReceiverLevel"]
    receiverLoginJobTitle <- map["ReceiverLoginJobTitle"]
    receiverLoginName <- map["ReceiverLoginName"]
    receiverLoginPicture <- map["ReceiverLoginPicture"]
    receiverName <- map["ReceiverName"]
    senderLoginName <- map["SenderLoginName"]
    senderName <- map["SenderName"]
    status <- map["Status"]
    transferID <- map["TransferID"]
    transferTime <- (map["TransferTime"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    transferType <- map["TransferType"]
  }
}
