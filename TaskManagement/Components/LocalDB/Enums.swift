//
//  Enums.swift
//  TaskManagement
//
//  Created by Tuanhnm on 11/29/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation

enum RolesInTask: Int {
  case process = 1
  case collaborate = 2
  case acknowledge = 3
  case other
}

enum ProcessTrackingStatus: Int {
  case inprogress = 1
  case report = 2
  case finished = 4
  case remain = 5
}

enum AppriseTrackingStatus: Int {
  case rate = 4 // Include: Normal, Good, Excellence
  case refund = 0 // Like disagree
  case cancel = 3
  case confirm = 1
}

enum AppriseTrackingType: Int {
  case normal = 1
  case good = 2
  case excellence = 3
}
