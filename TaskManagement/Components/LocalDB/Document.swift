//
//  Document.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/27/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import GRDB
import ObjectMapper


enum DocumentAction: String {
  case assign = "Assign", approve = "Approve", finish = "Finish", forward = "Forward" , appraise = "Appraise", process = "Process" , sendMail = "SendMail", history = "HistoryTracking"
  
  func getIntTagValue() -> Int {
    switch self {
    case .assign:
      return 1
    case .approve:
        return 2
    case .finish:
        return 3
    case .forward:
        return 4
    case .appraise:
        return 5
    case .process:
        return 6
    case .sendMail:
        return 7
    case .history:
        return 8
    }
  }
    
    // Will update in the future
    var successMessage: String {
        switch self {
        case .sendMail:
            return "Gửi mail thành công!"
        default: return "Xử lý thành công!"
        }
    }
    
    var image: UIImage {
        let imgAction: String = "ic_action_\(self.rawValue)"
        if let imgObj = UIImage(named: imgAction) {
            return imgObj.imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        } else {
            return  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        }
    }
}

struct DocumentActionType {
  var actionType: DocumentAction?
  var actionText: String?
}

enum ItemType: String {
  case document = "Document", task = "Task", calendar = "Calendar"
}

class DocumentActionObject: Mappable {
    var actionName: String?
    var actionCode: String? {
        didSet{
            if let code = actionCode {
                actionType = DocumentAction(rawValue: code)
            }
        }
    }
    var actionType: DocumentAction?
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        actionName  <- map["Name"]
        actionCode  <- map["Code"]
    }
}

class DocumentPage: Mappable {
  
  var totalItems:Int?
  var offsetItems:Int?
  var currentPage:Int?
  var pageSize:Int?
  var totalPages:Int?
  var startPage:Int?
  var endPage:Int?
  var summary:String?
  
  required init?(map: Map) {}
  func mapping(map: Map) {
    totalItems  <- map["TotalItems"]
    offsetItems  <- map["OffsetItems"]
    currentPage  <- map["CurrentPage"]
    pageSize  <- map["PageSize"]
    totalPages  <- map["TotalPages"]
    startPage  <- map["StartPage"]
    endPage <- map["EndPage"]
    summary  <- map["Summary"]
  }
}



class TrackingFileDocument: Mappable {
    
    var id:String?
    var name:String?
    var ext:String?
    var size:Int?
    var deptID:String?
    var trackingWorkflowDocumentID:String?
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        id <- map["ID"]
        name <- map["FileName"]
        ext  <- map["FileExt"]
        size  <- map["Size"]
        deptID  <- map["DeptID"]
        trackingWorkflowDocumentID  <- map["TrackingWorkflowDocumentID"]
    }
}

/*Status 0 : Mới

1: Đang xử lý

2: báo cáo

3: Hoãn

4 : Kết thúc */
class StatusDocument: Mappable {
  
    var id:Int? {
        didSet {
            isNew = (id == 0)
        }
    }
  var name:String?
  var code:String?
  var colorCode:String?
  
    var isNew = false
  required init?(map: Map) {}
  func mapping(map: Map) {
    id <- map["ID"]
    name <- map["Name"]
    code <- map["Code"]
    colorCode <- map["ColorCode"]
    if colorCode == nil {
      colorCode <- map["Color"]
    }
  }
    
    static var finishStatus: StatusDocument {
        let status = Mapper<StatusDocument>().map(JSON:[:])!
        status.id = 4
        status.name = "Kết thúc"
        status.colorCode = "#73C101"
        return status
    }

}

class PriorityDocument: Mappable {
  
  var id:String?
  var name:String?
  var mapingID:Int?
  
  required init?(map: Map) {}
  func mapping(map: Map) {
    id <- map["ID"]
    name <- map["Name"]
    mapingID <- map["MapingID"]
  }
}

class SecretDocument: Mappable {
  var id:String?
  var name:String?
  var mapingID:Int?
  
  required init?(map: Map) {}
  func mapping(map: Map) {
    id <- map["ID"]
    name <- map["Name"]
    mapingID <- map["MapingID"]
  }
}

class Document: Record, Mappable {
  
  var id:String?
  var approvedDate: Date?
  var approvedByJobTitle:String?
  var approvedByName:String?
  var approvedByPicture:String?
  var docNumber:Int?
  var serialNumber:String?
  var summary:String?
  var content:String?
  var docDate:String?
  var sender:String?
  var receiver: String?
  var treeFilterPath: String?
  var status:Int?
  var authorLoginName:String?
  var statusDocument:StatusDocument?
  var priorityDocument:PriorityDocument?
  var secretDocument:SecretDocument?
  var fileDocuments:[FileDocument]?
  var actions: [DocumentActionObject]?
    var actionsJson: [String]?
  var type: ItemType?
  var isActive: Bool?
  var fromDate: Date?
  var toDate: Date?
  var finishedDate: Date?
  var docType: String?
  var secret: String?
  var scope: Int?
  var sendProvider: Int?
  var workFlow: String?
  var workFlowInfos: String?
  var isBroadCast: Bool?
  var extensiveInfo: String?
  var created: Date?
  var authorID: String?
  var authorName: String?
  var authorJobTitle: String?
  var authorPicture: String?
  var modified: Date?
  var editDepartment: String?
  var editorID: String?
  var editorName: String?
  var editorLoginName: String?
  var taskName: String?
  var taskDescription: String?
  var bookDocument: String?
  var documentType: String?
    var trackingDocuments: [ProgressTracking]?
    var unReadColor:String?
    var relatedDocuments: [RelatedDocument]?
    var categorizeName: String?
    var receivedDate: Date?
    var signedBy: String?
    var workFlowStepID: String?
    var isInComing: Bool?
    var importantDocument: ImportantDocument?
  required init?(map: Map) {
    super.init()
  }
  
  func mapping(map: Map) {
    id <- map["ID"]
    approvedDate <- (map["Approved"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    approvedByJobTitle <- map["ApprovedByJobTitle"]
    approvedByName <- map["ApprovedByName"]
    approvedByPicture <- map["ApprovedByPicture"]
    docNumber <- map["DocNumber"]
    serialNumber <- map["SerialNumber"]
    summary <- map["Summary"]
    content <- map["Content"]
    docDate <- map["DocDate"]
    sender <- map["Sender"]
    receiver <- map["Receiver"]
    status <- map["Status"]
    authorLoginName <- map["AuthorLoginName"]
    statusDocument <- map["StatusDocument"]
    priorityDocument <- map["PriorityDocument"]
    secretDocument <- map["SecretDocument"]
    fileDocuments <- map["FileDocuments"]
    workFlowStepID <- map["WorkFlowStepID"]
    isInComing <- map["IsInComing"]
    actions <- map["ItemActions"]
//    actions = []
//    if let array = map["ItemActions"].currentValue as? NSArray{
//      for i in 0..<array.count {
//        if let item = array[i] as? [String : Any] {
//          if let i = item["Code"] as? DocumentAction.RawValue,
//            let value = item["Name"] as? String {
//            let typeItem: DocumentActionType = DocumentActionType(actionType: DocumentAction(rawValue: i), actionText: value)
//            actions?.append(typeItem)
//          }
//        }
//      }
//    }
    //actions <- (map["ItemActions"], EnumTransform<DocumentAction>())
    type <- (map["ItemType"], EnumTransform<ItemType>())
    isActive <- map["IsActive"]
    fromDate <- map["FromDate"]
    if(fromDate == nil){
      fromDate <- (map["FromDateString"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    }
    toDate <- map["ToDate"]
    if(toDate == nil){
      toDate <- (map["ToDateString"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    }
    finishedDate <- map["FinishedDate"]
    docType <- map["DocType"]
    secret <- map["Secret"]
    scope <- map["Scope"]
    sendProvider <- map["SendProvider"]
    workFlow <- map["WorkFlow"]
    workFlowInfos <- map["WorkFlowInfos"]
    isBroadCast <- map["IsBroadCast"]
    extensiveInfo <- map["ExtensiveInfo"]
    created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ssZZZZZ"))
    if (created == nil) {
      created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    }
    if (created == nil) {
        created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSS"))
    }
    authorID <- map["AuthorID"]
    authorName <- map["AuthorName"]
    authorJobTitle <- map["AuthorJobTitle"]
    authorPicture <- map["authorPicture"]
    modified <- (map["Modified"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ssZZZZZ"))
    editDepartment <- map["EditDepartment"]
    editorID <- map["EditorID"]
    editorName <- map["EditorName"]
    editorLoginName <- map["EditorLoginName"]
    taskName <- map["TaskName"]
    taskDescription <- map["TaskDescription"]
    bookDocument <- map["BookDocument"]
    documentType <- map["DocumentType"]
    trackingDocuments <- map["TrackingDocuments"]
    unReadColor <- map["UnReadColor"]
    relatedDocuments <- map["RelatedDocuments"]
    categorizeName <- map["CategorizeName"]
    receivedDate <- (map["ReceivedDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    signedBy <- map["SignedBy"]
    importantDocument <- map["ImportantDocument"]

    treeFilterPath <- map["TreeFilterPath"]
    
  }
  
  // MARK: Record overrides
  
  override class var databaseTableName: String {
    return "document"
  }
  
  required init(row: Row) {
    id = row.value(named: "ID")
    docNumber = row.value(named: "DocNumber")
    serialNumber  = row.value(named: "SerialNumber")
    summary  = row.value(named: "Summary")
    content  = row.value(named: "Content")
    docDate  = row.value(named: "DocDate")
    sender  = row.value(named: "Sender")
    receiver  = row.value(named: "Receiver")
    status  = row.value(named: "Status")
    authorLoginName  = row.value(named: "AuthorLoginName")
    statusDocument  = StatusDocument(JSONString: row.value(named: "StatusDocument"))
    priorityDocument = PriorityDocument(JSONString: row.value(named: "PriorityDocument"))
    secretDocument = SecretDocument(JSONString: row.value(named: "SecretDocument"))
    //fileDocuments  = Mapper<FileDocument>().mapArray(JSONString: row.value(named: "FileDocuments"))
    actions  = Mapper<DocumentActionObject>().mapArray(JSONString: row.value(named: "ItemActions"))
    //    actions = Mapper<EnumTransform<DocumentAction>()>().mapArray(JSONString: row.value(named: "ItemActions"))
    type = ItemType(rawValue: row.value(named: "ItemType"))
    isActive  = row.value(named: "IsActive")
    fromDate  = row.value(named: "FromDate")
    toDate  = row.value(named: "ToDate")
    finishedDate  = row.value(named: "FinishedDate")
    docType  = row.value(named: "DocType")
    secret  = row.value(named: "Secret")
    scope  = row.value(named: "Scope")
    sendProvider  = row.value(named: "SendProvider")
    workFlow  = row.value(named: "WorkFlow")
    workFlowInfos  = row.value(named: "WorkFlowInfos")
    isBroadCast  = row.value(named: "IsBroadCast")
    extensiveInfo  = row.value(named: "ExtensiveInfo")
    created  = row.value(named: "Created")
    authorID  = row.value(named: "AuthorID")
    authorName  = row.value(named: "AuthorName")
    authorJobTitle = row.value(named: "AuthorJobTitle")
    authorPicture = row.value(named: "authorPicture")
    modified  = row.value(named: "Modified")
    editDepartment = row.value(named: "EditDepartment")
    editorID  = row.value(named: "EditorID")
    editorName  = row.value(named: "EditorName")
    editorLoginName  = row.value(named: "EditorLoginName")
    taskName  = row.value(named: "TaskName")
    taskDescription  = row.value(named: "TaskDescription")
    bookDocument  = row.value(named: "BookDocument")
    documentType  = row.value(named: "DocumentType")
    workFlowStepID = row.value(named: "WorkFlowStepID")
    
    approvedByJobTitle  = row.value(named: "ApprovedByJobTitle")
    approvedByName  = row.value(named: "ApprovedByName")
    approvedByPicture = row.value(named: "ApprovedByPicture")
    isInComing = row.value(named: "IsInComing")
    receivedDate = row.value(named: "ReceivedDate")
    importantDocument = ImportantDocument(JSONString: row.value(named: "ImportantDocument"))
    
    super.init(row: row)
  }
  
  override func encode(to container: inout PersistenceContainer) {
    container["ID"] = id
    container["DocNumber"] = docNumber
    container["SerialNumber"] = serialNumber
    container["Summary"] = summary
    container["Content"] = content
    container["DocDate"] = docDate
    container["Sender"] = sender
    container["Receiver"] = receiver
    container["Status"] = status
    container["AuthorLoginName"] = authorLoginName
    container["StatusDocument"] = statusDocument?.toJSONString()
    container["PriorityDocument"] = priorityDocument?.toJSONString()
    container["SecretDocument"] = secretDocument?.toJSONString()
    //container["FileDocuments"] = fileDocuments?.toJSONString()
    container["WorkFlowStepID"] = workFlowStepID
    container["ItemActions"] = actions?.toJSONString()
    container["ItemType"] = type?.rawValue
    container["IsActive"] = isActive
    container["FromDate"] = fromDate
    container["ToDate"] = toDate
    container["FinishedDate"] = finishedDate
    container["DocType"] = docType
    container["Secret"] = secret
    container["Scope"] = scope
    container["SendProvider"] = sendProvider
    container["WorkFlow"] = workFlow
    container["WorkFlowInfos"] = workFlowInfos
    container["IsBroadCast"] = isBroadCast
    container["ExtensiveInfo"] = extensiveInfo
    container["Created"] = created
    container["AuthorID"] = authorID
    container["AuthorName"] = authorName
    container["AuthorJobTitle"] = authorJobTitle
    container["AuthorPicture"] = authorPicture
    container["Modified"] = modified
    container["EditDepartment"] = editDepartment
    container["EditorID"] = editorID
    container["EditorName"] = editorName
    container["EditorLoginName"] = editorLoginName
    container["TaskName"] = taskName
    container["TaskDescription"] = taskDescription
    container["BookDocument"] = bookDocument
    container["DocumentType"] = documentType
    
    container["ApprovedByJobTitle"] = approvedByJobTitle
    container["ApprovedByName"] = approvedByName
    container["ApprovedByPicture"] = approvedByPicture
    container["IsInComing"] = isInComing
    container["ReceivedDate"] = receivedDate
    
    container["ImportantDocument"] = importantDocument?.toJSONString()
    
  }
    
    func saveToDB(_ db: Database) throws {
        try self.save(db)
        
        //save trackingDocuments
        if let trackingDocs = trackingDocuments {
            for item in trackingDocs {
                item.docID = self.id
                try item.save(db)
            }
        }
        
        //save FileDocuments
        if let files = fileDocuments {
            for item in files {
                item.docID = self.id
                try item.save(db)
            }
        }
    }
    
    func removeAction(_ action: DocumentAction) {
        guard let actions = actions else {
            return
        }
        
        self.actions = actions.filter {$0.actionType != action}
    }
    
    func downloadFiles() {
        if let files = fileDocuments {
            for file in files {
                file.downloadFile()
            }
        }
    }
}

class DocumentData:Mappable{
  
  var page:DocumentPage?
  var items = [Document]()
  
  required init?(map: Map) {}
  func mapping(map: Map) {
    page <- map["Page"]
    items <- map["Items"]
  }
}

class ImportantDocument: Mappable{
    
    var ID: String?
    var name: String?
    var mappingID: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        mappingID <- map["MappingID"]
        name <- map["Name"]
    }
}

class RelatedDocument: Mappable{
  
  var authorID: String?
  var created: Date?
  var fromDocID: String?
  var toDocID: String?
  var toDocSerialNumber: String?
  var type: Int?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    authorID <- map["AuthorID"]
    created <- map["Created"]
    fromDocID <- map["FromDocID"]
    toDocID  <- map["ToDocID"]
    toDocSerialNumber <- map["ToDocSerialNumber"]
    type <- map["Type"]
  }
}

class CategoryTracking: Mappable {
  
  var id: String?
  var name: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    id <- map["ID"]
    name <- map["Name"]
  }
}

class AssignTracking: Mappable {
  
  var assignByLoginName: String?
  var categorizes: [CategoryTracking]?
  var description: String?
  var files: [FileDocument]?
  var fromDate: Date?
  var toDate: Date?
  var isReport: Bool?
  var isSendMail: Bool?
  var isSendSMS: Bool?
    var isNotice: Bool?
    var isReply: Bool?
  var parentID: String?
  var primaryAssignTo: [String]?
  var readOnlyAssignTo: [String]?
  var supportAssignTo: [String]?
    var important: ImportantDocument?
    
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    assignByLoginName <- map["AssignByLoginName"]
    categorizes <- map["Categorizes"]
    description <- map["Description"]
    files <- map["Files"]
    fromDate <- (map["FromDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    toDate <- (map["ToDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    isReport <- map["IsReport"]
    isSendMail <- map["IsSendMail"]
    isSendSMS <- map["IsSendSMS"]
    isNotice <- map["IsNotice"]
    isReply <- map["IsReplyDoc"]
    parentID <- map["ParentID"]
    primaryAssignTo <- map["PrimaryAssignTo"]
    readOnlyAssignTo <- map["ReadOnlyAssignTo"]
    supportAssignTo <- map["SupportAssignTo"]
    important <- map["Important"]
  }
}

class ProcessTracking: Mappable {
  
  var assignBy: String?
  var assignByJobTitle: String?
  var assignByLoginName: String?
  var assignByName: String?
  var assignByPicture: String?
  var assignTo: String?
  var assignToJobTitle: String?
  var assignToLoginName: String?
  var assignToName: String?
  var assignToPicture: String?
  var authorID: String?
  var childs: [ProcessTracking]?
  var created: Date?
  var deptID: String?
  var docID: String?
  var docRelatedID: String?
  var editorID: String?
  var fileTrackingDocuments: [FileDocument]?
  var finishedDate: Date?
  var fromDate: Date?
  var hasChild: Bool?
  var ID: String?
  var isOverDue: Bool?
  var isRecursive: Bool?
  var isReport: Bool?
  var itemActions: [DocumentActionType]?
  var lastResult: Bool?
  var modified: Date?
  var name: String?
  var parentID: String?
  var percentFinish: Float?
  var rating: Float?
  var status: String?
  var toDate: Date?
  var trackingStatus: StatusDocument?
  var type: Int?
  var description: String?
  var trackingID: String?
  var documentID: String?
  var newDueDate: Date?
  
    var files:[TrackingFileDocument]?   //TODO: using for apprise task -> should be move to a new object
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    trackingID <- map["TrackingID"]
    documentID <- map["DocumentID"]
    assignBy <- map["AssignBy"]
    assignByJobTitle <- map["AssignByJobTitle"]
    assignByLoginName <- map["AssignByLoginName"]
    assignByName <- map["AssignByName"]
    assignByPicture <- map["AssignByPicture"]
    assignTo <- map["AssignTo"]
    assignToJobTitle <- map["AssignToJobTitle"]
    assignToLoginName <- map["AssignToLoginName"]
    assignToName <- map["AssignToName"]
    assignToPicture <- map["AssignToPicture"]
    authorID <- map["AuthorID"]
    childs <- map["Childs"]
    created <- map["Created"]
    deptID <- map["DeptID"]
    docID <- map["DocID"]
    docRelatedID <- map["DocRelatedID"]
    editorID <- map["EditorID"]
    fileTrackingDocuments <- map["FileTrackingDocuments"]
    finishedDate <- (map["FinishedDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    fromDate <- (map["FromDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    toDate <- (map["ToDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    newDueDate <- (map["NewDueDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    hasChild <- map["HasChild"]
    ID <- map["ID"]
    isOverDue <- map["IsOverDue"]
    isRecursive <- map["IsRecursive"]
    isReport <- map["IsReport"]
    itemActions <- map["ItemActions"]
    lastResult <- map["LastResult"]
    modified <- map["modified"]
    name <- map["Name"]
    parentID <- map["ParentID"]
    percentFinish <- map["PercentFinish"]
    rating <- map["Rating"]
    status <- map["Status"]
    trackingStatus <- map["TrackingStatus"]
    type <- map["Type"]
    description <- map["Description"]
    
    files <- map["Files"]
  }
    
    class func defaultProcessTracking() -> ProcessTracking {
        let tracking = Mapper<ProcessTracking>().map(JSON:[:])!
        tracking.ID = Constants.default.DEFAULT_PARENT_ID
        tracking.name = "Công việc mới"
        
        return tracking
    }
  
}

class NotificationItem: Mappable {
  
  var authorID: String?
  var authorJobTitle: String?
  var authorLoginName: String?
  var authorName: String?
  var authorPicture: String?
  var created: Date?
  var itemID: String?
  var itemIcon: String?
  var itemType: String?
  var itemTypeName: String?
  var summary: String?
  var moduleCode: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    authorID <- map["AuthorID"]
    authorJobTitle <- map["AuthorJobTitle"]
    authorLoginName <- map["AuthorLoginName"]
    authorName <- map["AuthorName"]
    authorPicture <- map["AuthorPicture"]
    created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    itemID <- map["ItemID"]
    itemIcon <- map["ItemIcon"]
    itemType <- map["ItemType"]
    itemTypeName <- map["ItemTypeName"]
    summary <- map["Summary"]
    moduleCode <- map["ModuleCode"]
  }
}


func ==(l: Document, r: Document) -> Bool {
    return l.id == r.id
}
extension Document : Equatable {
    
}

