//
//  ProgressData.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 10/10/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper
import GRDB

class ProgressHistory: Mappable {
  var action: String?
  var authorFullName: String?
  var authorID: String?
  var authorLoginName: String?
  var authorPicture: String?
  var authorJobTitle: String?
  var created: Date?
  var docID: String?
  var editorID: String?
  var id: String?
  var isHighLight: Bool?
  var summary: String?
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    action <- map["Action"]
    authorFullName <- map["AuthorFullName"]
    if authorFullName == nil {
        authorFullName <- map["AuthorName"]
    }
    authorID <- map["AuthorID"]
    authorJobTitle <- map["AuthorJobTitle"]
    authorLoginName <- map["AuthorLoginName"]
    authorPicture <- map["AuthorPicture"]
    created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSS"))
    docID <- map["DocID"]
    editorID <- map["EditorID"]
    id <- map["ID"]
    isHighLight <- map["IsHighLight"]
    summary <- map["Summary"]
    if summary == nil {
        summary <- map["Description"]
    }
  }
}

class ProgressTracking: Record, Mappable {
  
  var assignBy: String?
  var assignByLoginName: String?
  var assignByName: String?
  var assignByPicture: String?
  var assignTo: String?
  var assignToLoginName: String?
  var assignToName: String?
  var assignToPicture: String?
  var authorID: String?
  var created: Date?
  var deptID: String?
  var docID: String?
  var docRelatedID: String?
  var editorID: String?
  var finishedDate: Date?
  var fromDate: Date?
  var hasChild: Bool?
  var id: String?
  var isRecursive: Bool?
  var isReport: Bool?
  var lastResult: String?
  var modified: Date?
  var name: String?
  var parentID: String?
  var percentFinish: Int?
  var rating: Int?
  var recursiveType: Int?
  var status: Int?
  var toDate: Date?
  var type: Int?
  var statusDocument: StatusDocument?
  var childs: [ProgressTracking]?
  var recentChilds: [ProgressTracking]?
  var fileTrackingDocuments: [FileDocument]?
  var assignToJobTitle: String?
    var actions = [DocumentActionObject]()
    required init?(map: Map) {
        super.init()
    }
    
    var typeColor: UIColor {
        get{
            guard let type = type else {
                return UIColor.clear
            }
            if type == 3 {
                return Theme.default.normalBlueSelectedColor
            } else if type == 7 {
                return Theme.default.normalOrangeSelectedColor
            }
            return Theme.default.normalGreenSelectedColor
        }
    }
  
  func mapping(map: Map) {
    assignBy <- map["AssignBy"]
    assignByLoginName <- map["AssignByLoginName"]
    assignByName <- map["AssignByName"]
    assignByPicture <- map["AssignByPicture"]
    assignTo   <- map["AssignTo"]
    assignToLoginName <- map["AssignToLoginName"]
    assignToName <- map["AssignToName"]
    assignToPicture <- map["AssignToPicture"]
    authorID <- map["AuthorID"]
    created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    deptID <- map["DeptID"]
    docID <- map["DocID"]
    docRelatedID <- map["DocRelatedID"]
    editorID <- map["EditorID"]
    finishedDate <- (map["FinishedDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    fromDate <- (map["FromDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    hasChild <- map["HasChild"]
    id <- map["ID"]
    isRecursive <- map["IsRecursive"]
    isReport <- map["IsReport"]
    lastResult <- map["LastResult"]
    modified   <- (map["Modified"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    name <- map["Name"]
    parentID <- map["ParentID"]
    percentFinish <- map["PercentFinish"]
    rating <- map["Rating"]
    recursiveType <- map["RecursiveType"]
    status <- map["Status"]
    toDate <- (map["ToDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    type <- map["Type"]
    statusDocument <- map["TrackingStatus"]
    childs <- map["Childs"]
    recentChilds <- map["RecentChilds"]
    fileTrackingDocuments <- map["FileTrackingDocuments"]
    assignToJobTitle <- map["AssignToJobTitle"]
    actions <- map["ItemActions"]
  }
    
    // MARK: Record overrides
    
    override class var databaseTableName: String {
        return "progress_tracking"
    }
    
    required init(row: Row) {
        id = row.value(named: "ID")
        assignBy = row.value(named: "AssignBy")
        assignByLoginName = row.value(named: "AssignByLoginName")
        assignByName = row.value(named: "AssignByName")
        assignByPicture = row.value(named: "AssignByPicture")
        assignTo = row.value(named: "AssignTo")
        assignToLoginName = row.value(named: "AssignToLoginName")
        assignToName = row.value(named: "AssignToName")
        assignToPicture = row.value(named: "AssignToPicture")
        authorID = row.value(named: "AuthorID")
        created = row.value(named: "Created")
        deptID = row.value(named: "DeptID")
        docID = row.value(named: "DocID")
        docRelatedID = row.value(named: "DocRelatedID")
        editorID = row.value(named: "EditorID")
        finishedDate = row.value(named: "FinishedDate")
        fromDate = row.value(named: "FromDate")
        hasChild = row.value(named: "HasChild")
        isRecursive = row.value(named: "IsRecursive")
        isReport = row.value(named: "IsReport")
        lastResult = row.value(named: "LastResult")
        modified = row.value(named: "Modified")
        name = row.value(named: "Name")
        parentID = row.value(named: "ParentID")
        percentFinish = row.value(named: "PercentFinish")
        rating = row.value(named: "Rating")
        recursiveType = row.value(named: "RecursiveType")
        status = row.value(named: "Status")
        toDate = row.value(named: "ToDate")
        type = row.value(named: "Type")
        
        if let trackingStatus = row.value(named: "TrackingStatus") as? String{
            statusDocument = StatusDocument(JSONString: trackingStatus)
        }
        
        if let childsJson = row.value(named: "Childs") as? String {
            childs = Mapper<ProgressTracking>().mapArray(JSONString: childsJson)
        }
        
        if let recentJson = row.value(named: "RecentChilds") as? String {
            recentChilds = Mapper<ProgressTracking>().mapArray(JSONString: recentJson)
        }

        if let fileTrackingJson = row.value(named: "FileTrackingDocuments") as? String {
            fileTrackingDocuments = Mapper<FileDocument>().mapArray(JSONString: fileTrackingJson)
        }
        
        actions  = Mapper<DocumentActionObject>().mapArray(JSONString: row.value(named: "ItemActions")) ?? []
        
        assignToJobTitle = row.value(named: "AssignToJobTitle")
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container["ID"] = id
        container["AssignBy"] = assignBy
        container["AssignByLoginName"] = assignByLoginName
        container["AssignByName"] = assignByName
        container["AssignByPicture"] = assignByPicture
        container["AssignTo"] = assignTo
        container["AssignToLoginName"] = assignToLoginName
        container["AssignToName"] = assignToName
        container["AssignToPicture"] = assignToPicture
        container["AuthorID"] = authorID
        container["Created"] = created
        container["DeptID"] = deptID
        container["DocID"] = docID
        container["DocRelatedID"] = docRelatedID
        container["EditorID"] = editorID
        container["FinishedDate"] = finishedDate
        container["FromDate"] = fromDate
        container["HasChild"] = hasChild
        container["IsRecursive"] = isRecursive
        container["IsReport"] = isReport
        container["LastResult"] = lastResult
        container["Modified"] = modified
        container["Name"] = name
        container["ParentID"] = parentID
        container["PercentFinish"] = percentFinish
        container["Rating"] = rating
        container["RecursiveType"] = recursiveType
        container["Status"] = status
        container["ToDate"] = toDate
        container["Type"] = type
        container["AssignToJobTitle"] = assignToJobTitle
        container["TrackingStatus"] = statusDocument?.toJSONString()
        container["FileTrackingDocuments"] = fileTrackingDocuments?.toJSONString()
        container["Childs"] = childs?.toJSONString()
        container["RecentChilds"] = recentChilds?.toJSONString()
        container["ItemActions"] = actions.toJSONString()
    }
}
