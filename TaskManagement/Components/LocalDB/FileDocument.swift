//
//  FileDocument.swift
//  TaskManagement
//
//  Created by Scor Doan on 4/10/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import GRDB
import ObjectMapper
import Alamofire

class FileDocument: Record, Mappable {
    
    var id:String?
    var name:String?
    var ext:String?
    var size:Int?
    var docID:String?
    var previewImg:String?
    var downloadLink:String?
    var localLink:URL?
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        id <- map["ID"]
        name <- map["Name"]
        ext  <- map["Ext"]
        size  <- map["Size"]
        docID  <- map["DocID"]
        previewImg  <- map["PreviewImg"]
        downloadLink  <- map["DownloadLink"]
        
        if name == nil {
            name <- map["FileName"]
        }
        if ext == nil {
            ext <- map["FileExt"]
        }
    }
    
    override class var databaseTableName: String {
        return "file_document"
    }
    
    required init(row: Row) {
        id = row.value(named: "ID")
        name  = row.value(named: "Name")
        ext  = row.value(named: "Ext")
        size = row.value(named: "Size")
        docID  = row.value(named: "DocID")
        previewImg  = row.value(named: "PreviewImg")
        downloadLink = row.value(named: "DownloadLink")
        if let path = row.value(named: "LocalLink") as? String, let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            localLink = documentDirectory.appendingPathComponent(path)
        }
        
        
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container["ID"] = id
        container["Name"] = name
        container["Ext"] = ext
        container["Size"] = size
        container["DocID"] = docID
        container["PreviewImg"] = previewImg
        container["DownloadLink"] = downloadLink
        if let path = localLink?.lastPathComponent {
            container["LocalLink"] = path
        }
        
    }
    
    func downloadFile() {
        guard let link = self.downloadLink, localLink == nil, let fileName = self.name else {
            return
        }
 
        InstanceDB.default.downloadFilePDF(fileName, path: link, done: {[weak self] (result) in
            guard let sself = self else {
                return
            }
            sself.localLink = result
            do {
                try dbQueue.inTransaction { db in
                    try sself.save(db)
                    return .commit
                }
            } catch let e {
                print(e.localizedDescription)
            }
        })
    }
}
