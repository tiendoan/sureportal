//
//  InstanceDB.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import GRDB

let _shared = InstanceDB()

class InstanceDB: NSObject {
  class var `default`: InstanceDB {
    return _shared
  }
    
    func getMenuID(code: String, done:@escaping (_ result:Menu?) -> ()) {
        do {
            try dbQueue.inDatabase{ db in
                let menuData = try Menu.filter(Column("Code") == code).fetchAll(db)
                done(menuData.first)
            }
        } catch let e {
            print(e.localizedDescription)
            done(nil)
        }
    }
  
  func getMenu(isPull: Bool = false, parentId: String = "00000000-0000-0000-0000-000000000000", treeFilterCode: String = "", done:@escaping (_ result:[Menu]) -> ()){
    SyncProvider.getMenu(parentId: parentId, treeFilterCode: treeFilterCode) { (result) in
        
    if ( dbQueue == nil )
    {
        return
    }
        
      if !result.isEmpty {
        
        do {
            try dbQueue.inTransaction { db in
                for item in result {
                    item.parentId = parentId
                    try item.save(db)
                    if let childrens = item.childrens {
                        for child in childrens{
                            child.parentId = item.id
                            try child.save(db)
                        }
                    }
                }
                return .commit
            }
        } catch let e {
            print(e.localizedDescription)
        }

      }
        var menu = [Menu]()
        do {
            try dbQueue.inDatabase{ db in
                let menuData = try Menu.filter(Column("ParentID") == parentId).fetchAll(db)
                for item in menuData {
                    item.childrens = try Menu.filter(Column("ParentID") == item.id).fetchAll(db)
                }
                menu = menuData
            }
        } catch let e {
            print(e.localizedDescription)
        }
        
        if parentId == "00000000-0000-0000-0000-000000000000" {
            //menu.append(Menu.analyticsMenu())
            menu.insert(Menu.analyticsMenu(), at: 0)
        }
        
        done(menu)
    }
  }
  
  func getDocument(isPull: Bool = false, treefilterID: String, page: Int, done:@escaping (_ result:DocumentData?) -> ()){
    SyncProvider.getDocument(treefilterID: treefilterID, page: page) { (result) in
        
        if ( UserDefaults.standard.object(forKey: Constants.SYNC_TIME_SERVER) != nil &&
        (UserDefaults.standard.object(forKey: Constants.SYNC_TIME_SERVER) as! Int) == 0)
        {
            if let items = result?.items {
                do {
                    try dbQueue.inTransaction { db in
                        for item in items {
                            try item.saveToDB(db)
                            let ID = treefilterID + "-" + item.id!
                            try db.execute("INSERT OR REPLACE INTO menu_document (ID, MenuID, DocID, CreatedAt) VALUES (?, ?, ?, ?)", arguments: [ID, treefilterID, item.id, Date()])
                        }
                        return .commit
                    }
                } catch let e {
                    print(e.localizedDescription)
                }
                let attachmentSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ATTACHMENT_DOWNLOAD)
                if attachmentSelected != nil && (attachmentSelected as! Bool) == true {
                    items.forEach{$0.downloadFiles()}
                }
                done(result)
                return
            }
        }
        if let items = result?.items {
            
            done(result)
            return
        }
        var documents = [Document]()
        do {
            try dbQueue.inDatabase{ db in
                documents = try MenuDocumentPair.fetchAll(db, menuId: treefilterID).map{$0.document}
                for doc in documents {
                    //get trackingDocuments
                    let trackingDocuments = try ProgressTracking.filter(Column("DocID") == doc.id).fetchAll(db)
                    doc.trackingDocuments = trackingDocuments
                    
                    //get files
                    let files = try FileDocument.filter(Column("DocID") == doc.id).fetchAll(db)
                    doc.fileDocuments = files
                }
            }
        } catch let e {
            print(e.localizedDescription)
        }
        let data = DocumentData(JSON: [:])
        data?.page = DocumentPage(JSON: [:])
        data?.items = documents
        done(data)
    }
  }
  
  func getDocumentDetail(isPull: Bool = false, documentId: String, done:@escaping (_ result:Document?) -> ()){
    SyncProvider.getDetail(documentId: documentId) { (document) in
        if let doc = document {
            do {
                try dbQueue.inTransaction { db in
                    try doc.saveToDB(db)
                    return .commit
                }
            } catch let e {
                print(e.localizedDescription)
            }

        }
        
        var result:Document?
        // get detail from local
        do {
            try dbQueue.inDatabase{ db in
                if let doc = try Document.filter(Column("ID") == documentId).fetchOne(db) {
                    let trackingDocuments = try ProgressTracking.filter(Column("DocID") == doc.id).fetchAll(db)
                    doc.trackingDocuments = trackingDocuments
                    
                    //get files
                    let files = try FileDocument.filter(Column("DocID") == doc.id).fetchAll(db)
                    doc.fileDocuments = files
                    
                    result = doc
                }
                
            }
        } catch let e {
            print(e.localizedDescription)
        }
      done(result)
    }
  }
  
  func getTopNewItemHomeDashboard(isPull: Bool = false, itemType: String = "Document", page: Int = 1, done:@escaping (_ result: [Document]) -> ()){
    SyncProvider.getTopNewItemHomeDashboard(itemType: itemType, page: page) { (result) in
        if let result = result {
            do {
                try dbQueue.inTransaction { db in
                    for item in result {
                        try item.saveToDB(db) //item.save(db)
                        let menuID = "Home"
                        let ID = menuID + "-" + item.id!
                        try db.execute("INSERT OR REPLACE INTO menu_document (ID, MenuID, DocID, CreatedAt) VALUES (?, ?, ?, ?)", arguments: [ID, menuID, item.id, Date()])
                    }
                    return .commit
                }
            } catch let e {
                print(e.localizedDescription)
            }
            //done(result)
            result.forEach{$0.downloadFiles()}
            done(result)
            return
        }
        var documents = [Document]()
        do {
            try dbQueue.inDatabase{ db in
                documents = try MenuDocumentPair.fetchAll(db, menuId: "Home").map{$0.document}
                for doc in documents {
                    let trackingDocuments = try ProgressTracking.filter(Column("DocID") == doc.id).fetchAll(db)
                    doc.trackingDocuments = trackingDocuments
                    
                    //get files
                    let files = try FileDocument.filter(Column("DocID") == doc.id).fetchAll(db)
                    doc.fileDocuments = files
                }
            }
        } catch let e {
            print(e.localizedDescription)
        }
        //documents.forEach{$0.downloadFiles()}
        done(documents)
    }
  }
  
  func getChartCountOverviewHomeDashboard(isPull: Bool = false, itemType: String = "Document", done:@escaping (_ result: [Chart]) -> ()){
    SyncProvider.getChartCountOverviewHomeDashboard(itemType: itemType) { (result) in
        if let chartData = result {
            chartData.id = itemType
            do {
                try dbQueue.inTransaction { db in
                    try chartData.save(db)
                    return .commit
                }
            } catch let e {
                print(e.localizedDescription)
            }
        }
        var results = [Chart]()
        do {
            try dbQueue.inDatabase{ db in
                let chartData = try ChartData.filter(Column("ID") == itemType).fetchOne(db)
                results = chartData?.charts ?? [Chart]()
            }
        } catch let e {
            print(e.localizedDescription)
        }
        
        done(results)
    }
  }
  
  func getChartLineOverviewHomeDashboard(isPull: Bool = false, viewType: String = "week",  done:@escaping (_ result: [Chart]) -> ()){
    SyncProvider.getChartLineOverviewHomeDashboard(viewType: viewType) { (result) in
        if let chartData = result {
            chartData.id = viewType
            do {
                try dbQueue.inTransaction { db in
                    try chartData.save(db)
                    return .commit
                }
            } catch let e {
                print(e.localizedDescription)
            }
        }
        var results = [Chart]()
        do {
            try dbQueue.inDatabase{ db in
                let chartData = try ChartData.filter(Column("ID") == viewType).fetchOne(db)
                results = chartData?.charts ?? [Chart]()
            }
        } catch let e {
            print(e.localizedDescription)
        }
        
        done(results)
    }
  }
  
  func updateStatusDocument(documentID:String, done:@escaping (_ result:Bool) -> ()){
    SyncProvider.updateStatusDocument(documentID: documentID) { (result) in
      done(result)
    }
  }
  
    func downloadFilePDF(_ fileName: String, path:String, done:@escaping (_ result:URL?) -> ()) {
    
    if let url = UserDefaults.standard.url(forKey: path) {
      done(url)
      SyncProvider.downloadFilePDF(fileName, path: path) { (result) in
        if result != nil{
          UserDefaults.standard.set(result!, forKey: path)
        }
      }
    } else {
      SyncProvider.downloadFilePDF(fileName, path: path) { (result) in
        if result != nil{
          UserDefaults.standard.set(result!, forKey: path)
          done(result)
        }
        else{
          done(nil)
        }
      }
    }
  }
    
    func finishDocuments(_ documents:[Document]) {
        for doc in documents {
            self.finishDocument(document: doc)
        }
    }
    // remove document from New and Processing
    func finishDocument(document:Document) {
        guard let docID = document.id else {
            return
        }
        // load processing menu
        var menu:Menu?
        do {
            try dbQueue.inDatabase{ db in
                menu = try Menu.filter(Column("Code") == MenuCode.currentTasks.codeWithChildMenu(.processing) || Column("Code") == MenuCode.tasksManagement.codeWithChildMenu(.processing)).fetchOne(db)
            }
        } catch let e {
            print(e.localizedDescription)
            return
        }
        
        // save database
        let count = menu?.count ?? 0
        menu?.count = count - 1 < 0 ? 0 : count - 1
        document.statusDocument = StatusDocument.finishStatus
        document.removeAction(.finish)
        let newCode = MenuCode.currentTasks.codeWithChildMenu(.new)
        let processCode = MenuCode.currentTasks.codeWithChildMenu(.processing)
        do {
            try dbQueue.inTransaction { db in
                try document.saveToDB(db)
                try menu?.save(db)
                try db.execute("DELETE FROM menu_document WHERE DocID = ? AND MenuID IN (SELECT ID FROM menu WHERE Code = ? OR Code = ?)", arguments: [docID, newCode, processCode])
                return .commit
            }
        } catch let e {
            print(e.localizedDescription)
        }
    }
    
    func getProdureDigitalPage(keySearch: String, page: Int,status:Int, done:@escaping (_ result:DigitalResponse?) -> ()){
        SyncProvider.getListItemProdure(keword: keySearch, page: page, status: status, done: { (result) in
            
            done(result)
            
        })
    }
    func advanceSearchDigital(keyParam: [String:Any], done:@escaping (_ result:DigitalResponse?) -> ()){
        SyncProvider.advanceSearchDigitalkeyParam(paramSearch: keyParam, done: { (result) in
            
            done(result)
            
        })
    }
    

}

