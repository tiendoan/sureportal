//
//  Chart.swift
//  TaskManagement
//
//  Created by HaiComet on 10/2/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import GRDB
import ObjectMapper

class Chart: Mappable {
    var data:Int?
    var name:String?
    var color:String?
    var id:String?
    
    required init?(map: Map) {}
    
    let transformToInt = TransformOf<Int, AnyObject>(fromJSON: { (value: AnyObject?) -> Int? in
        // transform value from String? to Int?
        if let value = value {
            if value is String {
                return Int(value as! String)
            }else if value is NSNumber {
                return value.integerValue
            }
        }
        return Int(0)
        
    }, toJSON: { (value: Int?) -> AnyObject? in
        // transform value from Int? to String?
        if let value = value {
            return String(value) as AnyObject?
        }
        return nil
    })
    
    func mapping(map: Map) {
        data <- (map["Data"], transformToInt)
        
        name <- map["Name"]
        if(name == nil){
            name <- map["Label"]
        }
        color <- map["Color"]
        id <- map["ID"]
    }
}


class ChartData: Record, Mappable {
    var charts:[Chart]?
    var id:String?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        charts <- map["Data"]
    }
    
    override class var databaseTableName: String {
        return "chart_data"
    }
    
    required init(row: Row) {
        id = row.value(named: "ID")
        charts  = Mapper<Chart>().mapArray(JSONString: row.value(named: "Data"))
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container["ID"] = id
        container["Data"] = charts?.toJSONString()
        
    }
}
