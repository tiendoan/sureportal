//
//  Database.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/27/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import GRDB
import UIKit

// The shared database queue
var dbQueue: DatabaseQueue!

fileprivate func databaseDir() -> String {
    let documentsPath = NSSearchPathForDirectoriesInDomains(
        .documentDirectory, .userDomainMask, true).first! as NSString
    
    return documentsPath as String
}

fileprivate func databasePath() throws -> String {
    let dbPath = databaseDir()
    try FileManager.default.createDirectory(atPath: dbPath, withIntermediateDirectories: true)
    return NSString(string: dbPath).appendingPathComponent("local.db")
}


func setupDatabase(_ application: UIApplication) throws {
  
  // Connect to the database
  // See https://github.com/groue/GRDB.swift/#database-connections
  let dbPath = try databasePath()
  dbQueue = try DatabaseQueue(path: dbPath)
    debugPrint("===> Database Path: \(dbPath)")
  
  // Be a nice iOS citizen, and don't consume too much memory
  // See https://github.com/groue/GRDB.swift/#memory-management
  
  dbQueue.setupMemoryManagement(in: application)
  
  
  // Use DatabaseMigrator to setup the database
  // See https://github.com/groue/GRDB.swift/#migrations
  
  var migrator = DatabaseMigrator()
  
  migrator.registerMigration("createLocalDB") { db in
    
    // Create a table
    // See https://github.com/groue/GRDB.swift#create-tables
    try db.create(table: "menu") { t in
      t.column("ID", .text).primaryKey()
      t.column("ParentID", .text).notNull()
      // Sort doument names in a localized case insensitive fashion by default
      // See https://github.com/groue/GRDB.swift/#unicode
      t.column("Name", .text).notNull().collate(.localizedCaseInsensitiveCompare)
      t.column("Code", .text)
        t.column("Count", .integer)
      t.column("IconLink", .text)
      t.column("NoOrder", .integer)
      t.column("CreatedAt", .datetime)
      t.column("UpdatedAt", .datetime)
    }
    // Populate the table with random data
  }
  
  //    // Migrations for future application versions will be inserted here:
  migrator.registerMigration("createDocumentTable") { db in
    try db.create(table: "document") { t in
      t.column("ID", .text).primaryKey()
      t.column("DocNumber", .integer)
      t.column("SerialNumber", .text)
      t.column("Summary", .text)
      t.column("Content", .text)
      t.column("DocDate", .datetime)
      t.column("SignedBy", .text)
      t.column("Sender", .text)
      t.column("Receiver", .text)
      t.column("InternalReceiver", .text)
      t.column("CarbonCopy", .text)
      t.column("Pages", .integer)
      t.column("Appendix", .integer)
      t.column("Status", .integer)
      t.column("IsInComing", .boolean)
      t.column("IsExternal", .boolean)
      t.column("ExpiredDate", .datetime)
      t.column("Approved", .datetime)
      t.column("ApprovedBy", .datetime)
      t.column("PublishedDate", .datetime)
      t.column("PublishedBy", .datetime)
      t.column("ReceivedDate", .datetime)
      t.column("IsActive", .boolean)
      t.column("FromDate", .datetime)
      t.column("ToDate", .datetime)
      t.column("FinishedDate", .datetime)
      t.column("DocType", .text)
      t.column("Secret", .text)
      t.column("Scope", .integer)
      t.column("Priority", .text)
      t.column("SendProvider", .integer)
      t.column("DocBook", .text)
      t.column("EditDepartment", .text)
      t.column("WorkFlow", .text)
      t.column("WorkFlowInfos", .text)
      t.column("IsBroadCast", .boolean)
      t.column("ExtensiveInfo", .text)
      t.column("Created", .datetime)
      t.column("AuthorID", .text)
      t.column("AuthorName", .text)
         t.column("AuthorJobTitle", .text)
        t.column("authorPicture", .text)
      t.column("AuthorLoginName", .text)
      t.column("Modified", .datetime)
      t.column("EditorID", .text)
      t.column("EditorName", .text)
      t.column("EditorLoginName", .text)
      t.column("TaskName", .text)
      t.column("TaskDescription", .text)
      t.column("BookDocument", .text)
      t.column("DocumentType", .text)
      t.column("ScopeDocument", .text)
        t.column("StatusDocument", .text)
        t.column("PriorityDocument", .text)
        t.column("SecretDocument", .text)
      //t.column("FileDocuments", .text)
        t.column("ItemType", .text)
        t.column("ItemActions", .text)
        t.column("WorkFlowStepID", .text)
        
        t.column("ApprovedByJobTitle", .text)
        t.column("ApprovedByName", .text)
        t.column("ApprovedByPicture", .text)
        t.column("ImportantDocument", .text)
    }
  }
    migrator.registerMigration("createProgressTrackingDocumentTable") { db in
        try db.create(table: "progress_tracking") { t in
            t.column("ID", .text).primaryKey()
            t.column("AssignBy", .text)
            t.column("AssignByLoginName", .text)
            t.column("AssignByName", .text)
            t.column("AssignByPicture", .datetime)
            t.column("AssignTo", .text)
            t.column("AssignToLoginName", .text)
            t.column("AssignToName", .text)
            t.column("AssignToPicture", .text)
            t.column("AuthorID", .text)
            
            t.column("Created", .datetime)
            
            t.column("DeptID", .text)
            t.column("DocID", .text)
            t.column("DocRelatedID", .text)
            t.column("EditorID", .text)
            t.column("FinishedDate", .datetime)
            t.column("FromDate", .datetime)
            t.column("HasChild", .boolean)
            t.column("IsRecursive", .boolean)
            t.column("IsReport", .boolean)
            t.column("LastResult", .text)
            t.column("Modified", .datetime)
            t.column("Name", .text)
            t.column("ParentID", .text)
            t.column("PercentFinish", .integer)
            t.column("Rating", .integer)
            t.column("RecursiveType", .integer)
            t.column("Status", .integer)
            t.column("ToDate", .datetime)
            t.column("Type", .integer)
            t.column("TrackingStatus", .text)
            t.column("Childs", .text)
            t.column("RecentChilds", .text)
            t.column("FileTrackingDocuments", .text)
            t.column("AssignToJobTitle", .text)
            t.column("ItemActions", .text)
        }
    }
    
    migrator.registerMigration("createFileDocumentTable") { db in
        
        try db.create(table: "file_document") { t in
            t.column("ID", .text).primaryKey()
            t.column("Name", .text)
            t.column("Ext", .text)
            t.column("Size", .integer)
            t.column("DocID", .text)
            t.column("PreviewImg", .text)
            t.column("DownloadLink", .text)
            t.column("LocalLink", .text)
        }
    }
    
    migrator.registerMigration("createChartDataTable") { db in
        
        try db.create(table: "chart_data") { t in
            t.column("ID", .text).primaryKey()
            t.column("Data", .text)
        }
    }
  
  migrator.registerMigration("createMenuAndDocumentTable") { db in
    
    try db.create(table: "menu_document") { t in
      t.column("ID", .text).primaryKey()
      t.column("MenuID", .text)
      t.column("DocID", .text)
      t.column("CreatedAt", .datetime)
    }
  }
  
//  migrator.registerMigration("editDocumentTable1") { db in
//    try db.alter(table: "document") { t in
//      t.add(column: "EditDepartment", .text)
//    }
//  }
    
  
  try migrator.migrate(dbQueue)
}

func dropAndRecreateDatabase(_ application: UIApplication) throws {
    dbQueue = nil
    try? FileManager.default.removeItem(atPath: databasePath())
    try setupDatabase(application)
}
