//
//  Announcement.swift
//  TaskManagement
//
//  Created by Scor Doan on 8/14/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

enum AnnouncementType: Int {
    case news = 0
    case event
    case birthDate
}

class Announcement: Mappable {
    
    var id:String?
    var authorID:String?
    var createdDate:Date?
    var type: AnnouncementType = .event
    var category:String?
    var subject:String?
    var summary:String?
    var thumbnail:String?
    var body:String?
    var location:String?
    var startDate:Date?
    var endDate:Date?
    var published: String?
    var authorName: String?
    var authorUserName: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["UserName"]
        authorID <- map["AuthorID"]
        createdDate <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
        category <- map["Category"]
        subject <- map["Subject"]
        summary <- map["Summary"]
        thumbnail <- map["Thumbnail"]
        body <- map["Body"]
        location <- map["Location"]
        startDate <- (map["StartDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
        endDate <- (map["EndDate"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
        published <- map["Published"]
        authorName <- map["AuthorName"]
        authorUserName <- map["AuthorUserName"]
        
        var type = 0
        type <- map["Type"]
        self.type = AnnouncementType(rawValue: type) ?? .news

    }
}
