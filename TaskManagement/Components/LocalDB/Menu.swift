//
//  Menu.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/27/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import GRDB
import ObjectMapper

enum MenuCode: String {
    case currentTasks = "DOCUMENT"          // Công Việc Hiện Tại
    case tasksManagement = "TASK"          // Quản lý công việc (CVDG)
    case analytic = "TK"
    
    func codeWithChildMenu(_ childMenu:MenuChildCode) -> String{
        return self.rawValue + "_" + childMenu.rawValue
    }
}

enum MenuChildCode: String {
    case new = "MOI"
    case processing = "DANGXULY"
    case processed = "DAXULY"
    case all = "TATCA"
    
    func getMenuChildCodeWithRootMenu(_ rootMenu:MenuCode) -> String{
        return rootMenu.rawValue + "_" + self.rawValue
    }
}

class Menu: Record, Mappable {
  var id: String?
  var parentId: String?
  var name: String?
  var code: String?
  var count: Int?
  var icon: String?
  var order: Int?
  var childrens: [Menu]?
  var createdAt: Date?
  var updatedAt: Date?
  
  required init?(map: Map) {
    super.init()
  }
  
  func mapping(map: Map) {
    id <- map["ID"]
    parentId <- map["ParentID"]
    name <- map["Name"]
    code <- map["Code"]
    count <- map["Count"]
    icon <- map["IconLink"]
    order <- map["NoOrder"]
    childrens <- map["Childrens"]
  }
  
  // MARK: Record overrides
  
  override class var databaseTableName: String {
    return "menu"
  }
    
    func isFinishOrNewMenu() -> Bool {
        guard let code = code else {return false}
        if code.contains(MenuChildCode.new.rawValue) || code.contains(MenuChildCode.processing.rawValue) {
            return true
        }
        return false
    }
    
    func isFinishMenu() -> Bool {
        guard let code = code else {return false}
        if code.contains(MenuChildCode.processing.rawValue) {
            return true
        }
        return false
    }
    
    func isNewMenu() -> Bool {
        guard let code = code else {return false}
        if code.contains(MenuChildCode.new.rawValue) {
            return true
        }
        return false
    }
  
  required init(row: Row) {
    id = row.value(named: "ID")
    parentId = row.value(named: "ParentID")
    name = row.value(named: "Name")
    code = row.value(named: "Code")
    count = row.value(named: "Count")
    icon = row.value(named: "IconLink")
    order = row.value(named: "NoOrder")
    createdAt = row.value(named: "CreatedAt")
    order = row.value(named: "UpdatedAt")
    super.init(row: row)
  }
  
  override func encode(to container: inout PersistenceContainer) {
    container["ID"] = id
    container["ParentID"] = parentId
    container["name"] = name
    container["Code"] = code
    container["Count"] = count
    container["IconLink"] = icon
    container["NoOrder"] = order
    container["CreatedAt"] = createdAt
    container["UpdatedAt"] = updatedAt
  }
  
    class func analyticsMenu() -> Menu {
        let menu = Mapper<Menu>().map(JSON:[:])!
        menu.id = "-1"
        menu.name = "Trang Chủ"//"Thông kê"
        menu.code = "TK"
        menu.childrens = []
        menu.count = 0
        menu.order = -1
        menu.parentId = "00000000-0000-0000-0000-000000000000"
        
        return menu
    }
}

/*
 class DBMenu: Record {
 var id: Int64?
 var name: String
 var createdAt: Date
 var updatedAt: Date?
 
 
 init(name: String) {
 self.name = name
 self.createdAt = Date()
 self.updatedAt = nil
 super.init()
 }
 
 // MARK: Record overrides
 
 override class var databaseTableName: String {
 return "menu"
 }
 
 required init(row: Row) {
 id = row.value(named: "id")
 name = row.value(named: "name")
 createdAt = row.value(named: "created_at")
 updatedAt = row.value(named: "updated_at")
 super.init(row: row)
 }
 
 override func encode(to container: inout PersistenceContainer) {
 container["id"] = id
 container["name"] = name
 container["created_at"] = createdAt
 container["updated_at"] = updatedAt
 }
 
 override func didInsert(with rowID: Int64, for column: String?) {
 id = rowID
 }
 
 }*/
