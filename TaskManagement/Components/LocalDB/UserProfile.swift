//
//  UserProfile.swift
//  TaskManagement
//
//  Created by HaiComet on 10/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

extension UserProfile: Equatable {
    
    static func == (lhs: UserProfile, rhs: UserProfile) -> Bool {
        return lhs.loginName == rhs.loginName
    }
    
}

class UserProfile: Mappable {
  
  var address:String?
  var avartar:String?
  var birthDate:String?
  var email:String?
  var ext:String?
  var firstName:String?
  var fullName:String?
  var gender:String?
  var homePhone:String?
  var id:String?
  var isActive:Bool?
  var jobTitle:String?
  var langID:String?
  var lastName:String?
  var loginName:String?
  var mobile:String?
  var userCode:String?
  var userDepartments:String?
  var picture:String?
  var password:String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    address <- map["Address"]
    avartar <- map["Avartar"]
    birthDate <- map["BirthDate"]
    email <- map["Email"]
    ext <- map["Ext"]
    firstName <- map["FirstName"]
    fullName <- map["FullName"]
    gender <- map["Gender"]
    homePhone <- map["HomePhone"]
    id <- map["ID"]
    isActive <- map["IsActive"]
    jobTitle <- map["JobTitle"]
    langID <- map["LangID"]
    lastName <- map["LastName"]
    loginName <- map["LoginName"]
    mobile <- map["Mobile"]
    userCode <- map["UserCode"]
    userDepartments <- map["UserDepartments"]
    picture <- map["Picture"]
    password <- map["Password"]
  }
    func getBackgroundColorToTokenView () -> UIColor {
        return Theme.default.normalGreenSelectedColor
    }
  func save() {
    let data = NSKeyedArchiver.archivedData(withRootObject: self.toJSON())
    UserDefaults.standard.set(data, forKey: Constants.USER_DATA_KEY)
  }
  
  func logout() {
    UserDefaults.standard.set(nil, forKey: Constants.USER_DATA_KEY)
  }
}

var CurrentUser: UserProfile? {
  if let load = UserDefaults.standard.object(forKey: Constants.USER_DATA_KEY) as? NSData {
    if let userData = NSKeyedUnarchiver.unarchiveObject(with: load as Data) as? [String:Any] {
      return UserProfile(JSON: userData)
    }
  }
  return nil
}

class Assignee: Mappable {
  
  var isAdmin: Bool?
  var childrens: [Assignee]?
  var code: String?
  var iCon: String?
  var isChecked: Bool?
  var isDept: Bool?
  var isVisible: Bool?
  var jobTitle: String?
  var parentID: String?
  var parentName: String?
  var picture: String?
  var text: String?
  var value: String?
  var isProcessRoleInTask: Bool?
  var isCollaborateRoleInTask: Bool?
  var isAcknownledgeRoleInTask: Bool?
  var selectedRole:RolesInTask = .other       //using for press menu in assignPerson
  
    init() {
        
    }
       required init? (map: Map) {
        mapping(map: map)
      }
  
  func mapping (map: Map) {
    isAdmin <- map["Admin"]
    childrens <- map["Childrens"]
    code <- map["Code"]
    iCon <- map["Icon"]
    isChecked <- map["IsChecked"]
    isDept <- map["IsDept"]
    isVisible <- map["IsVisible"]
    jobTitle <- map["JobTitle"]
    parentID <- map["ParentID"]
    parentName <- map["ParentName"]
    picture <- map["Picture"]
    text <- map["Text"]
    value <- map["Value"]
    isProcessRoleInTask <- map["IsProcessRoleInTask"]
    isCollaborateRoleInTask <- map["IsCollaborateRoleInTask"]
    isAcknownledgeRoleInTask <- map["IsAcknownledgeRoleInTask"]
  }
  
  func getBackgroundColorToTokenView () -> UIColor {
    if let isProcessRole: Bool = isProcessRoleInTask, isProcessRole {
      return Theme.default.normalGreenSelectedColor
    } else if let isCollaborateRole: Bool = isCollaborateRoleInTask, isCollaborateRole {
      return Theme.default.normalBlueSelectedColor
    } else if let isAcknownledgeRole: Bool = isAcknownledgeRoleInTask, isAcknownledgeRole {
      return Theme.default.normalOrangeSelectedColor
    }
    return Theme.default.normalGreenSelectedColor
  }
  
  static func isEqualOthers (_ source: [Assignee], _ other: [Assignee]) -> Bool {
    var localSource: [Assignee] = source
    var localOther: [Assignee] = other
    if localSource.count != localOther.count {
      return false
    }
    localSource = localSource.sorted { (s1, s2) -> Bool in
      guard let str1: String = s1.value, let str2: String = s2.value else {
        return false
      }
      return str1 > str2
    }
    localOther = localOther.sorted { (s1, s2) -> Bool in
      guard let str1: String = s1.value, let str2: String = s2.value else {
        return false
      }
      return str1 > str2
    }
    let result = zip(localSource, localOther).enumerated().filter() {
        $1.0.value == $1.1.value
    }.count
    return result == localSource.count
  }
}

extension Assignee: Equatable {
  
  static func == (lhs: Assignee, rhs: Assignee) -> Bool {
    return lhs.value == rhs.value
  }
  
}
