//
//  Requests.swift
//  TaskManagement
//
//  Created by Tuanhnm on 11/30/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class AssignTrackingRequest: Mappable {
  var documentID: String?
  var assignTrackings: [AssignTracking]?
    var files: [TrackingFileDocument]?
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    documentID <- map["documentID"]
    assignTrackings <- map["lstAssignTracking"]
    files <- map["Files"]
  }
}
