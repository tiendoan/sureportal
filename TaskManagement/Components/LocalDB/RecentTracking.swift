//
//  RecentTracking.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 10/16/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class RecentTracking : Mappable {
  var id: String?
  var assignToJobTitle: String?
  var assignToLoginName: String?
  var assignToName: String?
  var assignToPicture: String?
  var percentFinish: Float?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    id <- map["ID"]
    assignToJobTitle <- map["AssignToJobTitle"]
    assignToLoginName <- map["AssignToLoginName"]
    assignToName <- map["AssignToName"]
    assignToPicture <- map["AssignToPicture"]
    percentFinish <- map["PercentFinish"]
  }
  
  
}

class GetNextWorkFlowStep : Mappable {
    var backStep: String?
    var ccUser: String?
    var ccUserFullName: String?
    var deptID: String?
    var deptName: String?
    var id: String?
    var isActive: Bool?
    var isAllowAssign: Bool?
    var isEndWorkFlow: Bool?
    var isJobTitle: Bool?
    var isMoveToStep: Bool?
    var isPassed: Bool?
    var isSignCA: Bool?
    var isWaitForDone: Bool?
    var jobTitleName: String?
    var nextStep: String?
    var objectAction: String?
    var objectActionName: String?
    var sequences: Int?
    var typeCode: String?
    var typeID: String?
    var typeName: String?
    var userName: String?
    var workFlowID: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        backStep <- map["BackStep"]
        ccUser <- map["CCUser"]
        ccUserFullName <- map["CCUserFullName"]
        deptID <- map["DeptID"]
        deptName <- map["DeptName"]
        id <- map["ID"]
        isActive <- map["IsActive"]
        isAllowAssign <- map["IsAllowAssign"]
        isEndWorkFlow <- map["IsEndWorkFlow"]
        isJobTitle <- map["IsJobTitle"]
        isMoveToStep <- map["IsMoveToStep"]
        isPassed <- map["IsPassed"]
        isSignCA <- map["IsSignCA"]
        
        isWaitForDone <- map["IsWaitForDone"]
        jobTitleName <- map["JobTitleName"]
        nextStep <- map["NextStep"]
        objectAction <- map["ObjectAction"]
        objectActionName <- map["ObjectActionName"]
        sequences <- map["Sequences"]
        typeCode <- map["TypeCode"]
        
        typeID <- map["TypeID"]
        typeName <- map["TypeName"]
        userName <- map["UserName"]
        workFlowID <- map["WorkFlowID"]
    }
    
    
}
