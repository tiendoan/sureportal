//
//  ProdureDigital.swift
//  TaskManagement
//
//  Created by Mirum User on 5/31/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
//
//  Notification.swift
//  TaskManagement
//
//  Created by Tuanhnm on 1/9/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class Procedures: Mappable {
    
    var procedureData: ProceduresData?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        procedureData <- map["Procedures"]
        
    }
}
class ProceduresObject: Mappable {
    
    var ProcedureID: String?
    var Name:String?
    var workFlowStep:[ProcedureWorkflowSteps]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ProcedureID <- map["ProcedureID"]
        Name <- map["Name"]
        workFlowStep <- map["ProcedureWorkflowSteps"]
        
    }
}
class ProcedureWorkflowSteps: Mappable {
    var Name:String?
    var workTask:[ProcedureWorkflowTasks]?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Name <- map["Name"]
        workTask <- map["ProcedureWorkflowTasks"]
    }
    
}
class ProcedureWorkflowTasks: Mappable {
    
    var UserName:String?
    var isFirst:Bool?
    var Name:String?
    var number:Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        UserName <- map["UserName"]
        isFirst <- map["isFirst"]
        Name <- map["Name"]
        number <- map["number"]
    }
}
class PageProcedure: Mappable {
    
    var CurrentPage: Int?
    var EndPage: Int?
    var OffsetItems: Int?
    var PageSize: Int?
    var TotalItems: Int?
    var TotalPages: Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        CurrentPage <- map["CurrentPage"]
        EndPage <- map["EndPage"]
        OffsetItems <- map["OffsetItems"]
        PageSize <- map["PageSize"]
        TotalItems <- map["TotalItems"]
        TotalPages <- map["TotalPages"]
    }

}

class ProceduresData: Mappable {
    
    var Items: [ProdureDigital]?
    var Page: PageProcedure?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Items <- map["Items"]
        Page <- map["Page"]
    }
}
class ProceduresHistoryObject: Mappable {
    
    var PerformerName: String?
    var Comment:String?
    var Occurred:String?
    var Action:Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        PerformerName <- map["PerformerName"]
        Comment <- map["Comment"]
        Occurred <- map["Occurred"]
        Action <- map["Action"]

    }
}

class ProdureDigital: Mappable {
    
    var ID: String?
    var digitalName: String?
    var ProcedureType: String?
    var ProcedureDate: String?
    var Created: String?
    var Modified: Date?
    var Author: String?
    var Editor: String?
    var Status: Int?
    var Summary: String?
    var Name: String?
    var AuthorName: String?
    var ProcedureTypeName: String?
    var ProcedureCategoryName: String?
    var StatusColor: String?
    var created: Date?
    var JobTitle: String?
    var AuthorAvatar: String?
    var trackingAvatar:[String]?
    var document: [FileDocument]?
    var itemAction:[ItemActions]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        digitalName <- map["Name"]
        ProcedureType <- map["ProcedureType"]
        ProcedureDate <- map["ProcedureDate"]
        created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
        Author <- map["Author"]
        Editor <- map["Editor"]
        Status <- map["Status"]
        Summary <- map["Summary"]
        AuthorName <- map["AuthorName"]
        ProcedureTypeName <- map["ProcedureTypeName"]
        ProcedureCategoryName <- map["ProcedureCategoryName"]
         StatusColor <- map["StatusColor"]
         trackingAvatar <- map["Avatars"]
        AuthorAvatar <- map["AuthorAvatar"]
        JobTitle <- map["JobTitle"]
        document <- map["Attachments"]
        Name <- map["Name"]
        itemAction <- map["ItemActions"]
        
    }
    

}
class ItemActions: Mappable {
    
    var Code: String?
    var Name:String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Code <- map["Code"]
        Name <- map["Name"]
    }
    init() {
        self.Code = ""
        self.Name = ""
    }
}

class ProceduresAttachmentFileObject: Mappable {
    
    var ID: String?
    var Name:String?
    var Ext:String?
    var Size:String?
    var IsDigitalSign:Int?
    var ProcedureAttachmentSigningPositions:[ProcedureAttachmentSigningPositions]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        Name <- map["Name"]
        Ext <- map["Ext"]
        Size <- map["Size"]
        IsDigitalSign <- map["IsDigitalSign"]
        ProcedureAttachmentSigningPositions <- map["ProcedureAttachmentSigningPositions"]
    }
}

class ProcedureAttachmentSigningPositions: Mappable {
    var ID: String?
    var AttachmentID:String?
    var PageNumber:Int?
    var CoordinateX:Int?
    var CoordinateY:Int?
    var HasSignature:Bool?
    var HasStamp:Bool?
    var HasInfo:Bool?
    var SignerID:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        AttachmentID <- map["AttachmentID"]
        PageNumber <- map["PageNumber"]
        CoordinateX <- map["CoordinateX"]
        CoordinateY <- map["CoordinateY"]
        
        HasSignature <- map["HasSignature"]
        HasStamp <- map["HasStamp"]
        HasInfo <- map["HasInfo"]
        SignerID <- map["SignerID"]
        
    }
}

class ProcedureDataObject: Mappable {
    var ID: String?
    var Name:String?
    required init?(map: Map) {
        
    }    
    func mapping(map: Map) {
        ID <- map["ID"]
        Name <- map["Name"]
    }
}

class ProcedureDigitalWorkFlow: Mappable {
    var Author: String?
    var ID:String?
    var Editor:String?
    var IsDeleted: Int?
    var IsPersonal:Int?
    var ReadOnly: Int?
    var Name:String?
    var ProcedureType:String?
    var ProcedureWorkflowTemplateSteps:[ProcedureWorkflowTemplateSteps]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        ID <- map["ID"]
        Author <- map["Author"]
        Editor <- map["Editor"]
        IsDeleted <- map["IsDeleted"]
        IsPersonal <- map["IsPersonal"]
        Name <- map["Name"]
        ProcedureType <- map["ProcedureType"]
        ReadOnly <- map["ReadOnly"]
        ProcedureWorkflowTemplateSteps <- map["ProcedureWorkflowTemplateSteps"]
    }
    
}




class ProcedureWorkflowTemplateSteps: Mappable {    
    var ID: String?
    var IsFirst:Int?
    var Name:String?
    var NextTemplateStepID:String?
    var ProcessType:Int?
    var ReturnToTemplateStepID:String?
    var SortOrder:Int?
    var type:Int?
    var WorkflowTemplateID:String?
    var ProcedureWorkflowTemplateTasks:[ProcedureWorkflowTemplateTasks]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        ID <- map["ID"]
        IsFirst <- map["IsFirst"]
        Name <- map["Name"]
        ProcessType <- map["ProcessType"]
        WorkflowTemplateID <- map["WorkflowTemplateID"]
        ProcedureWorkflowTemplateTasks <- map["ProcedureWorkflowTemplateTasks"]
        NextTemplateStepID <- map["NextTemplateStepID"]
        ReturnToTemplateStepID <- map ["ReturnToTemplateStepID"]
        SortOrder <- map["SortOrder"]
        type <- map["Type"]
    }
    
}
class ProcedureWorkflowTemplateTasks: Mappable {
    var Duration:Int?
    var ID: String?
    var LoginName:String?
    var SignType:Int?
    var SortOrder:Int?    
    var UserID:String?
    var Name:String?
    var UserName:String?
    var WorkflowTemplateStepID:String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Duration <- map["Duration"]
        ID <- map["ID"]
        UserID <- map["UserID"]
        LoginName <- map["LoginName"]
        Name <- map["Name"]
        SignType <- map["SignType"]
        SortOrder <- map["SortOrder"]
        UserName <- map["UserName"]
        WorkflowTemplateStepID <- map["WorkflowTemplateStepID"]

    }
    
}



class UserWorkTaskObject: Mappable {
    var Email: String?
    var ID:String?
    var FullName: String?
    var UserName:String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        ID <- map["ID"]
        Email <- map["Email"]
        FullName <- map["FullName"]
        UserName <- map["UserName"]
    }
}

class ProcedureAttachmentInfos: NSObject {
 
    var Name:String
    var IsDigitalSign:Bool

    override init() {
        self.Name = ""
        self.IsDigitalSign = false
    }
    func toJSON() -> [String:Any] {
        var dictionary: [String : Any] = [:]        
        dictionary["Name"] = self.Name
        dictionary["IsDigitalSign"] = self.IsDigitalSign
        return dictionary
    }
}
class ProcedurePositions: NSObject {
    var ID: String
    var AttachmentID:String
    var PageNumber:Int
    var CoordinateX:Int
    var CoordinateY:Int
    var HasSignature:Bool
    var HasStamp:Bool
    var HasInfo:Bool
    var SignerID:String
    var orX:Int
    var orY:Int
    
    override init() {
        self.ID = ""
        self.AttachmentID = ""
        self.PageNumber = 0
        self.CoordinateX = 0
        self.CoordinateY = 0
        self.orX = 0
        self.orY = 0
        self.HasSignature = false
        self.HasStamp = false
        self.HasInfo = false
        self.SignerID = ""
        
    }
    func toJSON() -> [String:Any] {
        var dictionary: [String : Any] = [:]
        
        dictionary["ID"] = self.ID
        dictionary["AttachmentID"] = self.AttachmentID
        dictionary["PageNumber"] = self.PageNumber
        dictionary["AttachmentID"] = self.AttachmentID
        dictionary["CoordinateX"] = self.CoordinateX
        dictionary["AttachmentID"] = self.AttachmentID
        dictionary["CoordinateY"] = self.CoordinateY
        dictionary["HasSignature"] = self.HasSignature
        
        dictionary["HasStamp"] = self.HasStamp
        dictionary["HasInfo"] = self.HasInfo
        dictionary["SignerID"] = self.SignerID
        return dictionary
    }
}
class NavDigitalObjectArr: Mappable {
    var data: [NavDigitalObject]?

    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        data <- map["Data"]
   
    }
}

class NavDigitalObject: Mappable {
    var Code: String?
    var IsShow:Bool?
    var Total: Int?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Code <- map["Code"]
        IsShow <- map["IsShow"]
        Total <- map["Total"]
    }
}


