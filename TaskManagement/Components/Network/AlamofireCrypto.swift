//
//  AlamofireCrypto.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import Alamofire
import CryptoSwift
import ObjectMapper

private let emptyDataStatusCodes: Set<Int> = [204, 205]

extension Request {
  /// Returns a JSON object contained in a result type constructed from the response data using `JSONSerialization`
  /// with the specified reading options.
  ///
  /// - parameter options:  The JSON serialization reading options. Defaults to `.allowFragments`.
  /// - parameter response: The response from the server.
  /// - parameter data:     The data returned from the server.
  /// - parameter error:    The error already encountered if it exists.
  ///
  /// - returns: The result data type.
  public static func serializeResponseJSON(
    options: JSONSerialization.ReadingOptions,
    response: HTTPURLResponse?,
    data: Data?,
    error: Error?)
    -> Result<Any>
  {
    guard error == nil else { return .failure(error!) }
    
    if let response = response, emptyDataStatusCodes.contains(response.statusCode) { return .success(NSNull()) }
    
    guard let validData = data, validData.count > 0 else {
      return .failure(AFError.responseSerializationFailed(reason: .inputDataNilOrZeroLength))
    }
    
    do {
      let json = try JSONSerialization.jsonObject(with: validData, options: options)
      let key: String = UserDefaults.standard.string(forKey: kSettingSecurityKey) ?? "SureP0rt@l!@2017"
      let iv: String  = "1234567890123456"
        

        guard let dict = json as? [String: Any] else {
            let encryptedString:String = json as! String 
            var result = [String: Any]()
            do {
                if let encryptedBytes = encryptedString.stringToBytes() {
                    let aes = try AES(key: Array(key.utf8), iv: Array(iv.utf8), blockMode: .CBC, padding: .pkcs7)
                    let plainText = try encryptedBytes.decrypt(cipher: aes)
                    if let byteString = String(bytes: plainText, encoding: .utf8) {
                        result = (try JSONSerialization.jsonObject(with: byteString.data(using: .utf8)!, options: []) as? [String: Any])!
                        debugPrint(response ?? "")
                        debugPrint(result)
                    }
                }
            } catch {
                return .failure(AFError.responseSerializationFailed(reason: .jsonSerializationFailed(error: error)))
            }
            
            return .success(result)
            
            return .failure(AFError.responseSerializationFailed(reason: .inputDataNilOrZeroLength))
        }
        guard let encryptedString = dict["d"] as? String else {
            return .failure(AFError.responseSerializationFailed(reason: .inputDataNilOrZeroLength))
        }
     
      var result = [String: Any]()
      do {
        if let encryptedBytes = encryptedString.stringToBytes() {
          let aes = try AES(key: Array(key.utf8), iv: Array(iv.utf8), blockMode: .CBC, padding: .pkcs7)
          let plainText = try encryptedBytes.decrypt(cipher: aes)
          if let byteString = String(bytes: plainText, encoding: .utf8) {
            result = (try JSONSerialization.jsonObject(with: byteString.data(using: .utf8)!, options: []) as? [String: Any])!
            debugPrint(response ?? "")
            debugPrint(result)
          }
        }
      } catch {
        return .failure(AFError.responseSerializationFailed(reason: .jsonSerializationFailed(error: error)))
      }
      
      return .success(result)
    } catch {
      return .failure(AFError.responseSerializationFailed(reason: .jsonSerializationFailed(error: error)))
    }
  }
}

extension DataRequest {
  /// Creates a response serializer that returns a JSON object result type constructed from the response data using
  /// `JSONSerialization` with the specified reading options.
  ///
  /// - parameter options: The JSON serialization reading options. Defaults to `.allowFragments`.
  ///
  /// - returns: A JSON object response serializer.
  public static func jsonResponseSerializer(
    options: JSONSerialization.ReadingOptions = .allowFragments)
    -> DataResponseSerializer<Any>
  {
    return DataResponseSerializer { _, response, data, error in
      return Request.serializeResponseJSON(options: options, response: response, data: data, error: error)
    }
  }
  
  /// Adds a handler to be called once the request has finished.
  ///
  /// - parameter options:           The JSON serialization reading options. Defaults to `.allowFragments`.
  /// - parameter completionHandler: A closure to be executed once the request has finished.
  ///
  /// - returns: The request.
  @discardableResult
  public func responseDecrypted(
    queue: DispatchQueue? = nil,
    options: JSONSerialization.ReadingOptions = .allowFragments,
    completionHandler: @escaping (DataResponse<Any>) -> Void)
    -> Self
  {
    return response(
      queue: queue,
      responseSerializer: DataRequest.jsonResponseSerializer(options: options),
      completionHandler: completionHandler
    )
  }
}

extension DataRequest {
  
  enum ErrorCode: Int {
    case noData = 1
    case dataSerializationFailed = 2
  }
  
  internal static func newError(_ code: ErrorCode, failureReason: String) -> NSError {
    let errorDomain = "com.alamofireobjectmapper.error"
    
    let userInfo = [NSLocalizedFailureReasonErrorKey: failureReason]
    let returnError = NSError(domain: errorDomain, code: code.rawValue, userInfo: userInfo)
    
    return returnError
  }
  
  /// Utility function for checking for errors in response
  internal static func checkResponseForError(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
    if let error = error {
      return error
    }
    guard let _ = data else {
      let failureReason = "Data could not be serialized. Input data was nil."
      let error = newError(.noData, failureReason: failureReason)
      return error
    }
    return nil
  }
  
  /// Utility function for extracting JSON from response
  internal static func processResponse(request: URLRequest?, response: HTTPURLResponse?, data: Data?, keyPath: String?) -> Any? {
    let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
    let result = jsonResponseSerializer.serializeResponse(request, response, data, nil)
    
    let JSON: Any?
    if let keyPath = keyPath , keyPath.isEmpty == false {
      JSON = (result.value as AnyObject?)?.value(forKeyPath: keyPath)
    } else {
      JSON = result.value
    }
    
    return JSON
  }
  
  /// BaseMappable Object Serializer
  public static func ObjectMapperSerializer<T: BaseMappable>(_ keyPath: String?, mapToObject object: T? = nil, context: MapContext? = nil) -> DataResponseSerializer<T> {
    return DataResponseSerializer { request, response, data, error in
      if let error = checkResponseForError(request: request, response: response, data: data, error: error){
        return .failure(error)
      }
      
      let JSONObject = processResponse(request: request, response: response, data: data, keyPath: keyPath)
      
      if let object = object {
        _ = Mapper<T>(context: context, shouldIncludeNilValues: false).map(JSONObject: JSONObject, toObject: object)
        return .success(object)
      } else if let parsedObject = Mapper<T>(context: context, shouldIncludeNilValues: false).map(JSONObject: JSONObject){
        return .success(parsedObject)
      }
      
      let failureReason = "ObjectMapper failed to serialize response."
      let error = newError(.dataSerializationFailed, failureReason: failureReason)
      return .failure(error)
    }
  }
  
  /// ImmutableMappable Array Serializer
  public static func ObjectMapperImmutableSerializer<T: ImmutableMappable>(_ keyPath: String?, context: MapContext? = nil) -> DataResponseSerializer<T> {
    return DataResponseSerializer { request, response, data, error in
      if let error = checkResponseForError(request: request, response: response, data: data, error: error){
        return .failure(error)
      }
      
      let JSONObject = processResponse(request: request, response: response, data: data, keyPath: keyPath)
      
      if let JSONObject = JSONObject,
        let parsedObject = (try? Mapper<T>(context: context, shouldIncludeNilValues: false).map(JSONObject: JSONObject)){
        return .success(parsedObject)
      }
      
      let failureReason = "ObjectMapper failed to serialize response."
      let error = newError(.dataSerializationFailed, failureReason: failureReason)
      return .failure(error)
    }
  }
  
  /**
   Adds a handler to be called once the request has finished.
   
   - parameter queue:             The queue on which the completion handler is dispatched.
   - parameter keyPath:           The key path where object mapping should be performed
   - parameter object:            An object to perform the mapping on to
   - parameter completionHandler: A closure to be executed once the request has finished and the data has been mapped by ObjectMapper.
   
   - returns: The request.
   */
  @discardableResult
  public func responseObject<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
    return response(queue: queue, responseSerializer: DataRequest.ObjectMapperSerializer(keyPath, mapToObject: object, context: context), completionHandler: completionHandler)
  }
  
  @discardableResult
  public func responseObject<T: ImmutableMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
    return response(queue: queue, responseSerializer: DataRequest.ObjectMapperImmutableSerializer(keyPath, context: context), completionHandler: completionHandler)
  }
  
  /// BaseMappable Array Serializer
  public static func ObjectMapperArraySerializer<T: BaseMappable>(_ keyPath: String?, context: MapContext? = nil) -> DataResponseSerializer<[T]> {
    return DataResponseSerializer { request, response, data, error in
      if let error = checkResponseForError(request: request, response: response, data: data, error: error){
        return .failure(error)
      }
      
      let JSONObject = processResponse(request: request, response: response, data: data, keyPath: keyPath)
      
      if let parsedObject = Mapper<T>(context: context, shouldIncludeNilValues: false).mapArray(JSONObject: JSONObject){
        return .success(parsedObject)
      }
      
      let failureReason = "ObjectMapper failed to serialize response."
      let error = newError(.dataSerializationFailed, failureReason: failureReason)
      return .failure(error)
    }
  }
  
  /// ImmutableMappable Array Serializer
  public static func ObjectMapperImmutableArraySerializer<T: ImmutableMappable>(_ keyPath: String?, context: MapContext? = nil) -> DataResponseSerializer<[T]> {
    return DataResponseSerializer { request, response, data, error in
      if let error = checkResponseForError(request: request, response: response, data: data, error: error){
        return .failure(error)
      }
      
      if let JSONObject = processResponse(request: request, response: response, data: data, keyPath: keyPath){
        
        if let parsedObject = try? Mapper<T>(context: context, shouldIncludeNilValues: false).mapArray(JSONObject: JSONObject){
          return .success(parsedObject)
        }
      }
      
      let failureReason = "ObjectMapper failed to serialize response."
      let error = newError(.dataSerializationFailed, failureReason: failureReason)
      return .failure(error)
    }
  }
  
  /**
   Adds a handler to be called once the request has finished. T: BaseMappable
   
   - parameter queue: The queue on which the completion handler is dispatched.
   - parameter keyPath: The key path where object mapping should be performed
   - parameter completionHandler: A closure to be executed once the request has finished and the data has been mapped by ObjectMapper.
   
   - returns: The request.
   */
  @discardableResult
  public func responseArray<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
    return response(queue: queue, responseSerializer: DataRequest.ObjectMapperArraySerializer(keyPath, context: context), completionHandler: completionHandler)
  }
  
  /**
   Adds a handler to be called once the request has finished. T: ImmutableMappable
   
   - parameter queue: The queue on which the completion handler is dispatched.
   - parameter keyPath: The key path where object mapping should be performed
   - parameter completionHandler: A closure to be executed once the request has finished and the data has been mapped by ObjectMapper.
   
   - returns: The request.
   */
  @discardableResult
  public func responseArray<T: ImmutableMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
    return response(queue: queue, responseSerializer: DataRequest.ObjectMapperImmutableArraySerializer(keyPath, context: context), completionHandler: completionHandler)
  }
}
