//
//  Router.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
  
    static var baseURLString:String {
        get {
            return UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? "http://sureportal.bioportal.vn"
        }
    }
  
  case getKey()
  case login(username:String, password:String)
  case sendCode(emailOrMobileNumber: String)
  case verify(code:String)
  case resetPass(password: String, code:String)
  case logout()
  case updateDeviceInfo(deviceToken: String, deviceName: String, imei: String, os: String, osVersion: String)
  case removeDeviceInfo(deviceToken: String)
  
  case getAllUsersDepartments(deptID: String, mode: Int, keyword: String)
  case getAllUsersInfo()
  case getCategorizeTracking(keyWord: String)
  case menu(parentId: String, treeFilterCode: String)
  case document(treefilterID:String, page:Int)
  case documentSync(page:Int)
    
  case detail(id: String)
  case documentProcessHistory(editDeptID: String, documentID: String)
    case documentTracking(deptID: String, status: Int, viewType: Int, documentID: String, isChild: Bool)
  case documentRecentTracking(deptID: String, top: Int, documentID: String)
  case updateStatusDocument(documentID: String)
  case documentTranfer(documentID: String)
  case getAssignersForTracking(documentID: String)
  
  case approveDocument(documentID: String, workFlowStepID: String, contentObj: String, nextWFStep: Parameters?)
  case assignTrackingDocument(params: Parameters, isCreateTask: Bool)
  case returnDocument(documentID: String, comment: String)
  case updateAssignTrackingInfoDocument(params: Parameters)
  case finishDocuments(docIDs: [String], deptIDs: [String])
    
  case getListTrackingForUser(documentID: String)
  case processTrackingDocument(document: Parameters)
  
  case getListAppraiseTrackingDocument(documentID: String)
  case getListTrackingRating(trackingID: String)
  case appriseTrackingDocument(apprise: Parameters)
  
  case topNewItem(itemType: String, page:Int)
  case chartCountOverview(itemType: String)
  case chartLineOverview(viewType:String)
  
  case advancedSearchDocument(keyWord: String, advancedParams: [String], page: Int)
  case getListNotification()
  case updateNotification(itemId: String, itemType: String)
  case updateNotifications(itemIds: [String], itemTypes: [String])
  case uploadFile(UploadFileInfo)
  case getListTrackingWorkflowDocument(deptID: String, documentID: String)
  case sendMail(documentID: String, subject: String, body: String, receivers: String)
    
    
//    Procedure Digital
  case getListSignatureDigital(keywork: String, pageNumber:Int,status: Int)
  case getListImportantDocument()
  case getProcedureWorkflow(ID:String)
  case getProcedureHistory(ID:String)
  case getProcedureAttachment(ID:String)
  case getAttachmentFile(ID: String)
    case getNextWorkFlowStep(docID: String)
  case getProcedureCategories()
  case getProcedureTypesByProcedureCategoryID(ID: String)
  case getProcedureWorkflowTemplatesByProcedureTypeId(ID: String)
  case getProcedureWorkflowTemplateById(ID:String)
  case addNewProcedure(AttachmentNames : [[String:Any]],
        IsDraft : Bool,
        Name : String,
        Summary : String,
        ProcedureCategory : String,
        ProcedureType : String,
        ProcedureWorkflowTemplateId : String,ProcedureAttachmentSigningPositions:[[String:Any]])
    
//    modify worktask
    case getUserInfos()
    case getListUserInWorkflowTemplateSignWithDigitalSignature(Id:String)
    case countPage(Name:String)
    case checkAttachment(Name:String,Attach:String)
    case createPersonalWorkflowTemplate(json:Parameters)
    case deleteProcedureDigital(ID:String)
    case rejectProcedureDigital(ID:String,Comment:String)
    case returnProcedureDigital(ID:String,Comment:String)
    case tranferProcedureDigital(ID:String,UserId:String,Comment:String)
    case assignProcedureDigital(ID:String,procedureType:Int,Comment:String,UserIDs:[String])
    case auditProcedureDigital(ID:String,Comment:String)
    case checkCurrentUserSignWithDigitalSignature(ID:String)
    case approveProcedureDigital(ID:String,Comment:String)
    case getPersonalSignatureInfos()
    case approveProcedureDigitalWithSignature(ID:String,Comment:String,SignatureId:String,PrivateKey:String)

    case advanceSearchDigital(paramSearch: [String:Any])
    case getLeftNavigationCount()
    
  // Meeting Room
    
   case getTopAnnouncements()
   case getDepartmentMeetingRoom()
   case getListDepartmentMeetingRoom(DepartmentID:String,RoomID:String)
   case getBookingRooms(DepartmentID:String,FromDate:String,Page:String,PageSize:String,RoomID:String,Status:String,ToDate:String)
   case createBookingRoom(json:Parameters)
   case getDetailBookingRoom(OrgID:String)
    
   case approveBookingMeetingRoom(json:Parameters)
    case deleteBookingMeetingRoom(json:Parameters)
    case rejectBookingMeetingRoom(json:Parameters)
    case updateBookingMeetingRoom(json:Parameters)
    case getCurrentDepartment()
    case AuthorDeleteBooking(json:Parameters)
    case ReturnBookingMeeting(json:Parameters)
    case getChartMeeting(json:Parameters)
//     Vehicels
    
    case getLocationVehicels()
    case getVehicleType(ID:String)
    case getBookingVehicels(LocationID:String,VehicleID:String,FromDate:String,Status:String,ToDate:String)
    case createBookingVehicel(json:Parameters)
    case getDetailBookingVehicel(OrgID:String)
    case approveBookingVehicel(json:Parameters)
    case arrangerBookingVehicel(json:Parameters)
    case getVehiclesByType(DeparmentID:String,ID:String)
    case getDriversByVehicleType(ID:String)
    case rejectBookingVehicel(json:Parameters)
    case deleteBookingVehicel(json:Parameters)
    case checkOutBookingVehicel(json:Parameters)
    case checkinBookingVehicel(json:Parameters)
    case updateBookingVehicel(json:Parameters)
    case externalBookingVehicel(json:Parameters)
    case externalCheckinBookingVehicel(json:Parameters)
    case extendBookingVehicel(json:Parameters)
    case getFeeTypes()
    case updateCosts(json: [[String:Any]])
    case getCostsByBooking(ID:String)
    case getVehicleRegistrationStatistics(StartTime:String,EndTime:String,LocationId:String,VehicleTypeId:String,
    DepartmentId:String,Status:String)
    
    case getCurrentDepartmentVehicle()
    case getActiveModule()
    case getCurrentTask(moduleCode: String)

    
    
  var method: HTTPMethod {
    switch self {
    default:
      return .post
    }
  }
  
  var path: String {
    switch self {
      
    case .getKey:
        return "mobileapis/SurePortalServices.svc/GetEncryptKey"
    case .login:
      return "mobileapis/SurePortalServices.svc/LoginAccount"
    case .sendCode:
      return "mobileapis/SurePortalServices.svc/SendCodeResetPasswordAccount"
    case .verify:
      return "mobileapis/SurePortalServices.svc/VerifyCodeResetPasswordAccount"
    case .resetPass:
      return "mobileapis/SurePortalServices.svc/ResetPasswordAccount"
    case .logout:
      return "mobileapis/SurePortalServices.svc/LogoutAccount"
    case .updateDeviceInfo:
      return "mobileapis/SurePortalServices.svc/UpdateDeviceInfo"
    case .removeDeviceInfo:
      return "mobileapis/SurePortalServices.svc/RemoveDeviceInfo"
      
    case .getAllUsersDepartments:
      return "/mobileapis/SurePortalServices.svc/GetListFilterTreeUserDepartment"
    case .getAllUsersInfo:
        return "/mobileapis/SurePortalServices.svc/GetAllUserInfo"
    case .getCategorizeTracking:
      return "mobileapis/SurePortalServices.svc/GetListCategorizeTracking"
    case .document:
      return "mobileapis/SurePortalServices.svc/GetListFilterDocument"
        
    case .documentSync:
        return "mobileapis/SurePortalServices.svc/GetListSyncDocument"

    case .menu:
      return "mobileapis/SurePortalServices.svc/GetListTreeFilterDocument"
      
    case .detail:
      return "mobileapis/SurePortalServices.svc/GetDocument"
    case .documentProcessHistory:
      return "mobileapis/SurePortalServices.svc/GetListProcessHistoryDocument"
    case .documentTracking(_, _, _, _, let isChild):
       return isChild ? "/mobileapis/SurePortalServices.svc/GetListChildTrackingDocument" : "mobileapis/SurePortalServices.svc/GetListTrackingDocument"
    case .updateStatusDocument:
      return "mobileapis/SurePortalServices.svc/UpdateStatusDocument"
    case .documentRecentTracking:
      return "mobileapis/SurePortalServices.svc/GetListRecentTrackingDocument"
    case .documentTranfer:
      return "mobileapis/SurePortalServices.svc/GetListTransferViewDocument"
    case .getAssignersForTracking:
      return "mobileapis/SurePortalServices.svc/GetAssignTrackingInfoDocument"
      
    case .approveDocument:
      return "mobileapis/SurePortalServices.svc/ApproveDocument"
    case .assignTrackingDocument(_, let isCreateTask):
        return isCreateTask ? "mobileapis/SurePortalServices.svc/CreateTask" : "mobileapis/SurePortalServices.svc/AssignTrackingDocument"
    case .updateAssignTrackingInfoDocument:
      return "mobileapis/SurePortalServices.svc/UpdateAssignTrackingInfoDocument"
    case .finishDocuments:
        return "mobileapis/SurePortalServices.svc/FinishDocument"
    case .returnDocument:
      return "mobileapis/SurePortalServices.svc/ReturnDocument"
      
    case .getListTrackingForUser:
      return "mobileapis/SurePortalServices.svc/GetListTrackingForUser"
    case .processTrackingDocument:
      return "mobileapis/SurePortalServices.svc/ProcessTrackingDocument"
    
    case .getListAppraiseTrackingDocument:
      return "mobileapis/SurePortalServices.svc/GetListAppraiseTrackingDocument"
    case .getListTrackingRating:
      return "mobileapis/SurePortalServices.svc/GetListTrackingRating"
    case .appriseTrackingDocument:
      return "mobileapis/SurePortalServices.svc/AppraiseTrackingDocument"
      
    case .topNewItem:
      return "mobileapis/SurePortalServices.svc/GetListTopNewDocument"
    case .chartCountOverview:
      return "mobileapis/SurePortalServices.svc/GetListChartCountDashBoard"
    case .chartLineOverview:
      return "mobileapis/SurePortalServices.svc/GetListChartLineDashBoard"
      
    case .advancedSearchDocument:
      return "mobileapis/SurePortalServices.svc/AdvancedSearchDocument"
    case .getListNotification:
      return "mobileapis/SurePortalServices.svc/GetListNotification"
    case .updateNotification:
        return "mobileapis/SurePortalServices.svc/UpdateNotification"
    case .updateNotifications:
        return "mobileapis/SurePortalServices.svc/UpdateMultiNotifications"
    case .uploadFile:
        return "mobileapis/SurePortalServices.svc/UploadFile"
    case .getListTrackingWorkflowDocument:
        return "mobileapis/SurePortalServices.svc/GetListTrackingWorkflowDocument"
    case .sendMail:
        return "mobileapis/SurePortalServices.svc/SendMail"
        
        
        //
    case .getListSignatureDigital:
        return "mobileapis/api/DigitalProcedure/ProceduresWithPaging"
    case .getListImportantDocument:
        return "mobileapis/SurePortalServices.svc/GetListImportantDocument"
    case .getProcedureWorkflow:
        return "/mobileapis/api/DigitalProcedure/ProcedureWorkflow"
    case .getProcedureHistory:
        return "/mobileapis/api/DigitalProcedure/ProcedureWorkflowHistory"
    case .getProcedureAttachment:
        return "/mobileapis/api/DigitalProcedure/ProcedureAttachment"
        
    case .getAttachmentFile:
        return "/mobileapis/api/DigitalProcedure/ProcedureAttachment"
        
    case .getNextWorkFlowStep:
        return "mobileapis/SurePortalServices.svc/GetNextWorkFlowStep"
    case .getProcedureCategories:
        return "/mobileapis/api/DigitalProcedure/GetProcedureCategories"

    case .getProcedureTypesByProcedureCategoryID:
        return "/mobileapis/api/DigitalProcedure/GetProcedureTypesByProcedureCategoryID"
                
        
    case.getProcedureWorkflowTemplatesByProcedureTypeId:
        return "/mobileapis/api/DigitalProcedure/GetProcedureWorkflowTemplatesByProcedureTypeId"
        
    case .getProcedureWorkflowTemplateById:
        return "/mobileapis/api/DigitalProcedure/GetProcedureWorkflowTemplateById"
        
   case .addNewProcedure:
        return "/mobileapis/api/DigitalProcedure/AddNewProcedure"
        
    case .getUserInfos:
        return "/mobileapis/api/DigitalProcedure/GetUserInfos"
    
    case .getListUserInWorkflowTemplateSignWithDigitalSignature:
        return "/mobileapis/api/DigitalProcedure/GetListUserInWorkflowTemplateSignWithDigitalSignature"
        
    case .countPage:
        return "/mobileapis/api/DigitalProcedure/CountPage"
        
    case .checkAttachment:
        return "/mobileapis/api/DigitalProcedure/CheckAttachment"

    case .createPersonalWorkflowTemplate:
         return "/mobileapis/api/DigitalProcedure/CreatePersonalWorkflowTemplate"

    case .deleteProcedureDigital:
        return "/mobileapis/api/DigitalProcedure/Delete"
    case .rejectProcedureDigital:
        return "/mobileapis/api/DigitalProcedure/Reject"
     
    case .returnProcedureDigital:
        return "/mobileapis/api/DigitalProcedure/Return"

    case .tranferProcedureDigital:
        return "/mobileapis/api/DigitalProcedure/Transfer"
        
    case .assignProcedureDigital:
        return "/mobileapis/api/DigitalProcedure/Assign"
        
    case .auditProcedureDigital:
        return "/mobileapis/api/DigitalProcedure/Audit"
        
    case .checkCurrentUserSignWithDigitalSignature:
        return "/mobileapis/api/DigitalProcedure/CheckCurrentUserSignWithDigitalSignature"
        
    case .approveProcedureDigital:
        return "/mobileapis/api/DigitalProcedure/Approve"

    case .getPersonalSignatureInfos:
        return "/mobileapis/api/DigitalProcedure/GetPersonalSignatureInfos"
        
    case .approveProcedureDigitalWithSignature:
        return "/mobileapis/api/DigitalProcedure/ApproveWithDigitalSignature"

    case .advanceSearchDigital:
        return "mobileapis/api/DigitalProcedure/ProceduresWithPaging"
        
    case .getLeftNavigationCount:
        return "mobileapis/api/DigitalProcedure/GetLeftNavigationCount"
        
    case .getTopAnnouncements:
        return "mobileapis/api/Announcement/GetTopAnnouncements"
   // Meeting Room
        
    case .getDepartmentMeetingRoom:
        return "mobileapis/api/MeetingRoom/GetOrgs"
        
    case .getListDepartmentMeetingRoom:
        return "mobileapis/api/MeetingRoom/GetRooms"

    case .getBookingRooms:
        return "mobileapis/api/MeetingRoom/GetBookings"
        
    case .createBookingRoom:
        return "mobileapis/api/MeetingRoom/CreateBooking"
    case .getDetailBookingRoom:
        return "mobileapis/api/MeetingRoom/GetBooking"
        
    case .approveBookingMeetingRoom:
        return "mobileapis/api/MeetingRoom/ApproveBooking"
        
    case .deleteBookingMeetingRoom:
        return "mobileapis/api/MeetingRoom/DeleteBooking"
    case .rejectBookingMeetingRoom:
        return "mobileapis/api/MeetingRoom/RejectBooking"
    case .updateBookingMeetingRoom:
        return "mobileapis/api/MeetingRoom/UpdateBooking"
    case .getCurrentDepartment:
        return "mobileapis/api/MeetingRoom/GetCurrentUserDepartments"
    case .getChartMeeting:
        return "mobileapis/api/MeetingRoom/GetCharts"
    case .AuthorDeleteBooking:
        return "mobileapis/api/MeetingRoom/AuthorDeleteBooking"

    case .ReturnBookingMeeting:
        return "mobileapis/api/MeetingRoom/ReturnBooking"
        
    case.getLocationVehicels:
        return "mobileapis/api/Vehicles/GetLocations"

    case .getVehicleType:
        return "mobileapis/api/Vehicles/GetVehicleType"
    case .getVehiclesByType:
         return "mobileapis/api/Vehicles/GetVehiclesByType"
    case .getBookingVehicels:
        return "mobileapis/api/Vehicles/GetBookings"
        
    case .createBookingVehicel:
        return "mobileapis/api/Vehicles/CreateBooking"

    case .getDetailBookingVehicel:
        return "mobileapis/api/Vehicles/GetBooking"
        
    case .rejectBookingVehicel:
        return "mobileapis/api/Vehicles/RejectBooking"
        
    case .deleteBookingVehicel:
        return "mobileapis/api/Vehicles/DeleteBooking"

        
    case .arrangerBookingVehicel:
         return "mobileapis/api/Vehicles/ArrangeBooking"
    case .checkOutBookingVehicel:
        return "mobileapis/api/Vehicles/CheckOutBooking"

    case .checkinBookingVehicel:
        return "mobileapis/api/Vehicles/CheckInBooking"
        
    case .getDriversByVehicleType:
        return "mobileapis/api/Vehicles/GetDriversByVehicleType"

    case .approveBookingVehicel:
        return "mobileapis/api/Vehicles/ApproveBooking"
    case .updateBookingVehicel:
        return "mobileapis/api/Vehicles/UpdateBooking"
        
    case .externalBookingVehicel:
        return "mobileapis/api/Vehicles/ExternalCheckOutBooking"
        
    case .externalCheckinBookingVehicel:
        return "mobileapis/api/Vehicles/ExternalCheckInBooking"
        
    case .extendBookingVehicel:
        return "mobileapis/api/Vehicles/ExtendBooking"
        
    case .getFeeTypes:
        return "mobileapis/api/Vehicles/GetFeeTypes"
        
    case .updateCosts:
           return "mobileapis/api/Vehicles/UpdateCosts"
        
    case .getCostsByBooking:
        return "mobileapis/api/Vehicles/GetCostsByBooking"
    case .getVehicleRegistrationStatistics:
        return "mobileapis/api/Vehicles/GetVehicleRegistrationStatistics"
        
    case .getCurrentDepartmentVehicle:
        return "mobileapis/api/Vehicles/GetCurrentUserDepartments"
    case .getActiveModule:
        return "/mobileapis/SurePortalServices.svc/GetActiveModules"
    case .getCurrentTask(let moduleCode):
        return "/mobileapis/api/\(moduleCode)/GetCurrentTask"
        
    }

  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let result: (path: String, parameters: Parameters) = {
      switch self {
      case .getKey():
        var secretKey = ""
        if let udidDevice: UUID = UIDevice.current.identifierForVendor {
            secretKey += udidDevice.uuidString
        } else {
            secretKey += "XXX-XXX"
        }
        let dateTime = Date().toFormatDate(format: "yyyy_MM_dd_HH_mm")
        secretKey += "_\(dateTime)"
        return (self.path, ["secretKey":secretKey])
      case let .login(username,password):
        return (self.path, ["username":username, "password":password])
      case let .sendCode(emailOrMobileNumber):
        return (self.path, ["emailOrMobileNumber": emailOrMobileNumber])
      case let .verify(code):
        return (self.path, ["code":code])
      case let .resetPass(password, code):
        return (self.path, ["password": password, "code": code])
      case .logout():
        return (self.path, [:])
      case let .updateDeviceInfo(deviceToken, deviceName, imei, os, osVersion):
        return (self.path, ["token": deviceToken, "name": deviceName, "imei": imei, "osName": os, "osVersion": osVersion])
      case let .removeDeviceInfo(deviceToken):
        return (self.path, ["token": deviceToken])
        
      case .getAllUsersDepartments(let deptID, let mode, let keyword):
        return (self.path, ["deptID": deptID, "mode": mode, "keyword": keyword])
      case .getAllUsersInfo():
        return (self.path, [:])
      case .getCategorizeTracking(let keyWord):
        return (self.path, ["keyWord": keyWord])
      case .document(let treefilterID, let page):
        return (self.path, ["keyword": "", "treefilterID": treefilterID, "page": page])

      case .documentSync(let page):
        return (self.path, ["keyword": "", "page": page])

      case let .menu(parentId, treeFilterCode) where treeFilterCode != "":
        return (self.path, ["parentID": parentId, "treeFilterCode": treeFilterCode, "userdelegate":""])
      case let .menu(parentId, _):
        return (self.path, ["parentID": parentId])
      case let .detail(documentId):
        return (self.path, ["documentID": documentId])
      case let .updateStatusDocument(documentId):
        return (self.path, ["documentID": documentId])
        
      case let .topNewItem(itemType, page):
        return (self.path, ["itemType": itemType, "page": page])
      case let .chartCountOverview(itemType):
        return (self.path, ["itemType": itemType])
      case let .chartLineOverview(viewType):
        return (self.path, ["viewType": viewType])
        
      case let .documentProcessHistory(editDeptID, documentID):
        return (self.path, ["editDeptID": editDeptID, "documentID": documentID])
      case let .documentTracking(deptID, status, viewType, documentID, isChild):
        if isChild {
            return (self.path, ["deptID": deptID, "status": status, "viewType":viewType, "parentID": documentID])
        } else {
            return (self.path, ["deptID": deptID, "status": status, "viewType":viewType, "documentID": documentID])
        }
        
      case let .documentRecentTracking(deptID, top, documentID):
        return (self.path, ["deptID": deptID, "top": top, "documentID": documentID])
      case let .documentTranfer(documentID):
        return (self.path, ["documentID": documentID])
      case let .getAssignersForTracking(documentID):
        return (self.path, ["documentID": documentID])
        
      case let .approveDocument(documentID, workFlowStepID, contentObj, nextWFStep):
        if let nextStep = nextWFStep {
            return (self.path, ["documentID": documentID, "workFlowStepID": workFlowStepID, "contentObj": contentObj, "nextWFStep":nextStep])
        } else {
            return (self.path, ["documentID": documentID, "workFlowStepID": workFlowStepID, "contentObj": contentObj])
        }
        
      case let .assignTrackingDocument(params, _):
        return (self.path, params)
      case let .returnDocument(documentID, comment):
        return (self.path, ["documentID": documentID, "comment": comment])
      case let .updateAssignTrackingInfoDocument(params):
        return (self.path, params)
      case let .finishDocuments(docIDs, deptIDs):
        return (self.path, ["lstDocID": docIDs, "lstDeptID":deptIDs])
      case let .getListTrackingForUser(documentID):
        return (self.path, ["documentID": documentID])
      case let .processTrackingDocument(document):
        return (self.path, ["processTracking": document])
      case let .getListAppraiseTrackingDocument(documentID):
        return (self.path, ["documentID": documentID])
      case let .getListTrackingRating(trackingID):
        return (self.path, ["trackingID": trackingID])
      case let .appriseTrackingDocument(document):
        return (self.path, ["appraiseTracking": document])
      
      case let .getNextWorkFlowStep(docID):
        return (self.path, ["documentID": docID])
        
      case let .advancedSearchDocument(keyword, advancedParams, page):
        
        //let advancedParams = ["OrgID|", "DocDateFrom|2018-01-01", "DocDateTo|2018-12-31", "Category|All"]
        var params = ["keyword": keyword, "page": page] as [String : Any]
         params["lstParams"] = advancedParams
        
         return (self.path, params)
      case .getListNotification():
        return (self.path, [:])
      
      case .updateNotification(let itemId, let itemType):
        return (self.path, ["itemID":itemId, "itemType": itemType])
      case .updateNotifications(let itemIds, let itemTypes):
        return (self.path, ["itemIDs":itemIds, "itemTypes": itemTypes])
      //Upload file
      case .uploadFile(let fileInfo):
        let params = ["fileExt": fileInfo.type!, "fileContentBase64":fileInfo.data64, "offset":fileInfo.offset, "length":fileInfo.size, "isEndFile":false] as [String : Any]
        return (self.path, params)
      case .sendMail(let documentID, let subject, let body, let receivers):
        let params = ["documentID": documentID, "subject": subject, "body": body, "receivers": receivers] as [String : String]
        return (self.path, params)
        
      case let .getListTrackingWorkflowDocument(editDeptID, documentID):
        return (self.path, ["deptID": editDeptID, "trackingDocumentID": documentID])
        
      case .getListImportantDocument:
        return (self.path, [:])

        
      case .getListSignatureDigital(let keywork, let pageNumber, let status):
        if status == 100
        {
            let params = ["keyword": keywork, "page": pageNumber] as [String : Any]
            return (self.path, params)
        }
        else
        {
            let params = ["keyword": keywork, "page": pageNumber, "status": status] as [String : Any]
            return (self.path, params)
            
        }
      case .getProcedureWorkflow(let Id):
            return (self.path, ["ID": Id])
        
      case .getProcedureHistory(let Id):
         return (self.path, ["ID": Id])
        
      case .getProcedureAttachment(let Id):        
         return (self.path, ["ID": Id])
        
      case .getAttachmentFile(let Id):
       return (self.path, ["ID": Id])
        
      case .getProcedureCategories():
            return (self.path,[:])
        
      case .getProcedureTypesByProcedureCategoryID(let Id):
        return (self.path, ["ID": Id])
        
        
      case .getProcedureWorkflowTemplatesByProcedureTypeId(let Id):
            return (self.path, ["ID": Id])
      case .getProcedureWorkflowTemplateById(let Id):
        return (self.path, ["ID": Id])
        
      case .addNewProcedure(let AttachmentNames,
                           let IsDraft,
                           let Name,
                           let Summary,
                           let ProcedureCategory,
                           let ProcedureType,
                           let ProcedureWorkflowTemplateId,let ProcedureAttachmentSigningPositions):

        return (self.path, ["ProcedureAttachmentInfos": AttachmentNames, "IsDraft": IsDraft
            , "Name": Name, "Summary": Summary, "ProcedureCategory": ProcedureCategory,"ProcedureType": ProcedureType
            , "ProcedureWorkflowTemplateId": ProcedureWorkflowTemplateId,"ProcedureAttachmentSigningPositions":ProcedureAttachmentSigningPositions] as [String : Any])
        
      case .getUserInfos:
        return (self.path, [:])
        
      case .getListUserInWorkflowTemplateSignWithDigitalSignature(let Id):
        return (self.path, ["ID": Id])
        
        case .countPage(let Id):
            return (self.path, ["Name": Id])
        
      case .checkAttachment(let Name, let Attach):
            return (self.path, ["AttachmentName": Name,"AttachmentPreview":Attach])

      case .createPersonalWorkflowTemplate(let valuesObject):
        return (self.path, valuesObject as! Parameters)

      case .deleteProcedureDigital(let Id):
            return (self.path, ["ProcedureID": Id])
        
      case .rejectProcedureDigital(let procedureId,let comment):
        return (self.path, ["ProcedureID": procedureId, "Comment":comment])
        
      case .returnProcedureDigital(let procedureId,let comment):
        return (self.path, ["ProcedureID": procedureId, "Comment":comment])
        
      case .tranferProcedureDigital(let Id,let userId, let comment):
        return (self.path, ["ProcedureID": Id,"UserID":userId, "Comment":comment])
        
      case .assignProcedureDigital(let Id,let proceType,let comment,let userIds):
        return (self.path, ["ProcedureID": Id,"UserIDs":userIds,"ProcedureStepTypeID":proceType, "Comment":comment])
        
      case .auditProcedureDigital(let procedureId,let comment):
        return (self.path, ["ProcedureID": procedureId, "Comment":comment])
        
      case .checkCurrentUserSignWithDigitalSignature(let procedureId):
        return (self.path, ["ID": procedureId])
        
      case .approveProcedureDigital(let procedureId,let comment):
        return (self.path, ["ProcedureID": procedureId, "Comment":comment])
        
        
      case .getPersonalSignatureInfos():
            return (self.path, ["":""])

      case .approveProcedureDigitalWithSignature(let ID,let Comment,let SignatureId,let PrivateKey):
         return (self.path, ["ProcedureID": ID, "Comment":Comment,"SignatureId":SignatureId,"PrivateKey":PrivateKey])

        
      case .advanceSearchDigital(let keyParam):
            return (self.path, keyParam)
        
      case .getLeftNavigationCount():
         return (self.path, ["":""])
        
      case .getTopAnnouncements():
        return (self.path, ["":""])
        
//        Meeting Room
      case .getDepartmentMeetingRoom():
            return (self.path, ["":""])

      case .getListDepartmentMeetingRoom(let DepartmentID,let RoomID):
        return (self.path, ["OrgID": DepartmentID, "RoomID":RoomID])
        
        
      case .getBookingRooms(let DepartmentID,let FromDate,let Page,let PageSize,let RoomID,let Status,let ToDate):
        return (self.path, ["OrgID": DepartmentID, "FromDate":FromDate, "Page":Page, "PageSize":PageSize, "RoomID":RoomID
            , "Status":Status, "ToDate":ToDate])
        
        
        case .createBookingRoom(let keyParam):
             return (self.path, keyParam)
        
      case .getDetailBookingRoom(let OrgID):
        return (self.path, ["ID": OrgID ])
        
      case .approveBookingMeetingRoom(let keyParam):
             return (self.path, keyParam)
        
      case .deleteBookingMeetingRoom(let keyParam):
        return (self.path, keyParam)

      case .rejectBookingMeetingRoom(let keyParam):
        return (self.path, keyParam)

      case .updateBookingMeetingRoom(let keyParam):
        return (self.path, keyParam)
    
      case .getChartMeeting(let keyParam):
        return (self.path, keyParam)
        
      case .getCurrentDepartment():
         return (self.path, ["":""])
        
      case .AuthorDeleteBooking(let keyParam):
        return (self.path, keyParam)
        
      case .ReturnBookingMeeting(let keyParam):
        return (self.path, keyParam)
        
      case .getLocationVehicels():
         return (self.path, ["":""])
        
      case .getVehicleType(let ID):
        return (self.path, ["ID":ID])
        
      case .getVehiclesByType(let DepartmentID,let ID):
        return (self.path, ["DepartmentID":DepartmentID, "ID":ID])
      case .getBookingVehicels(let LocationID,let VehicleID,let FromDate,let Status,let ToDate):
        return (self.path, ["LocationID": LocationID, "VehicleID":VehicleID, "FromDate":FromDate
            , "Status":Status, "ToDate":ToDate])

      case .createBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
       case .getDetailBookingVehicel(let ID):
        return (self.path, ["ID":ID])
        
        
       case .rejectBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .deleteBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .arrangerBookingVehicel(let keyParam):
        return (self.path, keyParam)

      case .checkOutBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .checkinBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .getDriversByVehicleType(let ID):
        return (self.path, ["ID":ID])
        
      case .approveBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .updateBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .externalBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .externalCheckinBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .extendBookingVehicel(let keyParam):
        return (self.path, keyParam)
        
      case .getFeeTypes():
        return (self.path, ["":""])
        
      case .updateCosts(let keyParam as [[String:Any]] ):
        return (self.path,  ["":keyParam])
        
      case .getCostsByBooking(let Id):
        return (self.path, ["ID": Id])
        
        case .getVehicleRegistrationStatistics(let StartTime,let EndTime,let LocationId,let VehicleTypeId,let DepartmentId,let Status):
          return (self.path, ["StartTime": StartTime,"EndTime": EndTime,"LocationId": LocationId,"VehicleTypeId": VehicleTypeId,"DepartmentId": DepartmentId,"Status": Status])
        
      case .getCurrentDepartmentVehicle():
        return (self.path, ["":""])

      case .getActiveModule:
        return (self.path, [:])
      case .getCurrentTask:
        return (self.path, [:])
        
        
        }
    }()
    
    let url = try Router.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
    urlRequest.httpMethod = method.rawValue
    urlRequest.setValue("com.pvgas", forHTTPHeaderField: "User-Agent")
    debugPrint("Request: \(urlRequest.url)")
    debugPrint("Params: \(result.parameters)")
    switch self {
    default:
      urlRequest = try JSONEncoding.default.encode(urlRequest, with: result.parameters)
    }
    
    return urlRequest
  }
}
