//
//  LoginManager.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

class LoginManager: NSObject {
  
    static func getEncryptKey(done:@escaping (_ result: String?,_ message:String) -> ()){
        let router = Router.getKey()
        request(router)
            .validate()
            .responseObject {  (response : DataResponse<GetKeyResponse>) in
                switch response.result {
                case .success:
                    if let data = response.data, let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any], let key = dict?["d"] as? String {
                        done(key, "")
                    } else {
                        done(nil, "Không thể kết nối đến máy chủ!")
                    }
                case .failure(let error):
                    print(error)
                    done(nil,"Không thể kết nối đến máy chủ!")
                }
        }
    }
    
  static func login(username:String, password:String, done:@escaping (_ result: UserProfile?,_ message:String) -> ()){
    let router = Router.login(username: username, password:password)
    request(router)
      .validate()
      .responseObject {  (response : DataResponse<UserProfileResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data, data.message ?? "")
          } else {
            done(nil, "Không thể kết nối đến máy chủ!")
          }
        case .failure(let error):
          print(error)
          done(nil,"Không thể kết nối đến máy chủ!")
        }
    }
  }
  
  static func sendCode(emailOrMobileNumber: String, done:@escaping(_ result:Bool,_ message:String) -> ()){
    let router = Router.sendCode(emailOrMobileNumber: emailOrMobileNumber)
    request(router)
      .validate()
      .responseObject {  (response : DataResponse<SendCodeResponse>) in
        switch response.result {
        case .success:
          
          var status = 0
          var message = ""
          var dataInt = 0
          
          if let data = response.result.value {
            status = data.status ?? 0
            message = data.message ?? ""
            dataInt = data.data ?? 0
          }
          if status == 1 && dataInt == 1{
            done(true, message != "" ? message : "Mã OTP đã được gửi đến email hoặc số điện thoại của bạn")
          }
          else{
            done(false, message != "" ? message : "Không thể kết nối đến máy chủ!")
          }
          
        case .failure(let error):
          print(error)
          done(false,"Không thể kết nối đến máy chủ!")
        }
    }
  }
  
  static func verifyCode(code: String, done:@escaping(_ result:Bool,_ message:String) -> ()){
    let router = Router.verify(code: code)
    request(router)
      .validate()
      .responseObject {  (response : DataResponse<VerifyCodeResponse>) in
        switch response.result {
        case .success:
          
          var status = 0
          var message = ""
          var dataBool = false
          
          if let data = response.result.value {
            status = data.status ?? 0
            message = data.message ?? ""
            dataBool = data.data ?? false
          }
          if status == 1 && dataBool == true{
            done(true, message != "" ? message : "Cài đặt mật khẩu thành công!")
          }
          else{
            done(false, message != "" ? message : "Cài đặt mật khẩu thất bại!")
          }
          
        case .failure(let error):
          print(error)
          done(false,"Cài đặt mật khẩu thất bại!")
        }
    }
  }
  
  static func resetPass(password:String, code: String, done:@escaping(_ result:Bool,_ message:String) -> ()){
    let router = Router.resetPass(password: password, code: code)
    request(router)
      .validate()
      .responseObject {  (response : DataResponse<SendCodeResponse>) in
        switch response.result {
        case .success:
          
          var status = 0
          var message = ""
          var dataInt = 0
          
          if let data = response.result.value {
            status = data.status ?? 0
            message = data.message ?? ""
            dataInt = data.data ?? 0
          }
          if status == 1 && dataInt == 1{
            done(true, message != "" ? message : "Cài đặt mật khẩu thành công!")
          }
          else{
            done(false, message != "" ? message : "Cài đặt mật khẩu thất bại!")
          }
          
        case .failure(let error):
          print(error)
          done(false,"Cài đặt mật khẩu thất bại!")
        }
    }
  }
  
  static func logout(done:@escaping(_ result:Bool, _ message:String) -> ()){
    let router = Router.logout()
    request(router)
      .validate()
      .responseObject {  (response : DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          var status = 0
          var message = ""
          if let data = response.result.value {
            status = data.status ?? 0
            message = data.message ?? ""
          }
          
          if status == 1 {
            done(true, message != "" ? message : "Đăng xuất thành công!")
          }
          else{
            done(false, message != "" ? message : "Đăng xuất thất bại!")
          }
        case .failure(let error):
          print(error)
          done(false,"Đăng xuất thất bại!")
        }
    }
  }
  
  static func saveDeviceInformation(deviceToken: String, deviceName: String, imei: String, osName: String, osVersion: String, done:@escaping(_ result: Bool, _ message: String) -> ()) {
    let router = Router.updateDeviceInfo(
      deviceToken: deviceToken,
      deviceName: deviceName,
      imei: imei,
      os: osName,
      osVersion: osVersion
    )
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<BaseResponse>) in
        switch response.result {
          case .success:
            var status = 0
            var message = ""
            if let data = response.result.value {
              status = data.status ?? 0
              message = data.message ?? ""
            }
            if status == 1 {
              done(true, message != "" ? message : "Lưu thông tin thiết bị thành công!")
            }
            else{
              done(false, message != "" ? message : "Lưu thông tin thiết bị thất bại!")
            }
          case .failure(let error):
            done(false,"Lưu thông tin thiết bị thất bại: \(error)")
        }
    }
  }
  
  static func removeDeviceToken(deviceToken: String, done: @escaping(_ result: Bool, _ message: String) -> ()) {
    let router = Router.removeDeviceInfo(deviceToken: deviceToken)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response : DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          var status = 0
          var message = ""
          if let data = response.result.value {
            status = data.status ?? 0
            message = data.message ?? ""
          }
          if status == 1 {
            done(true, message != "" ? message : "Xoá thông tin thiết bị thành công!")
          }
          else{
            done(false, message != "" ? message : "Xoá thông tin thiết bị thất bại!")
          }
        case .failure(let error):
          done(false, "Xoá thông tin thiết bị thất bại: \(error)")
        }
      }
  }
}
