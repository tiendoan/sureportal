//
//  BaseResponse.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseResponse: Mappable {
  var status: Int?
  var message: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    status <- map["Status"]
    message <- map["Message"]
  }
}

class UserProfileResponse: BaseResponse {
    var data:UserProfile?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class ModuleResponse: BaseResponse {
    
    var data: [String]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class GetKeyResponse: BaseResponse {
    var data:String?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["d"]
    }
}

class SendCodeResponse: BaseResponse {
    var data:Int?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class VerifyCodeResponse: BaseResponse {
    var data:Bool?
  
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class UploadFileResponse: BaseResponse{
    var fileID:String?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        fileID <- map["Data"]
    }
}

class ResetPassResponse: BaseResponse{
    var data:Int?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class MenuResponse: BaseResponse {
  var data: [Menu]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class DocumentResponse: BaseResponse {
    var data:DocumentData?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class DetailResponse: BaseResponse {
  var data:Document?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class ChartResponse: BaseResponse {
    var data:[Chart]?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}


class ProgressHistoryResponse: BaseResponse {
  var data:[ProgressHistory]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class ProgressTrackingResponse: BaseResponse {
  var data:[ProgressTracking]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class UpdateStatusDocumentResponse: BaseResponse {
    var data = false
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class RecentTrackingResponse: BaseResponse {
  var data:[RecentTracking]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class UsersResponse: BaseResponse {
    var data: [UserProfile]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class AssigneeResponse: BaseResponse {
  var data: [Assignee]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class CategoriesResponse: BaseResponse {
  var data:[CategoryTracking]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class TranferResponse: BaseResponse {
  var data:[Transfer]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class AssignTrackingResponse: BaseResponse {
  var data: [AssignTracking]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class ProcessTrackingResponse: BaseResponse {
  var data: [ProcessTracking]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class TrackingRatingResponse: BaseResponse {
  
  var data: [StatusDocument]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
  
}

class NotificationResponse: BaseResponse {
  
  var data: [NotificationItem]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
  
}

class ImportantResponse: BaseResponse {
    
    var data: [ImportantDocument]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
    
}

class AnnouncementResponse: BaseResponse {
    var data: [Announcement]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class DigitalResponse: BaseResponse {
    
    var data: Procedures?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
    
}

class ProcedureDigitalResponse: BaseResponse {
    
    var data: ProceduresObject?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
    
}

class ProcedureHistoryResponse: BaseResponse {
    
    var data: [ProceduresHistoryObject]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
    
}

class ProcedureAttachmentFileResponse: BaseResponse {
    
    var data: [ProceduresAttachmentFileObject]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class ProcedureDigitalObject: BaseResponse {
    
    var data: [ProcedureDataObject]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class ProcedureDigitalWorkFlowObject: BaseResponse {
    
    var data: ProcedureDigitalWorkFlow?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}
class UserListWorkTaskResponse: BaseResponse {
    var data: [UserWorkTaskObject]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class NextWorkFlowStepResponse: BaseResponse {
    
    var data: [GetNextWorkFlowStep]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}
class SinagureReponse: BaseResponse {
    
    var userSignautre: [SinagureDigitalReponse]?
    
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userSignautre <- map["Data"]
    }
}

class SinagureDigitalReponse: BaseResponse {
    
    var Email: String?
    var FullName: String?
    var ID: String?
    var UserName: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        Email <- map["Email"]
        FullName <- map["FullName"]
        ID <- map["ID"]
        UserName <- map["UserName"]
    }
}

class CountPageNumb: BaseResponse {
    
    var totalPage: Int?
    
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        totalPage <- map["Data"]
    }
}

class ProcedureDigitalSigningResponse: BaseResponse {
    
    var data:SignWithDigitalSignatureResponse?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class SignWithDigitalSignatureResponse: BaseResponse {

    var Ext:String?
    var ID:String?
    var IsDigitalSign:Int?
    var Name:String?
    var Size:Int?
    var Version:Int?
    var SignWithDigitalSignature: Int?
    var SignatureID:String?
    var ProcedureAttachmentSigningPositions:[ProcedureAttachmentSigningPositions]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        SignWithDigitalSignature <- map["SignWithDigitalSignature"]
        SignatureID <- map["SignatureID"]

        Ext <- map["Ext"]
        ID <- map["ID"]
        IsDigitalSign <- map["IsDigitalSign"]
        Name <- map["Name"]
        Size <- map["Size"]
        Version <- map["Version"]

    }
}

class PersonalSignatureInfosResponse: BaseResponse {
    
    var data:[PersonalSignatureInfos]?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class PersonalSignatureInfos: BaseResponse {
    
    var CertificateName:String?
    var ID:String?
    var Name:String?
    var SignatureImageFileName:String?
    var SignatureImageFileType:String?
    var StampImageFileName:String?
    var StampImageFileType:String?
    var UserID:String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        CertificateName <- map["CertificateName"]
        ID <- map["ID"]
        Name <- map["Name"]
        SignatureImageFileName <- map["SignatureImageFileName"]
        SignatureImageFileType <- map["SignatureImageFileType"]
        StampImageFileName <- map["StampImageFileName"]
        StampImageFileType <- map["StampImageFileType"]
        UserID <- map["UserID"]
    }
}



