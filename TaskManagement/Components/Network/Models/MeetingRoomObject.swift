//
//  MeetingRoomObject.swift
//  TaskManagement
//
//  Created by Mirum User on 7/23/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper
class FeeTypeResponse:  Mappable {
    var data: [FeeTypeObject]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}
class LineChart: Mappable {
    var Data:Int?
    var ID:Int?
    var Label:String?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Data <- map["Data"]
        ID <- map["ID"]
        Label <- map["Label"]
    }
}
class StackedColumnChart: Mappable {
    var Approve:Int?
    var Delete:Int?
    var Wait:Int?
    var Label:String?
    var ID:String?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Approve <- map["Approve"]
        Delete <- map["Delete"]
        Label <- map["Label"]
        ID <- map["ID"]
        Wait <- map["Wait"]
        
    }
}
class PieChart: Mappable {
    var ID:Int?
    var MeetingTotal:Int?
    var TimeTotal:Int?
    var Label:String?
    var MeetingTimeTotal:String?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        MeetingTotal <- map["MeetingTotal"]
        TimeTotal <- map["TimeTotal"]
        Label <- map["Label"]
        ID <- map["ID"]
        MeetingTimeTotal <- map["MeetingTimeTotal"]
        
    }
}
class LineChartResponse:  Mappable {
    var data: [LineChart]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}
class PieChartResponse:  Mappable {
    var data: [PieChart]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}
class StackedChartResponse:  Mappable {
    var data: [StackedColumnChart]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}

class ChartResponseData:  Mappable {
    var LineChartResponse: LineChartResponse?
    var StackColumnChartReponse:StackedChartResponse?
    var PieCharReponse:PieChartResponse?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        LineChartResponse <- map["LineChart"]
        StackColumnChartReponse <- map["StackedColumnChart"]
        PieCharReponse <- map["PieChart"]
    }
}
class MeetingRoomChartResponse:  Mappable {
    var ChartResponse: ChartResponseData?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        ChartResponse <- map["Data"]
    }
}

    
class MeetingRoomResponse:  Mappable {
    var data: [MeetingRoomObject]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}
class MeetingRoomEquipmentObject:  Mappable {
    var ID: String?
    var Name: String?
    var isChecked:Bool?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        Name <- map["Name"]
        isChecked <- map["isChecked"]
    }
}
class MeetingRoomObject:  Mappable {
    var ID: String?
    var Name: String?
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        Name <- map["Name"]
    }
}
class RoomResponse:  Mappable {
    var data: [GetMeetingRoomObject]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}


class GetMeetingRoomObject:  Mappable {
    var Approvers:[ApproversObject]?
    var Assistances:[MeetingRoomObject]?
    var Capacity: Int?
    var Department:[MeetingRoomObject]?
    var Equipments:[MeetingRoomEquipmentObject]?
    var ID: String?
    var Name: String?
    var SortOrder: Int?
    var IsActive: Int?
    var Description: String?
    var Org:MeetingRoomObject?
    var User:ApproversObject?
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Approvers <- map["Approvers"]
        Assistances <- map["Assistances"]
        Capacity <- map["Capacity"]
        Department <- map["Department"]
        ID <- map["ID"]
        Name <- map["Name"]
        SortOrder <- map["SortOrder"]
        IsActive <- map["IsActive"]
        Equipments <- map["Equipments"]
        Description <- map["Description"]
        Org <- map["Org"]
        User <- map["User"]

        
    }
}
class ApproversObject: Mappable
{
    var UserName: String?
    var Name: String?
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        UserName <- map["UserName"]
        Name <- map["Name"]
    }
    
}

class GetRoomResponse:  Mappable {
    var data: [GetDetailMeetingRoomObject]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}

class GetDetailMeetingRoomObject:  Mappable {
    var FromDate: String?
    var ToDate: String?
    var Title: String?
    var Owner: ApproversObject?
    var ID:String?
    var RoomID:String?
    var WorkflowID:Int?
    var Status:Int?
    var Color:String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        FromDate <- map["FromDate"]
        ToDate <- map["ToDate"]
        Title <- map["Title"]
        Owner <- map["Owner"]
        ID <- map["ID"]
        RoomID <- map["RoomID"]
        WorkflowID <- map["WorkflowID"]
        Color <- map["Color"]
        Status <- map["Status"]

    }
}
class BookingObjectDetailResponse: Mappable
{
     var data: CreateBookingObject?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}

class VehicelObjectDetailResponse: Mappable
{
    var data: DetailVehicelsBookingObject?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}


class CurrentTask: Mappable
{
    var itemAction:[ItemActions]?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        itemAction <- map["Actions"]
    }
}
class DeleteBookingObject: Mappable{
    
    var Comment:String?
    var Booking: BookingObject?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        
        Comment <- map["Comment"]
        Booking <- map["Booking"]
        
    }
}
class BookingObject: Mappable{
    
    var ID:String?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        
        ID <- map["ID"]
      
        
    }
}
class UpdateBookingObject: Mappable{
    
    var Comment:String?
    var Booking: CreateBookingObject?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
       
        Comment <- map["Comment"]
        Booking <- map["Booking"]

    }
}

class ActionCode: Mappable{
    
    var Code:String?
    var Color:String?
    var Name:String?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        
        Code <- map["Code"]
        Color <- map["Color"]
        Name <- map["Name"]
        
    }
}

class HistoryMeeting: Mappable{
    
    var Comment:String?
    var Name: String?
    var userOwner: ApproversObject?
    var Occurred:String?
    var Action:ActionCode?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        
        Comment <- map["Comment"]
        Name <- map["Name"]
        Occurred <- map["Occurred"]
        userOwner <- map["User"]
        Action <- map["Action"]
    }
}


class CreateBookingObject:  Mappable {
    
    var Assistances: [MeetingRoomObject]?
    var Equipments: [MeetingRoomEquipmentObject]?
    var Description:String?
    var FromDate:String?
    var ID:String?
    var Owner:ApproversObject?
    var ParticipantNumber:Int?
    var Participants:[ApproversObject]?
    var RequiredParticipants:[ApproversObject]?
    var RoomID:String?
    var Scope:Int?
    var Title:String?
    var ToDate:String?
    var CurrentTask:CurrentTask?
    var DepartmentID:String?
    var Histories :[HistoryMeeting]?
    
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Assistances <- map["Assistances"]
        Equipments <- map["Equipments"]
        Description <- map["Description"]
        FromDate <- map["FromDate"]
        ID <- map["ID"]
        Owner <- map["Owner"]
        ParticipantNumber <- map["ParticipantNumber"]
        Participants <- map["Participants"]
        RequiredParticipants <- map["RequiredParticipants"]
        RoomID <- map["RoomID"]
        Scope <- map["Scope"]
        Title <- map["Title"]
        ToDate <- map["ToDate"]
        CurrentTask <- map["CurrentTask"]
        DepartmentID <- map["DepartmentID"]
        Histories <- map["Histories"]
        
    }
}
class CreateBookingSecre : Mappable
{
    var ID:String?
    var Location: MeetingRoomObject?
    var Department: MeetingRoomObject?
    var Description:String?
    var StartTime:String?
    var EndTime:String?
    var VehicleType: MeetingRoomObject?
    var Contact:ApproversObject?
    var ParticipantCount:Int?
    var Participants:[ApproversObject]?
    var Departure:String?
    var Destination:String?
    var Note:String?
    var Comment:String?
    var KMEnd:Int?
    var Cost:Int?
    var Title:String?
    var ExternalParticipants:String?
    var Histories :[HistoryMeeting]?
    
    //    var Driver:MeetingRoomObject?
    //    var Vehicle:MeetingRoomObject?
        var Secretary:ApproversObject?
    
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Location <- map["Location"]
        Description <- map["Description"]
        StartTime <- map["StartTime"]
        EndTime <- map["EndTime"]
        VehicleType <- map["VehicleType"]
        Department <- map["Department"]
        Contact <- map["Contact"]
        ParticipantCount <- map["ParticipantCount"]
        Participants <- map["Participants"]
        Departure <- map["Departure"]
        Destination <- map["Destination"]
        Note <- map["Note"]
        Comment <- map["Comment"]
        KMEnd <- map["KMEnd"]
        Cost <- map["Cost"]
        Title <- map["Title"]
        ExternalParticipants <- map["ExternalParticipants"]
        Histories <- map["Histories"]
        ID <- map["ID"]
        
        //        Driver <- map["Driver"]
        //        Vehicle <- map["Vehicle"]
                Secretary <- map["Secretary"]
        
    }
    
}

class CreateBookingVehicel : Mappable
{
    var ID:String?
    var Location: MeetingRoomObject?
    var Department: MeetingRoomObject?
    var Description:String?
    var StartTime:String?
    var EndTime:String?
    var VehicleType: MeetingRoomObject?
    var Contact:ApproversObject?
    var ParticipantCount:Int?
    var Participants:[ApproversObject]?
    var Departure:String?
    var Destination:String?
    var Note:String?
    var Comment:String?
    var KMEnd:Int?
    var Cost:Int?
    var Title:String?
    var ExternalParticipants:String?
    var Histories :[HistoryMeeting]?
    
    //    var Driver:MeetingRoomObject?
        var Vehicle:MeetingRoomObject?
//    var Secretary:ApproversObject?
    
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Location <- map["Location"]
        Description <- map["Description"]
        StartTime <- map["StartTime"]
        EndTime <- map["EndTime"]
        VehicleType <- map["VehicleType"]
        Department <- map["Department"]
        Contact <- map["Contact"]
        ParticipantCount <- map["ParticipantCount"]
        Participants <- map["Participants"]
        Departure <- map["Departure"]
        Destination <- map["Destination"]
        Note <- map["Note"]
        Comment <- map["Comment"]
        KMEnd <- map["KMEnd"]
        Cost <- map["Cost"]
        Title <- map["Title"]
        ExternalParticipants <- map["ExternalParticipants"]
        Histories <- map["Histories"]
        ID <- map["ID"]
        
        //        Driver <- map["Driver"]
                Vehicle <- map["Vehicle"]
//        Secretary <- map["Secretary"]
        
    }
    
}

class CreateBookingDriver : Mappable
{
    var ID:String?
    var Location: MeetingRoomObject?
    var Department: MeetingRoomObject?
    var Description:String?
    var StartTime:String?
    var EndTime:String?
    var VehicleType: MeetingRoomObject?
    var Contact:ApproversObject?
    var ParticipantCount:Int?
    var Participants:[ApproversObject]?
    var Departure:String?
    var Destination:String?
    var Note:String?
    var Comment:String?
    var KMEnd:Int?
    var Cost:Int?
    var Title:String?
    var ExternalParticipants:String?
    var Histories :[HistoryMeeting]?
    
     var Driver:ApproversObject?
    var Vehicle:MeetingRoomObject?
    //    var Secretary:ApproversObject?
    
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Location <- map["Location"]
        Description <- map["Description"]
        StartTime <- map["StartTime"]
        EndTime <- map["EndTime"]
        VehicleType <- map["VehicleType"]
        Department <- map["Department"]
        Contact <- map["Contact"]
        ParticipantCount <- map["ParticipantCount"]
        Participants <- map["Participants"]
        Departure <- map["Departure"]
        Destination <- map["Destination"]
        Note <- map["Note"]
        Comment <- map["Comment"]
        KMEnd <- map["KMEnd"]
        Cost <- map["Cost"]
        Title <- map["Title"]
        ExternalParticipants <- map["ExternalParticipants"]
        Histories <- map["Histories"]
        ID <- map["ID"]
        
        Driver <- map["Driver"]
        Vehicle <- map["Vehicle"]
        //        Secretary <- map["Secretary"]
        
    }
    
}

class CreateBookingVehicelObject:  Mappable {
    
    var ID:String?
    var Location: MeetingRoomObject?
    var Department: MeetingRoomObject?
    var Description:String?
    var StartTime:String?
    var EndTime:String?
    var VehicleType: MeetingRoomObject?
    var Contact:ApproversObject?
    var ParticipantCount:Int?
    var Participants:[ApproversObject]?
    var Departure:String?
    var Destination:String?
    var Note:String?
    var Comment:String?
    var KMEnd:Int?
    var Cost:Int?
    var Title:String?
    var ExternalParticipants:String?
    var Histories :[HistoryMeeting]?
    

    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Location <- map["Location"]
        Description <- map["Description"]
        StartTime <- map["StartTime"]
        EndTime <- map["EndTime"]
        VehicleType <- map["VehicleType"]
        Department <- map["Department"]
        Contact <- map["Contact"]
        ParticipantCount <- map["ParticipantCount"]
        Participants <- map["Participants"]
        Departure <- map["Departure"]
        Destination <- map["Destination"]
        Note <- map["Note"]
        Comment <- map["Comment"]
        KMEnd <- map["KMEnd"]
        Cost <- map["Cost"]
        Title <- map["Title"]
        ExternalParticipants <- map["ExternalParticipants"]
        Histories <- map["Histories"]
        ID <- map["ID"]
 
    }
}

class VehiceTypeObject: Mappable{
    var ID: Int?
    var Name: String?
    var Brand: String?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        ID <- map["ID"]
        Name <- map["Name"]
        Brand <- map["Brand"]
    }
}
class ArrangersObject:Mappable
{
    var ID: Int?
    var Name: String?
    var UserName: String?
    init() {
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        ID <- map["ID"]
        Name <- map["Name"]
        UserName <- map["UserName"]
    }
}
class VehiceDetailObject: Mappable{
    var Arrangers: [ArrangersObject]?
    var Capacity:Int?
    var ID:String?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        Arrangers <- map["Arrangers"]
         Capacity <- map["Capacity"]
         ID <- map["ID"]
    }
}
class GetVehicelsResponse:  Mappable {
    var data: [DetailVehicelsBookingObject]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}
class DetailVehicelsBookingObject:  Mappable {
    
    var Author:ArrangersObject?
    var Vehicle:VehiceTypeObject?
    var VehicleType:VehiceDetailObject?
    var Editor:ArrangersObject?
    var Location:MeetingRoomObject?
    var Driver:VehiceTypeObject?
    var Action: [ItemActions]?
    
    var ID:String?
    var FromDate:String?
    var Title:String?
    var ToDate:String?
    var Name:String?
    var Capacity:Int?
    var Description:String?
    var Status:Int?
    var ColorStatus:String?
    var Destination:String?
    var Departure:String?
    var Histories:[HistoryMeeting]?
    var KMEnd:Int?
    var notes:String?
    var ExternalParticipants:String?
    var Participants:[ArrangersObject]?
    var Contact:ArrangersObject?
    var Secretary:ArrangersObject?
    var ParticipantCount:Int?
    var Department:MeetingRoomObject?
    required init?(map: Map) {
    }
    func mapping(map: Map) {

        Author <- map["Author"]
        Vehicle <- map["Vehicle"]
        VehicleType <- map["VehicleType"]
        Editor <- map["Editor"]
        ToDate <- map["EndTime"]
        ID <- map["ID"]
        Location <- map["Location"]
        FromDate <- map["StartTime"]
        Name <- map["Name"]
        Description <- map["Description"]
        Capacity <- map["Capacity"]
        Destination <- map["Destination"]
        Title <- map["Title"]
        Status <- map["Status"]
        ColorStatus <- map["ColorStatus"]
        Driver <- map["Driver"]
        
        ParticipantCount <- map["ParticipantCount"]
        Departure <- map["Departure"]
        Histories <- map["Histories"]
        KMEnd <- map["KMEnd"]
        notes <- map["Note"]
        ExternalParticipants <- map["ExternalParticipants"]
        Participants <- map["Participants"]
        Contact <- map["Contact"]
        Action <- map["Action"]
        Department <- map["Department"]
        Secretary <- map["Secretary"]
    }

}

class RegistrationStatisticsObject: Mappable{
    var data: [DetailVehicelsRegistrationStatistics]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data <- map["Data"]
    }
}



class DetailVehicelsRegistrationStatistics:  Mappable {
    
    var Author:String?
    var CheckInDate:String?
    var CheckInOn:String?
    var CheckOutDate:String?
    var CheckOutTo:String?
    var ColorStatus:String?
    
    var Comment:String?
    var Contact:String?
    var Cost:String?
    var Created:String?
    var DepartmentId:Int?
    var DepartmentName:String?
    var Departure:String?
    var Destination:String?
    var Driver:String?
    var Editor:String?
    var EndTime:String?
    var ExternalParticipants:String?
    var ID:Int?
    var KMEnd:String?
    var Location:String?
    var Modified:String?
    var Note:String?
    var ParticipantCount:Int?
    var Participants:String?
    var Secretary:String?
    var StartTime:String?
    var Title:String?
    var Vehicle:String?
    var VehicleType:String?
    var dateGroup:String?
    var dateGroupOrder:Date?

    required init?(map: Map) {
    }
    func mapping(map: Map) {
        
        Author <- map["Author"]
        CheckInDate <- map["CheckInDate"]
        CheckInOn <- map["CheckInOn"]
        CheckOutDate <- map["CheckOutDate"]
        CheckOutTo <- map["CheckOutTo"]
        ColorStatus <- map["ColorStatus"]
        Comment <- map["Comment"]
        Contact <- map["Contact"]
        Cost <- map["Cost"]
        Created <- map["Created"]
        DepartmentId <- map["DepartmentId"]
        DepartmentName <- map["DepartmentName"]
        Departure <- map["Departure"]
        Destination <- map["Destination"]
        Driver <- map["Driver"]
        Editor <- map["Editor"]
        EndTime <- map["EndTime"]
        ExternalParticipants <- map["ExternalParticipants"]
        ID <- map["ID"]
        KMEnd <- map["KMEnd"]
        Location <- map["Location"]
        Modified <- map["Modified"]
        Note <- map["Note"]
        ParticipantCount <- map["ParticipantCount"]
        
        Participants <- map["Participants"]
        Secretary <- map["Secretary"]
        StartTime <- map["StartTime"]
        Title <- map["Title"]
        VehicleType <- map["VehicleType"]
        dateGroup <- map["dateGroup"]
        dateGroupOrder <- map["dateGroupOrder"]
        Vehicle <- map["Vehicle"]
        
    }
    
}


class FeeTypeObject:  Mappable {
    var ID: Int?
    var Name: String?
    init() {
        self.ID = 0
        self.Name = ""
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        Name <- map["Name"]
    }
}
class CreateFeeUpdateObjectResponse:  Mappable {
    
    var Data:[CreateFeeUpdateObject]?
  
    
    init() {
       
        self.Data = []
        
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
       Data <- map["Data"]
        
        
    }
}
class FeeUpdate:Mappable
{
    var fee:[CreateFeeUpdateObject]?
    init() {
        self.fee = []
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        fee <- map["fee"]
    }
}
class CreateFeeUpdateObject:  Mappable {
    
    var BookingID:String?
    var ID: String?
    var FeeType: FeeTypeObject?
    var Note:String?
    var Title:String?
    var Total:Int?
    
    init() {
        self.ID = "0"
        self.BookingID = ""
        self.Note = ""
        self.Title = ""
        self.Total = 0
        self.FeeType = FeeTypeObject()
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        BookingID <- map["BookingID"]
        ID <- map["ID"]
        FeeType <- map["FeeType"]
        Note <- map["Note"]
        Title <- map["Title"]
        Total <- map["Total"]
        
        
    }
}

