//
//  TaskBoard.swift
//  TaskManagement
//
//  Created by HaiComet on 9/22/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class TaskBoard: NSObject {
    var title = ""
    var tag = ""
    var content = ""
    var code = ""
    var createdAt = ""
    var numberAttach = 1
}
