//
//  Task.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/3/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class Task: Mappable {
  var id: String?
  var title: String?
  var description: String?
  var roomName: String?
  var createdAt: Date?
  var updatedAt: Date?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    
  }
}

class TaskProgress: Mappable {
  var id: String?
  var title: String?
  var description: String?
  var roomName: String?
  var createdAt: Date?
  var updatedAt: Date?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    
  }
}
