//
//  ActiveModule.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/8/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper
import GRDB

class ActiveModule: BaseResponse {
    
    var data: ActiveModuleData?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class ActiveModuleData: Mappable {
    var count:Int?
    var moduleCode:String?
    var item:[ItemTask] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count <- map["Count"]
        moduleCode <- map["ModuleCode"]
        item <- map["Items"]
    }
}

class ItemTask: Mappable {
    
    var author:String?
    var title:String?
    var summary:String?
    var status:String?
    var backgroundColorStatus:String?
    var textColorStatus:String?
    var time:String?
    var webUrl:String?
    var mobileObjectId:String?
    var extensionData:String?
    var timeString:String?
    var file: [Files]?
    
    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        author <- map["Author"]
        title <- map["Title"]
        summary <- map["Summary"]
        status <- map["Status"]
        backgroundColorStatus <- map["BackgroundColorStatus"]
        textColorStatus <- map["TextColorStatus"]
        time <- map["Time"]
        webUrl <- map["WebUrl"]
        mobileObjectId <- map["MobileObjectID"]
        extensionData <- map["ExtensionData"]
        timeString <- map["TimeString"]
        file <- map["Files"]
    }
}

class Files: Record, Mappable {
    var content: String?
    var downloadUrl:String?
    var ext: String?
    var id: String?
    var name: String?
    var localLink:URL?
    
    required init?(map: Map) {
        super.init()
    }
    
    required init(row: Row) {
        fatalError("init(row:) has not been implemented")
    }
    
    func mapping(map: Map) {
        content <- map["Content"]
        downloadUrl <- map["DownloadUrl"]
        ext <- map["Ext"]
        id <- map["ID"]
        name <- map["Name"]
    }
    
    func downloadFile() {
        guard let link = self.downloadUrl, localLink == nil, let fileName = self.name else {
            return
        }

        InstanceDB.default.downloadFilePDF(fileName, path: link, done: {[weak self] (result) in
            guard let sself = self else {
                return
            }
            sself.localLink = result
            do {
                try dbQueue.inTransaction { db in
                    try sself.save(db)
                    return .commit
                }
            } catch let e {
                print(e.localizedDescription)
            }
        })
    }
}
