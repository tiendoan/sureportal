//
//  SyncProvider.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

class SyncProvider {
  
  //MARK: - Menu
  /// get menu data
  ///
  /// - Parameters:
  ///   - parentId: id of parent
  ///   - done: return escaping with result
  static func getMenu(parentId: String = "00000000-0000-0000-0000-000000000000", treeFilterCode: String = "", done:@escaping (_ result: [Menu]) -> ()) {
    let router = Router.menu(parentId: parentId, treeFilterCode: treeFilterCode)
    request(router)
      .validate()
      .responseObject {  (response : DataResponse<MenuResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data ?? [])
          } else {
            done([])
          }
        case .failure(let error):
          print(error)
          done([])
        }
    }
  }
  
  ///MARK: - Assigners of tasks
  /// Get list assigners for documents
  ///
  /// - Parameters:
  ///   - documentID: document identify.
  ///   - done: return
  static func getAssignersByDocument(documentID: String, done: @escaping(_ result: [AssignTracking]?) -> ()) {
    let router = Router.getAssignersForTracking(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response : DataResponse<AssignTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
    }
  }
  
  //MARK: - Document
  /// get list document for tree id
  ///
  /// - Parameters:
  ///   - treefilterID: id of tree to get
  ///   - page: index of page
  ///   - done: done: return escaping with result
  static func getDocument(treefilterID: String, page: Int,done:@escaping (_ result: DocumentData?) -> ()) {
    let router = Router.document(treefilterID: treefilterID, page: page)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<DocumentResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
    }
  }
    
    static func getDocumentSync(page: Int,done:@escaping (_ result: DocumentData?) -> ()) {
        let router = Router.documentSync(page: page)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<DocumentResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
  
  
  /// get list progress history for document
  ///
  /// - Parameters:
  ///   - editDeptID: id of dept
  ///   - documentID: id of document
  ///   - done: return escaping with result
  static func getListProcessHistoryDocument(editDeptID: String, documentID:String, done:@escaping (_ result: [ProgressHistory]?) -> ()) {
    let router = Router.documentProcessHistory(editDeptID: editDeptID, documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ProgressHistoryResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
    
    /// get List TrackingWorkflowDocument
    ///
    /// - Parameters:
    ///   - deptID: id of dept
    ///   - trackingDocumentID: id of document
    ///   - done: return escaping with result
    static func getListTrackingWorkflowDocument(deptID: String, trackingDocumentID:String, done:@escaping (_ result: [ProgressHistory]?) -> ()) {
        let router = Router.getListTrackingWorkflowDocument(deptID: deptID, documentID: trackingDocumentID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<ProgressHistoryResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
            }.responseDecrypted { (response) in
                print(response.value ?? "")
        }
    }
  
  
  /// get list tracking for document
  ///
  /// - Parameters:
  ///   - deptID: id of dept
  ///   - status:  "-1" = "Tất cả", "0" = "Mới", "1" = "Đang xử lý", "2" = "Báo cáo", "3" = "Hoãn xử lý", "4" = "Kết thúc"
  ///   - viewType: "0" = "Tất cả", "1" = "Phòng ban", "2" = "Cá nhân"
  ///   - documentID: id of document
  ///   - done: return escaping with result
    static func getListTrackingDocument(deptID: String, status: Int, viewType: Int, documentID:String, isChild: Bool = false, done:@escaping (_ result: [ProgressTracking]?) -> ()) {        let router = Router.documentTracking(deptID: deptID, status: status, viewType: viewType, documentID: documentID, isChild: isChild)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ProgressTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
  
  
  /// get detail of a document
  ///
  /// - Parameters:
  ///   - documentId: id of document
  ///   - done: done: return escaping document full detail
  static func getDetail(documentId: String, done:@escaping (_ result: Document?) -> ()){
    let router = Router.detail(id: documentId)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<DetailResponse>) in
        if let data = response.result.value {
          done(data.data)
        } else {
          done(nil)
        }
    }
  }
  
  //MARK: - Home
  /// get top new item of home dashboard
  ///
  /// - Parameters:
  ///   - itemType: itemType, ex: "Document"
  ///   - page: current page
  ///   - done: done: return escaping with result
  static func getTopNewItemHomeDashboard(itemType: String, page:Int, done:@escaping (_ result: [Document]?) -> ()){
    let router = Router.topNewItem(itemType: itemType, page:page)
    request(router)
      .responseObject {  (response : DataResponse<DocumentResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            if(data.data != nil){
              done(data.data!.items)
            }
            else{
              done(nil)
            }
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
        
    }
  }
  
  /// get chart count of home dashboard
  ///
  /// - Parameters:
  ///   - itemType: itemType, ex: "Document"
  ///   - done: done: return escaping with result
  static func getChartCountOverviewHomeDashboard(itemType: String,done:@escaping (_ result: ChartData?) -> ()){
    let router = Router.chartCountOverview(itemType: itemType)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ChartData>) in
        if let chartData = response.result.value {
            done(chartData)
        } else {
            done(nil)
        }
    }
  }
  
  
  /// get chart line of home dashboard
  ///
  /// - Parameters:
  ///   - viewType: viewType, ex: "month"
  ///   - done: done: return escaping with result
  static func getChartLineOverviewHomeDashboard(viewType: String, done:@escaping (_ result: ChartData?) -> ()){
    let router = Router.chartLineOverview(viewType: viewType)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ChartData>) in
        if let chartData = response.result.value {
          done(chartData)
        } else {
          done(nil)
        }
    }
  }
  
  
  /// update status a document
  ///
  /// - Parameters:
  ///   - documentID: id of document
  ///   - done: done: return escaping with true or flase
  static func updateStatusDocument(documentID:String, done:@escaping(_ success:Bool) -> ()){
    let router = Router.updateStatusDocument(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<UpdateStatusDocumentResponse>) in
        if let data = response.result.value {
          done(data.data)
        } else {
          done(false)
        }
    }
  }
  
  static func downloadFilePDF(_ fileName: String, path:String, done:@escaping(_ result:URL?) -> ()){
    
    let url = Router.baseURLString + path
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
        let pathComponent = fileName
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(pathComponent)
        return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    download(url, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
      print("Progress: \(progress.fractionCompleted)")
      } .validate().responseData { ( response ) in
        //print(response.destinationURL!.absoluteString)
        done(response.destinationURL)
    }
  }
  
  static func getListRecentTrackingDocument(deptID: String, top: Int, documentID:String, done:@escaping (_ result: [RecentTracking]?) -> ()) {
    let router = Router.documentRecentTracking(deptID: deptID, top: top, documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<RecentTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
  
  static func getListTranferDocument(documentID:String, done:@escaping (_ result: [Transfer]?) -> ()) {
    let router = Router.documentTranfer(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<TranferResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
    
    static func getAllUsersInfo(done: @escaping(_ result: [UserProfile]?) -> ()) {
        let router = Router.getAllUsersInfo()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (repsones: DataResponse<UsersResponse>) in
                switch repsones.result {
                case .success:
                    if let data = repsones.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    debugPrint("\(error.localizedDescription)")
                    done(nil)
                }
            }.responseDecrypted { (repsones) in
                print(repsones.value ?? "")
        }
    }
  
  static func getAllUsersInDepartments(done: @escaping(_ result: [Assignee]?) -> ()) {
    let router = Router.getAllUsersDepartments(deptID: Constants.default.DEFAULT_PARENT_ID, mode: 1, keyword: "")
    request(router)      
      .validate(statusCode: 200..<300)
      .responseObject { (repsones: DataResponse<AssigneeResponse>) in
        switch repsones.result {
        case .success:
          if let data = repsones.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          debugPrint("\(error.localizedDescription)")
          done(nil)
        }
      }.responseDecrypted { (repsones) in
        print(repsones.value ?? "")
    }
  }
  
  static func getCategoryTracking(keyWord: String, done: @escaping(_ result: [CategoryTracking]?) -> ()) {
    let router = Router.getCategorizeTracking(keyWord: keyWord)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<CategoriesResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
  
    static func approveDocument(documentID: String, workFlowStepID: String, contentObj: String, nextWFStep:GetNextWorkFlowStep?, done: @escaping() -> ()) {
        let router = Router.approveDocument(documentID: documentID, workFlowStepID: workFlowStepID, contentObj: contentObj, nextWFStep: nextWFStep?.toJSON())
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            print("approveDocument: \(String(describing: data.message))")
            done()
          } else {
            done()
          }
        case .failure(let error):
          print(error)
          done()
        }
    }
  }
  
    static func assignTrackingDocument(params: AssignTrackingRequest, isCreateTask: Bool, done: @escaping() -> ()) {    
        let router = Router.assignTrackingDocument(params: params.toJSON(), isCreateTask: isCreateTask)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            print("assignTrackingDocument: \(String(describing: data.message))")
            done()
          } else {
            done()
          }
        case .failure(let error):
          print(error)
          done()
        }
    }
  }
    
    static func finishDocuments(docIDs: [String], deptIDs: [String], done: @escaping(Bool) -> ()) {
        let router = Router.finishDocuments(docIDs: docIDs, deptIDs: deptIDs)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        print("finishDocuments: \(String(describing: data.message))")
                        done(true)
                    } else {
                        done(false)
                    }
                case .failure(let error):
                    print(error)
                    done(false)
                }
        }
    }
  
  static func returnDocument(documentID: String, comment: String, done: @escaping() -> ()) {
    let router = Router.returnDocument(documentID: documentID, comment: comment)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            print("\(String(describing: data.message))")
            done()
          } else {
            done()
          }
        case .failure(let error):
          print(error)
          done()
        }
    }
  }
    
    static func sendEmail(documentID: String, subject: String, body: String, receivers: String, done: @escaping(_ result: Bool?, _ error: String?) -> ()) {
        let router = Router.sendMail(documentID: documentID, subject: subject, body: body, receivers: receivers)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value, let status = data.status {
                        if status == 1 {    //success
                            done(true, nil)
                        } else {
                            done(false, data.message)
                        }
                    } else {
                        done(false, nil)
                    }
                case .failure(let error):
                    print(error)
                    done(false, error.localizedDescription)
                }
        }
    }
  
  static func getListTrackingForUser(documentID: String, done: @escaping(_ result: [ProcessTracking]?, _ error: String?) -> ()) {
    let router = Router.getListTrackingForUser(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<ProcessTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data, nil)
          }
        case .failure(let error):
          debugPrint(error)
          done(nil, error.localizedDescription)
        }
    }
  }
  
  static func processTrackingDocument(tracking: ProcessTracking, done: @escaping(_ result: Bool?, _ error: String?) -> ()) {
    let router = Router.processTrackingDocument(document: tracking.toJSON())
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<VerifyCodeResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value, let status = data.status {
            if status == 1 {    //success
                done(true, nil)
            } else {
                done(false, data.message)
            }
          } else {
            done(false, nil)
          }
        case .failure(let error):
          done(false, error.localizedDescription)
        }
    }
  }
  
  static func getListAppraiseTrackingDocument(documentID: String, done: @escaping(_ trackings: [ProcessTracking]?, _ message: String?) -> ()) {
    let router = Router.getListAppraiseTrackingDocument(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<ProcessTrackingResponse>) in
        switch response.result {
        case .success:
          if let data: ProcessTrackingResponse = response.result.value {
            done(data.data, data.message)
          }
        case .failure(let error):
          debugPrint(error)
          done(nil, error.localizedDescription)
        }
    }
  }
  
  static func getListTrackingRating(trackingID: String, done: @escaping(_ ratings: [StatusDocument]?, _ msg: String? ) -> ()) {
    let router = Router.getListTrackingRating(trackingID: trackingID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<TrackingRatingResponse>) in
        switch response.result {
        case .success:
          if let data: TrackingRatingResponse = response.result.value {
            done(data.data, data.message)
          }
        case .failure(let error):
          debugPrint(error)
          done(nil, error.localizedDescription)
        }
    }
  }
  
    static func advancedSearchDocument(keyword: String, advancedParams: [String], page:Int, done:@escaping (_ result: [Document]) -> ()){
        let router = Router.advancedSearchDocument(keyWord: keyword, advancedParams: advancedParams, page: page)
    request(router)
      .responseObject {  (response : DataResponse<DocumentResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            if(data.data != nil){
              done(data.data!.items)
            }
            else{
              done([])
            }
          } else {
            done([])
          }
        case .failure(let error):
          debugPrint(error)
          done([])
        }        
    }
  }
  
  static func getListNotification(done:@escaping (_ result: [NotificationItem]?) -> ()){
    let router = Router.getListNotification()
    request(router)
      .responseObject {  (response : DataResponse<NotificationResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done([])
          }
        case .failure(let error):
          debugPrint(error)
          done([])
        }
    }
  }
    static func getListImportantDocument(done:@escaping (_ result: [ImportantDocument]?) -> ()){
        let router = Router.getListImportantDocument()
        request(router)
            .responseObject {  (response : DataResponse<ImportantResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data.data)
                    } else {
                        done([])
                    }
                case .failure(let error):
                    debugPrint(error)
                    done([])
                }
        }
    }
    
    static func updateNotification(itemID: String, itemType: String, done: @escaping (_ result: Bool?, _ error: String?) -> ()){
        let router = Router.updateNotification(itemId: itemID, itemType: itemType)
        request(router)
            .responseObject {  (response : DataResponse<VerifyCodeResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value, let status = data.status {
                        if status == 1 {    //success
                            done(true, nil)
                        } else {
                            done(false, data.message)
                        }
                    } else {
                        done(false, nil)
                    }
                case .failure(let error):
                    debugPrint(error)
                    done(false, error.localizedDescription)
                }
        }
    }
    
    static func updateNotifications(itemIDs: [String], itemTypes: [String], done: @escaping (_ result: Bool?, _ error: String?) -> ()){
        let router = Router.updateNotifications(itemIds: itemIDs, itemTypes: itemTypes)
        request(router)
            .responseObject {  (response : DataResponse<VerifyCodeResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value, let status = data.status {
                        if status == 1 {    //success
                            done(true, nil)
                        } else {
                            done(false, data.message)
                        }
                    } else {
                        done(false, nil)
                    }
                case .failure(let error):
                    debugPrint(error)
                    done(false, error.localizedDescription)
                }
        }
    }
  
  static func appriseTrackingDocument(tracking: ProcessTracking, done: @escaping(_ result: Bool?, _ error: String?) -> ()) {
    let router = Router.appriseTrackingDocument(apprise: tracking.toJSON())
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<VerifyCodeResponse>) in
        switch response.result {
        case .success:
            if let data = response.result.value, let status = data.status {
                if status == 1 {    //success
                    done(true, nil)
                } else {
                    done(false, data.message)
                }
            } else {
                done(false, nil)
            }
        case .failure(let error):
          done(false, error.localizedDescription)
        }
    }
  }
    
    static func uploadFile(_ uploadParams:UploadFileInfo, updateHandler:@escaping (_ percent:Float) -> Void ,done: @escaping (_ fileID: String?, _ error: String?) -> Void){
        let router = Router.uploadFile(uploadParams)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<UploadFileResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value,
                        let value = data.fileID {
                        done(value, nil)
                    } else {
                        done(nil, nil)
                    }
                case .failure(let error):
                    done(nil, error.localizedDescription)
                }
        }
        
    }
    
    static func getTopAnnouncements(done:@escaping (_ result: [Announcement]) -> ()){
        let router = Router.getTopAnnouncements()
        request(router)
            .responseObject {  (response : DataResponse<AnnouncementResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data.data ?? [])
                    } else {
                        done([])
                    }
                case .failure(let error):
                    debugPrint(error)
                    done([])
                }
        }
    }
    
    //MARK: - Document
    /// get list document for tree id
    ///
    /// - Parameters:
    ///   - treefilterID: id of tree to get
    ///   - page: index of page
    ///   - done: done: return escaping with result
    static func getListItemProdure(keword: String, page: Int, status:Int ,done:@escaping (_ result: DigitalResponse?) -> ()) {
        let router = Router.getListSignatureDigital(keywork: keword, pageNumber: page, status: status)
        request(router)
            .validate(statusCode: 200..<300)
  
            .responseObject {  (response : DataResponse<DigitalResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func getProcedureWorkflow(Id: String,done:@escaping (_ result: ProcedureDigitalResponse?) -> ()) {
        let router = Router.getProcedureWorkflow(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<ProcedureDigitalResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getProcedureHistory(Id: String,done:@escaping (_ result: ProcedureHistoryResponse?) -> ()) {
        let router = Router.getProcedureHistory(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<ProcedureHistoryResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getProcedureAttachment(Id: String,done:@escaping (_ result: ProcedureHistoryResponse?) -> ()) {
        let router = Router.getProcedureAttachment(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<ProcedureHistoryResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getNextWorkFlowSteps(docID: String, done:@escaping (_ result: [GetNextWorkFlowStep]?) -> ()) {
        let router = Router.getNextWorkFlowStep(docID: docID)
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<NextWorkFlowStepResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getProcedureAttachmentFile(Id: String,done:@escaping (_ result: ProcedureAttachmentFileResponse?) -> ()) {
        let router = Router.getProcedureAttachment(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<ProcedureAttachmentFileResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func getProcedureCategories(done:@escaping (_ result: ProcedureDigitalObject?) -> ()) {
        let router = Router.getProcedureCategories()
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<ProcedureDigitalObject>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getProcedureTypesByProcedureCategoryID(Id: String,done:@escaping (_ result: ProcedureDigitalObject?) -> ()) {
        let router = Router.getProcedureTypesByProcedureCategoryID(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<ProcedureDigitalObject>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func GetProcedureWorkflowTemplatesByProcedureTypeId(Id: String,done:@escaping (_ result: ProcedureDigitalObject?) -> ()) {
        let router = Router.getProcedureWorkflowTemplatesByProcedureTypeId(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<ProcedureDigitalObject>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func GetProcedureWorkflowTemplateById(Id: String,done:@escaping (_ result: ProcedureDigitalWorkFlowObject?) -> ()) {
        let router = Router.getProcedureWorkflowTemplateById(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<ProcedureDigitalWorkFlowObject>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func addNewProcedure(AttachmentNames : [ProcedureAttachmentInfos],
                                IsDraft : Bool,
                                Name : String,
                                Summary : String,
                                ProcedureCategory : String,
                                ProcedureType : String,
                                ProcedureWorkflowTemplateId : String,ProcedureAttachmentSigningPositions: [ProcedurePositions],done:@escaping (_ result: ProcedureDigitalWorkFlowObject?) -> ()) {
        
        var dictionary = [[String: Any]]()
        if ProcedureAttachmentSigningPositions.count > 0 {
            for index in 0...ProcedureAttachmentSigningPositions.count - 1 {
                let product: ProcedurePositions = ProcedureAttachmentSigningPositions[index]
                dictionary.append(product.toJSON())
            }
        }
        
        var dictionaryName = [[String: Any]]()
        if AttachmentNames.count > 0 {
            for index in 0...AttachmentNames.count - 1 {
                let product: ProcedureAttachmentInfos = AttachmentNames[index]
                dictionaryName.append(product.toJSON())
            }
        }
        
        let router = Router.addNewProcedure(AttachmentNames: dictionaryName, IsDraft: IsDraft, Name: Name, Summary: Summary, ProcedureCategory: ProcedureCategory, ProcedureType: ProcedureType, ProcedureWorkflowTemplateId: ProcedureWorkflowTemplateId,ProcedureAttachmentSigningPositions: dictionary)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<ProcedureDigitalWorkFlowObject>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func getListUserTask(done:@escaping (_ result: UserListWorkTaskResponse?) -> ()) {
        let router = Router.getUserInfos()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<UserListWorkTaskResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func GetListUserInWorkflowTemplateSignWithDigitalSignature(Id: String,done:@escaping (_ result: SinagureReponse?) -> ()) {
        let router = Router.getListUserInWorkflowTemplateSignWithDigitalSignature(Id: Id)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<SinagureReponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getCountPage(Name: String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.countPage(Name: Name)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func checkAttachment(Name: String,Attach: String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.checkAttachment(Name: Name, Attach: Attach)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func createPersonalWorkflowTemplate(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.createPersonalWorkflowTemplate(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func deleteProcedureDigital(Id: String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.deleteProcedureDigital(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func rejectProcedureDigital(Id: String,comment:String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.rejectProcedureDigital(ID: Id, Comment: comment)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func returnProcedureDigital(Id: String,comment:String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.returnProcedureDigital(ID: Id, Comment: comment)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func tranferProcedureDigital(Id: String,UserId: String,comment:String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.tranferProcedureDigital(ID: Id, UserId: UserId, Comment: comment)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func assignProcedureDigital(Id: String,procedureType: Int,comment:String,UserIDs:[String],done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.assignProcedureDigital(ID: Id, procedureType: procedureType, Comment: comment, UserIDs: UserIDs)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func auditProcedureDigital(Id: String,comment:String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.auditProcedureDigital(ID: Id, Comment: comment)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
         }
    }
    
    static func checkCurrentUserSignWithDigitalSignature(Id: String,done:@escaping (_ result: ProcedureDigitalSigningResponse?) -> ()) {
        let router = Router.checkCurrentUserSignWithDigitalSignature(ID: Id)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<ProcedureDigitalSigningResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func approveProcedureDigital(Id: String,comment:String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.approveProcedureDigital(ID: Id, Comment: comment)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }

    
    
    static func getPersonalSignatureInfos(done:@escaping (_ result: PersonalSignatureInfosResponse?) -> ()) {
        let router = Router.getPersonalSignatureInfos()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<PersonalSignatureInfosResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }

    static func approveProcedureDigitalWithSignature(Id: String,comment:String,SignatureId: String,PrivateKey:String,done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.approveProcedureDigitalWithSignature(ID: Id, Comment: comment, SignatureId: SignatureId, PrivateKey: PrivateKey)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func advanceSearchDigitalkeyParam(paramSearch: [String:Any],done:@escaping (_ result: DigitalResponse?) -> ()) {
        let router = Router.advanceSearchDigital(paramSearch: paramSearch)
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<DigitalResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getLeftNavigationCount(done:@escaping (_ result: NavDigitalObjectArr?) -> ()) {
        let router = Router.getLeftNavigationCount()
        request(router)
            .validate(statusCode: 200..<300)
            
            .responseObject {  (response : DataResponse<NavDigitalObjectArr>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    // Metting Rooom
    static func getDepartmentMeetingRoom(done:@escaping (_ result: MeetingRoomResponse?) -> ()) {
        let router = Router.getDepartmentMeetingRoom()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<MeetingRoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    // Metting Rooom
    static func getChartMeetingRoom(json: [String:Any],done:@escaping (_ result: MeetingRoomChartResponse?) -> ()) {
        let router = Router.getChartMeeting(json: json)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<MeetingRoomChartResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    
    static func getListDepartmentMeetingRoom(DepartmentID: String,RoomID:String,done:@escaping (_ result: RoomResponse?) -> ()) {
        let router = Router.getListDepartmentMeetingRoom(DepartmentID: DepartmentID, RoomID: RoomID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<RoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getBookingRoom(DepartmentID: String,FromDate:String,Page: String,PageSize:String
        ,RoomID: String,Status:String,ToDate:String,done:@escaping (_ result: GetRoomResponse?) -> ()) {
        let router = Router.getBookingRooms(DepartmentID: DepartmentID, FromDate: FromDate, Page: "0", PageSize: "0", RoomID: RoomID, Status: "-1", ToDate: ToDate)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<GetRoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func createBookingRoom(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.createBookingRoom(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
         }
    }
    
    static func getDetailBookingRoom(OrgID: String,done:@escaping (_ result: BookingObjectDetailResponse?) -> ()) {
        let router = Router.getDetailBookingRoom(OrgID: OrgID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<BookingObjectDetailResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func approveBookingRoom(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.approveBookingMeetingRoom(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func deleteBookingRoom(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.deleteBookingMeetingRoom(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func rejectBookingRoom(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.rejectBookingMeetingRoom(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func updateBookingRoom(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.updateBookingMeetingRoom(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getCurrentDepartment(done:@escaping (_ result: RoomResponse?) -> ()) {
        let router = Router.getCurrentDepartment()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<RoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func authorBookingRoom(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.AuthorDeleteBooking(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func ReturnBookingMeeting(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.ReturnBookingMeeting(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    // Vehicels
    static func getLocationsVehicels(done:@escaping (_ result: MeetingRoomResponse?) -> ()) {
        let router = Router.getLocationVehicels()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<MeetingRoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getVehiclesByType(DeparmentID:String,ID:String,done:@escaping (_ result: RoomResponse?) -> ()) {
        let router = Router.getVehiclesByType(DeparmentID: DeparmentID, ID: ID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<RoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getListVehicelsType(ID:String,done:@escaping (_ result: RoomResponse?) -> ()) {
        let router = Router.getVehicleType(ID: ID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<RoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getVehicelsBooking(LocationID: String,FromDate:String
        ,VehicleID: String,Status:String,ToDate:String,done:@escaping (_ result: GetVehicelsResponse?) -> ()) {
        let router = Router.getBookingVehicels(LocationID: LocationID, VehicleID: VehicleID, FromDate: FromDate, Status: "-1", ToDate: ToDate)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<GetVehicelsResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func createBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.createBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getDetailBookingVehicel(OrgID: String,done:@escaping (_ result: VehicelObjectDetailResponse?) -> ()) {
        let router = Router.getDetailBookingVehicel(OrgID: OrgID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<VehicelObjectDetailResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func rejectBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.rejectBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func deleteBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.deleteBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func arrangerBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.arrangerBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func checkoutBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.checkOutBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func checkinBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.checkinBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getDriversByVehicleType(ID:String,done:@escaping (_ result: RoomResponse?) -> ()) {
        let router = Router.getDriversByVehicleType(ID: ID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<RoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func approveBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.approveBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func updateBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.updateBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func externalBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.externalBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func externalCheckinBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.externalCheckinBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func extendBookingVehicel(json: [String:Any], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.extendBookingVehicel(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    
    static func getFeeVehicelType( done:@escaping (_ result: FeeTypeResponse?) -> ()) {
        let router = Router.getFeeTypes()
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<FeeTypeResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func updateCostVehicel(json: [[String:Any]], done:@escaping (_ result: CountPageNumb?) -> ()) {
        let router = Router.updateCosts(json: json)
        request(router)
            .validate(statusCode: 200..<600)
            .responseObject {  (response : DataResponse<CountPageNumb>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getBookingCost(ID:String,done:@escaping (_ result: CreateFeeUpdateObjectResponse?) -> ()) {
        let router = Router.getCostsByBooking(ID: ID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<CreateFeeUpdateObjectResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getAllUsersInAllDepartments(done: @escaping(_ result: [Assignee]?) -> ()) {
        let router = Router.getAllUsersDepartments(deptID: Constants.default.DEFAULT_PARENT_ID, mode: 10, keyword: "")
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (repsones: DataResponse<AssigneeResponse>) in
                switch repsones.result {
                case .success:
                    if let data = repsones.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    debugPrint("\(error.localizedDescription)")
                    done(nil)
                }
            }.responseDecrypted { (repsones) in
                print(repsones.value ?? "")
        }
    }
    
    static func getVehicleRegistrationStatistics(StartTime:String,EndTime:String,LocationId:String,
                                                 VehicleTypeId:String,DepartmentId:String,Status:String,done:@escaping (_ result: RegistrationStatisticsObject?) -> ()) {
        let router = Router.getVehicleRegistrationStatistics(StartTime: StartTime, EndTime: EndTime, LocationId: LocationId, VehicleTypeId: VehicleTypeId, DepartmentId: DepartmentId, Status: Status)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<RegistrationStatisticsObject>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    static func getCurrentDepartmentVehicle(done:@escaping (_ result: RoomResponse?) -> ()) {
        let router = Router.getCurrentDepartmentVehicle()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<RoomResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getActiveModule(done: @escaping (_ result: ModuleResponse?) -> ()) {
        let router = Router.getActiveModule()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<ModuleResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }
    
    static func getCurrentTask(moduleCode: String, done: @escaping (_ result: ActiveModuleData?) -> ()) {
        let router = Router.getCurrentTask(moduleCode: moduleCode)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<ActiveModule>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
        }
    }

}
