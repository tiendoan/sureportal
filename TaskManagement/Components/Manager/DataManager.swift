//
//  DataManager.swift
//  TaskManagement
//
//  Created by Scor Doan on 4/13/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation

class DataManager {
    static let shared = DataManager()
    
    var navigDigitalObject: [NavDigitalObject] = [NavDigitalObject]()
    var allUsers: [UserProfile] = [UserProfile]()
    var flatEmployees: [Assignee] = [Assignee]()
    var notificationList: [NotificationItem] = [NotificationItem]()
    var arrayMeetingRoomObject: [MeetingRoomObject] = []
    var arrrLocationVehicels: [MeetingRoomObject] = []
    var employees: [Assignee] = [Assignee]() {
        didSet {
            departments = getDepartments(list: employees)//flatEmployees.filter {$0.isDept == true}
        }
    }
    var departments: [Assignee] = [Assignee]()
    var importantDocuments = [ImportantDocument]()
    func getAllUsersInfo() {
        SyncProvider.getAllUsersInfo { (users) in
            self.allUsers = users ?? []
        }
    }
    func getAllEmployees () {
        SyncProvider.getAllUsersInDepartments { (assigners) in
            if let lAssigners: [Assignee] = assigners {
                self.employees = lAssigners
                self.flatEmployees = Constants.default.recursiveFlatmap(list: lAssigners, true).sorted{ ($0.value ?? "") < ($1.value ?? "") }
            } else {
                self.employees = Dummies.shared.getAllEmployees()
                self.flatEmployees = Dummies.shared.getAllEmployees()
            }
        }
    }
    
    func getDepartments(list: [Assignee]) -> [Assignee] {
        var results = [Assignee]()
        for element in list {
            if element.isDept == true {
                results.append(element)
            }
            if let subList: [Assignee] = element.childrens,
                !subList.isEmpty {
                results += getDepartments(list: subList)
            }
        }
        return results
    }
    
    class func finishDocument(document:Document, done: @escaping (Bool) -> ()) {
        guard let docID = document.id, let deptID = document.editDepartment else {
            done(false)
            return
        }
        let docIDs = [docID]
        let deptIDs = [deptID]
        SyncProvider.finishDocuments(docIDs: docIDs, deptIDs: deptIDs) { (finish) in
            //TODO: need to process with database
            //....
            InstanceDB.default.finishDocument(document: document)
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_RELOAD), object: nil, userInfo: nil)
            done(finish)
        }
    }
    
    //Using for future
    class func finishDocuments(docs:[Document], done: @escaping (Bool) -> ()) {
        let docIDs = docs.filter{$0.id != nil}.map{$0.id!}
        let deptIDs = docs.filter{$0.editDepartment != nil}.map{$0.editDepartment!}
        SyncProvider.finishDocuments(docIDs: docIDs, deptIDs: deptIDs) { (finish) in
            //TODO: need to process with database
            //....
            InstanceDB.default.finishDocuments(docs)
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_RELOAD), object: nil, userInfo: nil)
            done(finish)
        }
    }
    
    func getImportantDocuments () {
        SyncProvider.getListImportantDocument { (list) in
            if let importantDocuments = list {
                self.importantDocuments = importantDocuments
            }
        }
    }
    func getLeftNavigationCount(){
        SyncProvider.getLeftNavigationCount(done: {
            ( result ) in
            if ( result != nil )
            {
                self.navigDigitalObject = (result?.data)!
            }
        })
    }
    func getListNotification(){
        SyncProvider.getListNotification(done: { (result) in
            if let rs: [NotificationItem] = result {
                self.notificationList = rs
            }
        })
        
    }
    
    func getListNotification(completionHandler: @escaping () -> Void ) {
        SyncProvider.getListNotification(done: { (result) in
            if let rs: [NotificationItem] = result {
                self.notificationList = rs
                completionHandler()
            }
        })
    }

    
    func getBookingRoom(){
        SyncProvider.getDepartmentMeetingRoom(done: {
            (result) in
            if ( result?.data != nil )
            {
                self.arrayMeetingRoomObject = (result as! MeetingRoomResponse).data!
            }
        })
    }
    func getLocations(){
        SyncProvider.getLocationsVehicels(done: {
            (result) in
            if ( result?.data != nil )
            {
                self.arrrLocationVehicels = (result as! MeetingRoomResponse).data!
            }
        })
    }
   
}
