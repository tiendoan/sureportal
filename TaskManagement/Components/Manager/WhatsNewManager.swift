//
//  WhatsNewManager.swift
//  TaskManagement
//
//  Created by Scor Doan on 6/27/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation

class WhatsNewManager {
    static let shared = WhatsNewManager()
    
    func updateAppVersion() -> Bool {
        let currentVersion = UserDefaults.standard.string(forKey: Constants.UserDefault.kAppVersion)
        let newVersion = Constants.getVersionName()
        if currentVersion == nil || currentVersion?.compare(newVersion) == .orderedAscending {
            UserDefaults.standard.set(newVersion, forKey: Constants.UserDefault.kAppVersion)
            return true
        }
        return false
    }
}
