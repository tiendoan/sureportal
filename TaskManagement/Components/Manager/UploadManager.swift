//
//  UploadManager.swift
//  binumi-ios
//
//  Created by Tien Doan on 10/19/15.
//  Copyright © 2015 Binumi. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


typealias UploadCompletionHandler = (_ fileID:String?, _ err:String?)->Void
let kUploadChunkSize                     = 5024*1024

struct UploadFileInfo {
    var fileID: String?
    var offset: Int = 0
    //var chunks: NSNumber

    var size:Int
    var data64: String?
    var type:String?
    var endFile:Bool = false
    
//    var parameters: [String: AnyObject] {
//        var dict = [String: AnyObject]()
//        dict["chunk"] = chunk
//        dict["chunks"] = chunks
//
//        if (self.voice != nil) {
//            dict["voice"] = self.voice
//        }
//
//        if (self.name != nil) {
//            dict["name"] = self.name as AnyObject?
//        }
//
//        if (self.fileName != nil) {
//            dict["filename"] = self.fileName as AnyObject?
//        }
//
//        if (self.type != nil) {
//            dict["type"] = self.type as AnyObject?
//        }
//
//        if (self.folderId != nil) {
//            dict["folderId"] = NSNumber(value:self.folderId!)
//        }
//
//        return dict
//    }
}


class UploadManager: NSObject {
    
    static let sharedInstance = UploadManager()

    //var uploadManager:Alamofire.SessionManager!
    var defaultInfo:UploadFileInfo?
    var chunkIdx = 0
    var uploadData:Data!

    
    override init() {
        super.init()
//        let configuration = URLSessionConfiguration.default
//        configuration.timeoutIntervalForResource = 60*20
//        configuration.timeoutIntervalForRequest = 60*20
//        uploadManager = Alamofire.SessionManager(configuration: configuration)
    }

    
    
    class func uploadFile(_ data:Data, type:String?, updateHandler:@escaping (_ percent:Float) -> Void, completionHandler:@escaping UploadCompletionHandler){
        let size = Int(ceil(Float(data.count) / 1048576))

        sharedInstance.uploadFile(data, fileId: nil, type: type, updateHandler: updateHandler, completionHandler: completionHandler)
        
    }

    
    func uploadFile(_ data:Data, fileId: String?, type:String?, updateHandler:@escaping (_ percent:Float) -> Void, completionHandler:@escaping UploadCompletionHandler){
        uploadData = data
        chunkIdx = 0

        //let chunks = Int(ceil(Float(data.count)/Float(kUploadChunkSize)))
        defaultInfo = UploadFileInfo(fileID: nil, offset: 0, size: 0, data64: nil, type: type, endFile: false)
        getChunkInfo()
        
        uploadChunk(updateHandler:updateHandler, completion: completionHandler)
        
        
    }
    
    func getChunkInfo(){
        let offset = chunkIdx * kUploadChunkSize
        var range:Range<Int> = offset..<kUploadChunkSize+offset //NSMakeRange(offset, kUploadChunkSize)
        if offset+kUploadChunkSize > uploadData.count {
            let length = uploadData.count-offset
            range = offset..<(length > 0 ? length+offset : offset) //NSMakeRange(offset, length > 0 ? length : 0)
            defaultInfo?.endFile = true
        }
        let chunkData = uploadData.subdata(in: range)
        print("chunk \(chunkIdx) : \(chunkData)")
        
        defaultInfo?.size = chunkData.count
        defaultInfo?.offset = offset
        defaultInfo!.data64 = chunkData.base64EncodedString()
        
    }
    
    func uploadChunk(updateHandler:@escaping (_ percent:Float) -> Void, completion:@escaping UploadCompletionHandler){

        SyncProvider.uploadFile(defaultInfo!
            ,updateHandler: {(percent:Float) in
                print("percent:\(percent)")
            }
            ,done: { (fileID, error) -> Void in
                
                if let fileid = fileID {
                    if self.defaultInfo?.endFile == true {
                        completion(fileid, nil)
                    } else {
                        self.chunkIdx += 1
                        let percent = Float(self.chunkIdx * kUploadChunkSize) / Float(self.uploadData.count)
                        print("Continue upload chunk: \(self.chunkIdx) - percent:\(percent)")
                        updateHandler(percent)
                        self.getChunkInfo()
                        self.uploadChunk(updateHandler:updateHandler, completion: completion)
                    }
                    
                } else {
                    completion(nil, error)
                }

        })
    }

}
