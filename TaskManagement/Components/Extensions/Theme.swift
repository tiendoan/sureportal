//
//  Theme.swift
//  TaskManagement
//
//  Created by Tuanhnm on 11/21/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

class Theme {
  static let `default` = Theme()
  
  var normalWhiteColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
  var normalTextColor: UIColor = #colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1)
  var normalBlackColor: UIColor = #colorLiteral(red: 0.6039215686, green: 0.6039215686, blue: 0.6039215686, alpha: 1)
  var normalBlackBorderColor: UIColor = #colorLiteral(red: 0.6039215686, green: 0.6039215686, blue: 0.6039215686, alpha: 1)
  var normalGreenSelectedColor: UIColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
  var normalBlueOceanColor: UIColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
  var normalOrangeSelectedColor: UIColor = #colorLiteral(red: 0.968627451, green: 0.6666666667, blue: 0.2431372549, alpha: 1)
  var normalBlueSelectedColor: UIColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
    var normalGrayBorderColor: UIColor = #colorLiteral(red: 0.8619655967, green: 0.8815796971, blue: 0.9313835502, alpha: 1)
    
  var normalBorderWidth: CGFloat = 1.0
  var normalCornerRadius: CGFloat = 5.0
  var normalFontSize: CGFloat = 14.0
  var middleFontSize: CGFloat = 16.0
  var highFontSize: CGFloat = 18.0
  var smallButtonSize: CGFloat = 25.0
  
  func regularFont(size: CGFloat) -> UIFont {
    return UIFont(name: "Muli-Regular", size: size)!
  }
  
  func semiBoldFont(size: CGFloat) -> UIFont {
    return UIFont(name: "Muli-SemiBold", size: size)!
  }
  
  func lightFont(size: CGFloat) -> UIFont {
    return UIFont(name: "Muli-Light", size: size)!
  }
  
  func boldFont(size: CGFloat) -> UIFont {
    return UIFont(name: "Muli-Bold", size: size)!
  }
//    func italicFont(size: CGFloat) -> UIFont {
//        return UIFont(name: "Muli-Italic", size: size)!
//    }
    
    func italicFont(size: CGFloat) -> UIFont {
        return UIFont.italicSystemFont(ofSize: size)
        //return UIFont(name: "Muli-Italic", size: size)!
    }
}
