//
//  Extensions.swift
//  DZonesApp
//
//  Created by Thang Nguyen on 7/20/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    public func vsn_setImage(url: URL){
        //      let processor = ResizingImageProcessor(referenceSize: CGSize(width: self.firstImageView.width, height: 200), mode: .aspectFit)
        let placeholder = UIImage(color: UIColor(hexString: "#64767a")!, size: self.size)
        //self.kf.setImage(with: url, placeholder:placeholder, options: [.transition(.fade(0.4)), .cacheOriginalImage])
        self.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.4)),.cacheMemoryOnly], progressBlock: nil, completionHandler: nil)
    }
    
    public func setImage(url: URL, makeCircle: Bool = false){
        let placeholder = UIImage(color: UIColor(hexString: "#F4F4F4")!, size: self.size)
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.4)), .cacheOriginalImage])
    }
}
