//
//  TimeIntervalExtensions.swift
//  DZonesApp
//
//  Created by Thang Nguyen on 6/20/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation

public extension TimeInterval {
  func toString(_ format : String = "hh:mm:ss") -> String {
    var asInt   = NSInteger(self)
    let ago = (asInt < 0)
    if (ago) {
      asInt = -asInt
    }
    let s = asInt % 60
    let m = (asInt / 60) % 60
    let h = ((asInt / 3600))%24
    
    var value = format
    if h > 0{
      value = value.replacingOccurrences(of: "hh", with: String(format: "%0.2d", h))
    }
    else{
      value = value.replacingOccurrences(of: "hh:", with: "")
    }
    value = value.replacingOccurrences(of: "mm",  with: String(format: "%0.2d", m))
    value = value.replacingOccurrences(of: "ss",  with: String(format: "%0.2d", s))
    
    return value
  }
}
