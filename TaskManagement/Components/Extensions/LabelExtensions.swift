//
//  LabelExtensions.swift
//  TaskManagement
//
//  Created by Tuanhnm on 11/21/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

class LVLabel: UILabel {
  override func drawText(in rect: CGRect) {   
    super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)))
  }
}
