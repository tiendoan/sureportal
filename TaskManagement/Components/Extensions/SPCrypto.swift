//
//  SPCrypto.swift
//  TaskManagement
//
//  Created by Tuanhnm on 1/9/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import CryptoSwift

class SPCrypto {
  static let `default` = SPCrypto()
  
  func decryptAES(cleanText: String, key: String = "SureP0rt@l!@2017", iv: String = "1234567890123456") -> String? {
    do {
        let key: String = UserDefaults.standard.string(forKey: kSettingSecurityKey) ?? "SureP0rt@l!@2017"
      if let encryptedBytes = cleanText.stringToBytes() {
        let aes = try AES(key: Array(key.utf8), iv: Array(iv.utf8), blockMode: .CBC, padding: .pkcs7)
        let plainText = try encryptedBytes.decrypt(cipher: aes)
        return String(bytes: plainText, encoding: .utf8)
      }
    } catch {
      debugPrint("SPCrypto.decryptAES.error : \(error)")
    }
    return nil
  }
}
