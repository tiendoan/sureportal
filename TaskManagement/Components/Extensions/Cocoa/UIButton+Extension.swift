//
//  UIButton+Extension.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/13/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

extension UIButton {
    func setAttributed(title: String, color: UIColor, font: UIFont?, isUnderLine: Bool = false ) {
        var attr = NSAttributedString()
        if isUnderLine {
            attr = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName: color, NSFontAttributeName: font!, NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        } else {
            attr = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName: color, NSFontAttributeName: font!])
        }
        self.setAttributedTitle(attr, for: .normal)
    }
    
    func setBorder(cornerRadius: CGFloat = 0, borderWith: CGFloat = 0, borderColor: UIColor) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWith
        self.layer.borderColor = borderColor.cgColor
    }
    
}


