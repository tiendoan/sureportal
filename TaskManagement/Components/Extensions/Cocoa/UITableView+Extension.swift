//
//  UITableView+Extension.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/7/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

// MARK: Cell
extension UIView {
    public class var nibName: String {
        let name = "\(self)".components(separatedBy: ".").first ?? ""
        return name
    }
    
    public class var nib: UINib? {
        if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
            return UINib(nibName: nibName, bundle: nil)
        } else {
            return nil
        }
    }
}

extension UITableViewCell {
    
    static var className: String {
        return String(describing: self)
    }
    
    static var identifier: String {
        return self.className
    }
    
    //    static var nib: UINib {
    //        return UINib(nibName: self.className, bundle: nil)
    //    }
}

// MARK: Table
extension UITableView {
    
    // Cell
    func registerCustomCell<T: UITableViewCell>(_: T.Type, fromNib: Bool = true) {
        if fromNib {
            self.register( T.nib, forCellReuseIdentifier: T.identifier)
        } else {
            self.register(T.self, forCellReuseIdentifier: T.identifier)
        }
    }
    
    func dequeueCustomCell<T: UITableViewCell>(_: T.Type) -> T {
        let cell = self.dequeueReusableCell(withIdentifier: T.identifier)
        
        return cell as! T
    }
    
}


