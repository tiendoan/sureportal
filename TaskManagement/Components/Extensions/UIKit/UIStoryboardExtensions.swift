//
//  UIStoryboardExtensions.swift
//  SwifterSwift
//
//  Created by Steven on 2/6/17.
//  Copyright © 2017 omaralbeik. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit

// MARK: - Methods
public extension UIStoryboard {
    
    /// SwifterSwift: Get main storyboard for application
    public static var mainStoryboard: UIStoryboard? {
        let bundle = Bundle.main
        guard let name = bundle.object(forInfoDictionaryKey: "UIMainStoryboardFile") as? String else { return nil }
        return UIStoryboard(name: name, bundle: bundle)
    }
    
    /// SwifterSwift: Instantiate a UIViewController using its class name
    ///
    /// - Parameter name: UIViewController type
    /// - Returns: The view controller corresponding to specified class name
    public func instantiateViewController<T: UIViewController>(withClass name: T.Type) -> T {
        return instantiateViewController(withIdentifier: String(describing: name)) as! T
    }
}
  
  public extension UIStoryboard {
    
    public enum Storyboard : String {
        case main = "Main"
        case newMain = "NewMain"
        case ProcedureDigital = "PrecedureDigital"
    }
    
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
      self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    class func storyboard(_ storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
      return UIStoryboard(name: storyboard.rawValue, bundle: bundle)
    }
    
    public func instantiateViewController<T: UIViewController>() -> T where T: StoryboardIdentifiable {
      guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
        fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
      }
      
      return viewController
    }
  }
  
  extension UIViewController : StoryboardIdentifiable { }
  
  // MARK: identifiable
  
  public protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
  }
  
  extension StoryboardIdentifiable where Self: UIViewController {
    public static var storyboardIdentifier: String {
      return String(describing: self)
    }
  }
  
  
#endif
