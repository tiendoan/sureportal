//
//  attachmentCell.swift
//  TaskManagement
//
//  Created by Mirum User on 6/29/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class attachmentCell: UITableViewCell {

    @IBOutlet weak var lbFileName: UILabel!
    @IBOutlet weak var iconFile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
