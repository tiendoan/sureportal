//
//  FileCell.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/13/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class FileCell: UITableViewCell {
    
    @IBOutlet weak var lbName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
