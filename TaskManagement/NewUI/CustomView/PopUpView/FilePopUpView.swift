//
//  FilePopUpView.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/13/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

typealias CompletionClosure = (() -> Void)

protocol FilePopUpViewDelegate: class {
    func chooseFile(file: Files)
}

class FilePopUpView: BasePopUpViewCenter {
    
    var cancelComplete: CompletionClosure?
    weak var delegate: FilePopUpViewDelegate?
    
    lazy var vMargin: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var btnCancel: UIButton = {
        let btn             = UIButton()
        btn.backgroundColor = UIColor.clear
        btn.setAttributed(title: "Thoát",
                          color: UIColor.red,
                          font: UIFont.boldSystemFont(ofSize: 15))
        btn.addTarget(self, action: #selector(btnCancelTapped), for: .touchUpInside)
        return btn
    }()
    
    lazy var lbTitle: UILabel = {
        let lb = UILabel()
        lb.text = "Danh sách tập tin"
        lb.font = UIFont.boldSystemFont(ofSize: 17)
        lb.textColor = UIColor.black
        lb.textAlignment = .center
        return lb
    }()
    
    lazy var tbContent: UITableView = {
        let tb = UITableView()
        tb.rowHeight = UITableViewAutomaticDimension
        tb.separatorStyle = .none
        tb.registerCustomCell(FileCell.self)
        tb.delegate = self
        tb.dataSource = self
        return tb
    }()
    
    var listFile: [Files] = [] {
        didSet {
            tbContent.reloadData()
        }
    }
    
    override func setupView() {
        super.setupView()
        //        height = 100
        vContent.addSubview(vMargin)
        vMargin.addSubview(lbTitle)
        vMargin.addSubview(btnCancel)
        vMargin.addSubview(tbContent)
        setUpLayout()
    }
    
    func setUpLayout() {
        vMargin.anchor(vContent.topAnchor, left: vContent.leftAnchor, bottom: vContent.bottomAnchor, right: vContent.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        lbTitle.anchor(vMargin.topAnchor, left: vMargin.leftAnchor, right: vMargin.rightAnchor, topConstant: 5, leftConstant: 5, rightConstant: 5, widthConstant: 300, heightConstant: 30)
        tbContent.anchor(lbTitle.bottomAnchor, left: lbTitle.leftAnchor, right: lbTitle.rightAnchor, topConstant: 5, leftConstant: 0, rightConstant: 0, heightConstant: 250)
        btnCancel.anchor(tbContent.bottomAnchor, left: tbContent.leftAnchor, bottom: vMargin.bottomAnchor, right: tbContent.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 5, rightConstant: 0)
    }
    
    @objc func btnCancelTapped() {
        self.hidePopUp()
        self.cancelComplete?()
    }
    
    
    
    func showPopUp(title: String, cancel: CompletionClosure? = nil) {
        lbTitle.text = title
        self.cancelComplete = cancel
        if(cancel == nil){
            btnCancel.isHidden = true
        }
        super.showPopUp()
    }
    
    func showPopUp(title: String, cancelTitle : String?, cancel: CompletionClosure? = nil) {
        lbTitle.text = title
        self.cancelComplete = cancel
        if(cancel == nil){
            btnCancel.isHidden = true
        }
        self.btnCancel.setAttributed(title: cancelTitle ?? "",
                                     color: UIColor.red,
                                     font: UIFont.systemFont(ofSize: 15))
        super.showPopUp()
    }
    
    override func showPopUp() {
        super.showPopUp()
    }
}

extension FilePopUpView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCustomCell(FileCell.self)
        cell.lbName.text = listFile[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.chooseFile(file: listFile[indexPath.row])
        self.hidePopUp()
    }
 
}
