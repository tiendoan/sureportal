//
//  HistoryProcedureCell.swift
//  TaskManagement
//
//  Created by Mirum User on 6/6/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class HistoryProcedureCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblAction: UILabel!
    @IBOutlet weak var badge: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        badge.cornerRadius = 13
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
