//
//  CustomUserTaskCell.swift
//  TaskManagement
//
//  Created by Mirum User on 6/19/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class CustomUserTaskCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
