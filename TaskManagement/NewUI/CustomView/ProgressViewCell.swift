//
//  ProgressViewCell.swift
//  TaskManagement
//
//  Created by Mirum User on 6/5/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class ProgressViewCell: UITableViewCell {

    @IBOutlet weak var paddingTitle: NSLayoutConstraint!
    @IBOutlet weak var paddingAva: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lblBadge: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTop: UIView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var heightTop: NSLayoutConstraint!
    var delegate:CustomWorkflowDelegate!
    var rowIndex:Int!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var heightBtnModify: NSLayoutConstraint!
    @IBOutlet weak var paddingAvatar: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblBadge.cornerRadius = 15
        lblBadge.layer.masksToBounds = true
        lblRating.borderColor = UIColor.gray
        lblRating.borderWidth = 1.0
        heightBtnModify.constant = 0;
        btnEdit.isHidden = true

        // Initialization code
    }
    func reloadPadding() {
        paddingTitle.constant = 30
        paddingAvatar.constant = 20
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func selectedAtIndext(_ sender: Any) {
        if ((self.delegate) != nil) {
            self.delegate.replaceWorkflowAtIndex(indexReplace: self.rowIndex, withCustomer: "")
        }
    }
    
}
