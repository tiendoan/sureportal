//
//  DialogAttachmentVC.swift
//  TaskManagement
//
//  Created by Mirum User on 6/29/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class DialogAttachmentVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var arrayFileName:[FileDocument]!
    var procedureId:String!
    var delegate:viewAttachmentFileDelegate!
    @IBOutlet weak var tbView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbView.delegate = self
        self.tbView.dataSource = self
        self.tbView.estimatedRowHeight = 60
        self.tbView.rowHeight = UITableViewAutomaticDimension
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissPopOver(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayFileName != nil ? self.arrayFileName.count: 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DialogAttachCell", for: indexPath) as! DialogAttachCell
        let fileName = self.arrayFileName[indexPath.row]
        cell.lbFileName.text = fileName.name!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      let fileName = self.arrayFileName[indexPath.row]
        if ((self.delegate) != nil) {
            self.dismiss(animated: false, completion: {
                self.delegate.viewAttachmentfile(fileDocument: fileName, procedureId: self.procedureId)
            })
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
