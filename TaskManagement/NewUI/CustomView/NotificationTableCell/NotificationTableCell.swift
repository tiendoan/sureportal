//
//  NotificationTableCell.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Kingfisher
import SwipeCellKit

class NotificationTableCell: SwipeTableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var createLabel: UILabel!
    @IBOutlet weak var statusView: UIButton!
    @IBOutlet weak var backgroundTextView: UIView!
    
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    backgroundTextView.layer.cornerRadius = 10
    backgroundTextView.layer.masksToBounds = true
    statusView.layer.cornerRadius = 5
    statusView.layer.masksToBounds = true
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
  
  func config(_ notification: NotificationItem) {
    if let desc: String = notification.summary {
      titleLabel.text = desc
    }
    if let status: String = notification.itemTypeName {
      statusView.setTitle(status, for: .normal)
    }
    
    if let createBy: String = notification.authorName {
        createLabel.text = "\(NSLocalizedString("Create by:", comment: "")) \(createBy)"
    }
  }
  
}
