//
//  HeaderSupportView.swift
//  TaskManagement
//
//  Created by Mirum User on 8/1/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class HeaderSupportView: UICollectionReusableView {
    @IBOutlet weak var iconIMG: UIImageView!    
    @IBOutlet weak var lbTitle: UILabel!
}
