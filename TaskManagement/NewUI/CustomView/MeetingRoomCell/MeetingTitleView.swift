//
//  MeetingTitleView.swift
//  TaskManagement
//
//  Created by Mirum User on 7/25/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class MeetingTitleView: UITableViewHeaderFooterView {

    @IBOutlet weak var imgPicker: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
