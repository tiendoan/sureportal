//
//  TableSectionHeader.swift
//  TaskManagement
//
//  Created by Mirum User on 8/7/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import FSCalendar

class TableSectionHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var btnCollapse: UIButton!
    @IBOutlet weak var fsCalendar: FSCalendar!
    var delegate: colapseExpandDelegate!
    var isSelectedAdmore:Bool!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBAction func addMoreAction(_ sender: Any) {
        self.isSelectedAdmore = !self.isSelectedAdmore
        if ( self.delegate != nil)
        {
            self.delegate.colapseExpandAction(isColapse: self.isSelectedAdmore)
        }
        
    }

}
