//
//  MeetingHeaderView.swift
//  TaskManagement
//
//  Created by Mirum User on 7/23/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import FSCalendar

class MeetingHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var btnAddmore: UIButton!
    @IBOutlet weak var btnMajor: UIButton!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var fsCalendar: FSCalendar!
    @IBOutlet weak var viewCalendar: UIView!
    var delegate: colapseExpandDelegate!
    var isSelectedAdmore:Bool!
   
    @IBOutlet weak var heightContent: NSLayoutConstraint!
    @IBAction func addMoreAction(_ sender: Any) {
        self.isSelectedAdmore = !self.isSelectedAdmore        
        if ( self.delegate != nil)
        {
            self.delegate.colapseExpandAction(isColapse: self.isSelectedAdmore)
        }
       
    }
}

