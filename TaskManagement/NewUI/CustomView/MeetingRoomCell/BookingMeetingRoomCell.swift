//
//  BookingMeetingRoomCell.swift
//  TaskManagement
//
//  Created by Mirum User on 7/24/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class BookingMeetingRoomCell: UITableViewCell {

    @IBOutlet weak var titleEmpty: UILabel!
    @IBOutlet weak var statusColor: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbRoomMasterName: UILabel!
    @IBOutlet weak var heightBooking: NSLayoutConstraint!
    @IBOutlet weak var heightEmpty: NSLayoutConstraint!
    @IBOutlet weak var lbToDate: UILabel!
    @IBOutlet weak var lbFromDate: UILabel!
    @IBOutlet weak var lblRoomMaster: UILabel!
    @IBOutlet weak var lbRoomNumber: UILabel!
    @IBOutlet weak var lbVehicalNumber: UILabel!
    @IBOutlet weak var heightFooter: NSLayoutConstraint!
    
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var paddingTop: NSLayoutConstraint!
    @IBOutlet weak var lbVehicelType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
