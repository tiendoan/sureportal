//
//  MeetingCommentCell.swift
//  TaskManagement
//
//  Created by Mirum User on 8/1/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class MeetingCommentCell: UICollectionViewCell,UITextFieldDelegate {
    
    weak var delegate:AddMoreDelegate!
    @IBOutlet weak var txtDescription: UITextField!
    
    func updateContainLayout(){
        
        txtDescription.delegate = self
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if ( delegate != nil )
        {
            delegate.updateNotes(notes: textField.text!)
        }
    }
   
}
