//
//  MeetingRoomSupportCell.swift
//  TaskManagement
//
//  Created by Mirum User on 8/1/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class MeetingRoomSupportCell: UICollectionViewCell {

    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    var actionType:ActionMoreType!
    weak var delegate:AddMoreDelegate!
    var objEquipment:MeetingRoomEquipmentObject!
    var objAssistants: MeetingRoomObject!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        // Initialization code
    }
    @IBAction func selectedCheckbox(_ sender: Any) {
        self.btnCheck.isSelected = !self.btnCheck.isSelected
        if ( actionType == ActionMoreType.Assitants ) {
            if ( self.delegate != nil )
            {
                self.delegate.addMoreActionAssistants(objAssistants: objAssistants)
            }
        }
        else
        {
            if ( self.delegate != nil )
            {
                self.delegate.addMoreActionEquipment(objEquipments: objEquipment)
            }
            
        }
        
    }
    
}
