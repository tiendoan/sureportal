//
//  ProcessTaskViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//
import UIKit
import IQKeyboardManagerSwift
import DLRadioButton
import DropDown
import SVProgressHUD
import ObjectMapper

protocol ProcessTaskViewDelegate {
  func closeView()
  func sendProcessWork(status: Bool, message: String?, index: IndexPath?)
}

class ProcessTaskViewController: UIViewController {
  
  // MARK: define variables.
  // --------------------------------

  @IBOutlet weak var extendTimeView: UIView!
  @IBOutlet weak var chooseTaskButton: UIButton!
  
  @IBOutlet weak var inProgressDLRadioButton: DLRadioButton!
  @IBOutlet weak var finishedDLRadioButton: DLRadioButton!
  @IBOutlet weak var extendDLRadioButton: DLRadioButton!
  @IBOutlet weak var reportDLRadioButton: DLRadioButton!
    @IBOutlet weak var reportSwitch: UISwitch!
    
  @IBOutlet weak var topAttachmentsStackViewConstraint: NSLayoutConstraint!
  @IBOutlet weak var topStackViewConstraint: NSLayoutConstraint!
  @IBOutlet weak var fromDayStackView: UIStackView!
  @IBOutlet weak var toDayStackView: UIStackView!
  @IBOutlet weak var opinionTextView: UITextView!
  @IBOutlet weak var statusStackView: UIStackView!
  
  @IBOutlet weak var percentFinishTextView: UITextField!
  @IBOutlet weak var fromDayLabel: UILabel!
  @IBOutlet weak var fromHoursLabel: UILabel!
  @IBOutlet weak var toDayLabel: UILabel!
  @IBOutlet weak var toHoursLabel: UILabel!
  @IBOutlet weak var fromDateLabel: UILabel!
  @IBOutlet weak var toDateLabel: UILabel!
  @IBOutlet weak var jobTitleLabel: UILabel!
  @IBOutlet weak var fullNameLabel: UILabel!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var statusLabel: LVLabel!
  @IBOutlet weak var processTrackingGroupView: UIView!
  @IBOutlet weak var topGroupViewConstraint: NSLayoutConstraint!
  @IBOutlet weak var extDayLabel: UILabel!
  @IBOutlet weak var extHoursLabel: UILabel!
  
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var attachFilesContainerView: UIStackView!
    @IBOutlet weak var attachmentsStackView: UIStackView!
    var attachFiles:[TrackingFileDocument] = [TrackingFileDocument]()
    
  var delegate: ProcessTaskViewDelegate?
  var document: Document?
  var selectedProgressStatus: DLRadioButton?
  var startDate: Date?
  var endDate: Date?
  var extendDate: Date?
  var selectedProcessTracking: ProcessTracking?
  var indexPath: IndexPath?
  
  let OPINION_TXT_TAG: Int = 1
  let PERCENT_FINISH_TXT_TAG: Int = 2
  let DISMISS_ACTION_TAG: Int = 1
  let SEND_ACTION_TAG: Int = 2
  let HIDE_KB_TITLE: String = "Ẩn bàn phím"
  let OPINION_PLACE_HOLDER: String = "Nhập ý kiến"
  let PERCENT_FINISH_PLACE_HOLDER: String = "0"
  let PAGE_TITLE: String = "Xử lý"
  let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
  let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
  let EXTEND_DATE_OPTION_TITLE: String = "Chọn ngày gia hạn"
  let DEFAULT_UPLOAD_TIME_OUT: Int = 10
  let DEFAULT_SELECTED_ROW: Int = 0
  
  let taskDropDown: DropDown = DropDown()
  var processTrackings: [ProcessTracking] = [ProcessTracking]() {
    didSet {
      if !processTrackings.isEmpty {
        taskDropDown.dataSource.removeAll()
        taskDropDown.dataSource = processTrackings.map {
          if let uName: String = $0.name?.trimmingCharacters(in: .whitespacesAndNewlines) {
            return uName
          } else {
            return "Chưa xác định"
          }
        }

        if let seletedProgressID = seletedProgressID, let index = self.processTrackings.index(where:{$0.ID == seletedProgressID}) {
            self.selectedProcessTracking = self.processTrackings[safe: index]
        } else if selectedProcessTracking == nil {
            self.selectedProcessTracking = self.processTrackings[safe: DEFAULT_SELECTED_ROW]
        }
        self.loadReadyData()
      } else {
        self.loadDefaultState()
      }
    }
  }
    var seletedProgressID: String?
  // MARK: init functions.
  // --------------------------------
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.getListTrackingForUser()
    self.setupComponents()
  }
  
  fileprivate func resetView() {
    fromDateLabel.text = Constants.formatDate(startDate)
    toDateLabel.text = Constants.formatDate(endDate)
    if let uStartDate = self.startDate, let eEndDate = self.endDate {
      self.loadStartEndDays(uStartDate, eEndDate)
    }
    opinionTextView.text = OPINION_PLACE_HOLDER
    opinionTextView.textColor = UIColor.lightGray
    percentFinishTextView.text = PERCENT_FINISH_PLACE_HOLDER
    inProgressDLRadioButton.isSelected = false
    reportDLRadioButton.isSelected = false
    finishedDLRadioButton.isSelected = false
    extendDLRadioButton.isSelected = false
    extendTimeView.fadeOut(duration: 1) { (status) in
      self.extendTimeView.isHidden = true
    }
  }
  
  fileprivate func loadReadyData() {
    statusStackView.isHidden = false
    if let processTracking: ProcessTracking = self.selectedProcessTracking {
      self.resetView()
      if let fullName: String = processTracking.assignByName {
        fullNameLabel.text = fullName
      }
      if let avatar: String = processTracking.assignByPicture {
        avatarImageView.setImage(url: (URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar))!)
      }
      if let jobTitle: String = processTracking.assignByJobTitle {
        jobTitleLabel.text = jobTitle
      }
       
        let taskName: String = processTracking.name?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "Chưa xác định"
        chooseTaskButton.setTitle(taskName, for: .normal)
      if let trackingStatus = processTracking.trackingStatus {
        if let nameStatus: String = trackingStatus.name {
          statusLabel.text = nameStatus
        }
        if let colorStatus = trackingStatus.colorCode {
          statusLabel.backgroundColor = UIColor(hexString: colorStatus)!
          statusLabel.textColor = Theme.default.normalWhiteColor
        }
      }
      if let isReport: Bool = processTracking.isReport, isReport {
        reportDLRadioButton.isHidden = false
        finishedDLRadioButton.isHidden = true
      } else {
        reportDLRadioButton.isHidden = true
        finishedDLRadioButton.isHidden = false
      }
      if let progressStringStatus: String = processTracking.status,
        let processIntStatus: Int = Int(progressStringStatus) {
        if processIntStatus == ProcessTrackingStatus.inprogress.rawValue {
          inProgressDLRadioButton.isSelected = true
        } else if processIntStatus == ProcessTrackingStatus.report.rawValue {
          reportDLRadioButton.isSelected = true
        } else if processIntStatus == ProcessTrackingStatus.finished.rawValue {
          finishedDLRadioButton.isSelected = true
        } else if processIntStatus == ProcessTrackingStatus.remain.rawValue {
          extendDLRadioButton.isSelected = true
          extendTimeView.fadeIn(duration: 1, completion: { (status) in
            self.extendTimeView.isHidden = false
          })
        }
      }
      self.startDate = processTracking.fromDate
      self.endDate = processTracking.toDate
      fromDateLabel.text = Constants.formatDate(self.startDate, Constants.FULL_DATETIME_FORMAT)
      toDateLabel.text = Constants.formatDate(self.endDate, Constants.FULL_DATETIME_FORMAT)
      self.loadStartEndDays(self.startDate!, self.endDate!)
      self.view.layoutIfNeeded()
    }
  }
  
  fileprivate func loadDefaultState() {
    processTrackingGroupView.fadeOut(duration: 1, completion: { (status) in
      self.processTrackingGroupView.isHidden = true
      self.topGroupViewConstraint.constant = 0
    })
    if let uCurrentUser: UserProfile = CurrentUser {
      if let fName: String = uCurrentUser.fullName {
        fullNameLabel.text = fName
      }
      if let jTitle: String = uCurrentUser.jobTitle {
        jobTitleLabel.text = jTitle
      }
    }
    startDate = Constants.currentDate
    endDate = startDate?.adding(.day, value: 1)
    fromDateLabel.text = Constants.formatDate(startDate)
    toDateLabel.text = Constants.formatDate(endDate)
    self.loadStartEndDays(startDate!, endDate!)
  }
  
  fileprivate func setupComponents() {
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = HIDE_KB_TITLE
    
    //self.setTitleNavigationBar(PAGE_TITLE)
    self.setupDefaultNavigationBar()
    self.showBack()
    
    var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()
    
    let btnSend = UIButton(type: .custom)
    btnSend.setTitle("Gửi", for: .normal)
    btnSend.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnSend.tintColor = Theme.default.normalWhiteColor
    btnSend.addTarget(self, action: #selector(touchUpInsideAction(_:)), for: .touchUpInside)
    btnSend.backgroundColor = UIColor(hexString: "#17C209")
    btnSend.cornerRadius = Theme.default.normalCornerRadius
    let sendRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnSend)
    rightButtons.append(sendRightButton)
    
    
    /// Attach button
    let btnAttach = UIButton(type: .custom)
    btnAttach.setTitle("Đ.Kèm", for: .normal)
    btnAttach.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnAttach.tintColor = Theme.default.normalWhiteColor
    btnAttach.addTarget(self, action: #selector(attachFilesButton(_:)), for: .touchUpInside)
    btnAttach.backgroundColor = UIColor(hexString: "#17C209")
    btnAttach.cornerRadius = Theme.default.normalCornerRadius
    let attachRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnAttach)
    rightButtons.append(attachRightButton)
    self.addRightButtonNavigationBar(buttons: rightButtons)
    
    chooseTaskButton.setupArrowDropdownInRight()
    chooseTaskButton.showsTouchWhenHighlighted = true
    chooseTaskButton.tintColor = Theme.default.normalWhiteColor
    taskDropDown.anchorView = chooseTaskButton
    taskDropDown.textColor = UIColor.gray
    taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
    taskDropDown.bottomOffset = CGPoint(x: 0, y: chooseTaskButton.bounds.height + 10)
    taskDropDown.isMultipleTouchEnabled = false
    taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
    
    taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        guard let cell = cell as? CustomDropdownCell else { return }
        
        // Setup your custom UI components
        cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
    }
    
    taskDropDown.selectionAction = { [unowned self] (index, item) in
      self.selectedProcessTracking = self.processTrackings[safe: index]
      self.chooseTaskButton.setTitle(item, for: .normal)
      self.loadReadyData()
    }
    
    
    
    opinionTextView.tag = OPINION_TXT_TAG
    opinionTextView.delegate = self
    
    percentFinishTextView.tag = PERCENT_FINISH_TXT_TAG
    percentFinishTextView.delegate = self
    percentFinishTextView.addSubviews(percentFinishTextView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    percentFinishTextView.keyboardType = .numberPad
    
    //reportDLRadioButton.isHidden = true
    setupDLRadioButton(inProgressDLRadioButton, ProcessTrackingStatus.inprogress.rawValue)
    setupDLRadioButton(reportDLRadioButton, ProcessTrackingStatus.report.rawValue)
    setupDLRadioButton(finishedDLRadioButton, ProcessTrackingStatus.finished.rawValue)
    setupDLRadioButton(extendDLRadioButton, ProcessTrackingStatus.remain.rawValue)
    statusStackView.isHidden = true

    attachmentsStackView.isHidden = true
    topAttachmentsStackViewConstraint.constant = 0
    
    extendTimeView.fadeIn(duration: 1, completion: { (status) in
      self.extendTimeView.isHidden = true
    })
    self.loadExtendDay(Constants.currentDate)
  }
  
  fileprivate func setupDLRadioButton(_ sender: DLRadioButton, _ tag: Int = -1) {
    sender.setTitleColor(Theme.default.normalTextColor, for: [])
    sender.iconColor = Theme.default.normalTextColor;
    sender.indicatorColor = Theme.default.normalTextColor;
    sender.backgroundColor = UIColor.clear
    sender.tintColor = Theme.default.normalTextColor
    sender.tag = tag
    sender.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center;
    sender.addTarget(self, action: #selector(selectTaskDropDown(_:)), for: .touchUpInside)
  }
  
  fileprivate func loadStartEndDays(_ startDate: Date, _ endDate: Date) {
    fromDayLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    fromHoursLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    toDayLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    toHoursLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
  }
  
  fileprivate func loadExtendDay(_ extendDate: Date) {
    extDayLabel.text = extendDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    extHoursLabel.text = extendDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
  }
  
    //MARK: - Attach files
    func updateAttchFileView() {
        attachmentsStackView.isHidden = self.attachFiles.isEmpty
        contentView.setNeedsLayout()
        contentView.setNeedsUpdateConstraints()
        contentView.layoutIfNeeded()
    }
    
    func deleteAttachFile(fileView: AttachFileView) {
        if let file = fileView.file, let index = self.attachFiles.index(where: {$0.id == file.id}) {
            self.attachFiles.remove(at: index)
        }
        fileView.removeSubviews()
        self.updateAttchFileView()
    }
    
    func addAttachFilesView(file:TrackingFileDocument) {
        let fileView = AttachFileView.instanceFromNib()
        fileView.delegate = self
        fileView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        fileView.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        fileView.updateFileInfo(f: file)
        self.attachFilesContainerView.addArrangedSubview(fileView)
        self.attachFiles.append(file)
        self.updateAttchFileView()
    }
    
  // MARK: define actions, triggers.
  // --------------------------------
  
  @IBAction func actionStartTimeButton(_ sender: Any) {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func actionEndTimeButton(_ sender: Any) {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func actionExtendTimeButton(_ sender: Any) {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = EXTEND_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func selectTaskDropDown(_ sender: DLRadioButton) {
    self.selectedProgressStatus = sender
    if sender.tag == ProcessTrackingStatus.remain.rawValue {
      extendTimeView.fadeIn(duration: 1, completion: { (status) in
        self.extendTimeView.isHidden = false
      })
    } else {
      extendTimeView.fadeOut(duration: 1, completion: { (status) in
        self.extendTimeView.isHidden = true
      })
    }
  }
  
  @IBAction func showTaskDropDownButton(_ sender: Any) {
    taskDropDown.show()
  }
  
  @IBAction func attachFilesButton(_ sender: Any) {
    debugPrint("ProcessTaskViewController.attachFilesButton")
    let importMenu = UIDocumentMenuViewController(documentTypes: ["public.content","public.item"], in: .import)
    importMenu.delegate = self
    importMenu.modalPresentationStyle = .formSheet
    self.present(importMenu, animated: true, completion: nil)
  }
  
  @IBAction func touchUpInsideAction(_ sender: Any) {
    self.processTrackingDocument()
  }
}

extension ProcessTaskViewController: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.tag == OPINION_TXT_TAG {
        textView.textColor = UIColor.init(hexString: "363636")
      if textView.text == OPINION_PLACE_HOLDER {
        textView.text = nil
      }
      textView.borderColor = Theme.default.normalGreenSelectedColor
    }
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.tag == OPINION_TXT_TAG && textView.text.isEmpty {
      textView.text = OPINION_PLACE_HOLDER
        textView.textColor = UIColor.lightGray
      textView.borderColor = Theme.default.normalBlackBorderColor
    }
  }
  
}

extension ProcessTaskViewController: UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    if textField.tag == PERCENT_FINISH_TXT_TAG {
      if let value: String = textField.text,
         value == PERCENT_FINISH_PLACE_HOLDER {
        textField.text = nil
      }
      textField.addSubviews(textField.addBorder(edges: .bottom, color: Theme.default.normalGreenSelectedColor))
    }
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField.tag == PERCENT_FINISH_TXT_TAG {
      if textField.isEmpty {
        textField.text = PERCENT_FINISH_PLACE_HOLDER
      }
      textField.addSubviews(textField.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    }
  }
  
}

extension ProcessTaskViewController: WWCalendarTimeSelectorProtocol {
  
  func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
    if let title: String = selector.optionTopPanelTitle {
      if title == START_DATE_OPTION_TITLE { // Start day option.
        self.startDate = date
        self.loadStartEndDays(date, Constants.currentDate)
      } else if title == END_DATE_OPTION_TITLE { // End day option.
        self.endDate = date
        self.loadStartEndDays(Constants.currentDate, date)
      } else if title == EXTEND_DATE_OPTION_TITLE {
        self.extendDate = date
        self.loadExtendDay(date)
      }
    }
  }
  
}

extension ProcessTaskViewController {
  
  fileprivate func getListTrackingForUser() {
    if let objectDocument = self.document,
       let documentID = objectDocument.id {
      SyncProvider.getListTrackingForUser(documentID: documentID) { (result, error) in
        if let lsTracking: [ProcessTracking] = result {
          self.processTrackings = lsTracking
        } else {
          debugPrint("ProcessTaskViewController.getListTrackingForUser: empty data.")
          self.processTrackings = []
        }
      }
    }
  }
  
  fileprivate func processTrackingDocument() {
    if let exp: ProcessTracking = ProcessTracking(JSON: [:]) {
      if let selectedPT = self.selectedProcessTracking {
        exp.trackingID = selectedPT.ID
        exp.documentID = selectedPT.docID
      } else {
        exp.trackingID = "bf2229a2-e347-41b6-954e-1d422119d62c"
        exp.documentID = "2d21bcf3-5665-4f2c-8376-c5d82ccf5a08"
      }
      exp.fromDate = self.startDate
      exp.toDate = self.endDate
      if let selectedStatus = selectedProgressStatus {
        if selectedStatus.tag == ProcessTrackingStatus.remain.rawValue {
          exp.newDueDate = self.extendDate
        }
        exp.status = "\(selectedStatus.tag)"
      } else {
        exp.status = "\(ProcessTrackingStatus.inprogress.rawValue)"
      }
      if let description: String = opinionTextView.text, description != OPINION_PLACE_HOLDER {
        exp.description = description
      }
      if let percent: String = percentFinishTextView.text {
        exp.percentFinish = percent.floatValue
      }
        exp.files = attachFiles
      SyncProvider.processTrackingDocument(tracking: exp, done: { (result, error) in
        if let res = result, res {
          self.dismiss(animated: true, completion: {
            self.delegate?.sendProcessWork(status: res, message: error, index: self.indexPath)
          })
        } else {
          self.dismiss(animated: true, completion: {
            self.delegate?.sendProcessWork(status: false, message: error, index: self.indexPath)
          })
        }
        if let docStatus = self.document?.statusDocument, docStatus.isNew {
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_RELOAD), object: nil, userInfo: nil)
        }
      })
    }
  }
  
}

extension ProcessTaskViewController: UIDocumentPickerDelegate {
  
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    let cico = url as URL
    debugPrint("The Url is : \(cico)")
    
    let attachment = Mapper<TrackingFileDocument>().map(JSON:[:])!
    attachment.name = url.lastPathComponent
    attachment.ext = url.pathExtension
    if let fileData = try? Data(contentsOf: URL(fileURLWithPath: url.path)) {
        attachment.size = fileData.count
        self.view.makeToast("Đang quá trình upload.")
        SVProgressHUD.show()
        UploadManager.uploadFile(fileData, type: url.pathExtension, updateHandler: { (percent) in
            print("Percent: \(percent)")
        }) { ( fileID, error) in
            if let id = fileID?.split(separator: ".").first {
                attachment.id = String(id)
                self.addAttachFilesView(file: attachment)
                SVProgressHUD.dismiss()
                self.view.makeToast("Upload thành công, bạn có thể tiếp tục bổ sung thông tin khác.")
            } else {
                self.view.makeToast("Upload thất bại!")
            }
        }
    }
  }
  
  func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
    
  }
  
}

extension ProcessTaskViewController: UIDocumentMenuDelegate {
  
  func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    documentPicker.delegate = self
    self.present(documentPicker, animated: true, completion: nil)
  }
  
  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    
  }
  
}
extension ProcessTaskViewController: AttachFileViewDelegate {
    func didDeleteAttachFile(_ fileView: AttachFileView) {
        self.deleteAttachFile(fileView: fileView)
    }
}
