//
//  TaskAssignViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/14/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import KSTokenView
import IQKeyboardManagerSwift
import Toast_Swift

class TaskAssignOldViewController: UIViewController {
  
  //MARK: - Init variables.
  //----------------------------------------------
  
  @IBOutlet weak var authorNameLabel: UILabel!
  @IBOutlet weak var authorJobTitleLabel: UILabel!
  @IBOutlet weak var createdDateLabel: LVLabel!
  @IBOutlet weak var summaryLabel: UILabel!
  @IBOutlet weak var authorPictureImage: UIImageView!
  
  @IBOutlet weak var currentUserNameLabel: UILabel!
  @IBOutlet weak var currentUserJobTitleLabel: UILabel!
  @IBOutlet weak var currentUserPictureImage: UIImageView!
  @IBOutlet weak var opinionTextView: UITextView!
  @IBOutlet weak var categoriesTokenView: KSTokenView!
  @IBOutlet weak var assignersTokenView: KSTokenView!
  @IBOutlet weak var totalDateLabel: LVLabel!
  @IBOutlet weak var fromDaysLabel: UILabel!
  @IBOutlet weak var fromHoursLabel: UILabel!
  @IBOutlet weak var toDaysLabel: UILabel!
  @IBOutlet weak var toHoursLabel: UILabel!
  @IBOutlet weak var reportNotificationButton: UIButton!
  @IBOutlet weak var emailNotificationButton: UIButton!
  @IBOutlet weak var smsNotificationButton: UIButton!
  @IBOutlet weak var fromToDaysGroupView: UIView!
  @IBOutlet var notificaionButtons: [UIButton]!
  
  var startDate: Date?
  var endDate: Date?
  
  var document: Document? {
    didSet {
      // Reload data in views.
    }
  }
  
  var assignTrackings: [AssignTracking]? {
    didSet {
      // Reload data in views.
    }
  }
  
  var categories: [CategoryTracking] = Dummies.shared.getAllCategories()
  
  var employees: [Assignee] = Dummies.shared.getAllEmployees()
  
  let CATEGORIES_TOKEN_TAG: Int = 1
  let ASSIGNERS_TOKEN_TAG: Int = 2
  let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
  let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
  
  //MARK: - Init functions, localize actions
  //----------------------------------------------
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    addTapEndEdit()
    self.navigationController?.isNavigationBarHidden = true
    self.popoverPresentationController?.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.01)
    createdDateLabel.layer.cornerRadius = 8
    createdDateLabel.layer.masksToBounds = true
    totalDateLabel.layer.cornerRadius = 8
    totalDateLabel.layer.masksToBounds = true
    totalDateLabel.layer.borderColor = Theme.default.normalBlackColor.cgColor
    totalDateLabel.layer.borderWidth = Theme.default.normalBorderWidth
    
    assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    assignersTokenView.promptText = ""
    assignersTokenView.descriptionText = "người xử lý"
    assignersTokenView.delegate = self
    assignersTokenView.tag = ASSIGNERS_TOKEN_TAG
    assignersTokenView.returnKeyType(type: .default)
    
    categoriesTokenView.addSubviews(categoriesTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    categoriesTokenView.promptText = ""
    categoriesTokenView.descriptionText = "loại công việc"
    categoriesTokenView.delegate = self
    categoriesTokenView.tag = CATEGORIES_TOKEN_TAG
    categoriesTokenView.shouldDisplayAlreadyTokenized = true
    categoriesTokenView.returnKeyType(type: .default)
    
    // Binding data.
    self.loadDocumentSection()
    self.loadAssignTrackingSection()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  fileprivate func loadDocumentSection() {
    if let documentObject: Document = document {
      if let authorAvatar: String = documentObject.authorPicture {
        authorPictureImage.setImage(url: (URL(string: Constants.default.domainAddress)?.appendingPathComponent(authorAvatar))!)
      }
      if let authorName: String = documentObject.authorName {
        authorNameLabel.text = "\(authorName)"
      }
      if let authorJobTile: String = documentObject.authorJobTitle {
        authorJobTitleLabel.text = "\(authorJobTile)"
      }
      if let createdDate: Date = documentObject.created {
        createdDateLabel.text = createdDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
      }
      if let summary: String = documentObject.summary {
        summaryLabel.text = "\(summary)"
      }
      if let user: UserProfile = CurrentUser {
        if let currentUserName: String = user.fullName {
          currentUserNameLabel.text = currentUserName
        }
        if let currentJobTitle: String = user.jobTitle {
          currentUserJobTitleLabel.text = currentJobTitle
        }
        if let currentUserAvatar: String = user.avartar {
          currentUserPictureImage.setImage(url: (URL(string: Constants.default.domainAddress)?.appendingPathComponent(currentUserAvatar))!)
        }
      }
    }
  }
  
  fileprivate func loadAssignTrackingSection() {
    if let assigners: [AssignTracking] = self.assignTrackings, !assigners.isEmpty {
      let assigner: AssignTracking = assigners[0]
      if let desc: String = assigner.description {
        opinionTextView.text = desc
      }
      // Calculate total day.
      if let fromDay: Date = assigner.fromDate {
        fromDaysLabel.text = fromDay.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        fromHoursLabel.text = fromDay.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
        self.startDate = fromDay
      } else {
        let currentDate: Date = Constants.currentDate
        fromDaysLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        fromHoursLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
        self.startDate = currentDate
      }
      
      if let toDay: Date = assigner.toDate {
        toDaysLabel.text = toDay.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        toHoursLabel.text = toDay.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        var diffComponents = Calendar.current.dateComponents([.day], from: Constants.currentDate, to: toDay)
        if let fromDay: Date = assigner.fromDate {
          diffComponents = Calendar.current.dateComponents([.day], from: fromDay, to: toDay)
        }
        if let totalDay: Int = diffComponents.day {
          totalDateLabel.text = "\(totalDay)"
        }
        self.endDate = toDay
      } else if let nextDate: Date = Calendar.current.date(byAdding: .day, value: Constants.DEFAULT_NEXT_DATE, to: Constants.currentDate) {
        toDaysLabel.text = nextDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        toHoursLabel.text = nextDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
        totalDateLabel.text = "\(Constants.DEFAULT_NEXT_DATE) ngày"
        self.endDate = nextDate
      } else {
        let currentDate: Date = Constants.currentDate
        toDaysLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        toHoursLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
        self.endDate = currentDate
      }
      
      if let isReport: Bool = assigner.isReport, isReport {
        reportNotificationButton.backgroundColor = Theme.default.normalGreenSelectedColor
        reportNotificationButton.isSelected = true
      }
      if let isSendEmail: Bool = assigner.isSendMail, isSendEmail {
        emailNotificationButton.backgroundColor = Theme.default.normalGreenSelectedColor
        emailNotificationButton.isSelected = true
      }
      if let isSendSMS: Bool = assigner.isSendSMS, isSendSMS {
        smsNotificationButton.backgroundColor = Theme.default.normalGreenSelectedColor
        smsNotificationButton.isSelected = true
      }
    }
  }
  
  fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
    fromDaysLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    fromHoursLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    toDaysLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    toHoursLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    let diffComponents = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
    if let totalDay: Int = diffComponents.day {
      totalDateLabel.text = "\(totalDay) ngày"
    }
  }
  
  
  
  //MARK: - Action Handlers.
  //__________________________________________________________________________________
  //
  
  @IBAction func actionAddPersonButton(_ sender: Any){
    //    let assginVC: AssignPersonViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    //    let nav = UINavigationController(rootViewController: assginVC)
    //    nav.isNavigationBarHidden = true
    //    nav.modalPresentationStyle = .overCurrentContext
    //    self.present(nav, animated: false, completion: nil)
  }
  
  @IBAction func actionStartTimeButton(_ sender: Any){
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func actionEndTimeButton(_ sender: Any){
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func actionAdvanceButton(_ sender: Any) {
    let assginVC: AssignAdvanceViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    let nav = UINavigationController(rootViewController: assginVC)
    nav.isNavigationBarHidden = true
    nav.modalPresentationStyle = .overCurrentContext
    self.present(nav, animated: false, completion: nil)
  }
  
  @IBAction func actionNotificationButtons(_ sender: UIButton){
    sender.isSelected = !sender.isSelected
    sender.backgroundColor = sender.isSelected ? #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1) : .clear
  }
  
  @IBAction func actionCancelButton(_ sender: Any){
    self.dismiss(animated: false, completion: nil)
  }
  
  @IBAction func actionAssginButton(_ sender: UIButton){
    if sender.tag == 1 { // Approve & Assign
      if let document = self.document, let assigns = self.assignTrackings {
        guard let documentID = document.id else {
          print("document id invalid")
          self.dismiss(animated: true, completion: nil)
          return
        }
        var opinonStr = ""
        if let opinon: String = opinionTextView.text {
          opinonStr = opinon
        }
        var workFlowStepIDStr = ""
        if let workFlowStepID: String = document.workFlowStepID {
          workFlowStepIDStr = workFlowStepID
        }
        SyncProvider.approveDocument(documentID: documentID, workFlowStepID: workFlowStepIDStr, contentObj: opinonStr, nextWFStep: nil, done: {
          print("actionAssginButton.approveDocument: \(documentID) success")
        })
        if let assignRequest: AssignTrackingRequest = AssignTrackingRequest(JSON: [:]) {
          assignRequest.documentID = documentID
          assignRequest.assignTrackings = assigns
            SyncProvider.assignTrackingDocument(params: assignRequest, isCreateTask: false, done: {
            print("assign tracking document \(documentID) success")
          })
        }
        
      }
    } else if sender.tag == 2 { // Return
      if let document = self.document, let comment = opinionTextView.text {
        guard let documentID = document.id else {
          print("document id invalid")
          self.dismiss(animated: true, completion: nil)
          return
        }
        SyncProvider.returnDocument(documentID: documentID, comment: comment, done: {
          print("return document \(documentID) success")
        })
      }
    }
    self.view.makeToast("Yều cầu của bạn được gửi thành công, sẽ cập nhật cho bạn sớm nhất !")
    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
      self.dismiss(animated: false, completion: nil)
    }
  }
  
  fileprivate func animateChange(value: CGFloat, objective: NSLayoutConstraint) {
    UIView.animate(withDuration: 0.5) {
      objective.constant = value
    }
  }
}

//MARK: - Extension actions
//----------------------------------------------

extension TaskAssignOldViewController {
  func getAssigners() {
    if let documentObj = self.document, let documentID = documentObj.id {
      SyncProvider.getAssignersByDocument(documentID: documentID, done: { (assigners) in
        self.assignTrackings = assigners ?? []
        self.loadAssignTrackingSection()
      })
    }
  }
}

extension TaskAssignOldViewController: WWCalendarTimeSelectorProtocol {
  
  func showCalendar() {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionSelectionType = .single
    present(selector, animated: true, completion: nil)
  }
  
  func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
    debugPrint("WWCalendarTimeSelectorDone: \(dates.debugDescription)")
  }
  
  func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
    debugPrint("WWCalendarTimeSelectorDone: \(date.debugDescription) - selector: \(String(describing: selector.optionTopPanelTitle))")
    if let title: String = selector.optionTopPanelTitle {
      if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
        self.loadStartEndDays(date, toDate)
      } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
        self.loadStartEndDays(fromDate, date)
      }
    }
  }
}

extension TaskAssignOldViewController: KSTokenViewDelegate {
  
  func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
    debugPrint("tokenViewDidHideSearchResults")
  }
  
  func tokenViewDidShowSearchResults(_ tokenView: KSTokenView) {
    debugPrint("tokenViewDidShowSearchResults")
  }
  
  func tokenViewDidEndEditing(_ tokenView: KSTokenView) {
    debugPrint("tokenViewDidEndEditing: \(tokenView.debugDescription)")
  }
  
  func tokenView(_ tokenView: KSTokenView, didSelectRowAtIndexPath indexPath: IndexPath) {
    debugPrint("didSelectRowAtIndexPath \(indexPath.row)")
  }
  
  func tokenView(_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
    if (string.isEmpty) {
      completion!([])
    }
    var data: Array<String> = []
    if tokenView.tag == CATEGORIES_TOKEN_TAG {
      for item: CategoryTracking in self.categories {
        if let categoryName: String = item.name,
          categoryName.lowercased().range(of: string.lowercased()) != nil {
          data.append(categoryName)
        }
      }
    } else if tokenView.tag == ASSIGNERS_TOKEN_TAG {
      for item: Assignee in self.employees {
        if let employeeName: String = item.text,
           employeeName.lowercased().range(of: string.lowercased()) != nil {
          data.append(employeeName)
        }
      }
    }
    completion!(data as Array<AnyObject>)
  }
  
  func tokenView(_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
    return object as! String
  }
  
  func tokenView(_ tokenView: KSTokenView, didSelectToken token: KSToken) {
    print("selected token: \(token.debugDescription)")
  }
  
  func tokenView(_ tokenView: KSTokenView, shouldAddToken token: KSToken) -> Bool {
    return true
  }
  
  func tokenView(_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
    token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
    return token
  }
  
  func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier = "KSSearchTableCell"
    var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell?
    if (cell == nil) {
      cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier)
    }
    var defaultTitle = "No Title"
    if tokenView.tag == ASSIGNERS_TOKEN_TAG {
      if let title = self.employees[safe: indexPath.row]?.value {
        defaultTitle = title
      }
    } else {
      if let title = self.categories[safe: indexPath.row],
        let catName: String = title.name {
        defaultTitle = catName
      }
    }
    cell!.textLabel!.text = defaultTitle
    cell!.selectionStyle = UITableViewCellSelectionStyle.none
    return cell!
  }
  
  //  func tokenViewDidShowSearchResults(_ tokenView: KSTokenView) {
  //    if tokenView.tag == CATEGORIES_TOKEN_TAG {
  //      animateChange(value: tokenView.searchResultHeight, objective: self.topTotalDayLayoutConstraint)
  //    } else if tokenView.tag == ASSIGNERS_TOKEN_TAG {
  //      animateChange(value: tokenView.searchResultHeight, objective: topCategoriesLayoutConstraint)
  //    }
  //  }
  //
  //  func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
  //    if tokenView.tag == CATEGORIES_TOKEN_TAG {
  //      animateChange(value: 10, objective: self.topTotalDayLayoutConstraint)
  //    } else if tokenView.tag == ASSIGNERS_TOKEN_TAG {
  //      animateChange(value: 10, objective: self.topCategoriesLayoutConstraint)
  //    }
  //  }
}
