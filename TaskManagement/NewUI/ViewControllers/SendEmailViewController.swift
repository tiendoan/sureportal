//
//  SendEmailViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//
import Foundation
import UIKit
import IQKeyboardManagerSwift
import KSTokenView
import NextGrowingTextView

protocol SendEmailViewDelegate {
    func sendEmailCloseView ()
    func sendEmailDoneView ()
}

class SendEmailViewController : UIViewController {
    
    // MARK: define variables.
    
    @IBOutlet weak var receiversTokenView: KSTokenView!
    @IBOutlet weak var subjectTextView: UITextView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subjectHeightConstraint: NSLayoutConstraint!
    
    // --------------------------------
    var searchResults = [UserProfile]() {
        didSet {
            tableView.isHidden = searchResults.count == 0
            tableView.reloadData()
        }
    }
    var selectedUsers = [UserProfile]()
    var document:Document!
    var delegate: SendEmailViewDelegate?
    
    let DISMISS_ACTION_TAG: Int = 1
    let SEND_ACTION_TAG: Int = 2
    let PAGE_TITLE: String = "Gửi email"
    let HIDE_KB_TITLE: String = "Ẩn bàn phím"
    let SUBJECT_PLACEHOLDER: String = "Nhập tiêu đề"
    let BODY_PLACEHOLDER: String = "Nhập nội dung"
    
    // MARK: define actions, triggers.
    // --------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupComponents()
    }
    
    fileprivate func setupComponents() {
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = HIDE_KB_TITLE
        
        self.setTitleNavigationBar(PAGE_TITLE)
        self.setupDefaultNavigationBar()
        self.showBack()
        
        var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()
        
        /// Send button
        let btnApprove = UIButton(type: .custom)
        btnApprove.setTitle("Gửi", for: .normal)
        btnApprove.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
        btnApprove.tintColor = Theme.default.normalWhiteColor
        btnApprove.addTarget(self, action: #selector(actionSendButton(_:)), for: .touchUpInside)
        btnApprove.backgroundColor = UIColor(hexString: "#17C209")
        btnApprove.cornerRadius = Theme.default.normalCornerRadius
        let approveRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnApprove)
        rightButtons.append(approveRightButton)
        
        
        self.addRightButtonNavigationBar(buttons: rightButtons)
        
        //Receiver token view
        receiversTokenView.addSubviews(receiversTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        receiversTokenView.promptText = ""
        //receiversTokenView.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        receiversTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        receiversTokenView.delegate = self
        receiversTokenView.searchResultHeight = 0
        receiversTokenView.returnKeyType(type: .done)
        receiversTokenView.shouldDisplayAlreadyTokenized = true
        receiversTokenView.maxTokenLimit = -1
        receiversTokenView.removesTokensOnEndEditing = false
        receiversTokenView.shouldSortResultsAlphabatically = false
        receiversTokenView.tokenizingCharacters = []
        _ = receiversTokenView.becomeFirstResponder()
        
        
        //
        tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "UserTableViewCell")
        tableView.rowHeight = 50
        
        //
        subjectTextView.text = document.summary
        contentTextView.text = BODY_PLACEHOLDER
        contentTextView.textColor = UIColor.lightGray
    }
    
    // MARK: init, load data functions
    // -----------------------------------
    
    @IBAction func actionSendButton (_ sender: UIButton) {
        guard selectedUsers.count > 0 else {
            self.view.makeToast("Bạn chưa nhập địa chỉ mail muốn gửi.")
            return
        }
        let receivers = selectedUsers.flatMap{$0.email}.joined(separator: ";")
        SyncProvider.sendEmail(documentID: document.id!, subject: subjectTextView.text, body: contentTextView.text, receivers: receivers) { (finish, error) in
            if finish == true {
                self.dismiss(animated: true, completion: {
                    self.delegate?.sendEmailDoneView()
                })
            } else {
                self.view.makeToast("Có lỗi xảy ra, chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !")
            }
        }
        
    }
}

//MARK: - KSTokenViewDelegate
extension SendEmailViewController: KSTokenViewDelegate {
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        receiversTokenView.layoutIfNeeded()
        receiversTokenView.layoutSubviews()
        
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            self.searchResults = []
            return completion!([])
        }
        var data = [UserProfile]()
        let locale = Locale(identifier: "vi_VN")
        let string = string.folding(options: .diacriticInsensitive, locale: locale)
        for item: UserProfile in DataManager.shared.allUsers {
            
            if let value = item.email, value.contains(string, caseSensitive: false) {
                data.append(item)
            }
        }
        
        let myCustomString = string.characters.map { $0 }
        
        data.sort { item1, item2 in
            let str1 = item1.email!
            let str2 = item2.email!
            let index1 = str1.isEmpty ? nil : myCustomString.index(of: str1[str1.startIndex])
            let index2 = str2.isEmpty ? nil : myCustomString.index(of: str2[str2.startIndex])
            
            switch (index1, index2) {
            case (nil, nil):
                return str1.compare(str2, options: .numeric) == .orderedAscending
            case (nil, _):
                return false
            case (_, nil):
                return true
            default:
                if index1 != index2 {
                    return index1! < index2!
                } else {
                    return str1.compare(str2, options: .numeric) == .orderedAscending
                }
            }
        }
        self.searchResults = data
        return completion!([])
    }
    
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if let user = object as? UserProfile,
            let email: String = user.email {
            return email
        } else {
            return ""
        }
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        token.tokenBackgroundColor = Theme.default.normalBlueOceanColor
        return token
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? UserProfile, let index = selectedUsers.index(of:user) {
            selectedUsers.remove(at: index)
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        tableView.isHidden = true
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        // For registering nib files
        let cellIdentifier = "UserTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
        cell!.selectionStyle = UITableViewCellSelectionStyle.none
        cell!.setUser(DataManager.shared.allUsers[indexPath.row])
        
        return cell!
    }
}
//MARK: - UITextViewDelegate
extension SendEmailViewController: UITextViewDelegate {
    
    //    func textViewDidChange(_ textView: UITextView) {
    //        let fixedWidth = textView.frame.size.width
    //        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //
    //        if newSize.height >= 50 && newSize.height < 100 {
    //            subjectHeightConstraint.constant = newSize.height
    //        }
    //    }
    
    func updateHeight(_ text: String) {
        
        let tmpTextView = UITextView(frame: self.subjectTextView.frame)
        tmpTextView.font = self.subjectTextView.font
        tmpTextView.isScrollEnabled = true
        tmpTextView.text = text
        tmpTextView.sizeToFit()
        
        
        let height = tmpTextView.frame.size.height
        if height >= 50 && height <= 100 {
            subjectHeightConstraint.constant = height
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.init(hexString: "363636")
        if textView.text == SUBJECT_PLACEHOLDER || textView.text == BODY_PLACEHOLDER {
            textView.text = ""
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == subjectTextView && textView.text.isEmpty {
            textView.text = SUBJECT_PLACEHOLDER
            textView.textColor = UIColor.lightGray
        } else if textView == contentTextView && textView.text.isEmpty {
            textView.text = BODY_PLACEHOLDER
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == subjectTextView, let viewText = textView.text {
            let newString = (viewText as NSString).replacingCharacters(in: range, with: text)
            self.updateHeight(newString)
        }
        return true
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension SendEmailViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "UserTableViewCell"
        tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
        cell!.selectionStyle = UITableViewCellSelectionStyle.none
        cell!.setUser(searchResults[indexPath.row])
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = searchResults[indexPath.row]
        let assignToken: KSToken = KSToken(title: user.email ?? "")
        
        assignToken.object = user
        if !selectedUsers.contains(user) {
            selectedUsers.append(user)
        }
        
        receiversTokenView.addToken(assignToken)
    }
}
