//
//  SplashViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/30/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

let kSettingDomainAddress = "kSettingDomainAddress"
let kSettingSecurityKey = "kSettingSecurityKey"


class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.perform(#selector(checkGlobalSetting), with: self, afterDelay: 0.2)
    }
    func checkGlobalSetting(){
       
        let domain = UserDefaults.standard.string(forKey: kSettingDomainAddress)
        let key = UserDefaults.standard.string(forKey: kSettingSecurityKey)
        if(domain != nil && domain! != "" && key != nil && key! != ""){
            
            if(CurrentUser != nil ){
                let username = CurrentUser!.loginName
                let password = CurrentUser!.password
                if username != nil && password != nil{
                    login(username!, password!)
                    return
                }
            }
            self.showLogin()
        }
        else{
            UserDefaults.standard.set(Constants.default.domainAddress, forKey: kSettingDomainAddress)
            UserDefaults.standard.synchronize()
            
            SVProgressHUD.show()
            LoginManager.getEncryptKey { (result, message) in
                SVProgressHUD.dismiss()
                if (result != nil) {
                    UserDefaults.standard.set(result!, forKey: kSettingSecurityKey)
                    UserDefaults.standard.synchronize()
                    self.showLogin()
                } else {
                    self.showAlert(title: nil, message: message != "" ? message : "Không thể kết nối đến máy chủ!")
                }
            }
        }
    }
    
    func login(_ username:String, _ password:String){
        SVProgressHUD.show()
        LoginManager.login(username: username, password: password) { (result, message) in
            self.showHome()
        }
    }
    func showLogin(){
        SVProgressHUD.dismiss()
        let vc = self.storyboard?.instantiateViewController(withClass: LoginViewController.self)
        vc?.modalTransitionStyle = .crossDissolve
        self.present(vc!, animated: true, completion: nil)
    }
    func showHome(){
        SVProgressHUD.dismiss()
        let vc = self.storyboard?.instantiateViewController(withClass: RootViewController.self)
        vc?.modalTransitionStyle = .crossDissolve
        self.present(vc!, animated: true, completion: nil)
    }
    func showGlobal(){
        let vc = self.storyboard?.instantiateViewController(withClass: GlobalSettingViewController.self)
        vc?.modalTransitionStyle = .crossDissolve
        self.present(vc!, animated: true, completion: nil)
    }
}
