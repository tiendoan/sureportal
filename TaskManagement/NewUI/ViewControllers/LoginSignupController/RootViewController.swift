//
//  RootViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/5/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import KYDrawerController

class RootViewController: KYDrawerController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenEdgePanGestureEnabled = false
        if(UIDevice.is3InchesIPHONE()||UIDevice.is4InchesIPHONE()){
            self.drawerWidth = 250
        }
        else{
            self.drawerWidth = 300
        }
        updateNotificationBadge()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func toggleMenu(forceClose: Bool = false) {
        if self.drawerState == .closed && !forceClose{
            self.setDrawerState(.opened, animated: true)
        }
        else{
            self.setDrawerState(.closed, animated: true)
        }
    }
    
    func updateNotificationBadge() {
        SyncProvider.getListNotification(done: { (result) in
            if let rs: [NotificationItem] = result {
                UIApplication.shared.applicationIconBadgeNumber = rs.count
            } else {
                 UIApplication.shared.applicationIconBadgeNumber = 0
            }
            
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
        })
    }
}

extension UIViewController {
    func mainNew() -> RootViewController? {
        var viewController: UIViewController? = self
        while viewController != nil {
            if viewController is KYDrawerController {
                return viewController as? RootViewController
            }
            viewController = viewController?.parent
        }
        return nil;
    }
    
    func showBackNew(){
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(closePageNew(_:)))
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    //Action func
    func closePageNew(_ sender: Any) {
        if let navigationController = navigationController, navigationController.viewControllers.first != self {
            navigationController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}
