//
//  ForgetPassVerifyViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Alamofire

class ForgetPassVerifyViewController: UIViewController {

    @IBOutlet fileprivate weak var topLayout:NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var btDone:UIButton!
    @IBOutlet fileprivate weak var btCancel:UIButton!
    
    @IBOutlet fileprivate weak var btCode1:UIButton!
    @IBOutlet fileprivate weak var btCode2:UIButton!
    @IBOutlet fileprivate weak var btCode3:UIButton!
    @IBOutlet fileprivate weak var btCode4:UIButton!
    @IBOutlet fileprivate weak var tfCode:UITextField!
    
    @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
    
    @IBOutlet fileprivate weak var btCountDown:UIButton!
    
    fileprivate var seconds = 60
    fileprivate var timer = Timer()
    
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNotification()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tfCode.text = ""
        btCode1.setTitle("", for: .normal)
        btCode2.setTitle("", for: .normal)
        btCode3.setTitle("", for: .normal)
        btCode4.setTitle("", for: .normal)
        tfCode.becomeFirstResponder()
        
        seconds = 60
        btCountDown.isEnabled = false
        btCountDown.setTitle("\(seconds)s", for: .normal)
        countDownTimer()
        self.checkBTDone()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: nil, queue: nil) { (notification) in
            if(self.tfCode.isFirstResponder){
                self.checkCode()
            }
            
            self.checkBTDone()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            
            if(UIDevice.is3InchesIPHONE() || UIDevice.is4InchesIPHONE()){
                self.topLayout.constant = -100
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.topLayout.constant = 30
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func checkBTDone(){
        if tfCode.text?.length == 4{
            btDone.isEnabled = true
            btDone.backgroundColor = UIColor(red: 30, green: 106, blue: 194)
        }
        else{
            btDone.isEnabled = false
            btDone.backgroundColor = UIColor(red: 186, green: 186, blue: 186)
        }
    }
    func showAlert(_ message:String){
        loading.stopAnimating()
        self.view.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "THÔNG BÁO", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Đồng Ý", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
extension ForgetPassVerifyViewController{
    @IBAction func pressesDone(_ sender: UIButton){
        self.view.endEditing(true)
        
        loading.startAnimating()
        self.view.isUserInteractionEnabled = false
        LoginManager.verifyCode(code: tfCode.text!) { (success, message) in
            self.loading.stopAnimating()
            self.view.isUserInteractionEnabled = true
            if(success){
                self.stopCountDown()
                let vc = self.storyboard?.instantiateViewController(withClass: ForgetPassConfirmViewController.self)
                vc?.code = self.tfCode.text!
                self.present(vc!, animated: true, completion: nil)
            }
            else{
                self.showAlert(message)
            }
        }
        
        
        
    }
    @IBAction func pressesCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pressesCode(_ sender: UIButton){
        tfCode.becomeFirstResponder()
    }
    @IBAction func pressesBTCountDown(_ sender:UIButton){
        seconds = 60
        btCountDown.isEnabled = false
        btCountDown.setTitle("\(seconds)s", for: .normal)
        countDownTimer()
        LoginManager.sendCode(emailOrMobileNumber: email) { (success, message) in
            if(success){}
            else{
                self.stopCountDown()
                self.showResentCode()
            }
        }
    }
}
extension ForgetPassVerifyViewController{
    func countDownTimer(){
        seconds = 60
        btCountDown.setTitle("\(seconds)s", for: .normal)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    func updateTimer() {
        seconds -= 1
        btCountDown.setTitle("\(seconds)s", for: .normal)
        if(seconds <= 0){
            stopCountDown()
            showResentCode()
        }
    }
    func stopCountDown(){
        seconds = 60
        timer.invalidate()
        self.view.endEditing(true)
    }
    func showResentCode(){
        btCountDown.isEnabled = true
        btCountDown.setTitle("Gửi Lại", for: .normal)
    }
}
extension ForgetPassVerifyViewController{
    
    func checkCode(){
        if self.tfCode.text != ""{
            let string = self.tfCode.text
            if string!.characters.count > 4 {
                let code = string!.substring(from: string!.characters.index(string!.startIndex, offsetBy: 0)).substring(to: string!.characters.index(string!.startIndex, offsetBy: 4))
                self.tfCode.text = code
            }
        }
        
        var code1 = ""
        var code2 = ""
        var code3 = ""
        var code4 = ""
        if self.tfCode.text != ""{
            let code = self.tfCode.text
            
            if((code?.characters.count)! >= 1){
                code1 = code!.substring(from: code!.characters.index(code!.startIndex, offsetBy: 0)).substring(to: code!.characters.index(code!.startIndex, offsetBy: 1))
            }
            if((code?.characters.count)! >= 2){
                code2 = code!.substring(from: code!.characters.index(code!.startIndex, offsetBy: 1)).substring(to: code!.characters.index(code!.startIndex, offsetBy: 1))
            }
            if((code?.characters.count)! >= 3){
                code3 = code!.substring(from: code!.characters.index(code!.startIndex, offsetBy: 2)).substring(to: code!.characters.index(code!.startIndex, offsetBy: 1))
            }
            if((code?.characters.count)! >= 4){
                code4 = code!.substring(from: code!.characters.index(code!.startIndex, offsetBy: 3)).substring(to: code!.characters.index(code!.startIndex, offsetBy: 1))
            }
            
        }
        
        self.btCode1.setTitle(code1, for: .normal)
        self.btCode2.setTitle(code2, for: .normal)
        self.btCode3.setTitle(code3, for: .normal)
        self.btCode4.setTitle(code4, for: .normal)
        
    }
}
