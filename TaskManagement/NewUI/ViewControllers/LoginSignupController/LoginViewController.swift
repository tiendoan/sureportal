//
//  LoginViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/3/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import CryptoSwift
import Alamofire
import Firebase

class LoginViewController: UIViewController {
  
  @IBOutlet fileprivate weak var topLayout:NSLayoutConstraint!
  
  @IBOutlet private weak var imageCell:UIImageView!
  @IBOutlet private weak var viewLogin:UIView!
  
  @IBOutlet fileprivate weak var tfUsername:UITextField!
  @IBOutlet fileprivate weak var tfPassword:UITextField!
  
  @IBOutlet fileprivate weak var btLogin:UIButton!
  @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
  
  @IBOutlet fileprivate weak var lbVersion:UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.edgesForExtendedLayout = .init(rawValue: 0)
    self.view.layoutIfNeeded()
    lbVersion.text = "Sure portal @ 2017 build \(Constants.getVersionName()) (\(Constants.getVersionBuild()))"
    
    //tfUsername.text = "hps2013\\tonggiamdoc"
    //tfPassword.text = "Bio@#2017"
    viewLogin.isHidden = false
    //decryptAES()
    
    tfUsername.returnKeyType = .next
    tfPassword.returnKeyType = .done
    registerNotification()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //tfUsername.text = ""
    //tfPassword.text = ""
    if let logoURL = URL(string: Constants.default.domainAddress + Constants.logoLink) {
        self.imageCell.kf.setImage(with: logoURL, placeholder: #imageLiteral(resourceName: "logo_pvgas"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    self.checkBTLogin()
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle{
    return .lightContent
  }
  
  func checkBTLogin(){
    if(tfUsername.text != "" && tfPassword.text != ""){
      btLogin.isEnabled = true
      btLogin.backgroundColor = UIColor(red: 30, green: 106, blue: 194)
    }
    else{
      btLogin.isEnabled = false
      btLogin.backgroundColor = UIColor(red: 186, green: 186, blue: 186)
    }
  }
  
  func registerNotification(){
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
      
      if(UIDevice.is3InchesIPHONE() || UIDevice.is4InchesIPHONE()){
        self.topLayout.constant = -130
      } else {
        self.topLayout.constant = 10
        }
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
      self.topLayout.constant = 50
      UIView.animate(withDuration: 0.3, animations: {
        self.view.layoutIfNeeded()
      })
    }
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: nil, queue: nil) { (notification) in
      self.checkBTLogin()
    }
  }
  
  func showAlert(_ message:String){
    loading.stopAnimating()
    self.view.isUserInteractionEnabled = true
    let alert = UIAlertController(title: LocalizationString.ALERT_TITLE, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: LocalizationString.AGREE_STRING, style: .default, handler: nil)
    alert.addAction(action)
    self.present(alert, animated: true, completion: nil)
  }
  
  func loginSuccess(){
    UIApplication.shared.applicationIconBadgeNumber = 0
    loading.stopAnimating()
    self.view.isUserInteractionEnabled = true
    let vc = self.storyboard?.instantiateViewController(withClass: RootViewController.self)
    self.present(vc!, animated: true, completion: nil)

  }
  
  func decryptAES() {
    let encrypted: String = ""
    let key: String = "SureP0rt@l!@2017"
    let iv: String  = "1234567890123456"
    do {
      if let encryptedBytes = encrypted.stringToBytes() {
        let aes = try AES(key: Array(key.utf8), iv: Array(iv.utf8), blockMode: .CBC, padding: .pkcs7)
        let plainText = try encryptedBytes.decrypt(cipher: aes)
        debugPrint("AES.decrypt.hex: \(plainText.description)")
        if let byteString = String(bytes: plainText, encoding: .utf8) {
          debugPrint("AES.decrypt.hex.utf8: \(byteString)")
        }
      }
    } catch {
      debugPrint("AES.decrypt: error \(error)")
    }
  }
  
}

extension LoginViewController {
  
  @IBAction func pressesLogin (_ sender: UIButton) {
    self.view.endEditing(true)
    loading.startAnimating()
    self.view.isUserInteractionEnabled = false
    self.login()
    
  }
  
  @IBAction func pressesForget (_ sender: UIButton) {
    self.view.endEditing(true)
    let vc = self.storyboard?.instantiateViewController(withClass: ForgetPassViewController.self)
    self.present(vc!, animated: true, completion: nil)
  }
  
  @IBAction func pressesSetting(_ sender: UIButton) {
    self.view.endEditing(true)
    let vc = self.storyboard?.instantiateViewController(withClass: SettingURLViewController.self)
    self.present(vc!, animated: true, completion: nil)
  }
  
  func login() {
    LoginManager.login(username: tfUsername.text!, password: tfPassword.text!) { (result, message) in
      if (result != nil) {
        self.saveDeviceInformations()
        result?.password = self.tfPassword.text!
        result?.save()
        self.loginSuccess()
      } else {
        self.showAlert(message != "" ? message : LocalizationString.CONNECTION_SERVER_ERROR)
      }
    }
  }
  
  func saveDeviceInformations() {
    var deviceToken: String = ""
    var deviceUUID: String = ""
    let deviceName = UIDevice.current.modelName
    let osName = "iOS " + UIDevice.current.systemVersion//UIDevice.current.systemName
    let osVersion = Bundle.versionNumber + "(\(Bundle.buildNumber))"

    if let token = Messaging.messaging().fcmToken {
      deviceToken = token
    } else {
      deviceToken = "unknown"
    }
    if let udidDevice: UUID = UIDevice.current.identifierForVendor {
      deviceUUID = udidDevice.uuidString
    } else {
      deviceUUID = "XXX-XXX"
    }
    LoginManager.saveDeviceInformation(
      deviceToken: deviceToken,
      deviceName: deviceName,
      imei: deviceUUID,
      osName: osName,
      osVersion: osVersion
    ) { (result, msg) in
      debugPrint("saveDeviceInformations.status: \(result)")
    }
  }
}

//MARK: - KSTokenViewDelegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfUsername {
            tfPassword.becomeFirstResponder()
        } else {
            pressesLogin(btLogin)
        }
        return true
    }
    
}
