//
//  ForgetPassConfirmViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Alamofire

class ForgetPassConfirmViewController: UIViewController {

    @IBOutlet fileprivate weak var topLayout:NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var tfPassword:UITextField!
    @IBOutlet fileprivate weak var tfConfirmPassword:UITextField!
    @IBOutlet fileprivate weak var btDone:UIButton!
    @IBOutlet fileprivate weak var btCancel:UIButton!
    @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
    
    var code = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNotification()
        self.checkBTDone()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: nil, queue: nil) { (notification) in

            self.checkBTDone()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            
            if(UIDevice.is3InchesIPHONE() || UIDevice.is4InchesIPHONE()){
                self.topLayout.constant = -100
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.topLayout.constant = 30
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func checkBTDone(){
        if tfPassword.text != "" && tfConfirmPassword.text != ""{
            btDone.isEnabled = true
            btDone.backgroundColor = UIColor(red: 30, green: 106, blue: 194)
        }
        else{
            btDone.isEnabled = false
            btDone.backgroundColor = UIColor(red: 186, green: 186, blue: 186)
        }
    }
    
}
extension ForgetPassConfirmViewController{
    @IBAction func pressesDone(_ sender: UIButton){
        self.view.endEditing(true)
        if(tfPassword.text != tfConfirmPassword.text){
            showAlert("Xác nhận mật khẩu không chính xác")
            return
        }
        resetPass()
    }
    @IBAction func pressesCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    func resetPass(){
        loading.startAnimating()
        self.view.isUserInteractionEnabled = false
        LoginManager.resetPass(password: tfPassword.text!, code: code) { (success, message) in
            if success {
                self.resetPasswordSuccess()
            }
            else{
                self.showAlert(message != "" ? message : "Không thể kết nối đến máy chủ!")
            }
        }
    }
    
    func resetPasswordSuccess(){
        loading.stopAnimating()
        self.view.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "THÔNG BÁO", message: "Cài đặt mật khẩu thành công!", preferredStyle: .alert)
        alert.addAction(title: "Đồng Ý", style: .default, isEnabled: true) { (action) in
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ message:String){
        loading.stopAnimating()
        self.view.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "THÔNG BÁO", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Đồng Ý", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
