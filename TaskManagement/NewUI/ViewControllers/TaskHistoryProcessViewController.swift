//
//  TaskHistoryProcessViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/22/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class TaskHistoryProcessViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet fileprivate weak var tableView:UITableView!
    @IBOutlet fileprivate weak var btExit:UIButton!
    
    var item: ProgressTracking!
    var historyItems = [ProgressHistory]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle =  .overCurrentContext
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        
        btExit.setImage(#imageLiteral(resourceName: "ic_clear").filled(withColor: UIColor.lightGray), for: .normal)
    
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(withClass: DetailPersonTableViewCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 110
        
        getHistoryDocuments()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getHistoryDocuments() {
        guard let deptID = item.deptID, let docID = item.id else {
            return
        }
        SyncProvider.getListTrackingWorkflowDocument(deptID: deptID, trackingDocumentID: docID) { [weak self] (results) in
            self?.historyItems = results ?? []
            self?.tableView.reloadData()
            
        }
    }
    
    @IBAction func pressesBTExit(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyItems.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: DetailPersonTableViewCell.self, for: indexPath)
        let item = historyItems[indexPath.row]
        cell.nameLabel.text = item.authorFullName ?? ""
        cell.titleLabe.text = item.authorJobTitle ?? ""
        cell.contentLabel.text = item.summary
        
        cell.wrapperDepartmentView.isHidden = cell.departmentLabel.text!.isEmpty
        if let avatar = item.authorPicture, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar){
            cell.avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
//        return cell
        return cell
    }
}
