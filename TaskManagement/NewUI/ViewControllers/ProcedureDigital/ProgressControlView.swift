//
//  ProgressControlView.swift
//  TaskManagement
//
//  Created by Mirum User on 6/5/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

class ProgressControlView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    var IDProce: String!
    var listWorkTask:[ProcedureWorkflowTasks] = []
    var workflowStep:[ProcedureWorkflowSteps] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.dataSource = self
        self.tblView.delegate = self
        let nib = UINib.init(nibName: "ProgressViewCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "ProgressViewCell")
        self.tblView.separatorStyle = .none
        self.tblView.estimatedRowHeight = 88.0
        self.tblView.rowHeight = UITableViewAutomaticDimension

        if self.IDProce != nil
         {
            SyncProvider.getProcedureWorkflow(Id: self.IDProce, done: { (result) in
                if ( result != nil )
                {
                    self.workflowStep = ((result as! ProcedureDigitalResponse).data?.workFlowStep)!
                    self.tblView.reloadData()
                }
            })
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.workflowStep != nil ? self.workflowStep.count : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.workflowStep != nil ? (self.workflowStep[section].workTask?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableViewAutomaticDimension
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProgressViewCell", for: indexPath) as! ProgressViewCell
        let workTask:ProcedureWorkflowTasks = self.workflowStep[indexPath.section].workTask![indexPath.row]

        if indexPath.row == 0 {
            let workStep:ProcedureWorkflowSteps = self.workflowStep[indexPath.section]
            cell.lblContent.text = workStep.Name!
            cell.lblBadge.text =  String(format: "%d", indexPath.section + 1)
            cell.lblRating.isHidden = false
        }
        else
        {
            cell.lblRating.isHidden = true
            cell.heightTop.constant = 0
        }
        cell.lbTitle.text = workTask.UserName
        
        return cell
    }
    
}

