//
//  AdvanceSearchVC.swift
//  TaskManagement
//
//  Created by Mirum User on 7/11/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown
import IQKeyboardManagerSwift
import Toast_Swift
import SVProgressHUD
import KSTokenView

class AdvanceSearchVC: UIViewController {

    var delegate:AdvanceSearchDelegate!
    @IBOutlet weak var keyWord: UITextField!
    @IBOutlet weak var btnContent: UIButton!
    @IBOutlet weak var commonView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnMajor: UIButton!
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var taskView: UIStackView!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var txFromDate: UITextField!
    @IBOutlet weak var txToDate: UITextField!
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var assignersTokenView: KSTokenView!
    let ASSIGNEES_TEXT_VIEW_PLACE_HOLDER: String = "người xử lý"
    let CATEGORIES_TOKEN_TAG: Int = 1
    let ASSIGNERS_TOKEN_TAG: Int = 2
    var employees: [Assignee] = Dummies.shared.getAllEmployees()
    var flatEmployees: [Assignee] = [Assignee]()
    var processors: [Assignee] = [Assignee]()
    var collaborators: [Assignee] = [Assignee]()
    var acknownledges: [Assignee] = [Assignee]()
    var addTokenFromSearchInput = true
    var arrayRootList:[UserProfile]!
    let DATE_FORMAT = "dd-MM-yyyy"
    //Date
    var fromDate = Date(month: 1, day: 1)!
    var toDate = Date(month: 12, day: 31)!
    
    var arrayParam:[String:Any] = [:]
    
    let taskContentDropDown: DropDown = DropDown()
    var arrayContent: [String] = [] {
        didSet {
            if !arrayContent.isEmpty {
                taskContentDropDown.dataSource.removeAll()
                taskContentDropDown.dataSource = arrayContent.map {
                    if let uName: String = ($0).trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    
    let taskStatusDropDown: DropDown = DropDown()
    var arrayStatus: [String] = [] {
        didSet {
            if !arrayStatus.isEmpty {
                taskStatusDropDown.dataSource.removeAll()
                taskStatusDropDown.dataSource = arrayStatus.map {
                    if let uName: String = ($0).trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    let taskDropDown: DropDown = DropDown()
    var arrayProcedureCategory: [ProcedureDataObject] = [] {
        didSet {
            if !arrayProcedureCategory.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayProcedureCategory.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    let taskDropDownProcess: DropDown = DropDown()
    
    var arrayProcedureCategoryData: [ProcedureDataObject] = []  {
        didSet {
            if !arrayProcedureCategoryData.isEmpty {
                taskDropDownProcess.dataSource.removeAll()
                taskDropDownProcess.dataSource = arrayProcedureCategoryData.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /* ------ load UI For Major */
        taskDropDown.anchorView = self.btnMajor
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.btnMajor.setTitle(item, for: .normal)
            self.loadProcessData(categoryId: self.arrayProcedureCategory[index].ID!)
            self.arrayParam["ProcedureCategory"] = self.arrayProcedureCategory[index].ID!
        }
        loadMajorData()
        
        
        /* ------ load UI For Content */
        self.arrayContent = ["Tiêu đề và Nội dung","Tiêu đề", "Nội dung"]
        taskContentDropDown.anchorView = self.btnContent
        taskContentDropDown.textColor = UIColor.gray
        taskContentDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskContentDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskContentDropDown.isMultipleTouchEnabled = false
        taskContentDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskContentDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskContentDropDown.selectionAction = { [unowned self] (index, item) in
            self.arrayParam["KeywordFilter"] = index
            self.btnContent.setTitle(item, for: .normal)
        }
        self.btnContent.setTitle(self.arrayContent[0], for: .normal)

        
        
        /* ------ load UI For Status */
        self.arrayStatus = ["Tất cả","Đang soạn thảo", "Cần phê duyệt","Đang xử lý","Đã ban hành", "Từ chối"]
        taskStatusDropDown.anchorView = self.btnStatus
        taskStatusDropDown.textColor = UIColor.gray
        taskStatusDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskStatusDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskStatusDropDown.isMultipleTouchEnabled = false
        taskStatusDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskStatusDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskStatusDropDown.selectionAction = { [unowned self] (index, item) in
            self.btnContent.setTitle(item, for: .normal)
            self.arrayParam["Status"] = index
        }
        self.btnStatus.setTitle(self.arrayStatus[0], for: .normal)

        
        
        /* ------ load UI For Content */
        taskDropDownProcess.anchorView = self.btnType
        taskDropDownProcess.textColor = UIColor.gray
        taskDropDownProcess.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDownProcess.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskDropDownProcess.isMultipleTouchEnabled = false
        taskDropDownProcess.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDownProcess.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        
        taskDropDownProcess.selectionAction = { [unowned self] (index, item) in
            self.arrayParam["ProcedureType"] = self.arrayProcedureCategoryData[index].ID!
            self.btnType.setTitle(item, for: .normal)
            
        }
        initUI()
    }
    func initUI(){
        
        assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        assignersTokenView.promptText = ""
        assignersTokenView.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        assignersTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        assignersTokenView.delegate = self
        assignersTokenView.searchResultHeight = 120
        assignersTokenView.tag = ASSIGNERS_TOKEN_TAG
        assignersTokenView.returnKeyType(type: .done)
        assignersTokenView.shouldDisplayAlreadyTokenized = true
        assignersTokenView.maxTokenLimit = -1
        assignersTokenView.shouldSortResultsAlphabatically = false
        assignersTokenView.removesTokensOnEndEditing = false
        assignersTokenView.tokenizingCharacters = []
        _ = assignersTokenView.becomeFirstResponder()
        
//        txFromDate.inputAccessoryView = accessory
//        txToDate.inputAccessoryView = accessory
        txFromDate.delegate = self
        txToDate.delegate = self
        self.getAllEmployees()
    }
    func updateDate() {
        txFromDate.text = fromDate.stringFromFormat(DATE_FORMAT)
        txToDate.text = toDate.stringFromFormat(DATE_FORMAT)
    }
    func loadMajorData()  {
        SyncProvider.getProcedureCategories(done: { (result) in
            let resultObject:ProcedureDigitalObject = (result as! ProcedureDigitalObject)
            if (resultObject.data?.count)! > 0
            {
                self.arrayProcedureCategory = (result?.data)!
                if self.arrayProcedureCategory.count > 0
                {
                    self.btnMajor.setTitle((self.arrayProcedureCategory[0].Name)!, for: .normal)
                     self.arrayParam["ProcedureCategory"] = self.arrayProcedureCategory[0].ID!
                    self.loadProcessData(categoryId: self.arrayProcedureCategory[0].ID!)
                }
            }
        })
    }
    func loadProcessData(categoryId: String) {
        
        SyncProvider.getProcedureTypesByProcedureCategoryID(Id: categoryId, done: { (result) in
            let resultObject:ProcedureDigitalObject = (result as! ProcedureDigitalObject)
            if (resultObject.data?.count)! > 0
            {
                self.arrayProcedureCategoryData = (result?.data)!
                if self.arrayProcedureCategoryData.count > 0
                {
                    self.btnType.setTitle((self.arrayProcedureCategoryData[0].Name)!, for: .normal)
                    self.arrayParam["ProcedureType"] = self.arrayProcedureCategoryData[0].ID!
                }
            }
        })
    }
    @IBAction func chooseStatusAction(_ sender: Any) {
        taskStatusDropDown.show()
    }
    @IBAction func chooseTypeAction(_ sender: Any) {
        taskDropDownProcess.show()
    }
    
    @IBAction func chooseMajorAction(_ sender: Any) {
        taskDropDown.show()
    }
    @IBAction func chooseContentAction(_ sender: Any) {
        taskContentDropDown.show()
        
    }
    @IBAction func chooseToDateAction(_ sender: Any) {
        
        
    }
    @IBAction func chooseFromDateAction(_ sender: Any) {
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        
    }
    @IBAction func searchAction(_ sender: Any) {
        
        var arrayUserAppen:[String] = []
        let arrayUser:[String] = self.processors.map({ $0.text! })
        for object in arrayUser
        {
            let objectUser = self.arrayRootList.filter({ $0.fullName! == object })
            if objectUser != nil
            {
                for item in objectUser
                {
                    arrayUserAppen.append(item.id!)
                }
            }
        }
        self.arrayParam["SignerIDs"] = arrayUserAppen
        self.arrayParam["Keyword"] = keyWord.text
        self.arrayParam["Page"] = 1
        self.dismiss(animated: false, completion: {
            
            if self.delegate != nil
            {
                self.delegate.getParamAdvanceSearch(parm: self.arrayParam, isAdvance: true)
            }
            
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? Assignee, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    func getAllEmployees () {
        
        self.employees = DataManager.shared.employees
        self.flatEmployees = DataManager.shared.flatEmployees
        self.arrayRootList = DataManager.shared.allUsers
        
//            SVProgressHUD.show()
//            SyncProvider.getAllUsersInDepartments { (assigners) in
//                if let lAssigners: [Assignee] = assigners {
//                    self.employees = lAssigners
//                    self.flatEmployees = Constants.default.recursiveFlatmap(list: lAssigners, true).sorted{ ($0.value ?? "") < ($1.value ?? "") }
//                } else {
//                    self.employees = Dummies.shared.getAllEmployees()
//                    self.flatEmployees = Dummies.shared.getAllEmployees()
//                }
//                SVProgressHUD.dismiss()
//            }
//            SyncProvider.getListUserTask(done: {(result) in
//                self.arrayRootList = result?.data
//            })
        
        }
}
extension AdvanceSearchVC: KSTokenViewDelegate {
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            return completion!([])
        }
        var data: Array<AnyObject> = []
        var valueResults = [Assignee]()
        var textResults = [Assignee]()
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            let locale = Locale(identifier: "vi_VN")
            for item: Assignee in self.flatEmployees {
                guard item.isDept == false else {
                    continue
                }
                if let text = item.value, let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    valueResults.append(item)
                }else if let text = item.text?.folding(options: .diacriticInsensitive, locale: locale), let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    textResults.append(item)
                }
                data = valueResults.sorted{ ($0.value ?? "") < ($1.value ?? "") }
                data.append(contentsOf: textResults)
                //data = results
            }
        }
        return completion!(data)
    }
    
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            if let assignee = object as? Assignee,
                let name: String = assignee.text {
                return name
            } else {
                return "Chưa xác định"
            }
        }
        return object as! String
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            if let tokenObject: Assignee = token.object as? Assignee {
                token.tokenBackgroundColor = tokenObject.getBackgroundColorToTokenView()
            } else {
                token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
            }
        } else if tokenView.tag == CATEGORIES_TOKEN_TAG {
            token.tokenBackgroundColor = Theme.default.normalOrangeSelectedColor
        }
        return token
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? Assignee {
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = false
            user.isCollaborateRoleInTask = false
            
            if let index = self.processors.index(of: user) {
                self.processors.remove(at: index)
            }
            
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let user = token.object as? Assignee, addTokenFromSearchInput {
            self.removeAssignersTokenView(assignersTokenView, [user])
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
            
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = true
            user.isCollaborateRoleInTask = false
            
            self.processors.append(user)
        }
        self.view.endEditing(true)
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let assignee = object as? Assignee  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            return cell!
        }
        return UITableViewCell()
    }
}
extension AdvanceSearchVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txToDate || textField == txFromDate {
            textField.selectedTextRange = textField.textRange(from: textField.endOfDocument, to: textField.endOfDocument)
        } else {
            IQKeyboardManager.shared.keyboardDistanceFromTextField = 10
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, let date = text.date(withFormat: DATE_FORMAT) {
            if textField == txFromDate {
                fromDate = date
                self.arrayParam["FromDate"] = fromDate
            } else if textField == txToDate {
                toDate = date
                self.arrayParam["ToDate"] = toDate
            }
        } else {
            self.view.makeToast("Bạn nhập sai định dạng ngày dd/mm/yyyy.")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Format Date dd-MM-yyyy
        guard let text = textField.text else { return false }
        
        //Check
        if textField == txFromDate || textField == txToDate {
            if !(string == "") {
                if (text.count == 2) || (text.count == 5) {
                    // append the text
                    textField.text = text + "-"
                }
            }
            // check the condition not exceed 9 chars
            return !(text.count > 9 && (string.count ) > range.length)
        }
        else {
            return true
        }
    }
    
}
