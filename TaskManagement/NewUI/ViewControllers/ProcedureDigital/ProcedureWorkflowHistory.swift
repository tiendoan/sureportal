//
//  ProcedureWorkflowHistory.swift
//  TaskManagement
//
//  Created by Mirum User on 6/5/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
class ProcedureWorkflowHistory: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tbView: UITableView!
    var listWorkTask:[ProceduresHistoryObject] = []
    var IDProce: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbView.dataSource = self
        self.tbView.delegate = self
        let nib = UINib.init(nibName: "HistoryProcedureCell", bundle: nil)
        self.tbView.register(nib, forCellReuseIdentifier: "HistoryProcedureCell")
        self.tbView.separatorStyle = .none
        self.tbView.estimatedRowHeight = 88.0
        self.tbView.rowHeight = UITableViewAutomaticDimension
        
        if self.IDProce != nil
        {
            SyncProvider.getProcedureHistory(Id: self.IDProce, done: { (result) in
                
                if ( result != nil )
                {
                    if ((result as! ProcedureHistoryResponse).data?.count)! > 0
                    {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        self.listWorkTask = (result as! ProcedureHistoryResponse).data!
                        self.listWorkTask = self.listWorkTask.sorted(by: {

                            dateFormatter.date(from:$0.Occurred!)?.compare(dateFormatter.date(from:$1.Occurred!)!) == .orderedDescending

                        })
                        self.tbView.reloadData()
                    }
                }
            })
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listWorkTask.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryProcedureCell", for: indexPath) as! HistoryProcedureCell
        
        let taskWork = self.listWorkTask[indexPath.row]
        cell.lblName.text = taskWork.PerformerName
        cell.lblComment.text = taskWork.Comment
        let createDate:String = Constants.formatDateFromString(taskWork.Occurred)!
        cell.lblDate.text = createDate
        switch taskWork.Action! {
        case 0:
            cell.lblAction.text = "Tạo Mới"
            break
        case 1:
            cell.lblAction.text = "Kiểm tra"
            break
        case 2:
              cell.lblAction.text = "Duyệt"
            break
        case 3:
              cell.lblAction.text = "Chỉnh sửa"
            break
        case 4:
              cell.lblAction.text = "Từ chối"
            break
        case 5:
             cell.lblAction.text = "Trả lại"
            break
        case 6:
            cell.lblAction.text = "Giao Việc"
            break
        case 7:
            cell.lblAction.text = "Chuyển duyệt"
            break
        case 8:
            cell.lblAction.text = "Xoá"
            break
        default:
             cell.lblAction.text = "Kiểm tra"
            break
        }
 
        return cell
    }
    
}
