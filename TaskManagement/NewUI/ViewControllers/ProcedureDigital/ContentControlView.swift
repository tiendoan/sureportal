//
//  ContentControlView.swift
//  TaskManagement
//
//  Created by Mirum User on 6/5/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


class ContentControlView: UIViewController {
   var IDProce: String!
    @IBOutlet weak var tbView: UITableView!
    var procedureName:String!
    var document: [FileDocument]!
    var documentPDF: [ProceduresAttachmentFileObject]! = []
    var documentNonePDF: [ProceduresAttachmentFileObject]! = []
    var documentFile:ProceduresAttachmentFileObject!
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var baseUrl:URL!
    var fileName:String!
    var arrayFileName:[ProceduresAttachmentFileObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewData.isHidden = true
        self.tbView.delegate = self
        self.tbView.dataSource = self
        self.tbView.tableFooterView = UIView()
        self.tbView.estimatedRowHeight = 80
        self.tbView.rowHeight = UITableViewAutomaticDimension
        let nib = UINib.init(nibName: "AttachmentViewFileCell", bundle: nil)
        self.tbView.register(nib, forCellReuseIdentifier: "AttachmentViewFileCell")
        
        if self.document != nil && self.document.count > 0  {

            SyncProvider.getProcedureAttachmentFile(Id: self.IDProce, done: { (result) in
                if result != nil
                {
                    if (( (result as! ProcedureAttachmentFileResponse)).data?.count)! > 0
                    {
                        self.arrayFileName = result?.data!
                        self.documentPDF = self.arrayFileName.filter({ ($0.Ext?.contains("pdf"))! || ($0.Ext?.contains("PDF"))!})
                        self.documentNonePDF = self.arrayFileName.filter({ !($0.Ext?.contains("pdf"))! && !($0.Ext?.contains("PDF"))!})
                        self.tbView.reloadData()
                    }
                }

            })
        }
    
    }
    
    @objc
    func tapFunction(sender:UITapGestureRecognizer) {
        

    }
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        self.tbView.isHidden = false
        self.viewData.isHidden = true
        if sender.selectedSegmentIndex == 0 {
           self.tbView.reloadData()
        }
        else
        {
           self.tbView.reloadData()
        }
    }
}
extension ContentControlView: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmentControl.selectedSegmentIndex == 0 {
             return self.documentPDF != nil ? self.documentPDF.count: 0
        }
        else
        {
             return self.documentNonePDF != nil ? self.documentNonePDF.count: 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentViewFileCell", for: indexPath) as! AttachmentViewFileCell
        var documentFile: ProceduresAttachmentFileObject!
        if self.segmentControl.selectedSegmentIndex == 0 {          
            documentFile = self.documentPDF[indexPath.row]
        }
        else
        {
             documentFile = self.documentNonePDF[indexPath.row]
        }
        
        cell.lbFileName.text = String(format: "%@", documentFile.Name!)
        cell.iconFile.isHidden = false
        if ( documentFile.Ext! == ".pdf" )
        {
            cell.iconFile.image = UIImage(named: "ic_file_pdf")
        }
        else if ( documentFile.Ext! == ".jpg" )
        {
            cell.iconFile.image = UIImage(named: "ic_file_jpg")
        }
        else if ( documentFile.Ext!.contains("doc"))
        {
            cell.iconFile.image = UIImage(named: "ic_file_doc")
        }
        else if ( documentFile.Ext!.contains("ppt"))
        {
            cell.iconFile.image = UIImage(named: "ic_file_ppt")
        }
        else if ( documentFile.Ext!.contains("xls") )
        {
            cell.iconFile.image = UIImage(named: "ic_file_xls")
        }
        else
        {
            cell.iconFile.image = UIImage(named: "ic_file_png")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        var documentFile: ProceduresAttachmentFileObject!
        if self.segmentControl.selectedSegmentIndex == 0 {
            documentFile = self.documentPDF[indexPath.row]
        }
        else
        {
            documentFile = self.documentNonePDF[indexPath.row]
        }
        
        let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "AttachmentDigittalDetailView") as! AttachmentDigittalDetailView
        
        mainViewController.baseUrl = self.baseUrl
        mainViewController.fileName = documentFile.Name!
        mainViewController.fileId = documentFile.ID!
        mainViewController.procedureId = self.IDProce
        self.present(mainViewController, animated: false, completion: nil)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
