//
//  ProcedureDigitalViewDetail.swift
//  TaskManagement
//
//  Created by Mirum User on 6/5/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

class ProcedureDigitalViewDetail: UIViewController,UIImagePickerControllerDelegate {
    
    enum TabIndex : Int {
        case firstChildTab  = 0
        case secondChildTab = 1
        case threeChildTab  = 2
    }
    @IBOutlet weak var avatarAuthor     : UIImageView!
    @IBOutlet weak var lblTitleAuthor   : UILabel!
    @IBOutlet weak var lblPosition      : UILabel!
    var procedureDetail                 :ProdureDigital!
    @IBOutlet weak var lbContent        : UILabel!
    @IBOutlet weak var lblStatus        : UILabel!
    @IBOutlet weak var lblProcessing    : UILabel!
    @IBOutlet weak var lblMajor         : UILabel!
    @IBOutlet weak var btnDate          : UIButton!
    @IBOutlet weak var btnAddmore       : UIButton!
    @IBOutlet weak var lblSumarry       : UILabel!
    @IBOutlet weak var btnIcon          : UIButton!
    @IBOutlet weak var heightHeader     : NSLayoutConstraint!
    @IBOutlet weak var headerView       : TNGradientView!
    @IBOutlet weak var heightMoreInfo   : NSLayoutConstraint!
    var selectedItemAction              :ItemActions!
    @IBOutlet weak var contenterView    : UIView!
    var currentViewController           : UIViewController?
    @IBOutlet weak var segmentCT        : UISegmentedControl!
    var isAddmore:Bool = false
    
    lazy var contentChildTabVC: UIViewController? = {
        let contentChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "ContentControlView")
        return contentChildTabVC
    }()
    lazy var progressChildTabVC : UIViewController? = {
        let progressChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "ProgressControlView")
        
        return progressChildTabVC
    }()
    
    lazy var historyChildTabVC : UIViewController? = {
        let historyChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "ProcedureWorkflowHistory")
        
        return historyChildTabVC
    }()
    

    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.segmentCT.addUnderlineForSelectedSegment()
        displayCurrentTab(TabIndex.firstChildTab.rawValue)
        self.lblTitleAuthor.text = self.procedureDetail.AuthorName
        self.lblPosition.text =
        self.procedureDetail.JobTitle
        self.lbContent.text = self.procedureDetail.Name
        lblMajor.text = self.procedureDetail.ProcedureCategoryName
        lblProcessing.text = self.procedureDetail.ProcedureTypeName
        btnDate.backgroundColor = .white
    
        btnDate.layer.cornerRadius = 15
        btnDate.layer.borderWidth = 0.8
        btnDate.layer.borderColor = UIColor.gray.cgColor
        let createDate:String = Constants.formatDate(self.procedureDetail.ProcedureDate)!
        btnDate.setTitle(createDate, for: .normal)
        btnDate.setTitleColor(UIColor.black, for: .normal)
        btnDate.titleLabel?.font = UIFont(name: FontFamily.Regular.rawValue, size: FontSize.Medium.rawValue)!
        
        self.loadUI()
        self.setUpNavigationBar()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    
    
    // MARK: - SETUP UI
    
    /*
     This function is using to set up navigationBar. It focus on the right navigation bar item.
     This view contain 2 view (UIButton load more, and stackview function).
     If number of function lower than 3, it only show the stackview. Otherwise, it show load more button
     */
    func setUpNavigationBar() {
        // Implement a view to insert into  right bar button item
        let viewFN = UIView(frame: CGRect(x: 0, y: 0, width: 180, height: self.navigationBar!.frame.size.height))
        
        // Implement stackview to stored button function
        let stackView = UIStackView(frame: CGRect(x: 60, y: 0, width: 120, height: self.navigationBar!.frame.size.height))
        stackView.backgroundColor = .yellow
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis  = .horizontal
        stackView.spacing   = 8.0
        stackView.distribution  = UIStackViewDistribution.fillProportionally
        stackView.alignment = .center
        stackView.semanticContentAttribute = .forceRightToLeft
        
        // Implement loadmore function button
        let moreButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: self.navigationBar!.frame.size.height))
        moreButton.setImage(UIImage(named: "ic_nav_more"), for: .normal)
        moreButton.addTarget(self, action: #selector(self.actionMore(_:)), for: UIControlEvents.touchUpInside)
        moreButton.isHidden = true
        moreButton.contentMode = .center
        
        if ( (self.procedureDetail.itemAction?.count)! >  0) {
            // Check show more button or not
            if ( (self.procedureDetail.itemAction?.count)! >  3) {
                moreButton.isHidden = false
            }
            
            // Implement function button and put it into stackview
            for index in 0...(((self.procedureDetail.itemAction?.count)! - 1) > 2 ? 2 : ((self.procedureDetail.itemAction?.count)! - 1)) {
                let itemValue:String = (self.procedureDetail.itemAction![index].Name)!
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: self.navigationBar!.frame.size.height))
                button.setTitle(itemValue, for: .normal)
                button.titleLabel?.font = UIFont(name: FontFamily.Bold.rawValue, size: FontSize.Large.rawValue)
                button.isHidden = false
                button.tag = index
                button.backgroundColor = UIColor.init(hexString: "#17C209")
                button.cornerRadius = 5
                button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
                button.titleLabel!.adjustsFontSizeToFitWidth = true
                button.addTarget(self, action: #selector(self.arrActionControl(_:)), for: UIControlEvents.touchUpInside)
                stackView.addArrangedSubview(button)
            }

        }
        
        viewFN.addSubview(moreButton)
        viewFN.addSubview(stackView)
        
        // Add constraint to stackView
        stackView.leadingAnchor.constraint(greaterThanOrEqualTo: viewFN.leadingAnchor, constant: 30).isActive = true
        stackView.trailingAnchor.constraint(equalTo: viewFN.trailingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: viewFN.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: viewFN.bottomAnchor).isActive = true
        
        // Add constraint to moreButton
        moreButton.widthAnchor.constraint(equalToConstant: 40)
        moreButton.trailingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        moreButton.topAnchor.constraint(equalTo: viewFN.topAnchor).isActive = true
        moreButton.bottomAnchor.constraint(equalTo: viewFN.bottomAnchor).isActive = true
        
        let rightBarButton = UIBarButtonItem(customView: viewFN)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }

    /*
     This function to help to setup UI of details controller
     */
    func loadUI(){
        if(self.procedureDetail.Status != nil && self.procedureDetail.StatusColor != nil) {
            switch self.procedureDetail.Status{
            case 0:
                lblStatus.text = " Đang soạn thảo   "
                break
            case 1:
                lblStatus.text = " Cần phê duyệt  "
                break
            case 2:
                lblStatus.text = " Đang xử lý   "
                break
            case 3:
                lblStatus.text = " Đã ban hành "
                break
            case 4:
                lblStatus.text = "  Từ chối  "
                break
            case 5:
                lblStatus.text = " Đang soạn thảo   "
                break
            default:
                break
            }
            lblStatus.backgroundColor = UIColor.init(hexString: self.procedureDetail.StatusColor ?? "#9E9E9E", transparency: 1)
            lblStatus.layer.cornerRadius = 15
            lblStatus.layer.masksToBounds = true
            
        } else {
            lblStatus.text = ""
            lblStatus.backgroundColor = UIColor.clear
        }
        
        let url = URL(string: Constants.default.domainAddress + (procedureDetail.AuthorAvatar ?? ""))
        if url != nil {
            self.avatarAuthor.kf.setImage(with: url!, placeholder: UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
        } else {
            self.avatarAuthor.image = UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50))
        }
        
    }

    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.contenterView.bounds
            self.contenterView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = contentChildTabVC
            (vc as! ContentControlView).IDProce = self.procedureDetail.ID
          (vc as! ContentControlView).document = self.procedureDetail.document
            
        case TabIndex.secondChildTab.rawValue :
            vc = ( progressChildTabVC as! ProgressControlView)
            (vc as! ProgressControlView).IDProce = self.procedureDetail.ID
        case TabIndex.threeChildTab.rawValue:
            vc = historyChildTabVC
            (vc as! ProcedureWorkflowHistory).IDProce = self.procedureDetail.ID
        default:
            return nil
        }
        
        return vc
    }

    // MARK: - Actions
    
    /*
     This function to help display more detail of current step
     */
    @IBAction func addMoreAction(_ sender: Any) {
        if ( isAddmore == false )
        {
            lblSumarry.text = self.procedureDetail.Summary
            btnAddmore.setTitle("Bớt thông tin", for: .normal)
            btnIcon.setImage(UIImage(named: "ic_info_up"), for: .normal)
            isAddmore = !isAddmore
            
            headerView.layoutIfNeeded()
            self.heightMoreInfo.constant = 40
            self.btnAddmore.sizeToFit()
            self.heightHeader.constant = 250
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
                self.headerView.gradientLayer?.frame = self.headerView.frame
                self.headerView.sizeToFit()
            }
        }
        else
        {
            lblSumarry.text = ""
            btnIcon.setImage(UIImage(named: "ic_info_down"), for: .normal)
            btnAddmore.setTitle("Thêm thông tin", for: .normal)
            isAddmore = !isAddmore
            
            headerView.layoutIfNeeded()
            self.heightMoreInfo.constant = 30
            self.heightHeader.constant = 200
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
                self.headerView.gradientLayer?.frame = self.headerView.frame
                self.headerView.sizeToFit()
            }
        }
    }
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentCT.changeUnderlinePosition()
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    func arrActionControl(_ sender: Any) {
        let btn = sender as! UIButton
        let code = self.procedureDetail.itemAction![btn.tag]
        self.selectedItemAction = code
        if( self.selectedItemAction.Code! == "Delete") {
            let alert = UIAlertController(title: "Xoá Quy Trình Số", message: "Bạn thực sự muốn xoá quy trình ?", preferredStyle: .alert)
            
            let actionOK = UIAlertAction(title: LocalizationString.AGREE_STRING, style: .cancel) { (action) in
                
                SyncProvider.deleteProcedureDigital(Id: self.procedureDetail.ID!, done: {
                    (result) in
                    
                    self.view.makeToast("Cập nhật thành công")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.navigationController?.popToRootViewController(animated: false)
                    }
                })
                
            }
            let actionCancel = UIAlertAction(title: LocalizationString.CANCEL_STRING, style: .default, handler: nil)
            alert.addAction(actionOK)
            alert.addAction(actionCancel)
            self.mainNew()?.present(alert, animated: true, completion: nil)
            
        }
        else if (  self.selectedItemAction.Code! == "Return" ||
            self.selectedItemAction.Code! == "Reject" || self.selectedItemAction.Code! == "Transfer"
            || self.selectedItemAction.Code! == "Assign" || self.selectedItemAction.Code! == "Audit" )
        {
            
            let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalTaskVC") as! ProcedureDigitalTaskVC
            mainViewController.procedureDetail = self.procedureDetail
            mainViewController.itemAction = self.selectedItemAction
            self.navigationController?.pushViewController(mainViewController)
            
        }
        if ( self.selectedItemAction.Code! == "Approve"  )
        {
            let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalApproveTaskVC") as! ProcedureDigitalApproveTaskVC
            mainViewController.procedureDetail = self.procedureDetail
            mainViewController.itemAction = self.selectedItemAction
            self.navigationController?.pushViewController(mainViewController)
        }
    }
    
    func actionMore(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Chọn", message: nil, preferredStyle: .actionSheet)
        for i in 0..<(self.procedureDetail.itemAction?.count)! {
            let action: UIAlertAction = UIAlertAction(title: self.procedureDetail.itemAction![i].Name!, style: .default) { (_) in
                let itemAction = self.procedureDetail.itemAction![i].Code!
                if ( itemAction == "Delete" )
                {
                    let alert = UIAlertController(title: "Xoá Quy Trình Số", message: "Bạn thực sự muốn xoá quy trình ?", preferredStyle: .alert)
                    
                    let actionOK = UIAlertAction(title: LocalizationString.AGREE_STRING, style: .cancel) { (action) in
                        
                        SyncProvider.deleteProcedureDigital(Id: self.procedureDetail.ID!, done: {
                            (result) in
                            
                            self.view.makeToast("Cập nhật thành công")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                self.navigationController?.popToRootViewController(animated: false)
                            }
                        })
                        
                        
                    }
                    let actionCancel = UIAlertAction(title: LocalizationString.CANCEL_STRING, style: .default, handler: nil)
                    alert.addAction(actionOK)
                    alert.addAction(actionCancel)
                    self.mainNew()?.present(alert, animated: true, completion: nil)
                }
                    
                else if (itemAction == "Return" ||
                    itemAction == "Reject" || itemAction == "Transfer" || itemAction == "Assign" || itemAction == "Audit")
                {
                    let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
                    let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalTaskVC") as! ProcedureDigitalTaskVC
                    mainViewController.procedureDetail = self.procedureDetail
                    mainViewController.itemAction = self.procedureDetail.itemAction![i]
                    self.navigationController?.pushViewController(mainViewController)
                }
                else if ( itemAction == "Approve")
                {
                    let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
                    let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalApproveTaskVC") as! ProcedureDigitalApproveTaskVC
                    mainViewController.procedureDetail = self.procedureDetail
                    mainViewController.itemAction = self.procedureDetail.itemAction![i]
                    self.navigationController?.pushViewController(mainViewController)
                    
                }
            }
            actionSheet.addAction(action)
        }
        let cancel: UIAlertAction = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
        
        
    }
}

