//
//  AttachmentDigittalDetailView.swift
//  TaskManagement
//
//  Created by Mirum User on 6/7/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import WebKit
class AttachmentDigittalDetailView: UIViewController,UIScrollViewDelegate,WKNavigationDelegate {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lbPaging: UILabel!
    @IBOutlet weak var btnRotateRight: UIButton!
    @IBOutlet weak var btnRotateLeft: UIButton!
    @IBOutlet weak var btnBack: UIView!
    @IBOutlet weak var mainView: UIView!
    var baseUrl:URL!
    var fileName:String!
    var procedureId:String!
    var fileId:String!
    
    @IBOutlet weak var lbTitle: UILabel!
    fileprivate let vImage = ZoomImageView(frame: UIScreen.main.bounds)

    fileprivate let webView = WKWebView(frame: UIScreen.main.bounds)
    
    lazy fileprivate var gradientTop: CAGradientLayer = {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(white: 0, alpha: 0.5).cgColor, UIColor(white: 0, alpha: 0.1).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        
        return gradient
    }()
    lazy fileprivate var gradientBotton: CAGradientLayer = {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(white: 0, alpha: 0.5).cgColor, UIColor(white: 0, alpha: 0.1).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        return gradient
    }()
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradientTop.frame = btnBack.bounds
        gradientBotton.frame = bottomView.bounds
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbPaging.textColor = UIColor.white
        lbPaging.font = UIFont(name: "Muli-Regular", size: 15.0)!
        btnBack.layer.insertSublayer(gradientTop, at: 0)
       bottomView.layer.insertSublayer(gradientBotton, at: 0)
        // Do any additional setup after loading the view.
        webView.backgroundColor = UIColor.white
        mainView.addSubview(webView)
        
        vImage.zoomMode = .fit
        vImage.maximumZoomScale = 5
        vImage.minimumZoomScale = 1
        mainView.addSubview(vImage)
        vImage.snp.makeConstraints { (make) in
            make.left.right.bottom.top.equalToSuperview()
        }
        
        webView.navigationDelegate = self
        webView.scrollView.delegate = self
        webView.backgroundColor = UIColor.white
        mainView.addSubview(webView)
        webView.snp.makeConstraints({ (make) in
            make.left.top.right.bottom.equalToSuperview()
        })
        
        
        if ( baseUrl != nil )
        {
            loadField(url: baseUrl)
        }
        else
        {
            if ( self.procedureId != nil )
            {
                let attachmentDownload = String(format: "/mobileapis/api/DigitalProcedure/DownloadProcedureAttachment?procedureId=%@&procedureAttachmentId=%@",self.procedureId, self.fileId)
                
                InstanceDB.default.downloadFilePDF(self.fileName, path: attachmentDownload , done: { (result) in
                    if(result != nil){
                        self.loadField(url: result!)
                    }
                })
            }
        }
      
        lbTitle.text = self.fileName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate func loadField(url:URL){
        self.webView.loadFileURL(url, allowingReadAccessTo: url)
        self.lbPaging.text = "1 of 1"
    }
    
    @IBAction func closeVC(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionRotateLeft(_ sender: Any) {
        
        self.webView.rotate(byAngle: -CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
        }
        self.vImage.rotate(byAngle: -CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
        }
        
        self.webView.frame = (self.webView.superview?.bounds)!;
        self.vImage.frame = (self.vImage.superview?.bounds)!;
        self.webView.snp.updateConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        self.vImage.snp.updateConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        self.view.layoutIfNeeded()
    }
    @IBAction func actionRotateRight(_ sender: Any) {
        
        self.webView.rotate(byAngle: CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
        }
        self.vImage.rotate(byAngle: CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
        }
        self.webView.frame = (self.webView.superview?.bounds)!;
        self.vImage.frame = (self.vImage.superview?.bounds)!;
        self.webView.snp.updateConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        self.vImage.snp.updateConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        self.view.layoutIfNeeded()
        
    }
    func scrollViewWillBeginDragging (_ scrollView: UIScrollView) {
        self.getPage()
    }
    
    func scrollViewWillBeginDecelerating (_ scrollView: UIScrollView) {
        self.getPage()
    }
    
    func scrollViewDidEndDragging (_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.getPage()
    }
    
    func scrollViewDidScroll (_ scrollView: UIScrollView) {
        self.getPage()
    }
    
    func webView (_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.getPage()
    }
    
    func getPage () {
        let webViewSubviews = self.getSubviewsOfView(v: self.webView)
        for v in webViewSubviews {
            if v.description.range(of:"WKPDFPageNumberIndicator") != nil {
                v.isHidden = true
                for i in v.subviews{
                    if let a = i as? UILabel{
                        self.lbPaging.text = a.text ?? "1 of 1"
                    }
                }
            }
        }
    }
    func getSubviewsOfView (v:UIView) -> [UIView] {
        var viewArray = [UIView]()
        for subview in v.subviews {
            viewArray += getSubviewsOfView(v: subview)
            viewArray.append(subview)
        }
        return viewArray
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
