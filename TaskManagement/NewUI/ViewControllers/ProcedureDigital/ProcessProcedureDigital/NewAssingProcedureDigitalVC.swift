//
//  NewAssingProcedureDigitalVC.swift
//  TaskManagement
//
//  Created by Mirum User on 6/10/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown
import SVProgressHUD

protocol ProcedureDigitalDetailDelegate {
    func selectWorkFlowTemplate(Id:String)
}
class NewAssingProcedureDigitalVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblTable: UITableView!
    var progressIDProcedure:String!
    var workflowTemplateId:String!
    var listWorkTask:[ProcedureWorkflowTemplateTasks] = []
    var delegate: ProcedureDigitalDetailDelegate!
    var workflowTask:ProcedureDigitalWorkFlow!
    @IBOutlet weak var btnProgresss: UIButton!
     let taskDropDown: DropDown = DropDown()
    var arrayProcedureCategory: [ProcedureDataObject] = [] {
        didSet {
            if !arrayProcedureCategory.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayProcedureCategory.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.tblTable.dataSource = self
        self.tblTable.delegate = self
        let nib = UINib.init(nibName: "ProgressViewCell", bundle: nil)
        self.tblTable.register(nib, forCellReuseIdentifier: "ProgressViewCell")
        self.tblTable.separatorStyle = .none
        self.tblTable.estimatedRowHeight = 88.0
        self.tblTable.rowHeight = UITableViewAutomaticDimension
        
        taskDropDown.anchorView = self.btnProgresss
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnProgresss.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.workflowTemplateId = (self.arrayProcedureCategory[index].ID)!
            self.delegate.selectWorkFlowTemplate(Id: self.workflowTemplateId)
            self.btnProgresss.setTitle(item, for: .normal)
            self.listWorkTask.removeAll()
            self.getWorkstark()

        }
        // Do any additional setup after loading the view.
    
     NotificationCenter.default.addObserver(self, selector: #selector(reloadDataforceData(notfication:)), name: NSNotification.Name(rawValue: "dataDownloadCompletedData"), object: nil)
 }
    @objc func reloadDataforceData(notfication: NSNotification) {
        
        self.listWorkTask.removeAll()
        self.loadWorkFlow()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "dataDownloadCompletedData"), object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.listWorkTask.removeAll()
        self.loadWorkFlow()
    }

    func loadWorkFlow() {
        
        guard let GuiId = self.progressIDProcedure else{ return }
        SVProgressHUD.show()
        SyncProvider.GetProcedureWorkflowTemplatesByProcedureTypeId(Id: GuiId, done: { (result) in
            let resultObject:ProcedureDigitalObject = (result as! ProcedureDigitalObject)
            if (resultObject.data?.count)! > 0
            {
            
                self.arrayProcedureCategory = (result?.data)!
                if self.arrayProcedureCategory.count > 0
                {
                    self.workflowTemplateId = (self.arrayProcedureCategory[0].ID)!
                    self.delegate.selectWorkFlowTemplate(Id: self.workflowTemplateId)
                    self.btnProgresss.setTitle((self.arrayProcedureCategory[0].Name)!, for: .normal)
                    self.getWorkstark()
                }
            }
                SVProgressHUD.dismiss()
        })
        
    }
    func getWorkstark() {
        

        SyncProvider.GetProcedureWorkflowTemplateById(Id: self.workflowTemplateId, done: { (result) in
            let resultObject:ProcedureDigitalWorkFlowObject = (result as! ProcedureDigitalWorkFlowObject)
            if (resultObject.data?.ProcedureWorkflowTemplateSteps?.count)! > 0
            {
                self.workflowTask = result?.data
        
//                let total:Int = (resultObject.data?.ProcedureWorkflowTemplateSteps?.count)!
//                for i in 0..<total
//                {
//                    let indexWorkTask:Int = (resultObject.data?.ProcedureWorkflowTemplateSteps![i].ProcedureWorkflowTemplateTasks!.count)!
//                    for j in 0..<indexWorkTask
//                    {
//                        let workTask = resultObject.data?.ProcedureWorkflowTemplateSteps![i].ProcedureWorkflowTemplateTasks![j]
////                        if ( j == 0 )
////                        {
////                            workTask?.IsFirst = true
////                            workTask?.Name = resultObject.data?.ProcedureWorkflowTemplateSteps![i].Name
////                            workTask?.numberIndex = i + 1
////                            self.listWorkTask.append(workTask!)
////                        }
////                        else
////                        {
//                            self.listWorkTask.append(workTask!)
////                        }
//                    }
//                }
                self.tblTable.reloadData()
            
            }
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func progressAction(_ sender: Any) {
        taskDropDown.show()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workflowTask != nil ? workflowTask.ProcedureWorkflowTemplateSteps!.count : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if self.listWorkTask.count > 0  {
//            let workTask = self.listWorkTask[indexPath.row]
//            if workTask.IsFirst != true
//            {
//                return UITableViewAutomaticDimension;
//            }
//        }
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProgressViewCell", for: indexPath) as! ProgressViewCell
        if workflowTask.ProcedureWorkflowTemplateSteps!.count > 0  {
            let workTask = self.workflowTask.ProcedureWorkflowTemplateSteps![indexPath.row]
//            if workTask.IsFirst != true {
//                cell.lblBadge.isHidden = true
//                cell.lblRating.isHidden = true
//                cell.lblContent.isHidden = true
//                cell.reloadPadding()
//            }
            cell.lblContent.text = workTask.Name != nil ? workTask.Name : ""
            if  workTask.Name != nil {
                cell.lblBadge.text =  String(indexPath.row + 1)
            }
            if (workTask.ProcedureWorkflowTemplateTasks?.count)! > 0
            {
                cell.lbTitle.text = workTask.ProcedureWorkflowTemplateTasks![0].UserName
            }
        }
        
        return cell
    }

    @IBAction func modifyAction(_ sender: Any) {
        self.performSegue(withIdentifier: "modifySegue", sender: self)
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "modifySegue" {
          
            let controller = segue.destination as! ModifyViewController
            controller.listWorkTask = self.listWorkTask
            controller.rootWorkTask = self.workflowTask
          
        }
    }
 

}
