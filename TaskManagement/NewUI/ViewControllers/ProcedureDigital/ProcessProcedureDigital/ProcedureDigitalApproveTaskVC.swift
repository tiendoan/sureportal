//
//  ProcedureDigitalApproveTaskVC.swift
//  TaskManagement
//
//  Created by Mirum User on 7/3/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown
import SVProgressHUD

class ProcedureDigitalApproveTaskVC: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {
    @IBOutlet weak var authorAvatar: UIImageView!
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var lblComment: UITextField!
    var procedureDetail:ProdureDigital!
    var itemAction:ItemActions!
    @IBOutlet weak var lblSignatureName: UILabel!
    @IBOutlet weak var SignatureImg: UIImageView!
    @IBOutlet weak var StampImage: UIImageView!
    @IBOutlet weak var btnAttatch: UIButton!
    let taskDropDownFile: DropDown = DropDown()
    var selectedFile:ProceduresAttachmentFileObject!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var attachViewer: UIImageView!
    @IBOutlet weak var viewStack: UIView!    
    @IBOutlet weak var supperStack: UIView!
    @IBOutlet weak var supperView: UIView!
    @IBOutlet weak var heightImg: NSLayoutConstraint!
    @IBOutlet weak var txPage: UITextField!
    @IBOutlet weak var lblPage: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollview: UIScrollView!
    var isSignatureProgress:Bool!
    var totalPage:Int!
    var currentPage:Int!
    var personalSignature:ProcedureDigitalSigningResponse!
    var signatureView:UIStackView!
    var personalSignatureDigital:PersonalSignatureInfos!
    
    @IBOutlet weak var txtSecure: UITextField!
    var attachFiles: [ProceduresAttachmentFileObject] = [] {
        didSet {
            if !attachFiles.isEmpty {
                taskDropDownFile.dataSource.removeAll()
                taskDropDownFile.dataSource = attachFiles.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.currentPage = 1
        self.txPage.text = String(format: "%d", self.currentPage)
        self.txPage.delegate = self
        
        
        SyncProvider.checkCurrentUserSignWithDigitalSignature(Id: self.procedureDetail.ID!, done: {
            (result) in
            if ( result?.data?.SignWithDigitalSignature == 1 )
            {
                self.personalSignature = result
                SyncProvider.getPersonalSignatureInfos(done: {
                    (resultSignature) in
                    self.personalSignatureDigital = resultSignature?.data?[0]
                    self.lblSignatureName.text = resultSignature?.data?[0].Name!
                    
                    let url = URL(string: Constants.default.domainAddress + (resultSignature?.data?[0].SignatureImageFileName ?? ""))
                    if url != nil {
                        self.StampImage.setImage(url: url!)
                    }
                    
                    let urlStamp = URL(string: Constants.default.domainAddress + (resultSignature?.data?[0].StampImageFileName ?? ""))
                    if urlStamp != nil {
                        self.SignatureImg.setImage(url: urlStamp!)
                    }
                    
                })
                SyncProvider.getProcedureAttachmentFile(Id: self.procedureDetail.ID!, done: { (result) in
                    if ( result != nil)
                    {
                        self.isSignatureProgress = false
                        if (( (result as! ProcedureAttachmentFileResponse)).data?.count)! > 0
                        {
                            self.attachFiles = (result?.data!)!
                            self.attachFiles = self.attachFiles.filter({
                                ($0.Ext?.contains("pdf"))! || ($0.Ext?.contains("PDF"))!
                            })
                            var isExits = false
                            self.isSignatureProgress = false
                            if ( self.attachFiles.count > 0  )
                            {
                                self.btnAttatch.setTitle(self.attachFiles[0].Name!, for: .normal)
                                for object in self.attachFiles
                                {
                                    if ( (object.ProcedureAttachmentSigningPositions?.count)! > 0  )
                                    {
                                        isExits = true
                                        self.isSignatureProgress = true
                                    }
                                }
                                if ( isExits )
                                {
                                    self.bottomView.isHidden = false
                                    self.selectedFile = self.attachFiles[0]
                                    self.reloadAttachment()
                                    
                                }
                            }
                            
                        }
                    }
                })
            }
        })
        
        let url = URL(string: Constants.default.domainAddress + (procedureDetail.AuthorAvatar ?? ""))
        if url != nil {
            self.authorAvatar.kf.setImage(with: url!, placeholder: UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
        } else {
            self.authorAvatar.image = UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50))
        }
        self.lblSummary.text = self.procedureDetail.Name!
        self.lblAuthorName.text = self.procedureDetail.AuthorName!
        self.btnAction.setTitle(self.itemAction.Name!, for: .normal)
        self.btnAction.titleLabel?.adjustsFontSizeToFitWidth = true
//
        
        taskDropDownFile.anchorView = self.btnAttatch
        taskDropDownFile.textColor = UIColor.gray
        taskDropDownFile.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDownFile.bottomOffset = CGPoint(x: 0, y: self.btnAttatch.bounds.height + 10)
        taskDropDownFile.isMultipleTouchEnabled = false
        taskDropDownFile.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDownFile.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDownFile.selectionAction = { [unowned self] (index, item) in
            self.stackView.removeSubviews()
            self.selectedFile = self.attachFiles[index]
            self.btnAttatch.setTitle(item, for: .normal)
            self.reloadAttachment()
            
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.title = " "
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func reloadAttachment(){
        
        for  object in self.selectedFile.ProcedureAttachmentSigningPositions!
        {
            let uiView = UIView()
            uiView.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
            uiView.heightAnchor.constraint(equalToConstant: 60).isActive = true
            
            let textLabel = UILabel(frame: CGRect(x: 10, y: 0, width: self.view.frame.width - 10, height: 30))
            textLabel.numberOfLines = 0
            textLabel.text  = self.selectedFile.Name!
            
            uiView.addSubview(textLabel)
            
            let textLabelE = UILabel(frame: CGRect(x: 10, y: 35, width: 20, height: 20))
            textLabelE.borderColor = UIColor.blue
            textLabelE.borderWidth = 1.0
            textLabelE.cornerRadius = 5.0
            textLabelE.textAlignment = .center
            textLabelE.text  = String(format: "%d", object.PageNumber!)
            uiView.addSubview(textLabelE)
            
            var paddingLeft = 35
            if ( object.HasSignature )!
            {
                let imgSign = UIImageView(frame: CGRect(x: paddingLeft, y: 35, width: 20, height: 20))
                imgSign.image = UIImage(named: "icon_sign_dg")
                uiView.addSubview(imgSign)
                paddingLeft += 25
            }
            if ( object.HasStamp )!
            {
                let imgSign = UIImageView(frame: CGRect(x: paddingLeft, y: 35, width: 20, height: 20))
                imgSign.image = UIImage(named: "icon_stamp_dg")
                uiView.addSubview(imgSign)
                paddingLeft += 25
            }
            if ( object.HasInfo )!
            {
                let imgSign = UIImageView(frame: CGRect(x: paddingLeft, y: 35, width: 20, height: 20))
                imgSign.image = UIImage(named: "icon_info_dg")
                uiView.addSubview(imgSign)
                paddingLeft += 25
            }
            
            let line = UIView(frame: CGRect(x: 0, y: 58, width: self.view.frame.width, height: 1))
            line.backgroundColor = UIColor.gray
            uiView.addSubview(line)
            uiView.tag = object.PageNumber!
            
            
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProcedureDigitalApproveTaskVC.handleTap(_:)))
            gestureRecognizer.delegate = self
            uiView.addGestureRecognizer(gestureRecognizer)
        
            self.stackView.addArrangedSubview(uiView)
        }
        
        SyncProvider.getCountPage(Name: (self.selectedFile.ID)!  + (self.selectedFile.Ext)!, done: {
            (result) in
            if ( result != nil  )
            {
                self.totalPage = (result?.totalPage)!
                self.lblPage.text = String(format: "/%d", (result?.totalPage)!)
                self.currentPage = 1
                self.txPage.text = String(format: "%d", self.currentPage)
                self.loadAttachemntPrev()
            }
          
            
        })
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.currentPage = sender.view?.tag
        self.txPage.text = String(format: "%d", self.currentPage)
        self.loadAttachemntPrev()
        
    }
    
    func loadAttachemntPrev() {
        let name = (self.selectedFile.ID)! + (self.selectedFile.Ext)!
        let attach = String(format: "%@/mobileapis/ImageAttach.ashx?AttachmentPreview=%@%@",Router.baseURLString,self.selectedFile.ID!, "_" + String(format: "%d", self.currentPage) +  ".png" )
        SVProgressHUD.show()
        SyncProvider.checkAttachment(Name: name, Attach: attach, done: {(result) in
            SVProgressHUD.dismiss()
            self.loadField()
            
        })
    }
    
    fileprivate func loadField(){
        
        let attachmentDownload = String(format: "%@/mobileapis/ImageAttach.ashx?AttachmentPreview=%@%@",Router.baseURLString,self.selectedFile.ID!, "_" +  String(format: "%d", self.currentPage) +  ".png")
        let url = URL(string: attachmentDownload)
        
        if let imageSource = CGImageSourceCreateWithURL(url! as CFURL, nil) {
            if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                let pixelWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                let pixelHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                
                print("the image width is: \(pixelWidth)")
                print("the image height is: \(pixelHeight)")
                
                let ratio:CGFloat = CGFloat(612)/CGFloat(792)
                
                let screenSize = UIScreen.main.bounds
                let screenWidth = screenSize.width
                
                heightImg.constant = CGFloat(screenWidth/CGFloat(ratio))
                self.attachViewer.setImage(url: url!)
                self.supperView.layoutIfNeeded()
                scrollview.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.attachViewer.frame.origin.y + heightImg.constant)
                self.scrollview.layoutIfNeeded()
                
            }
        }
        self.loadDataSignature()
        
    }
    override func viewDidLayoutSubviews() {       
        // Do any additional setup after loading the view
//        scrollview.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1400)
    }
    @IBAction func dismiss(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    @IBAction func sendAction(_ sender: Any) {        
        self.view.endEditing(true)
        if ( self.lblComment.text!.isEmpty )
        {
            self.view.makeToast("Vui lòng nhập nội dung")
            return
        }
        if isSignatureProgress == false {
            SyncProvider.approveProcedureDigital(Id: self.procedureDetail.ID!, comment: self.lblComment.text!, done: {
                (result) in
                
                self.view.makeToast("Cập nhật thành công")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                    self.navigationController?.popToRootViewController(animated: false)
                    
                }
            })
        }
        else
        {
            if ( self.txtSecure.text!.isEmpty )
            {
                self.view.makeToast("Vui lòng nhập mã chữ ký sô")
                return
            }
            SyncProvider.approveProcedureDigitalWithSignature(Id: self.procedureDetail.ID!, comment: self.lblComment.text!, SignatureId: (self.personalSignature.data?.SignatureID!)!, PrivateKey: self.txtSecure.text!, done: {
                (result) in
                
                if ( result?.status! == 1 )
                {
                    self.view.makeToast("Cập nhật thành công")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                        self.navigationController?.popToRootViewController(animated: false)
                        
                    }
                }
                else
                {
                    self.view.makeToast((result?.message!)!)
                }

            })
        }

        
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        let tmpCurrentPage:Int = Int(textField.text!)!
        if tmpCurrentPage != nil && tmpCurrentPage < self.totalPage  {
            textField.text = String(format: "%d", tmpCurrentPage)
            self.currentPage = tmpCurrentPage
            self.loadAttachemntPrev()

        }
        else
        {
            textField.text = String(format: "%d", self.currentPage)
        }
        
    }
    func loadDataSignature(){
        if ( signatureView != nil ) {
            signatureView.removeFromSuperview()
        }
        let positionObj:[ProcedureAttachmentSigningPositions] =  (self.selectedFile.ProcedureAttachmentSigningPositions?.filter({ $0.PageNumber == self.currentPage }))!
        if positionObj != nil && positionObj.count > 0 {
            let signatureOb = positionObj[0]
            var originX:Int = signatureOb.CoordinateX!
            var originY:Int = signatureOb.CoordinateY!
            
            var totalSelect = 0;
            if signatureOb.HasInfo!
            {
                totalSelect += 1
            }
            if signatureOb.HasStamp!
            {
                totalSelect += 1
            }
            if signatureOb.HasSignature!
            {
                totalSelect += 1
            }
            var heightImage = 150
            if ( totalSelect == 3 )
            {
                originX = originX - 160
                originY = originY - 50
                heightImage = 250
            }
            else if ( totalSelect == 2  )
            {
                originX = originX - 125
                originY = originY - 50
                heightImage = 200
            }
            else
            {
                originX = originX - 110
                originY = originY - 50
                heightImage = 100
            }

            let tmpX = originX
            let tmpY = originY

//            (125.0, 206.0)
            signatureView = UIStackView(frame: CGRect(x: tmpX, y: tmpY, width: heightImage, height: 70))
             let uiView =  UIView(frame: CGRect(x: 0, y: 0, width: self.signatureView.frame.width, height: 70))
            if ( totalSelect == 3 )
            {
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                label.font = UIFont.boldSystemFont(ofSize: 10)
                label.text = self.personalSignatureDigital.Name!
                label.numberOfLines = 0
                uiView.addSubview(label)
               
                let imgStam = UIImageView(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                let urlStam = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.StampImageFileName ?? ""))
                if urlStam != nil {
                    imgStam.setImage(url: urlStam!)
                }
                uiView.addSubview(imgStam)
                
                let imgSig = UIImageView(frame: CGRect(x: 160, y: 0, width: 70, height: 70))
                let url = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.SignatureImageFileName ?? ""))
                if url != nil {
                    imgSig.setImage(url: url!)
                }
                uiView.addSubview(imgSig)
                
            }
            else if ( totalSelect == 2 )
            {
                
                if ( signatureOb.HasSignature! && signatureOb.HasStamp! )
                {
                    let imgStam = UIImageView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                    let urlStam = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.StampImageFileName ?? ""))
                    if urlStam != nil {
                        imgStam.setImage(url: urlStam!)
                    }
                    uiView.addSubview(imgStam)
                    
                    let imgSig = UIImageView(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                    let url = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.SignatureImageFileName ?? ""))
                    if url != nil {
                        imgSig.setImage(url: url!)
                    }
                    uiView.addSubview(imgSig)
                }
                else if ( signatureOb.HasSignature! && signatureOb.HasInfo! )
                {
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                    label.font = UIFont.boldSystemFont(ofSize: 10)
                    label.text = self.personalSignatureDigital.Name!
                    label.numberOfLines = 0
                    uiView.addSubview(label)
                    
                    let imgStam = UIImageView(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                    let urlStam = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.SignatureImageFileName ?? ""))
                    if urlStam != nil {
                        imgStam.setImage(url: urlStam!)
                    }
                    uiView.addSubview(imgStam)
                }
                else
                {
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                    label.font = UIFont.boldSystemFont(ofSize: 10)
                    label.text = self.personalSignatureDigital.Name!
                    label.numberOfLines = 0
                    uiView.addSubview(label)
                    
                    let imgStam = UIImageView(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                    let urlStam = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.StampImageFileName ?? ""))
                    if urlStam != nil {
                        imgStam.setImage(url: urlStam!)
                    }
                    uiView.addSubview(imgStam)
                }
            }
            else
            {
                if ( signatureOb.HasSignature! )
                {
                    let imgSig = UIImageView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                    let url = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.SignatureImageFileName ?? ""))
                    if url != nil {
                        imgSig.setImage(url: url!)
                    }
                    uiView.addSubview(imgSig)
                }
                else if ( signatureOb.HasStamp! )
                {
                    let imgSig = UIImageView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                    let url = URL(string: Constants.default.domainAddress + (self.personalSignatureDigital.StampImageFileName ?? ""))
                    if url != nil {
                        imgSig.setImage(url: url!)
                    }
                    uiView.addSubview(imgSig)
                }
                else
                {
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                    label.font = UIFont.boldSystemFont(ofSize: 10)
                    label.text = self.personalSignatureDigital.Name!
                    label.numberOfLines = 0
                    uiView.addSubview(label)
                }
            }
           signatureView.addArrangedSubview(uiView)
           self.attachViewer.addSubview(signatureView)
            
        }
        
    }
    @IBAction func prevAction(_ sender: Any) {
        
        var number:Int = Int(self.txPage.text!)!
        if( number > 1) {
            number  = number - 1
            self.txPage.text = String(format: "%d", number)
            self.currentPage = number
            self.loadAttachemntPrev()
            
        }
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        var number:Int = Int(self.txPage.text!)!
        if( number < self.totalPage) {
            number  = number + 1
            self.currentPage = number
            self.txPage.text = String(format: "%d", number)
            self.loadAttachemntPrev()
        }
        
    }
    
    @IBAction func fileTap(_ sender: Any) {
        
        taskDropDownFile.show()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
