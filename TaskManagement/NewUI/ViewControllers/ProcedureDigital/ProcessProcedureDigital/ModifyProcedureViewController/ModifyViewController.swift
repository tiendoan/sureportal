//
//  ModifyViewController.swift
//  TaskManagement
//
//  Created by Mirum User on 6/18/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import Alamofire

protocol CustomWorkflowDelegate {
    
    func replaceWorkflowAtIndex(indexReplace: Int,withCustomer: String)
    func replaceByCustomUserOjbect(userObject: UserWorkTaskObject)
    
}

class ModifyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CustomWorkflowDelegate, UITextFieldDelegate {
    
    func replaceByCustomUserOjbect(userObject: UserWorkTaskObject) {
        
        self.replaceUser = userObject
        self.rootWorkTask.ProcedureWorkflowTemplateSteps![self.selectedChangeIndex].ProcedureWorkflowTemplateTasks![0].UserName = self.replaceUser.FullName
        self.rootWorkTask.ProcedureWorkflowTemplateSteps![self.selectedChangeIndex].ProcedureWorkflowTemplateTasks![0].UserID = self.replaceUser.ID
//        self.listWorkTask[self.selectedChangeIndex].UserName = self.replaceUser.FullName
        self.tblTableView.reloadData()
        
    }
    
    func replaceWorkflowAtIndex(indexReplace: Int, withCustomer: String) {
        
        self.selectedChangeIndex = indexReplace
        self.performSegue(withIdentifier: "customsegue", sender: self)
        
    }

    @IBOutlet weak var btnTitle: UITextField!
    var rootWorkTask:ProcedureDigitalWorkFlow!
    var replaceUser: UserWorkTaskObject!
    var selectedChangeIndex:Int!
    @IBOutlet weak var tblTableView: UITableView!
    var listWorkTask:[ProcedureWorkflowTemplateTasks] = []
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.btnTitle.delegate = self
        self.tblTableView.dataSource = self
        self.tblTableView.delegate = self
        let nib = UINib.init(nibName: "ProgressViewCell", bundle: nil)
        self.tblTableView.register(nib, forCellReuseIdentifier: "ProgressViewCell")
        self.tblTableView.separatorStyle = .none
        self.tblTableView.estimatedRowHeight = 88.0
        self.tblTableView.rowHeight = UITableViewAutomaticDimension
        self.tblTableView.tableFooterView = UIView()
        self.tblTableView.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.title = " "
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "customsegue" {
            if let destinationVC = segue.destination as? CustomWorkflowVC {
                destinationVC.delegate = self
            }
        }
    }

    @IBAction func saveModifyAction(_ sender: Any) {
        
        if (self.btnTitle.text != "")
        {
            self.rootWorkTask.Name = self.btnTitle.text!
//            
            let jsonObject:[String:Any] = self.rootWorkTask.toJSON()
            do {
                let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
                if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                    print(jsonString)
                    SyncProvider.createPersonalWorkflowTemplate(json: jsonObject, done: {
                        (result) in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompletedData"), object: nil)
                        self.dismiss(animated: false, completion: nil)
                    })

                }
                
            } catch let error as NSError {
                print(error)
            }    
        }
        else
        {
            self.view.makeToast("Nhập tên quy trình")
        }
        
    }
    
    @IBAction func dismissVC(_ sender: Any) {


        self.dismiss(animated: false, completion: nil)
//        self.navigationController?.dismiss(animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rootWorkTask != nil ? rootWorkTask.ProcedureWorkflowTemplateSteps!.count : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProgressViewCell", for: indexPath) as! ProgressViewCell
        cell.heightBtnModify.constant = 35
        cell.delegate = self
        cell.rowIndex = indexPath.row
        cell.btnEdit.isHidden = false
//        let workTask = self.listWorkTask[indexPath.row]
        let workTask = self.rootWorkTask.ProcedureWorkflowTemplateSteps![indexPath.row]
        
//        if workTask.IsFirst != true {
//            cell.lblBadge.isHidden = true
//            cell.lblRating.isHidden = true
//            cell.lblContent.isHidden = true
//            cell.reloadPadding()
//        }
//        cell.lblContent.text = workTask.Name != nil ? workTask.Name : ""
//        if  workTask.Name != nil {
//            cell.lblBadge.text =  String(indexPath.row + 1)
//        }
//
//        cell.lbTitle.text = workTask.UserName
        cell.lblContent.text = workTask.Name != nil ? workTask.Name : ""
        if  workTask.Name != nil {
            cell.lblBadge.text =  String(indexPath.row + 1)
        }
        
        cell.lbTitle.text = workTask.ProcedureWorkflowTemplateTasks![0].UserName
       
//        if workflowTask.ProcedureWorkflowTemplateSteps!.count > 0  {
//            let workTask = self.workflowTask.ProcedureWorkflowTemplateSteps![indexPath.row]
//            cell.lblContent.text = workTask.Name != nil ? workTask.Name : ""
//            if  workTask.Name != nil {
//                cell.lblBadge.text =  String(indexPath.row + 1)
//            }
//
//            cell.lbTitle.text = workTask.ProcedureWorkflowTemplateTasks![0].UserName
//        }
        
        
        
        
        
        return cell
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
}
