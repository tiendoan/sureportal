//
//  CustomWorkflowVC.swift
//  TaskManagement
//
//  Created by Mirum User on 6/19/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class CustomWorkflowVC: UIViewController {
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var changeAction: UIButton!
    @IBOutlet weak var tblView: UITableView!
    var arrayList:[UserWorkTaskObject]!
    var arrayRootList:[UserWorkTaskObject]!
    var userObjectSelected:UserWorkTaskObject!
    var delegate:CustomWorkflowDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchField.addTarget(self, action: #selector(CustomWorkflowVC.textFieldDidChange(_:)),
                            for: UIControlEvents.editingChanged)

        searchField.delegate = self
        self.tblView.tableFooterView = UIView()
        SyncProvider.getListUserTask(done: {(result) in
            self.arrayRootList = result?.data
         })

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.title = " "
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismissVC(_ sender: Any) {
        if self.navigationController != nil {
            self.navigationController?.popViewController()
        }
        else
        {
            self.dismiss(animated: false, completion: nil)
        }
    }

    @IBAction func changeCustomWF(_ sender: Any) {
        if (!(self.searchField.text?.isEmpty)! && self.userObjectSelected != nil) {
            if (( self.delegate) != nil)
            {
                self.delegate.replaceByCustomUserOjbect(userObject: self.userObjectSelected)
                if self.navigationController != nil {
                    self.navigationController?.popViewController()
                }
                else
                {
                    self.dismiss(animated: false, completion: nil)
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CustomWorkflowVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayList != nil ? self.arrayList.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CustomUserTaskCell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as! CustomUserTaskCell
        cell.lblTitle.text = (self.arrayList[indexPath.row].FullName)!
        return cell
        
    }
}
extension CustomWorkflowVC:UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.arrayList = self.arrayRootList.filter { ($0.FullName)?.uppercased() == textField.text?.uppercased()  }
        self.tblView.reloadData()
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.arrayList = self.arrayRootList.filter {
            (($0.FullName)?.uppercased().contains((textField.text?.uppercased())!))! ||
            (($0.UserName)?.uppercased().contains((textField.text?.uppercased())!))!
        }
        self.tblView.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        userObjectSelected = self.arrayList[indexPath.row];
        self.searchField.text = (userObjectSelected.FullName)!
        self.view.endEditing(true)
        
        
    }
    
}
