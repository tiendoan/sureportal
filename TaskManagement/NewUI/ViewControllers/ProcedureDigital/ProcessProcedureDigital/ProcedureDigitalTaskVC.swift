//
//  ProcedureDigitalTaskVC.swift
//  TaskManagement
//
//  Created by Mirum User on 6/28/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import KSTokenView
import IQKeyboardManagerSwift
import Toast_Swift
import SVProgressHUD
import DropDown


enum ProcedureStepType : String {
    case Serial = "Tuần tự", Parallel = "Song song", SimultaneousParallel = "Song song đồng thời"    
    static let allValues = [Serial, Parallel, SimultaneousParallel]
}

class ProcedureDigitalTaskVC: UIViewController,UITextFieldDelegate,CustomWorkflowDelegate {
    @IBOutlet weak var heightTop: NSLayoutConstraint!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblMessage: UITextField!
    var procedureDetail:ProdureDigital!
    @IBOutlet weak var avatarAuthor: UIImageView!
    var itemAction:ItemActions!
    @IBOutlet weak var heightComment: NSLayoutConstraint!
    @IBOutlet weak var tfChoose: UITextField!
    var replaceUser: UserWorkTaskObject!
    @IBOutlet weak var assignersTokenView: KSTokenView!
    let ASSIGNEES_TEXT_VIEW_PLACE_HOLDER: String = "người xử lý"
    let CATEGORIES_TOKEN_TAG: Int = 1
    let ASSIGNERS_TOKEN_TAG: Int = 2
    var employees: [Assignee] = Dummies.shared.getAllEmployees()
    var flatEmployees: [Assignee] = [Assignee]()
    var processors: [Assignee] = [Assignee]()
    var collaborators: [Assignee] = [Assignee]()
    var acknownledges: [Assignee] = [Assignee]()
    var addTokenFromSearchInput = true
    let taskDropDown: DropDown = DropDown()
    var selectedProcedureStepType:Int  = 0
    @IBOutlet weak var heightCommentAssign: UITextField!
    @IBOutlet weak var viewAssign: UIView!
    @IBOutlet weak var supperViewAssign: UIView!
    var arrayRootList:[UserProfile]!
    
    @IBOutlet weak var btnProgresss: UIButton!
    var arrayProcedureCategory: [ProcedureStepType] = [] {
        didSet {
            if !arrayProcedureCategory.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayProcedureCategory.map {
                    if let uName: String = ($0.rawValue).trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllEmployees()
        let url = URL(string: Constants.default.domainAddress + (procedureDetail.AuthorAvatar ?? ""))
        if url != nil {
            self.avatarAuthor.kf.setImage(with: url!, placeholder: UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
        } else {
            self.avatarAuthor.image = UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50))
        }
        self.lblSummary.text = self.procedureDetail.Summary!
        self.lblAuthor.text = self.procedureDetail.AuthorName!
        self.btnSend.setTitle(self.itemAction.Name!, for: .normal)
        self.btnSend.titleLabel?.adjustsFontSizeToFitWidth = true
        
        if (  self.itemAction.Code == "Assign"  )
        {
            heightTop.constant = 0
            heightComment.constant = 0
            self.initUI()            
        }
        else
        {
            viewAssign.isHidden = true
            heightCommentAssign.isHidden = true
            heightTop.constant = 0
            if ( self.itemAction.Code == "Delete" )
            {
                heightComment.constant = 0
            }
            if ( self.itemAction.Code == "Transfer" )
            {
                heightTop.constant = 85
            }
        }
        tfChoose.delegate = self

     
        // Do any additional setup after loading the view.
    }
    func initUI(){
        
        assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        assignersTokenView.promptText = ""
        assignersTokenView.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        assignersTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        assignersTokenView.delegate = self
        assignersTokenView.searchResultHeight = 120
        assignersTokenView.tag = ASSIGNERS_TOKEN_TAG
        assignersTokenView.returnKeyType(type: .done)
        assignersTokenView.shouldDisplayAlreadyTokenized = true
        assignersTokenView.maxTokenLimit = -1
        assignersTokenView.shouldSortResultsAlphabatically = false
        assignersTokenView.removesTokensOnEndEditing = false
        assignersTokenView.tokenizingCharacters = []
        _ = assignersTokenView.becomeFirstResponder()
        
        arrayProcedureCategory = ProcedureStepType.allValues
        taskDropDown.anchorView = self.btnProgresss
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnProgresss.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)

        self.btnProgresss.setTitle(ProcedureStepType.Serial.rawValue, for: .normal)

        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.btnProgresss.setTitle(item, for: .normal)
            self.selectedProcedureStepType = index
        }
        
        
    }
    
    @IBAction func changeProcedureType(_ sender: Any) {
        self.taskDropDown.show()
    }
    
    
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? Assignee, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
        self.navigationController?.popViewController()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.title = " "
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "moreSegue" {
            if let destinationVC = segue.destination as? CustomWorkflowVC {
                destinationVC.delegate = self
            }
        }
    }
    
    func replaceByCustomUserOjbect(userObject: UserWorkTaskObject) {
        self.replaceUser = userObject
        self.tfChoose.text = self.replaceUser.FullName!
    }    
    func replaceWorkflowAtIndex(indexReplace: Int, withCustomer: String) {
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if ( textField == self.tfChoose )
        {
            performSegue(withIdentifier: "moreSegue", sender: self)
            return false
        }
        return true
    }

    @IBAction func sendAction(_ sender: Any) {
        self.view.endEditing(true)
        if ( self.itemAction.Code! == "Delete" )  {
            SyncProvider.deleteProcedureDigital(Id: self.procedureDetail.ID!, done: {
                (result) in
                
                 self.view.makeToast("Cập nhật thành công")
                
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                     
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                        self.navigationController?.popToRootViewController(animated: false)
                        
                    }
            })
        }
        else if ( self.itemAction.Code! == "Assign" )
        {
            if ( self.heightCommentAssign.text!.isEmpty )
            {
                self.view.makeToast("Vui lòng nhập nội dung")
                return
            }
            else
            {
                if ( self.processors.count <= 0  )
                {
                    self.view.makeToast("Vui lòng chọn Người xử lý")
                    return
                }
                else
                {
                    var arrayUserAppen:[String] = []
                    let arrayUser:[String] = self.processors.map({ $0.text! })
                    for object in arrayUser
                    {
                        let objectUser = self.arrayRootList.filter({ $0.fullName == object })
                        if objectUser != nil
                        {
                            for item in objectUser
                            {
                                arrayUserAppen.append(item.id!)
                            }
                        }
                    }
                 
                    SyncProvider.assignProcedureDigital(Id: self.procedureDetail.ID!, procedureType: self.selectedProcedureStepType, comment: self.heightCommentAssign.text!, UserIDs: arrayUserAppen, done:
                        { (result) in

                            self.view.makeToast("Cập nhật thành công")

                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {

                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                               self.navigationController?.popToRootViewController(animated: false)

                            }
                    })
                    
                    
                }
            }
        }
        else
        {
           if ( self.lblMessage.text!.isEmpty ) {
                self.view.makeToast("Vui lòng nhập nội dung")
                return
            }
           else if ( self.itemAction.Code! == "Reject" || self.itemAction.Code! == "Return" || self.itemAction.Code! == "Transfer" || self.itemAction.Code! == "Audit" )
           {
            if ( self.itemAction.Code! == "Reject" )
                {
                    SyncProvider.rejectProcedureDigital(Id: self.procedureDetail.ID!, comment: self.lblMessage.text!, done:
                        { (result) in
                            
                               self.view.makeToast("Cập nhật thành công")
                            
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                    self.navigationController?.popToRootViewController(animated: false)
                            }
                    })
                }
                else  if ( self.itemAction.Code! == "Return" )
                {
                    SyncProvider.returnProcedureDigital(Id: self.procedureDetail.ID!, comment: self.lblMessage.text!, done:
                        { (result) in
                            
                                self.view.makeToast("Cập nhật thành công")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.navigationController?.popToRootViewController(animated: false)
                            }
                    })
                }
            else  if ( self.itemAction.Code! == "Audit" )
            {
                SyncProvider.auditProcedureDigital(Id: self.procedureDetail.ID!, comment: self.lblMessage.text!, done:
                    { (result) in
                        
                        self.view.makeToast("Cập nhật thành công")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                            self.navigationController?.popToRootViewController(animated: false)
                        }
                })
            }
                else
              {
                if ( self.tfChoose.text!.isEmpty ) {
                    self.view.makeToast("Vui lòng chọn người xử lý")
                    return
                }
                else
                {
                    SyncProvider.tranferProcedureDigital(Id: self.procedureDetail.ID!, UserId: self.replaceUser.ID!, comment: self.lblMessage.text!,done:
                        { (result) in
                            
                             self.view.makeToast("Cập nhật thành công")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.navigationController?.popToRootViewController(animated: false)
                            }
                    })
                }
                
              }

            }
        }
        
    }
    func getAllEmployees () {
        if self.itemAction.Code == "Assign" {
            
            self.employees = DataManager.shared.employees
            self.flatEmployees = DataManager.shared.flatEmployees
            self.arrayRootList = DataManager.shared.allUsers
            
            
//            SVProgressHUD.show()
//            SyncProvider.getAllUsersInDepartments { (assigners) in
//                if let lAssigners: [Assignee] = assigners {
//                    self.employees = lAssigners
//                    self.flatEmployees = Constants.default.recursiveFlatmap(list: lAssigners, true).sorted{ ($0.value ?? "") < ($1.value ?? "") }
//                } else {
//                    self.employees = Dummies.shared.getAllEmployees()
//                    self.flatEmployees = Dummies.shared.getAllEmployees()
//                }
//             SVProgressHUD.dismiss()
//            }
//            SyncProvider.getListUserTask(done: {(result) in
//                self.arrayRootList = result?.data
//            })
            
        }

    }

}

extension ProcedureDigitalTaskVC: KSTokenViewDelegate {
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            return completion!([])
        }
        var data: Array<AnyObject> = []
        var valueResults = [Assignee]()
        var textResults = [Assignee]()
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            let locale = Locale(identifier: "vi_VN")
            for item: Assignee in self.flatEmployees {
                guard item.isDept == false else {
                    continue
                }
                if let text = item.value, let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    valueResults.append(item)
                }else if let text = item.text?.folding(options: .diacriticInsensitive, locale: locale), let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    textResults.append(item)
                }
                data = valueResults.sorted{ ($0.value ?? "") < ($1.value ?? "") }
                data.append(contentsOf: textResults)
                //data = results
            }
        }
        return completion!(data)
    }
    
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            if let assignee = object as? Assignee,
                let name: String = assignee.text {
                return name
            } else {
                return "Chưa xác định"
            }
        }
        return object as! String
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            if let tokenObject: Assignee = token.object as? Assignee {
                token.tokenBackgroundColor = tokenObject.getBackgroundColorToTokenView()
            } else {
                token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
            }
        } else if tokenView.tag == CATEGORIES_TOKEN_TAG {
            token.tokenBackgroundColor = Theme.default.normalOrangeSelectedColor
        }
        return token
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? Assignee {
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = false
            user.isCollaborateRoleInTask = false
            
            if let index = self.processors.index(of: user) {
                self.processors.remove(at: index)
            }
            
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let user = token.object as? Assignee, addTokenFromSearchInput {
            self.removeAssignersTokenView(assignersTokenView, [user])
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
            
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = true
            user.isCollaborateRoleInTask = false
            
            self.processors.append(user)
        }
        self.view.endEditing(true)
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let assignee = object as? Assignee  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            return cell!
        }
        return UITableViewCell()
    }
}

