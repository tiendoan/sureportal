//
//  NewProcessProcedureDigitalVC.swift
//  TaskManagement
//
//  Created by Mirum User on 6/9/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewProcessProcedureDigitalVC: UIViewController,ProcedureDigitalDelegate,ProcedureDigitalDetailDelegate,ProcedureDigitalSignaturePositionDelegate {
    func parserSignaturePosition(signaturePosition: [ProcedurePositions]) {
        self.procedureAttachmentSigning = signaturePosition
    }
    
    func selectWorkFlowTemplate(Id: String) {
        self.ProcedureWorkflowTemplateId = Id
    }
    
    func selectCategoriesId(Id: String) {
        self.ProcedureCategory = Id
    }
    
    func selectDocumentId(attachment: ProcedureAttachmentInfos,ext: String) {
        if self.AttachmentNames.contains(where: { $0.Name == attachment.Name})
        {
            let tmpArray = self.AttachmentNames.filter({ $0.Name != attachment.Name })
            self.AttachmentNames = tmpArray
        }
        else
        {
            if ( ext == "pdf" || ext == "PDF" )
            {
                attachment.IsDigitalSign = true
            }
            self.AttachmentNames.append(attachment)
        }
    }
    
    func savetitleContent(title: String, content: String) {
        self.Name = title
        self.Summary = content
    }
    func loadAttachmentFile(attachFiles: [TrackingFileDocument]) {
        self.attachFiles = attachFiles
        self.isSignature = false
        for object in self.attachFiles {
            if ( (object.ext)! == "pdf" || (object.ext)! == "PDF" )
            {
                isSignature = true
            }
        }
        if self.attachFiles.count == 0 {
            isSignature = false
        }
        self.handleSegmentSignature()
    }
    
    
    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case threeChildTab = 2
    }
    @IBOutlet weak var lbDate: LVLabel!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblPositionAuthor: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var contenterView: UIView!
    var isSignature: Bool!
    
    var procedureAttachmentSigning:[ProcedurePositions] = []
    var attachFiles:[TrackingFileDocument] = [TrackingFileDocument]()
    var progressID:String!
    var AttachmentNames:[ProcedureAttachmentInfos] = [ProcedureAttachmentInfos]()
    var Name:String!
    var ProcedureCategory:String!
    var ProcedureWorkflowTemplateId:String!
    var Summary:String!
    
    var currentViewController: UIViewController?
    
    lazy var contentChildTabVC: UIViewController? = {
        let contentChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "NewAssingInfoDigitalVC")
        return contentChildTabVC
    }()
    lazy var progressChildTabVC : UIViewController? = {
        let progressChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "NewAssingProcedureDigitalVC")
        
        return progressChildTabVC
    }()

    
    lazy var newDigitalSigningVC : UIViewController? = {
        let newDigitalSigningVC = self.storyboard?.instantiateViewController(withIdentifier: "NewDigitalSigningVC")
        
        return newDigitalSigningVC
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        isSignature = false
        avatarImg.layer.cornerRadius = avatarImg.widthf/2
        avatarImg.layer.masksToBounds = true
        // Do any additional setup after loading the view.
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        self.lbDate.text = result
        self.loadData()
        
        
//        demo
        
    }
    fileprivate func loadData () {

        self.segmentControl.addUnderlineForSelectedSegment()
        displayCurrentTab(TabIndex.firstChildTab.rawValue)
        if let users: UserProfile = CurrentUser {
            
            let url = URL(string: Constants.default.domainAddress + (users.picture ?? ""))
            if url != nil {
                self.avatarImg.kf.setImage(with: url!, placeholder: UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
            } else {
                self.avatarImg.image = UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50))
            }
            
            if let fullName: String = users.fullName {
                lblAuthorName.text = fullName.uppercased()
            }
            if let jobTitle: String = users.jobTitle {
                lblPositionAuthor.text = jobTitle
            } else if let fName: String = users.fullName {
                lblPositionAuthor.text = fName
            }
        }
       
    }
    
    func handleSegmentSignature() {
        if self.isSignature {
         if ( self.segmentControl.numberOfSegments < 3 )
         {
            self.segmentControl.insertSegment(withTitle: "VÙNG KÝ", at: 2, animated: true)
            self.segmentControl.removeSubViewSegment()
            self.segmentControl.layoutIfNeeded()
            self.segmentControl.addUnderlineForSelectedSegment()

            }
        }
        else
        {
            if ( self.segmentControl.numberOfSegments > 2 )
            {
                self.segmentControl.removeSegment(at: 2, animated: true)
                self.segmentControl.removeSubViewSegment()
                self.segmentControl.layoutIfNeeded()
                self.segmentControl.addUnderlineForSelectedSegment()
            }
            
        }
   
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.title = " "
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentControl.changeUnderlinePosition()
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.contenterView.bounds
            self.contenterView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = (contentChildTabVC as! NewAssingInfoDigitalVC)
            (vc as! NewAssingInfoDigitalVC).delegate = self
        
        case TabIndex.secondChildTab.rawValue :
            vc = ( progressChildTabVC as! NewAssingProcedureDigitalVC)
            (vc as! NewAssingProcedureDigitalVC).progressIDProcedure = self.progressID
            (vc as! NewAssingProcedureDigitalVC).delegate = self
            
        case TabIndex.threeChildTab.rawValue:
              vc = ( newDigitalSigningVC as! NewDigitalSigningVC)
              (vc as! NewDigitalSigningVC).progressIDProcedure = self.ProcedureWorkflowTemplateId
             (vc as! NewDigitalSigningVC).attachFiles = self.attachFiles
              (vc as! NewDigitalSigningVC).delegate = self
            
        default:
            return nil
        }
        
        return vc
    }
    
    func selectProgressId(Id: String) {
        self.progressID = Id
        
        SyncProvider.GetProcedureWorkflowTemplatesByProcedureTypeId(Id: self.progressID, done: { (result) in
            let resultObject:ProcedureDigitalObject = (result as! ProcedureDigitalObject)
            if (resultObject.data?.count)! > 0
            {
                self.ProcedureWorkflowTemplateId = ((resultObject as ProcedureDigitalObject).data)![0].ID!
            }
        })
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func saveDrafaction(_ sender: Any) {
        self.view.endEditing(true)
          if self.Name != nil && self.Name != "" && self.Summary != nil  && self.Summary != "" && self.ProcedureWorkflowTemplateId != nil  {
            let title = self.Name
            let content = self.Summary

            SVProgressHUD.show()
            
            SyncProvider.addNewProcedure(AttachmentNames: self.AttachmentNames, IsDraft: true, Name: title!, Summary: content!, ProcedureCategory: self.ProcedureCategory, ProcedureType: self.progressID, ProcedureWorkflowTemplateId: self.ProcedureWorkflowTemplateId, ProcedureAttachmentSigningPositions: self.procedureAttachmentSigning, done: { (result) in
                SVProgressHUD.dismiss()
                if ( result?.status! == 1 )
                {
                    self.view.makeToast("Lưu thành công")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                         self.navigationController?.dismiss(animated: false, completion: nil)
                    }
                }
            })
        }
        else
        {
            self.view.makeToast("Vui lòng nhập tên, nội dung và tải tập tin đính kèm")
        }
        
    }
    @IBAction func sendAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if self.Name != nil && self.Name != "" && self.Summary != nil  && self.Summary != "" && self.ProcedureWorkflowTemplateId != nil  {
            let title = self.Name
            let content = self.Summary
            SVProgressHUD.show()
            
            SyncProvider.addNewProcedure(AttachmentNames: self.AttachmentNames, IsDraft: false, Name: title!, Summary: content!, ProcedureCategory: self.ProcedureCategory, ProcedureType: self.progressID, ProcedureWorkflowTemplateId: self.ProcedureWorkflowTemplateId, ProcedureAttachmentSigningPositions: self.procedureAttachmentSigning, done: { (result) in
                SVProgressHUD.dismiss()
                if ( result?.status! == 1 )
                {
                     self.view.makeToast("Lưu thành công")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                       
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                        self.dismiss(animated: false, completion: nil)
                        
                    }
                }
            })
        }
        else
        {
            self.view.makeToast("Vui lòng nhập tên, nội dung và tải tập tin đính kèm")
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {

        self.dismiss(animated: false, completion: nil)

    }
}
