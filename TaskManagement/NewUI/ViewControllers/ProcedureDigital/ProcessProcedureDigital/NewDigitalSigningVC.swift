//
//  NewDigitalSigningVC.swift
//  TaskManagement
//
//  Created by Mirum User on 6/19/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown
import WebKit
import SVProgressHUD

protocol ProcedureDigitalSignaturePositionDelegate {
    func parserSignaturePosition(signaturePosition:[ProcedurePositions])
}

class NewDigitalSigningVC: UIViewController,UITextFieldDelegate,UIScrollViewDelegate,WKNavigationDelegate {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var supperView: UIView!
    @IBOutlet weak var heightContentView: NSLayoutConstraint!
    @IBOutlet weak var checkSignAction: UIButton!
    @IBOutlet weak var infoSign: UIButton!
    @IBOutlet weak var titleSign: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrolview: UIScrollView!
    @IBOutlet weak var signatureFile: UIButton!
    @IBOutlet weak var signatureName: UIButton!
    var signatureView:UIStackView!
    var totalPage:Int!
    let taskDropDown: DropDown = DropDown()
    var signatureUsr:[SinagureDigitalReponse]!
    var progressIDProcedure:String!
    @IBOutlet weak var currentPage: UITextField!
    @IBOutlet weak var lblTotalPage: UILabel!
    var selectedFile:TrackingFileDocument!
    var selectedSignature:SinagureDigitalReponse!
    var delegate:ProcedureDigitalSignaturePositionDelegate!
    var ProcedureAttachmentSigningPositions:[ProcedurePositions] = []
    
    var arrayProcedureCategory: [SinagureDigitalReponse] = [] {
        didSet {
            if !arrayProcedureCategory.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayProcedureCategory.map {
                    if let uName: String = ($0.FullName)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
   let taskDropDownFile: DropDown = DropDown()
    var attachFiles: [TrackingFileDocument] = [] {
        didSet {
            if !attachFiles.isEmpty {
                taskDropDownFile.dataSource.removeAll()
                taskDropDownFile.dataSource = attachFiles.map {
                    if let uName: String = ($0.name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.totalPage = 10
        self.currentPage.delegate = self
        taskDropDown.anchorView = self.signatureName
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.signatureName.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.signatureName.setTitle(item, for: .normal)
            self.selectedSignature = self.signatureUsr[index]
        }
        
        
        
        taskDropDownFile.anchorView = self.signatureFile
        taskDropDownFile.textColor = UIColor.gray
        taskDropDownFile.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDownFile.bottomOffset = CGPoint(x: 0, y: self.signatureFile.bounds.height + 10)
        taskDropDownFile.isMultipleTouchEnabled = false
        taskDropDownFile.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDownFile.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDownFile.selectionAction = { [unowned self] (index, item) in
            self.selectedFile = self.attachFiles[index]
            self.signatureFile.setTitle(item, for: .normal)
            self.reloadAll()
        }

        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(tapAction))
        self.imgView.isUserInteractionEnabled = true
        self.imgView.addGestureRecognizer(tapGestureRecognizer)
        
//        self.loadFieldDemo()
        // Do any additional setup after loading the view.
    }
    
    func tapAction(sender: UITapGestureRecognizer) {
        
        let touchPoint = sender.location(in: self.imgView) // Change to whatever view you want the point for
        print(touchPoint)
        self.handleSignaturePosition(postX: Int(touchPoint.x), postY: Int(touchPoint.y))
    }
    fileprivate func loadField(){
        
        let attachmentDownload = String(format: "%@/mobileapis/ImageAttach.ashx?AttachmentPreview=%@%@",Router.baseURLString,self.selectedFile.id!, "_" + self.currentPage.text! +  ".png")
        let url = URL(string: attachmentDownload)

        if let imageSource = CGImageSourceCreateWithURL(url! as CFURL, nil) {
            if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                let pixelWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                let pixelHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                
                print("the image width is: \(pixelWidth)")
                print("the image height is: \(pixelHeight)")
                
                let ratio:CGFloat = CGFloat(612)/CGFloat(792)
                
                let screenSize = UIScreen.main.bounds
                let screenWidth = screenSize.width
                let screenHeight = screenSize.height
                
                heightContentView.constant = CGFloat(screenWidth/CGFloat(ratio))
                self.imgView.setImage(url: url!)
                self.supperView.layoutIfNeeded()
                
            }
        }
        
        self.loadDataSignature()
        
    }
    
    func loadProgress() {
        if ( self.progressIDProcedure == nil || self.progressIDProcedure.isEmpty ) {
            return
        }
        SyncProvider.GetListUserInWorkflowTemplateSignWithDigitalSignature(Id: self.progressIDProcedure, done: {
            (result) in
            if ( ((result as! SinagureReponse).userSignautre?.count)! > 0  )
            {
                self.signatureUsr = (result as! SinagureReponse).userSignautre
                self.signatureName.setTitle((self.signatureUsr[0].FullName)!, for: .normal)
                self.arrayProcedureCategory = (result as! SinagureReponse).userSignautre!
                self.selectedSignature = self.signatureUsr[0]
            }            
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadProgress()
        if ( self.attachFiles != nil && self.attachFiles.count > 0 )
        {
            self.attachFiles = self.attachFiles.filter({  $0.ext! == "pdf" || $0.ext! == "PDF"  })
            self.selectedFile = self.attachFiles[0]
            self.signatureFile.setTitle(self.selectedFile.name, for: .normal)
            
            SyncProvider.getCountPage(Name: (self.selectedFile.id)! + "." + (self.selectedFile.ext)!, done: {
                (result) in
                if ( result != nil  )
                {
                    self.totalPage = (result as! CountPageNumb).totalPage
                    self.lblTotalPage.text =  "/" + String(self.totalPage)
                    self.loadAttachemntPrev()

                }
                
            })
       
        }
    }
    func reloadAll(){
        
        SyncProvider.getCountPage(Name: (self.selectedFile.id)! + "." + (self.selectedFile.ext)!, done: {
            (result) in
            if ( result != nil )
            {
                self.totalPage = (result as! CountPageNumb).totalPage
                self.lblTotalPage.text =  "/" + String(self.totalPage)
                self.currentPage.text = String(format: "%d", 1)
                self.loadAttachemntPrev()

            }
            
        })
    }
    func loadAttachemntPrev() {
        let name = (self.selectedFile.id)! + "." + (self.selectedFile.ext)!
        let attach = String(format: "%@/mobileapis/ImageAttach.ashx?AttachmentPreview=%@%@",Router.baseURLString,self.selectedFile.id!, "_" + self.currentPage.text! +  ".png" )
        SVProgressHUD.show()
        SyncProvider.checkAttachment(Name: name, Attach: attach, done: {(result) in
              SVProgressHUD.dismiss()
            self.loadField()
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func userTap(_ sender: Any) {
        taskDropDown.show()
    }
    
    @IBAction func fileTap(_ sender: Any) {
        taskDropDownFile.show()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.loadAttachemntPrev()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.supperView.layoutIfNeeded()
        
    }
    @IBAction func nextAction(_ sender: Any) {
        var number:Int = Int(self.currentPage.text!)!
        if( number < self.totalPage) {
            number  = number + 1
            self.currentPage.text = String(format: "%d", number)
            self.loadAttachemntPrev()
        }

        
    }
    
    @IBAction func preAction(_ sender: Any) {
        
        var number:Int = Int(self.currentPage.text!)!
        if( number > 1) {
            number  = number - 1
            self.currentPage.text = String(format: "%d", number)
            self.loadAttachemntPrev()

        }
        
    }
    @IBAction func signAction(_ sender: Any) {

            self.titleSign.isSelected = !self.titleSign.isSelected
        
    }
  
    @IBAction func infoAction(_ sender: Any) {
        self.infoSign.isSelected = !self.infoSign.isSelected
    }
    
    @IBAction func checkSign(_ sender: Any) {
        self.checkSignAction.isSelected = !self.checkSignAction.isSelected
    }
    func handleSignaturePosition(postX: Int,postY: Int){
        
        if ( self.titleSign.isSelected || self.infoSign.isSelected || self.checkSignAction.isSelected )
        {
            let positionObject:ProcedurePositions = ProcedurePositions()
            positionObject.AttachmentID = self.selectedFile != nil ? self.selectedFile.id!: ""
            positionObject.PageNumber = Int(self.currentPage.text!)!
            positionObject.SignerID = (self.selectedSignature.ID)!
            positionObject.orX = postX
            positionObject.orY = postY
            
            var tmpX = 0
            var tmpY = 0
            var totalSelect = 0;
            if self.titleSign.isSelected
            {
                totalSelect += 1
            }
            if self.infoSign.isSelected
            {
                totalSelect += 1
            }
            if self.checkSignAction.isSelected
            {
                totalSelect += 1
            }
            
            if ( totalSelect == 3 )
            {
                tmpX = postX + 160
                tmpY = postY + 50
            }
            else if ( totalSelect == 2  )
            {
                tmpX = postX + 125
                tmpY = postY + 50
            }
            else
            {
                tmpX = postX + 110
                tmpY = postY + 50
            }
            
            positionObject.HasSignature = self.titleSign.isSelected
            positionObject.HasInfo = self.infoSign.isSelected
            positionObject.HasStamp = self.checkSignAction.isSelected
//            positionObject.CoordinateX = (tmpX * 612)/Int(self.imgView.frame.size.width)
//            positionObject.CoordinateY = (tmpY * 792)/Int(self.imgView.frame.size.height)
            positionObject.CoordinateX = tmpX
            positionObject.CoordinateY = tmpY
            
            if ProcedureAttachmentSigningPositions.count > 0{
                let indexNum:Int = Int(self.currentPage.text!)!
                let index = ProcedureAttachmentSigningPositions.index{$0.PageNumber == indexNum}
                
                if ( index != nil )
                {
                    ProcedureAttachmentSigningPositions[index!] = positionObject
                }
                else
                {
                    ProcedureAttachmentSigningPositions.append(positionObject)
                }
            }
            else
            {
                ProcedureAttachmentSigningPositions.append(positionObject)
            }
            if (( self.delegate ) != nil)
            {
                self.delegate.parserSignaturePosition(signaturePosition: ProcedureAttachmentSigningPositions)
            }
            self.loadDataSignature()
        }
        
    }
    func loadDataSignature(){
        if ( signatureView != nil ) {
            signatureView.removeFromSuperview()
        }
        
        if ProcedureAttachmentSigningPositions.count > 0{
            let indexNum:Int = Int(self.currentPage.text!)!
            let index = ProcedureAttachmentSigningPositions.index{$0.PageNumber == indexNum}
            
            if ( index != nil )
            {
                var heightImage = 150
                var totalSelect = 0
                let objectPosition = ProcedureAttachmentSigningPositions[index!]
                if objectPosition.HasSignature
                {
                    totalSelect += 1
                }
                if objectPosition.HasStamp
                {
                    totalSelect += 1
                }
                if objectPosition.HasInfo
                {
                    totalSelect += 1
                }
                if (totalSelect == 3)
                {
                    heightImage = 250
                }
                else if (totalSelect == 2)
                {
                    heightImage = 200
                }
                else
                {
                    heightImage = 100
                }
                
                
//                signatureView = UIImageView(frame: CGRect(x: objectPosition.orX, y: objectPosition.orY, width: heightImage, height: 70))
                signatureView = UIStackView(frame: CGRect(x: objectPosition.orX, y: objectPosition.orY, width: heightImage, height: 70))
                let uiView =  UIView(frame: CGRect(x: 0, y: 0, width: self.signatureView.frame.width, height: 70))
                if ( totalSelect == 3 )
                {
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                    label.font = UIFont.boldSystemFont(ofSize: 10)
                    label.text = "Chữ ký Tổng Giám Đốc - Nguyễn Hoàng Nam"
                    label.numberOfLines = 0
                    uiView.addSubview(label)
                    
                    let imgStam =  UILabel(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                    imgStam.font = UIFont.boldSystemFont(ofSize: 10)
                    imgStam.textColor = UIColor.red
                    imgStam.numberOfLines = 0
                    imgStam.text = "Con dấu Tổng Giám Đốc - Nguyễn Hoàng Nam"

                    uiView.addSubview(imgStam)
                    
                    let imgSig = UILabel(frame: CGRect(x: 160, y: 0, width: 70, height: 70))
                    imgSig.font = UIFont.boldSystemFont(ofSize: 10)
                    imgSig.numberOfLines = 0
                    imgSig.textColor = UIColor.blue
                    imgSig.text = "Thông tin Tổng Giám Đốc - Nguyễn Hoàng Nam"
                    
                    uiView.addSubview(imgSig)
                    
                }
                else if ( totalSelect == 2 )
                {
                    
                    if ( objectPosition.HasSignature && objectPosition.HasStamp )
                    {
                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                        label.font = UIFont.boldSystemFont(ofSize: 10)
                        label.text = "Chữ ký Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        label.numberOfLines = 0
                        uiView.addSubview(label)
                        
                        let imgStam =  UILabel(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                        imgStam.font = UIFont.boldSystemFont(ofSize: 10)
                        imgStam.numberOfLines = 0
                        imgStam.textColor = UIColor.red
                        imgStam.text = "Con dấu Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        
                        uiView.addSubview(imgStam)
                    }
                    else if ( objectPosition.HasSignature && objectPosition.HasInfo )
                    {
                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                        label.font = UIFont.boldSystemFont(ofSize: 10)
                        label.text = "Chữ ký Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        label.numberOfLines = 0
                        uiView.addSubview(label)
                        
                        let imgStam =  UILabel(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                        imgStam.font = UIFont.boldSystemFont(ofSize: 10)
                        imgStam.numberOfLines = 0
                        imgStam.textColor = UIColor.blue
                        imgStam.text = "Thông tin Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        
                        uiView.addSubview(imgStam)
                    }
                    else
                    {
                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                        label.font = UIFont.boldSystemFont(ofSize: 10)
                        label.text = "Chữ ký Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        label.numberOfLines = 0
                        uiView.addSubview(label)
                        
                        let imgStam =  UILabel(frame: CGRect(x: 80, y: 0, width: 70, height: 70))
                        imgStam.font = UIFont.boldSystemFont(ofSize: 10)
                        imgStam.numberOfLines = 0
                         imgStam.textColor = UIColor.blue
                         imgStam.text = "Thông tin Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        
                        uiView.addSubview(imgStam)
                    }
                }
                else
                {
                    if ( objectPosition.HasSignature )
                    {
                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                        label.font = UIFont.boldSystemFont(ofSize: 10)
                        label.text = "Chữ ký Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        label.numberOfLines = 0
                        uiView.addSubview(label)
                        
                    }
                    else if ( objectPosition.HasStamp )
                    {
                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                        label.font = UIFont.boldSystemFont(ofSize: 10)
                        label.text = "Con dấu Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        label.numberOfLines = 0
                         label.textColor = UIColor.red
                        uiView.addSubview(label)
                        
                    }
                    else
                    {
                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                        label.font = UIFont.boldSystemFont(ofSize: 10)
                        label.text = "Thông tin Tổng Giám Đốc - Nguyễn Hoàng Nam"
                        label.numberOfLines = 0
                        label.textColor = UIColor.blue
                        uiView.addSubview(label)
                        
                      
                    }
                }
                signatureView.addArrangedSubview(uiView)
                self.imgView.addSubview(signatureView)
                
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
