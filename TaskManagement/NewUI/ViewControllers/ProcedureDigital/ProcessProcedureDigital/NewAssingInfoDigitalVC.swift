//
//  NewAssingInfoDigitalVC.swift
//  TaskManagement
//
//  Created by Mirum User on 6/10/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import KSTokenView
import IQKeyboardManagerSwift
import Toast_Swift
import ObjectMapper
import SVProgressHUD
import DropDown

protocol ProcedureDigitalDelegate {
    func selectProgressId(Id:String)
    func selectCategoriesId(Id:String)
    func selectDocumentId(attachment:ProcedureAttachmentInfos,ext: String)
    func savetitleContent(title:String,content:String)
    func loadAttachmentFile(attachFiles:[TrackingFileDocument])
}
var Timestamp: String {
    return "\(NSDate().timeIntervalSince1970 * 1000)"
}
class NewAssingInfoDigitalVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIDocumentMenuDelegate, UIDocumentPickerDelegate,UITextFieldDelegate {
   
     let imagePicker = UIImagePickerController()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    var attachFiles:[TrackingFileDocument] = [TrackingFileDocument]()
    @IBOutlet weak var attachmentFile: UIStackView!

    @IBOutlet weak var btnProcess: UIButton!
    @IBOutlet weak var btnMajor: UIButton!
    var selectedMajor:String!
    var selectedProcess:String!
    var delegate: ProcedureDigitalDelegate!
    let documentInteractionController = UIDocumentInteractionController()
    @IBOutlet weak var lblTitle: UITextField!
    @IBOutlet weak var lblContent: UITextField!
    let taskDropDown: DropDown = DropDown()

    var arrayProcedureCategory: [ProcedureDataObject] = [] {
        didSet {
            if !arrayProcedureCategory.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayProcedureCategory.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    let taskDropDownProcess: DropDown = DropDown()
   
    var arrayProcedureCategoryData: [ProcedureDataObject] = []  {
        didSet {
            if !arrayProcedureCategoryData.isEmpty {
                taskDropDownProcess.dataSource.removeAll()
                taskDropDownProcess.dataSource = arrayProcedureCategoryData.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var lblHr: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.delegate = self
        self.lblContent.delegate = self
        taskDropDown.anchorView = self.btnMajor
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedMajor = (self.arrayProcedureCategory[index].ID)!
            self.btnMajor.setTitle(item, for: .normal)
             self.loadProcessData(categoryId: self.selectedMajor)
            self.delegate.selectCategoriesId(Id: self.selectedMajor)
        }
        
        
        
        taskDropDownProcess.anchorView = self.btnProcess
        taskDropDownProcess.textColor = UIColor.gray
        taskDropDownProcess.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDownProcess.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskDropDownProcess.isMultipleTouchEnabled = false
        taskDropDownProcess.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDownProcess.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        
        taskDropDownProcess.selectionAction = { [unowned self] (index, item) in
            self.selectedProcess = (self.arrayProcedureCategoryData[index].ID)!
            self.btnProcess.setTitle(item, for: .normal)
            self.delegate.selectProgressId(Id: self.selectedProcess)
        }
         self.loadMajorData()
        // Do any additional setup after loading the view.
    }
    
    func loadMajorData()  {
        SyncProvider.getProcedureCategories(done: { (result) in
            let resultObject:ProcedureDigitalObject = (result as! ProcedureDigitalObject)
            if (resultObject.data?.count)! > 0
            {
                self.arrayProcedureCategory = (result?.data)!
                if self.arrayProcedureCategory.count > 0
                {
                    self.selectedMajor = (self.arrayProcedureCategory[0].ID)!
                    self.btnMajor.setTitle((self.arrayProcedureCategory[0].Name)!, for: .normal)
                    self.delegate.selectCategoriesId(Id: self.selectedMajor)
                    self.loadProcessData(categoryId: self.selectedMajor)
                    
                }
            }
        })
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 700)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func loadProcessData(categoryId: String) {
        
        SyncProvider.getProcedureTypesByProcedureCategoryID(Id: categoryId, done: { (result) in
            let resultObject:ProcedureDigitalObject = (result as! ProcedureDigitalObject)
            if (resultObject.data?.count)! > 0
            {
                self.arrayProcedureCategoryData = (result?.data)!
                if self.arrayProcedureCategoryData.count > 0
                {
                    self.selectedProcess = (self.arrayProcedureCategoryData[0].ID)!
                    self.btnProcess.setTitle((self.arrayProcedureCategoryData[0].Name)!, for: .normal)
                    if (( self.delegate ) != nil)
                    {
                        self.delegate.selectProgressId(Id: self.selectedProcess)
                    }
                    
                }
            }
        })
    }
    
    
    
    
    @IBAction func captureAction(_ sender: Any) {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
  
//            imagePicker.sourceType = .camera
//            imagePicker.allowsEditing = false
//            imagePicker.delegate = self
//            present(imagePicker, animated: true, completion: nil)
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            present(imagePicker, animated: true, completion: nil)

        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
     func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
//        let url = info[UIImagePickerControllerReferenceURL] as! NSURL
        let attachment = Mapper<TrackingFileDocument>().map(JSON:[:])!
//        attachment.name = url.lastPathComponent
//        attachment.ext = url.pathExtension
        attachment.name = Timestamp + ".JPG"
        attachment.ext = "JPG"
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let data = UIImageJPEGRepresentation(image,0.7)
        
        if let fileData = data {
            attachment.size = fileData.count
            self.view.makeToast("Đang quá trình upload.")
            SVProgressHUD.show()
            UploadManager.uploadFile(fileData, type: "JPG", updateHandler: { (percent) in
                print("Percent: \(percent)")
            }) { ( fileID, error) in
                if let id = fileID?.split(separator: ".").first {
                    attachment.id = String(id)
                    let attachmentInfo = ProcedureAttachmentInfos()
                    attachmentInfo.Name = attachment.id! + "." + attachment.ext!
                    self.delegate.selectDocumentId(attachment: attachmentInfo,ext: attachment.ext!)
                    self.addAttachFilesView(file: attachment)
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Upload thành công, bạn có thể tiếp tục bổ sung thông tin khác.")
                } else {
                    self.view.makeToast("Upload thất bại!")
                }
            }
        }
        picker.dismiss(animated: false, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func uploadAction(_ sender: Any) {
        
        let importMenu = UIDocumentMenuViewController(documentTypes: ["public.content","public.item"], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        
    }
    
    
    @IBAction func createNewAction(_ sender: Any) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func majorAction(_ sender: Any) {
        taskDropDown.show()
    }
    
    @IBAction func processAction(_ sender: Any) {
        taskDropDownProcess.show()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
  
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        debugPrint("The Url is : \(cico)")
        
        let attachment = Mapper<TrackingFileDocument>().map(JSON:[:])!
        attachment.name = url.lastPathComponent
        attachment.ext = url.pathExtension
        if let fileData = try? Data(contentsOf: URL(fileURLWithPath: url.path)) {
            attachment.size = fileData.count
            self.view.makeToast("Đang quá trình upload.")
            SVProgressHUD.show()
            UploadManager.uploadFile(fileData, type: url.pathExtension, updateHandler: { (percent) in
                print("Percent: \(percent)")
            }) { ( fileID, error) in
                if let id = fileID?.split(separator: ".").first {
                    attachment.id = String(id)
                    
                    let attachmentInfo = ProcedureAttachmentInfos()
                    attachmentInfo.Name = attachment.id! + "." + attachment.ext!
                    self.delegate.selectDocumentId(attachment: attachmentInfo,ext: attachment.ext!)
                    
                    self.addAttachFilesView(file: attachment)
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Upload thành công, bạn có thể tiếp tục bổ sung thông tin khác.")
                } else {
                    self.view.makeToast("Upload thất bại!")
                }
            }
        }
    }
    func addAttachFilesView(file:TrackingFileDocument) {
        let fileView = AttachFileView.instanceFromNib()
        fileView.delegate = self
        fileView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        fileView.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        fileView.updateFileInfo(f: file)
        self.attachmentFile.addArrangedSubview(fileView)
        self.attachFiles.append(file)
        self.updateAttchFileView()
     
    }
    func updateAttchFileView() {
        attachmentFile.isHidden = self.attachFiles.isEmpty
        containerView.setNeedsLayout()
        containerView.setNeedsUpdateConstraints()
        containerView.layoutIfNeeded()
        if (self.delegate != nil) {
            self.delegate.loadAttachmentFile(attachFiles: self.attachFiles)
        }
    }
    
    func deleteAttachFile(fileView: AttachFileView) {
        if let file = fileView.file, let index = self.attachFiles.index(where: {$0.id == file.id}) {
            let attachmentInfo = ProcedureAttachmentInfos()
            attachmentInfo.Name = file.id! + "." + file.ext!
            self.delegate.selectDocumentId(attachment: attachmentInfo,ext: file.ext!)
            self.attachFiles.remove(at: index)
        }
        fileView.removeSubviews()
        self.updateAttchFileView()
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate.savetitleContent(title: self.lblTitle.text!, content: self.lblContent.text!)
    }

}
extension NewAssingInfoDigitalVC: AttachFileViewDelegate {
    func didDeleteAttachFile(_ fileView: AttachFileView) {
        self.deleteAttachFile(fileView: fileView)
    }
}

