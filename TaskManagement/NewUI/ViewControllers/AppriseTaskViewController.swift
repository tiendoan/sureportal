//
//  AppriseTaskViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import DLRadioButton
import DropDown
import ObjectMapper
import SVProgressHUD

protocol AppriseTaskViewDelegate {
  func appriseCloseView()
  func appriseDoneView(status: Bool, message: String?, index: IndexPath?)
}

class AppriseTaskViewController: UIViewController {
  
  // MARK: define variables.
  // --------------------------------
  @IBOutlet weak var attachmentStackView: UIStackView!
  @IBOutlet weak var fullNameLabel: UILabel!
  @IBOutlet weak var jobTitleLabel: UILabel!
  @IBOutlet weak var avatarLabel: UIImageView!
  @IBOutlet weak var statusLabel: LVLabel!
  @IBOutlet weak var chooseTaskButton: UIButton!
  @IBOutlet weak var fromDateLabel: UILabel!
  @IBOutlet weak var toDateLabel: UILabel!
  @IBOutlet weak var fullNameAssignToLabel: UILabel!
  @IBOutlet weak var jobTitleAssignToLabel: UILabel!
  @IBOutlet weak var fromDateSecondLabel: UILabel!
  @IBOutlet weak var toDateSecondLabel: UILabel!
  @IBOutlet weak var percentFinishLabel: UITextField!
  @IBOutlet weak var opinionTextView: UITextView!
  @IBOutlet weak var refundDLRadioButton: DLRadioButton!
  @IBOutlet weak var cancelDLRadioButton: DLRadioButton!
  //@IBOutlet weak var confirmDLRadioButton: DLRadioButton!
  @IBOutlet weak var ratingTypeStackView: UIStackView!
    @IBOutlet weak var ratingType2StackView: UIStackView!
  @IBOutlet weak var assignByNameAvatar: UIImageView!
    @IBOutlet weak var attachFileButton: UIButton!
    
  var delegate: AppriseTaskViewDelegate?
  var document: Document?
  var indexPath: IndexPath?
  var startDate: Date?
  var endDate: Date?
  
  let PERCENT_FINISH_PLACE_HOLDER: String = "0"
  let DEFAULT_SELECTED_IND: Int = 0
  let DISMISS_ACTION_TAG: Int = 1
  let PERCENT_FINISH_TXT_TAG: Int = 2
  let SEND_ACTION_TAG: Int = 2
  let RETURN_ACTION_TAG: Int = 3
  let PAGE_TITLE: String = "Đánh giá"
  let HIDE_KB_TITLE: String = "Ẩn bàn phím"
  let OPINION_TXT_TAG: Int = 1
  let OPINION_PLACE_HOLDER: String = "Ý kiến đánh giá"
  
  var defaultSelectedIDOfType: Int = 80
  var selectedStatusValue: String?
  var attachFiles:[TrackingFileDocument] = [TrackingFileDocument]()
    
  var selectedAppriseTracking: ProcessTracking? {
    didSet {
      // Reload apprise tracking types
      if selectedAppriseTracking != nil {
        self.bindingDataInView()
      }
    }
  }
  
    //var progressTrackingID: 
  var typesDLRadioButtonGroups: [DLRadioButton] = [DLRadioButton] ()
  var statusDLRadioButtonGroups: [DLRadioButton] = [DLRadioButton] ()
  let taskDropDown: DropDown = DropDown()
  
  var appriseTrackingTypes: [StatusDocument]? {
    didSet {
      if appriseTrackingTypes != nil {
        self.setupStatusDLRadioButtonGroups()
      }
    }
  }
  
    var seletedProgressID: String?
  var appriseTrackings: [ProcessTracking] = [ProcessTracking] () {
    didSet {
      if !appriseTrackings.isEmpty {
        self.selectedAppriseTracking = appriseTrackings[safe: DEFAULT_SELECTED_IND]
        if let seletedProgressID = seletedProgressID, let index = self.appriseTrackings.index(where:{$0.ID == seletedProgressID}) {
            self.selectedAppriseTracking = self.appriseTrackings[safe: index]
        }
        
        taskDropDown.dataSource.removeAll()
        taskDropDown.dataSource = appriseTrackings.map {
          if let uName: String = $0.name?.trimmingCharacters(in: .whitespacesAndNewlines) {
            return uName
          } else {
            return "Chưa xác định"
          }
        }
        self.loadReadyDataInView()
      } else {
        //self.loadDefaultDataInView()
        self.loadReadyDataInView()
      }
    }
  }
  
  // MARK: init, load data functions
  // -----------------------------------
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.getAppriseTrackings()
    self.setupComponents()
  }
  
  fileprivate func setupComponents() {
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = HIDE_KB_TITLE
    
    //self.setTitleNavigationBar(PAGE_TITLE)
    self.setupDefaultNavigationBar()
    self.showBack()
    
    var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()

    
    /// Approve button
    let btnApprove = UIButton(type: .custom)
    btnApprove.setTitle("Đồng ý", for: .normal)
    btnApprove.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnApprove.tintColor = Theme.default.normalWhiteColor
    btnApprove.addTarget(self, action: #selector(touchUpInsideAction(_:)), for: .touchUpInside)
    btnApprove.backgroundColor = UIColor(hexString: "#17C209")
    btnApprove.cornerRadius = Theme.default.normalCornerRadius
    let approveRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnApprove)
    rightButtons.append(approveRightButton)

//    /// Reject button
//    let btnReject = UIButton(type: .custom)
//    btnReject.setTitle("Không đồng ý", for: .normal)
//    btnReject.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
//    btnReject.tintColor = Theme.default.normalWhiteColor
//    btnReject.addTarget(self, action: #selector(rejectAction(_:)), for: .touchUpInside)
//    btnReject.backgroundColor = UIColor(hexString: "#17C209")
//    btnReject.cornerRadius = Theme.default.normalCornerRadius
//    let rejectRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnReject)
//    rightButtons.append(rejectRightButton)

    /// Attach button
    let btnAttach = UIButton(type: .custom)
    btnAttach.setTitle("Đ.Kèm", for: .normal)
    btnAttach.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnAttach.tintColor = Theme.default.normalWhiteColor
    btnAttach.addTarget(self, action: #selector(attachFilesButton(_:)), for: .touchUpInside)
    btnAttach.backgroundColor = UIColor(hexString: "#17C209")
    btnAttach.cornerRadius = Theme.default.normalCornerRadius
    let attachRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnAttach)
    rightButtons.append(attachRightButton)
    
    self.addRightButtonNavigationBar(buttons: rightButtons)
    
    chooseTaskButton.setupArrowDropdownInRight()
    chooseTaskButton.showsTouchWhenHighlighted = true
    chooseTaskButton.tintColor = Theme.default.normalWhiteColor
    
    taskDropDown.anchorView = chooseTaskButton
    taskDropDown.textColor = UIColor.gray
    taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
    taskDropDown.bottomOffset = CGPoint(x: 0, y: chooseTaskButton.bounds.height + 10)
    taskDropDown.isMultipleTouchEnabled = false
    taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
    
    taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        guard let cell = cell as? CustomDropdownCell else { return }
        
        // Setup your custom UI components
        cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
    }
    taskDropDown.selectionAction = { [unowned self] (index, item) in
      self.selectedAppriseTracking = self.appriseTrackings[safe: index]
      self.chooseTaskButton.setTitle(item, for: .normal)
      self.loadReadyDataInView()
    }
    
    opinionTextView.tag = OPINION_TXT_TAG
    opinionTextView.text = OPINION_PLACE_HOLDER
    opinionTextView.textColor = UIColor.lightGray
    opinionTextView.delegate = self
    
    percentFinishLabel.tag = PERCENT_FINISH_TXT_TAG
    percentFinishLabel.delegate = self
    percentFinishLabel.addSubviews(percentFinishLabel.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    
    refundDLRadioButton.contentHorizontalAlignment = .left
    cancelDLRadioButton.contentHorizontalAlignment = .left
    //confirmDLRadioButton.contentHorizontalAlignment = .left
    statusDLRadioButtonGroups.append(contentsOf: [refundDLRadioButton])
    
    attachmentStackView.isHidden = true
    attachFileButton.isHidden = true
  }
  
  fileprivate func bindingDataInView() {
    if let selectedApprise: ProcessTracking = self.selectedAppriseTracking {
      if let fullName: String = selectedApprise.assignByName {
        fullNameLabel.text = fullName
      }
      self.getListTrackingRatings()
    }
  }
  
  fileprivate func loadStartEndDays(_ startDate: Date, _ endDate: Date) {
    fromDateSecondLabel.text = startDate.toFormatDate(format: Constants.FULL_DATETIME_FORMAT)
    toDateSecondLabel.text = endDate.toFormatDate(format: Constants.FULL_DATETIME_FORMAT)
  }
  
  fileprivate func loadDefaultDataInView() {
    if let uCurrentUser: UserProfile = CurrentUser {
      if let fName: String = uCurrentUser.fullName {
        fullNameLabel.text = fName
      }
      if let jTitle: String = uCurrentUser.jobTitle {
        jobTitleLabel.text = jTitle
      }
    }
    startDate = Constants.currentDate
    endDate = startDate?.adding(.day, value: 1)
    fromDateLabel.text = Constants.formatDate(startDate)
    toDateLabel.text = Constants.formatDate(endDate)
    loadStartEndDays(startDate!, endDate!)
  }
  
  fileprivate func resetView() {
    fromDateLabel.text = Constants.formatDate(startDate)
    toDateLabel.text = Constants.formatDate(endDate)
    if let uStartDate = self.startDate, let eEndDate = self.endDate {
      self.loadStartEndDays(uStartDate, eEndDate)
    }
    opinionTextView.text = OPINION_PLACE_HOLDER
    percentFinishLabel.text = PERCENT_FINISH_PLACE_HOLDER
    refundDLRadioButton.isSelected = false
    cancelDLRadioButton.isSelected = false
    //confirmDLRadioButton.isSelected = false
  }
  
  fileprivate func loadReadyDataInView() {
    if let processTracking: ProcessTracking = self.selectedAppriseTracking {
      self.resetView()
      if let fullName: String = processTracking.assignByName {
        fullNameAssignToLabel.text = fullName
      }
        if let avatar = processTracking.assignByPicture, let url = URL(string: Constants.default.domainAddress + avatar){
            self.avatarLabel.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
      if let jobTitle: String = processTracking.assignByJobTitle {
        jobTitleLabel.text = jobTitle
      }
        let taskName: String = processTracking.name?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "Chưa xác định"
        chooseTaskButton.setTitle(taskName, for: .normal)
        
      if let trackingStatus = processTracking.trackingStatus {
        if let nameStatus: String = trackingStatus.name {
          statusLabel.text = nameStatus
        }
        if let colorStatus = trackingStatus.colorCode {
          statusLabel.backgroundColor = UIColor(hexString: colorStatus)!
          statusLabel.textColor = Theme.default.normalWhiteColor
        }
      }
      self.startDate = processTracking.fromDate
      self.endDate = processTracking.toDate
      fromDateLabel.text = Constants.formatDate(self.startDate, Constants.FULL_DATETIME_FORMAT)
      toDateLabel.text = Constants.formatDate(self.endDate, Constants.FULL_DATETIME_FORMAT)
      if let fullNameAssignTo: String = processTracking.assignToName {
        fullNameAssignToLabel.text = fullNameAssignTo
      }
      if let jobTitleAssignTo: String = processTracking.assignToJobTitle {
        jobTitleAssignToLabel.text = jobTitleAssignTo
      }
        if let assignToPicture = processTracking.assignToPicture, let url = URL(string: Constants.default.domainAddress + assignToPicture){
            self.assignByNameAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
      if let percentFinish: Float = processTracking.percentFinish {
        percentFinishLabel.text = "\(percentFinish)"
      }
        
        if let attachFile = processTracking.fileTrackingDocuments?.first {
            attachFileButton.isHidden = false
            attachFileButton.setTitle(attachFile.name, for: .normal)
        } else {
            attachFileButton.isHidden = true
        }
        
      self.loadStartEndDays(self.startDate!, self.endDate!)
      self.view.layoutIfNeeded()
    }
  }
  
  fileprivate func setupStatusDLRadioButtonGroups() {
    if let types: [StatusDocument] = self.appriseTrackingTypes,
       !types.isEmpty {
      ratingTypeStackView.removeSubviews()
      typesDLRadioButtonGroups.removeAll()
      for itemType: StatusDocument in types {
        if let itemTypeID: Int = itemType.id,
           let itemTypeName: String = itemType.name {
          let itemDLRadioButton: DLRadioButton = createDLRadioButton(itemTypeName, itemTypeID)
          itemDLRadioButton.isSelected = itemTypeID == defaultSelectedIDOfType
            self.selectedStatusValue = "\(itemTypeID)"
          typesDLRadioButtonGroups.append(itemDLRadioButton)
        }
      }
      cancelDLRadioButton.otherButtons = typesDLRadioButtonGroups + statusDLRadioButtonGroups
      for type: DLRadioButton in typesDLRadioButtonGroups {
        ratingTypeStackView.addArrangedSubview(type)
      }
        
        var count = typesDLRadioButtonGroups.count - 2
        while count > 0 {
            let tempItem: DLRadioButton = createDLRadioButton("", 999)
            tempItem.iconColor = UIColor.clear
            ratingType2StackView.addArrangedSubview(tempItem)
            count -= 1
        }
        
      ratingTypeStackView.layoutIfNeeded()
        ratingType2StackView.layoutIfNeeded()
    }
  }
  
  fileprivate func createDLRadioButton(_ title: String, _ tag: Int) -> DLRadioButton {
    let radioButton: DLRadioButton = DLRadioButton()
    radioButton.titleLabel?.font = Theme.default.regularFont(size: 17)
    radioButton.setTitle(title, for: [])
    radioButton.setTitleColor(Theme.default.normalTextColor, for: [])
    radioButton.iconColor = Theme.default.normalTextColor;
    radioButton.indicatorColor = Theme.default.normalTextColor;
    radioButton.backgroundColor = UIColor.clear
    radioButton.tintColor = Theme.default.normalTextColor
    radioButton.tag = tag
    radioButton.iconSize = 15
    radioButton.iconColor = Theme.default.normalBlueOceanColor
    radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
    radioButton.addTarget(self, action: #selector(selectDLTaskDropDown(_:)), for: .touchUpInside)
    return radioButton
  }
    
    func updateAttchFileView() {
        attachmentStackView.isHidden = self.attachFiles.isEmpty
//        contentView.setNeedsLayout()
//        contentView.setNeedsUpdateConstraints()
//        contentView.layoutIfNeeded()
    }
    
    func addAttachFilesView(file:TrackingFileDocument) {
        let fileView = AttachFileView.instanceFromNib()
        fileView.delegate = self
        fileView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        fileView.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        fileView.updateFileInfo(f: file)
        self.attachmentStackView.addArrangedSubview(fileView)
        self.attachFiles.append(file)
        self.updateAttchFileView()
    }
    
    func deleteAttachFile(fileView: AttachFileView) {
        if let file = fileView.file, let index = self.attachFiles.index(where: {$0.id == file.id}) {
            self.attachFiles.remove(at: index)
        }
        fileView.removeSubviews()
        self.updateAttchFileView()
    }
  
  // MARK: define actions, triggers.
  // --------------------------------
    @IBAction func openAttachFile(_ sender: Any) {
        if let file = self.selectedAppriseTracking?.fileTrackingDocuments?.first {
            let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
            vc?.fileDoucment = file
            vc?.document = document!
            vc?.hideAction = true
            self.present(vc!, animated: true, completion: nil)
        }
        
    }
    
  @IBAction func selectTaskDropDown(_ sender: Any) {
    taskDropDown.show()
  }
  
  @IBAction func selectDLTaskDropDown(_ sender: Any) {
    if let selectedStatusObject: DLRadioButton = sender as? DLRadioButton {
      debugPrint("AppriseTaskViewController.selectDLTaskDropDown.selectedValue: \(selectedStatusObject.tag)")
        if selectedStatusObject == cancelDLRadioButton || selectedStatusObject == refundDLRadioButton {
            return
        }
      self.selectedStatusValue = selectedStatusObject.tag.string
    }
  }
  
  @IBAction func attachFilesButton(_ sender: Any) {
    debugPrint("AppriseTaskViewController.attachFilesButton")
    let importMenu = UIDocumentMenuViewController(documentTypes: ["public.content","public.item"], in: .import)
    importMenu.delegate = self
    importMenu.modalPresentationStyle = .formSheet
    self.present(importMenu, animated: true, completion: nil)
  }
  
  fileprivate func doneAction() {
    self.appriseTrackingDocument()
  }
  
  fileprivate func dismissAction() {
    self.dismiss(animated: true, completion: {
      self.delegate?.appriseCloseView()
    })
  }
  
  @IBAction func touchUpInsideAction (_ sender: Any) {
    self.appriseTrackingDocument()
  }
    
    @IBAction func rejectAction (_ sender: Any) {
        self.selectedStatusValue = "0"
        self.appriseTrackingDocument()
    }
}

// MARKS: Request apis..
// -------------------------------
extension AppriseTaskViewController {
  
  fileprivate func getAppriseTrackings() {
    if let documentObject: Document = self.document,
       let documentID: String = documentObject.id {
      //  Begin loading
      SyncProvider.getListAppraiseTrackingDocument(documentID: documentID) { (trackings, msg) in
        // End loading
        if let result: [ProcessTracking] = trackings { // It have data
          self.appriseTrackings = result
        }
      }
    }
  }
  
  fileprivate func getListTrackingRatings() {
    if let selectedAppriseTrackingObject: ProcessTracking = self.selectedAppriseTracking,
      let trackingID: String = selectedAppriseTrackingObject.ID {
      SyncProvider.getListTrackingRating(trackingID: trackingID) { (ratings, msg) in
        if let result: [StatusDocument] = ratings {
          self.appriseTrackingTypes = result
        }
      }
    }
  }
  
  fileprivate func appriseTrackingDocument() {
    if let exp: ProcessTracking = ProcessTracking(JSON: [:]) {
      if let selectedPT = self.selectedAppriseTracking {
        exp.trackingID = selectedPT.ID
        exp.documentID = selectedPT.docID
      } else {
        exp.trackingID = "bf2229a2-e347-41b6-954e-1d422119d62c"
        exp.documentID = "2d21bcf3-5665-4f2c-8376-c5d82ccf5a08"
      }
      //exp.fromDate = self.startDate
      exp.newDueDate = self.endDate
      if let description: String = opinionTextView.text, description != OPINION_PLACE_HOLDER {
        exp.description = description
      }
//      if let percent: String = percentFinishLabel.text {
//        exp.percentFinish = percent.floatValue
//      }
        if cancelDLRadioButton.isSelected {
            exp.status = "3"
        } else if refundDLRadioButton.isSelected {
            exp.status = "0"
        } else if let selectedStatus: String = self.selectedStatusValue {
            exp.status = "4"
            exp.rating = Float(selectedStatus)
        }
      exp.files = attachFiles
      
      SyncProvider.appriseTrackingDocument(tracking: exp, done: { (result, error) in
        if let res = result, res {
          self.dismiss(animated: true, completion: {
            self.delegate?.appriseDoneView(status: res, message: error, index: self.indexPath)
          })
        } else {
          self.dismiss(animated: true, completion: {
            self.delegate?.appriseDoneView(status: false, message: error, index: self.indexPath)
          })
        }
        if let docStatus = self.document?.statusDocument, docStatus.isNew {
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_RELOAD), object: nil, userInfo: nil)
        }
      })
    }
  }
  
}

// MARKS: Define extensions,
// -------------------------------

extension AppriseTaskViewController: UIDocumentPickerDelegate {
  
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    let cico = url as URL
    debugPrint("The Url is : \(cico)")
    
    let attachment = Mapper<TrackingFileDocument>().map(JSON:[:])!
    attachment.name = url.lastPathComponent
    attachment.ext = url.pathExtension
    if let fileData = try? Data(contentsOf: URL(fileURLWithPath: url.path)) {
        attachment.size = fileData.count
        self.view.makeToast("Đang quá trình upload.")
        SVProgressHUD.show()
        UploadManager.uploadFile(fileData, type: url.pathExtension, updateHandler: { (percent) in
            print("Percent: \(percent)")
        }) { ( fileID, error) in
            if let id = fileID?.split(separator: ".").first {
                attachment.id = String(id)
                self.addAttachFilesView(file: attachment)
                SVProgressHUD.dismiss()
                self.view.makeToast("Upload thành công, bạn có thể tiếp tục bổ sung thông tin khác.")
            } else {
                self.view.makeToast("Upload thất bại!")
            }
        }
    }
  }
  
  func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
  }
  
}

extension AppriseTaskViewController: UIDocumentMenuDelegate {
  
  func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    documentPicker.delegate = self
    self.present(documentPicker, animated: true, completion: nil)
  }
  
  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    
  }
  
}

extension AppriseTaskViewController: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.tag == OPINION_TXT_TAG {
        textView.textColor = UIColor.init(hexString: "363636")
      if let value: String = textView.text,
        value == OPINION_PLACE_HOLDER {
        textView.text = nil
      }
      textView.addSubviews(textView.addBorder(edges: .bottom, color: Theme.default.normalGreenSelectedColor))
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.tag == OPINION_TXT_TAG {
      if textView.text.isEmpty {
        textView.text = OPINION_PLACE_HOLDER
        textView.textColor = UIColor.lightGray
      }
      textView.addSubviews(textView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    }
  }
  
}

extension AppriseTaskViewController: UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    if textField.tag == PERCENT_FINISH_TXT_TAG {
      if let value: String = textField.text,
        value == PERCENT_FINISH_PLACE_HOLDER {
        textField.text = nil
      }
      textField.addSubviews(textField.addBorder(edges: .bottom, color: Theme.default.normalGreenSelectedColor))
    }
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField.tag == PERCENT_FINISH_TXT_TAG {
      if textField.isEmpty {
        textField.text = PERCENT_FINISH_PLACE_HOLDER
      }
      textField.addSubviews(textField.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    }
  }
  
}

extension AppriseTaskViewController: AttachFileViewDelegate {
    func didDeleteAttachFile(_ fileView: AttachFileView) {
        self.deleteAttachFile(fileView: fileView)
    }
}

