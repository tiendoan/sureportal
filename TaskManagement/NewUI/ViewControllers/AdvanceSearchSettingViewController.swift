//
//  AdvanceSearchSettingViewController.swift
//  TaskManagement
//
//  Created by Scor Doan on 4/12/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import KSTokenView
import Toast_Swift
import IQKeyboardManagerSwift

enum SearchType: Int {
    case all = 0
    case serialNumber
    case docNumber
    case docSummary
    case sender
    case receiver
    case signedBy
    case docAttach
    
    var value:String {
        switch self {
        case .all:  return "All"
        case .serialNumber: return "SerialNumber"
        case .docNumber:  return "DocNumber"
        case .docSummary: return "DocSummary"
        case .sender:  return "Sender"
        case .receiver: return "Receiver"
        case .signedBy:  return "SignedBy"
        case .docAttach: return "DocAttach"
        }
    }
    
    var name:String {
        switch self {
        case .all:  return "Tất cả"
        case .serialNumber: return "Số hiệu"
        case .docNumber:  return "STT đến"
        case .docSummary: return "Trích yếu"
        case .sender:  return "Nơi gửi"
        case .receiver: return "Nơi nhận"
        case .signedBy:  return "Người ký"
        case .docAttach: return "File đính kèm"
        }
    }
}

enum DocType: Int {
    case go = 0
    case come
    case inHouse
    
    var name: String {
        switch self {
        case .go:
            return "Đi"
        case .come:
            return "Đến"
        case .inHouse:
            return "Nội bộ"
        
        }
    }
}
class AdvanceSearchSettingViewController: UIViewController {
    
    let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
    let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
    let DATE_FORMAT = "dd-MM-yyyy"
    
    @IBOutlet weak var popupView: TNGradientView!
    @IBOutlet weak var typeButton: UIButton!
    @IBOutlet weak var fromDateButton: UIButton!
    @IBOutlet weak var toDateButton: UIButton!
    @IBOutlet weak var assignersTokenView: KSTokenView!
    @IBOutlet weak var departmentView: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tfFromDate: UITextField!
    @IBOutlet weak var tfToDate: UITextField!
    @IBOutlet weak var documentTypeButton: UIButton!
    
    //Search type
    let typeDropDown: DropDown = DropDown()
    let documentTypeDropDown: DropDown = DropDown()
    let searchTypes:[SearchType] = [.all, .serialNumber, .docNumber, .docSummary, .sender, .receiver, .signedBy, .docAttach]
    var selectedType:SearchType = .all
    var docTypes:[DocType] = [.go, .come, .inHouse]
    var selectedDocType:DocType = .go
    
    //Person
    let personDropDown: DropDown = DropDown()
    var flatEmployees: [Assignee] = [Assignee]()
    var selectedDepartment:Assignee?
    
    //Date
    var fromDate = Date(month: 1, day: 1)!
    var toDate = Date(month: 12, day: 31)!
    
    var searchText = ""
    var onSetAdvanceSearch:((String, SearchType, DocType, Assignee?, Date, Date) -> Void)?
    
    let accessory: UIView = {
        let accessoryView = UIView(frame: .zero)
        accessoryView.backgroundColor = .white
        accessoryView.alpha = 1.0
        return accessoryView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Xong"
        
        ///
        self.popupView.layer.cornerRadius = 10
        self.popupView.layer.masksToBounds = true
        
        setupAccessory()
        tfSearch.text = searchText
        tfSearch.inputAccessoryView = accessory
        ///
        setupDropdownMenu()
        
        ///
        setupPersonView()
        
        //Date
        tfToDate.inputAccessoryView = accessory
        tfFromDate.inputAccessoryView = accessory
        updateDate()
        
    }
    
    func setupAccessory() {
        let doneButton: UIButton = {
            let button = UIButton(type: .custom)
            button.setTitle(" Ẩn Bàn Phím ", for: .normal)
            
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor.init(hexString: "17C209")
            button.addTarget(self, action:
                #selector(doneButtonTapped), for: .touchUpInside)
            button.layer.cornerRadius = 6
            button.layer.masksToBounds = true
            return button
        }()
        
        let searchButton: UIButton = {
            let button = UIButton(type: .custom)
            button.setTitle(" Tìm Kiếm ", for: .normal)
            button.setTitleColor(.white, for: .disabled)
            button.backgroundColor = UIColor.init(hexString: "17C209")
            button.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
            button.layer.cornerRadius = 6
            button.layer.masksToBounds = true
            return button
        }()
        
        accessory.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 45)
        accessory.translatesAutoresizingMaskIntoConstraints = false
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        accessory.addSubview(doneButton)
        accessory.addSubview(searchButton)
        NSLayoutConstraint.activate([
            searchButton.trailingAnchor.constraint(equalTo:
                accessory.trailingAnchor, constant: -10),
            searchButton.centerYAnchor.constraint(equalTo:
                accessory.centerYAnchor),
            doneButton.centerYAnchor.constraint(equalTo:
                accessory.centerYAnchor),
            doneButton.trailingAnchor.constraint(equalTo: searchButton.leadingAnchor, constant: -10)
            ])
    }
    
    func doneButtonTapped() {
        self.view.endEditing(true)
    }
    
    func searchButtonTapped() {
        self.view.endEditing(true)
        doneButtonClicked(nil)
    }
    
    func setupPersonView() {
        departmentView.layer.cornerRadius = 10
        departmentView.layer.masksToBounds = true
        assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        assignersTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        assignersTokenView.promptText = ""
        assignersTokenView.delegate = self
        assignersTokenView.searchResultHeight = 120
        assignersTokenView.returnKeyType(type: .done)
        assignersTokenView.shouldDisplayAlreadyTokenized = true
        assignersTokenView.maxTokenLimit = -1
        assignersTokenView.removesTokensOnEndEditing = false
        assignersTokenView.shouldAddTokenFromTextInput = false
        assignersTokenView.shouldHideSearchResultsOnSelect = true
        assignersTokenView.tokenizingCharacters = []
        assignersTokenView.editable = false
        
        if let department = self.selectedDepartment {
            self.loadAssignersTokenView(self.assignersTokenView, [department], Theme.default.normalOrangeSelectedColor)
        }
    }
    
    func setupDropdownMenu() {
        //Search type dropdown
        typeDropDown.anchorView = typeButton
        typeDropDown.width = typeButton.widthf
        typeDropDown.bottomOffset = CGPoint(x: 0, y: typeButton.bounds.height)
        typeDropDown.isMultipleTouchEnabled = false
        typeDropDown.textColor = UIColor.gray
        typeDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        typeDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        typeDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        typeDropDown.selectionAction = { [unowned self] (index, item) in
            self.typeButton.setTitle("\(item)", for: .normal)
            self.selectedType = SearchType(rawValue: index)!
        }
        typeDropDown.dataSource = searchTypes.map {$0.name}
        
        
        //Document type dropdown
        self.documentTypeButton.setTitle(selectedDocType.name, for: .normal)
        documentTypeDropDown.anchorView = typeButton
        documentTypeDropDown.width = typeButton.widthf
        documentTypeDropDown.bottomOffset = CGPoint(x: 0, y: typeButton.bounds.height)
        documentTypeDropDown.isMultipleTouchEnabled = false
        documentTypeDropDown.textColor = UIColor.gray
        documentTypeDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        documentTypeDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        documentTypeDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        documentTypeDropDown.selectionAction = { [unowned self] (index, item) in
            self.documentTypeButton.setTitle("\(item)", for: .normal)
            self.selectedDocType = DocType(rawValue: index)!
        }
        documentTypeDropDown.dataSource = docTypes.map {$0.name}
        
        //person dropdown
        personDropDown.anchorView = assignersTokenView
        personDropDown.width = assignersTokenView.widthf
        personDropDown.bottomOffset = CGPoint(x: 0, y: assignersTokenView.bounds.height)
        personDropDown.isMultipleTouchEnabled = false
        personDropDown.textColor = UIColor.gray
        personDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        personDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        personDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        personDropDown.selectionAction = { [unowned self] (index, item) in
            let person = DataManager.shared.departments[index]
            if self.selectedDepartment == person {
                self.assignersTokenView.deleteAllTokens()
                self.selectedDepartment = nil
                return
            }
            self.selectedDepartment = person
            self.loadAssignersTokenView(self.assignersTokenView, [person], Theme.default.normalOrangeSelectedColor)
        }
        personDropDown.dataSource = DataManager.shared.departments.map {$0.text ?? "N/A"}
    }
    
    var dateFormate = Bool()
    func updateDate() {
        tfFromDate.text = fromDate.stringFromFormat(DATE_FORMAT)
        tfToDate.text = toDate.stringFromFormat(DATE_FORMAT)
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonClicked(_ sender: Any?) {
        if fromDate > toDate {
            let from = fromDate
            fromDate = toDate
            toDate = from
        }
        self.onSetAdvanceSearch?(tfSearch.text ?? "", selectedType, selectedDocType, selectedDepartment, fromDate, toDate)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func typeSearchButtonClicked(_ sender: Any) {
        typeDropDown.show()
    }
    
    @IBAction func documentTypeSearchButtonClicked(_ sender: Any) {
        documentTypeDropDown.show()
    }
    
    @IBAction func fromDateButtonClicked(_ sender: Any) {
        self.view.endEditing(true)
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        selector.optionCurrentDate = fromDate
        //selector.optionCurrentDateRange.setEndDate(toDate)
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func toDateButtonClicked(_ sender: Any) {
        self.view.endEditing(true)
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        selector.optionCurrentDate = toDate
        //selector.optionCurrentDateRange.setStartDate(fromDate)
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func orgButtonClicked(_ sender: Any) {
        self.personDropDown.show()
    }
}

extension AdvanceSearchSettingViewController: KSTokenViewDelegate {
    
    fileprivate func loadAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee], _ backgroundColor: UIColor) {
        if assigners.isEmpty {
            return
        }
        
        self.assignersTokenView.deleteAllTokens()
        for assigner in assigners {
            let assignToken: KSToken = KSToken(title: "")
            if let title: String = assigner.text {
                assignToken.title = title
            }
            assignToken.object = assigner
            assignToken.tokenBackgroundColor = backgroundColor
            owner.addToken(assignToken)
        }
        
        self.assignersTokenView.layoutSubviews()
        _ = assignersTokenView.becomeFirstResponder()
    }
    
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? Assignee, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        assignersTokenView.layoutIfNeeded()
        assignersTokenView.layoutSubviews()
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            return completion!([])
        }
        
        var data: Array<AnyObject> = []
        for item: Assignee in DataManager.shared.departments {
            if let value = item.value, value.contains(string, caseSensitive: false) {
                data.append(item as AnyObject)
            } else if let text = item.text, text.contains(string, caseSensitive: false) {
                data.append(item as AnyObject)
            }
        }
        return completion!(data)
    }
    
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if let assignee = object as? Assignee,
            let name: String = assignee.text {
            return name
        } else {
            return "Chưa xác định"
        }
        return object as! String
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        if let _ = token.object as? Assignee {
            token.tokenBackgroundColor = Theme.default.normalOrangeSelectedColor
            return token
        }
        return nil
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        self.assignersTokenView.deleteAllTokens()
        
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        assignersTokenView.layoutIfNeeded()
        assignersTokenView.layoutSubviews()
        
        //assignersTokenView.editable = false
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if let assignee = object as? Assignee  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            cell?.backgroundColor = indexPath.row%2 == 0 ? UIColor.red : UIColor.green
            return cell!
        }
        return UITableViewCell()
    }
}

extension AdvanceSearchSettingViewController: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
        if let title: String = selector.optionTopPanelTitle {
            if title == START_DATE_OPTION_TITLE { // Start day option.
                self.fromDate = date
            } else if title == END_DATE_OPTION_TITLE { // End day option.
                self.toDate = date
            }
            
            self.updateDate()
        }
    }
}

extension AdvanceSearchSettingViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfFromDate || textField == tfToDate {
            textField.selectedTextRange = textField.textRange(from: textField.endOfDocument, to: textField.endOfDocument)
        } else {
            IQKeyboardManager.shared.keyboardDistanceFromTextField = 10
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, let date = text.date(withFormat: DATE_FORMAT) {
            if textField == tfFromDate {
                fromDate = date
            } else if textField == tfToDate {
                toDate = date
            }
        } else {
            self.view.makeToast("Bạn nhập sai định dạng ngày dd/mm/yyyy.")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Format Date dd-MM-yyyy
        guard let text = textField.text else { return false }
        
        //Check
        if textField == tfFromDate || textField == tfToDate {
            if !(string == "") {
                if (text.count == 2) || (text.count == 5) {
                    // append the text
                    textField.text = text + "-"
                }
            }
            // check the condition not exceed 9 chars
            return !(text.count > 9 && (string.count ) > range.length)
        }
        else {
            return true
        }
    }
    
}
