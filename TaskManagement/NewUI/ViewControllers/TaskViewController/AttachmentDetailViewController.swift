//
//  AttachmentDetailViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/13/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SnapKit
import DropDown
import WebKit
import Kingfisher
import SVProgressHUD

protocol AttachmentDetailViewDelegate {
    func doneView(rowIndex: IndexPath?, action: DocumentAction)
}

enum FileDocumentType: String {
  case pdf = ".pdf", jpg = ".jpg", png = ".png", doc = ".doc", docx = ".docx", xls = ".xls", xlsx = ".xlsx", ppt = ".ppt", pptx = ".pptx", other = ".other"
    func imageIcon() -> Image {
        switch self {
        case .pdf:
            return #imageLiteral(resourceName: "ic_file_pdf")
        case .jpg:
            return #imageLiteral(resourceName: "ic_file_jpg")
        case .png:
            return #imageLiteral(resourceName: "ic_file_pdf")
        case .doc:
            return #imageLiteral(resourceName: "ic_file_doc")
        case .docx:
            return #imageLiteral(resourceName: "ic_file_doc")
        case .xls:
            return #imageLiteral(resourceName: "ic_file_xls")
        case .xlsx:
            return #imageLiteral(resourceName: "ic_file_xls")
        case .ppt:
            return #imageLiteral(resourceName: "ic_file_ppt")
        case .pptx:
            return #imageLiteral(resourceName: "ic_file_ppt")
        default:
            return #imageLiteral(resourceName: "ic_file_other")
        }
    }
}

class AttachmentDetailViewController: UIViewController, UIScrollViewDelegate,WKNavigationDelegate {
  
  @IBOutlet fileprivate weak var vTop:UIView!
  @IBOutlet fileprivate weak var vBotton:UIView!
  @IBOutlet fileprivate weak var vMain:UIView!
    @IBOutlet weak var buttonStackView: UIStackView!
    
  @IBOutlet fileprivate weak var btBack:UIButton!
  @IBOutlet fileprivate weak var btMore:UIButton!
  @IBOutlet fileprivate weak var lbTitle:UILabel!
  
  @IBOutlet fileprivate weak var btRotateLeft:UIButton!
  @IBOutlet fileprivate weak var btRotateRight:UIButton!
  
  @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
  
  @IBOutlet fileprivate weak var lbPage:UILabel!
  
  fileprivate let vImage = ZoomImageView(frame: UIScreen.main.bounds)
  fileprivate let webView = WKWebView(frame: UIScreen.main.bounds)
  fileprivate var angle:CGFloat = 0
  
  fileprivate var dropDown = DropDown()
  
  fileprivate var fileType:FileDocumentType = .pdf
  
  lazy fileprivate var gradientTop: CAGradientLayer = {
    
    let gradient: CAGradientLayer = CAGradientLayer()
    
    gradient.colors = [UIColor(white: 0, alpha: 0.5).cgColor, UIColor(white: 0, alpha: 0.1).cgColor]
    gradient.locations = [0.0 , 1.0]
    gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
    gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
    
    return gradient
  }()
  
  lazy fileprivate var gradientBotton: CAGradientLayer = {
    
    let gradient: CAGradientLayer = CAGradientLayer()
    
    gradient.colors = [UIColor(white: 0, alpha: 0.5).cgColor, UIColor(white: 0, alpha: 0.1).cgColor]
    gradient.locations = [0.0 , 1.0]
    gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
    gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
    
    return gradient
  }()
  
  var fileDoucment:FileDocument?
  var document:Document!
  var indexPath: IndexPath?
  var delegate: AttachmentDetailViewDelegate?
    var hideAction = false
    
    var file: Files?
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    lbPage.textColor = UIColor.white
    lbPage.font = UIFont(name: "Muli-Regular", size: 15.0)!
    vTop.layer.insertSublayer(gradientTop, at: 0)
    vBotton.layer.insertSublayer(gradientBotton, at: 0)
    self.setupLayout()
    self.setupDropdown()
    
    
    if(fileDoucment != nil){
      if let ext = fileDoucment?.ext?.lowercased(){
        fileType = FileDocumentType.init(rawValue: ext) ?? .other
        //                if fileDoucment!.ext! == FileDocumentType.pdf.rawValue{
        //                    fileType = .pdf
        //                }
        //                else if fileDoucment!.ext! == FileDocumentType.jpg.rawValue{
        //                    fileType = .jpg
        //                }
      }
      lbTitle.text = fileDoucment!.name ?? ""
    }
    
    self.loadData()
    self.addAction()
    self.dowloadFile()
  }
    
    func dowloadFile() {
        guard let file = file else { return }
        guard let fileName = file.name else { return }
        loading.startAnimating()
        InstanceDB.default.downloadFilePDF(fileName, path: file.downloadUrl ?? "", done: { (result) in
            self.loading.stopAnimating()
            if(result != nil){
                self.loadField(url: result!)
            }
        })
    }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    gradientTop.frame = vTop.bounds
    gradientBotton.frame = vBotton.bounds
  }
  
  fileprivate func setupLayout(){
    
    vImage.zoomMode = .fit
    vImage.maximumZoomScale = 5
    vImage.minimumZoomScale = 1
    vMain.addSubview(vImage)
    vImage.snp.makeConstraints { (make) in
      make.left.right.bottom.top.equalToSuperview()
    }
    
    webView.navigationDelegate = self
    webView.scrollView.delegate = self
    webView.backgroundColor = UIColor.white
    vMain.addSubview(webView)
    webView.snp.makeConstraints({ (make) in
      make.left.top.right.bottom.equalToSuperview()
    })
  }
  
  fileprivate func loadData(){
    
    if let localLink = fileDoucment?.localLink {
        self.loadField(url: localLink)
    } else if let fileName = fileDoucment?.name{
        loading.startAnimating()
        InstanceDB.default.downloadFilePDF(fileName, path: fileDoucment!.downloadLink ?? "", done: { (result) in
            self.loading.stopAnimating()
            if(result != nil){
                self.loadField(url: result!)
            }
        })
    }
  }
  
  fileprivate func loadField(url:URL){
    
    switch self.fileType {
    case .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx:
      self.vImage.isHidden = true
      self.webView.isHidden = false
      self.webView.loadFileURL(url, allowingReadAccessTo: url)
      self.lbPage.text = "1 of 1"
    case .png, .jpg:
      self.vImage.isHidden = false
      self.webView.isHidden = true
      do {
        let imageData = try Data(contentsOf: url)
        self.vImage.image = UIImage(data: imageData)
      } catch {}
      self.lbPage.text = "1 of 1"
    default:
      self.vImage.isHidden = true
      self.webView.isHidden = true
      self.lbPage.text = ""
    }
  }
  
  fileprivate func setupDropdown() {
    DropDown.startListeningToKeyboard()
    DropDown.appearance().textColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
    DropDown.appearance().textFont = UIFont(name: "Muli-Regular", size: 19)!
    dropDown.anchorView = btMore
    dropDown.dataSource = ["Thao tác", "Giao việc", "Chuyển tiếp", "Chỉnh sửa", "Khác"]
    let icon = [#imageLiteral(resourceName: "ic_more_thaotac"), #imageLiteral(resourceName: "ic_more_giaoviec"), #imageLiteral(resourceName: "ic_more_chuyentiep"), #imageLiteral(resourceName: "ic_more_edit"), #imageLiteral(resourceName: "ic_more_orther")]
    dropDown.cellNib = UINib(nibName: "ActionPopupDropDownCell", bundle: nil)
    dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
      guard let cell = cell as? ActionPopupDropDownCell else { return }
      // Setup your custom UI components
      cell.iconImageView.image = icon[index]
    }
    dropDown.width = self.view.widthf - 20
    dropDown.bottomOffset = CGPoint(x: 50, y: 50)
    //[unowned self]
    dropDown.selectionAction = {  (index: Int, item: String) in
      print("Selected item: \(item) at index: \(index)")
      switch index {
      case 1:
        let assginVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        self.showViewController(viewController: assginVC)
        
      case 2:
        let forwardVC: TaskForwardViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        self.showViewController(viewController: forwardVC)
      case 3:
        let assginVC: TaskEditViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        self.showViewController(viewController: assginVC)
      case 4:
        let forwardVC: TaskProcessViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        self.showViewController(viewController: forwardVC)
      default:
        break
      }
    }
  }
  
  func scrollViewWillBeginDragging (_ scrollView: UIScrollView) {
    self.getPage()
  }
  
  func scrollViewWillBeginDecelerating (_ scrollView: UIScrollView) {
    self.getPage()
  }
  
  func scrollViewDidEndDragging (_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    self.getPage()
  }
  
  func scrollViewDidScroll (_ scrollView: UIScrollView) {
    self.getPage()
  }
  
  func webView (_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    self.getPage()
  }
  
  func getPage () {
    let webViewSubviews = self.getSubviewsOfView(v: self.webView)
    for v in webViewSubviews {
      if v.description.range(of:"WKPDFPageNumberIndicator") != nil {
        v.isHidden = true
        for i in v.subviews{
          if let a = i as? UILabel{
            self.lbPage.text = a.text ?? "1 of 1"
          }
        }
      }
    }
  }
  
  func getSubviewsOfView (v:UIView) -> [UIView] {
    var viewArray = [UIView]()
    for subview in v.subviews {
      viewArray += getSubviewsOfView(v: subview)
      viewArray.append(subview)
    }
    return viewArray
  }
}

extension AttachmentDetailViewController {
  
  func addAction() {
    guard self.document != nil, !hideAction else {
        return
    }
    var numberAction = 0
    if (self.document.actions != nil) {
//      if self.document.actions!.count > 3{
//        numberAction = 3
//        let button = UIButton(type: .custom)
//        button.setImage(#imageLiteral(resourceName: "ic_nav_more"), for: .normal)
//        button.showsTouchWhenHighlighted = true
//        button.tag = 3
//        button.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
//        button.layer.cornerRadius = 20
//        button.addTarget(self, action: #selector(pressesAction(_:)), for: .touchUpInside)
//        buttonStackView.addArrangedSubview(button)
//      } else {
//        numberAction = self.document.actions!.count
//      }
        numberAction = self.document.actions!.count
    }

    let actions:[DocumentActionObject] = document.actions!.reversed()
    numberAction = actions.count
    for i in 0..<numberAction {
      if let actionTypeItem: DocumentAction = actions[i].actionType {
        //let right = -((numberAction - i) * 44) + 40 - width
        let imgAction: String = "ic_action_\(actionTypeItem.rawValue)"
        let image = UIImage(named: imgAction)?.filled(withColor: .white).imageResize(sizeChange: CGSize(width: 40, height: 40)) ?? #imageLiteral(resourceName: "multiSelect_more").imageResize(sizeChange: CGSize(width: 40, height: 40))
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true

        button.setImage(image, for: .normal)        
        button.tag = actionTypeItem.getIntTagValue()
        button.addTarget(self, action: #selector(pressesAction(_:)), for: .touchUpInside)
        buttonStackView.addArrangedSubview(button)

      }
    }
  }
  
  @objc func pressesAction (_ sender: UIButton) {
    if (sender.tag == 99) {//more action
      let actionSheet = UIAlertController(title: "Chọn", message: nil, preferredStyle: .actionSheet)
      for i in 0..<document.actions!.count {
        let action: UIAlertAction = UIAlertAction(title: document.actions![i].actionName, style: .default) { (_) in
          if let actionType: DocumentAction = self.document.actions![i].actionType {
            if actionType == .assign {
              let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
              assignVC.document = self.document
              assignVC.delegate = self
              self.showViewController(viewController: assignVC, false)
            } else if actionType == .approve {
              let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
              assignVC.document = self.document
              assignVC.delegate = self
              self.showViewController(viewController: assignVC, false)
            } else if actionType == .process {
              let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
              processVC.delegate = self
              processVC.document = self.document
              self.showViewController(viewController: processVC, false)
            } else if actionType == .appraise {
              let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
              appriseVC.delegate = self
              appriseVC.document = self.document
              self.showViewController(viewController: appriseVC, false)
            } else if actionType == .sendMail {
              let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                sendEmailCV.document = self.document
              sendEmailCV.delegate = self
              self.showViewController(viewController: sendEmailCV, false)
            } else if actionType == .finish {
              self.view.makeToast("Xử lý thành công !")
                self.finishDocument()
            }
          }
        }
        actionSheet.addAction(action)
      }
      let cancel = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
      }
      actionSheet.addAction(cancel)
      self.present(actionSheet, animated: true, completion: nil)
    } else if sender.tag == DocumentAction.assign.getIntTagValue() {
      let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      assignVC.document = document
        assignVC.delegate = self
      assignVC.getAssigners()
      self.showViewController(viewController: assignVC, false)
    } else if sender.tag == DocumentAction.approve.getIntTagValue() {
      let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      assignVC.document = document
        assignVC.delegate = self
      self.showViewController(viewController: assignVC, false)
    } else if sender.tag == DocumentAction.process.getIntTagValue() {
      let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      processVC.delegate = self
      self.showViewController(viewController: processVC, false)
    } else if sender.tag == DocumentAction.appraise.getIntTagValue() {
      let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      appriseVC.delegate = self
      appriseVC.document = document
      self.showViewController(viewController: appriseVC, false)
    } else if sender.tag == DocumentAction.sendMail.getIntTagValue() {
      let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        sendEmailCV.document = document
      sendEmailCV.delegate = self
      self.showViewController(viewController: sendEmailCV, false)
    } else if sender.tag == DocumentAction.finish.getIntTagValue() {
      finishDocument()
    }
  }
  
    func finishDocument() {
        guard let document = document else {
            return
        }
        SVProgressHUD.show()
        DataManager.finishDocument(document: document, done: { (finish) in
            SVProgressHUD.dismiss()
            if finish {
                self.delegate?.doneView(rowIndex: self.indexPath, action: .finish)
                self.dismiss(animated: true, completion: nil)
                self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
            } else {
                self.view.makeToast("Có lỗi xảy ra, hệ thống sẽ cập nhật cho bạn sớm nhất !")
            }
        })
    }
}


extension AttachmentDetailViewController {
  
  @IBAction func pressesBTBack (_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func pressesBTMore (_ sender: UIButton) {
    dropDown.show()
  }
  
  @IBAction func pressesBTRotateLeft (_ sender: UIButton) {
    
    self.webView.rotate(byAngle: -CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
    }
    self.vImage.rotate(byAngle: -CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
    }
    
    self.webView.frame = (self.webView.superview?.bounds)!;
    self.vImage.frame = (self.vImage.superview?.bounds)!;
    self.webView.snp.updateConstraints { (make) in
      make.top.left.right.bottom.equalToSuperview()
    }
    self.vImage.snp.updateConstraints { (make) in
      make.top.left.right.bottom.equalToSuperview()
    }
    self.view.layoutIfNeeded()
  }
  
  @IBAction func pressesBTRotateRight (_ sender: UIButton) {
    self.webView.rotate(byAngle: CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
    }
    self.vImage.rotate(byAngle: CGFloat.pi/2, ofType: .radians, animated: true, duration: 0.25) { (success) in
    }
    self.webView.frame = (self.webView.superview?.bounds)!;
    self.vImage.frame = (self.vImage.superview?.bounds)!;
    self.webView.snp.updateConstraints { (make) in
      make.top.left.right.bottom.equalToSuperview()
    }
    self.vImage.snp.updateConstraints { (make) in
      make.top.left.right.bottom.equalToSuperview()
    }
    self.view.layoutIfNeeded()
  }
  
  
  @IBAction func pressesAddperson (_ sender: UIButton) {
    let assginVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    showViewController(viewController: assginVC)
  }
}

extension AttachmentDetailViewController: TaskAssignNewViewDelegate {
  
  func taskAssignCloseView(_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?) {
    var defaultMsg: String = "Xử lý thành công !"
    if let confirmMsg: String = approveMsg {
      defaultMsg = confirmMsg
    } else if let backMsg: String = returnMsg {
      defaultMsg = backMsg
    } else if let failMsg: String = errorMsg {
      defaultMsg = failMsg
    }
    self.view.makeToast(defaultMsg)
    
    if errorMsg == nil {
        self.delegate?.doneView(rowIndex: indexPath, action: .approve)
    }
  }
}

extension AttachmentDetailViewController: TaskAssignNoneApproveDelegate {
  
  func onCloseView(status: Bool, indexPath: IndexPath?) {
    if status {
      self.view.makeToast("Xử lý thành công !")
        self.delegate?.doneView(rowIndex: indexPath, action: .assign)
    } else {
      self.view.makeToast("Có lỗi xảy ra !")
    }
  }
}

extension AttachmentDetailViewController: ProcessTaskViewDelegate {
  
  func sendProcessWork(status: Bool, message: String?, index: IndexPath?) {
    if status {
      var msg = "Xử lý thành công !"
      if let msgTemp: String = message,
        msgTemp != "" {
        msg = msgTemp
      }
      self.view.makeToast(msg)
        
        self.delegate?.doneView(rowIndex: index, action: .process)
    } else {
      self.view.makeToast("Có lỗi xảy ra !")
    }
  }
  
  
  func closeView() {
  }
  
}

extension AttachmentDetailViewController: AppriseTaskViewDelegate {
  
  func appriseCloseView() {
  }
  
  func appriseDoneView(status: Bool, message: String?, index: IndexPath?) {
    self.view.makeToast("Gửi yêu cầu xử lý thành công")
    self.delegate?.doneView(rowIndex: index, action: .appraise)
  }
  
}

extension AttachmentDetailViewController: SendEmailViewDelegate {
  
  func sendEmailCloseView() {
    
  }
  
  func sendEmailDoneView() {
    self.view.makeToast("Gửi mail thành công !")
  }
  
}
