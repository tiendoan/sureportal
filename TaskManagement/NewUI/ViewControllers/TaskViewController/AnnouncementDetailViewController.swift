//
//  AnnouncementDetailViewController.swift
//  TaskManagement
//
//  Created by Scor Doan on 8/17/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit


class AnnouncementDetailViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var htmlBody: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        webView.loadHTMLString(htmlBody, baseURL: nil)
    }
}
