
//  BoardViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import EBCardCollectionViewLayout
import SVProgressHUD
import DropDown

protocol viewAttachmentFileDelegate {
    func viewAttachmentfile(fileDocument:FileDocument,procedureId:String)
}


class BoardViewController: UIViewController,viewAttachmentFileDelegate {
    func viewAttachmentfile(fileDocument: FileDocument, procedureId: String) {
        
        let fileName:String = fileDocument.name!
        let fileId:String = fileDocument.id!
        self.presentAttachmentFileDigital(filename: fileName, fileId: fileId, procedureId: procedureId)
        
    }
    
    
    @IBOutlet fileprivate weak var collectionView:UICollectionView!
    @IBOutlet fileprivate weak var pageControl:UIPageControl!
    
    @IBOutlet weak var subMenuButton: UIButton!
    var menus:[Menu] = []
    let taskDropDown: DropDown = DropDown()
    var needReload = false
    var needReloadDigital = false
    var selectedMenuIdx = 0
    var selectedForm:String!
    var selectedTo:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let fromDate = String(format: "%d-%02d-%02dT00:00:00+07:00",year,month,day)
        let toDate = String(format: "%d-%02d-%02dT23:59:00+07:00",year,month,day)
        self.selectedForm = fromDate
        self.selectedTo = toDate
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(withClass: BoardCollectionCell.self)
        if let layout = self.collectionView.collectionViewLayout as? EBCardCollectionViewLayout{
            //layout.offset = UIOffset(horizontal: 20, vertical: 0)
            layout.layoutType = .horizontal
        }
        //self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        self.collectionView.isScrollEnabled = false
        self.pageControl.numberOfPages = 0
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadDataforce(notfication:)), name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshNewData(notfication:)), name: NSNotification.Name(rawValue: NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil)
        
    }
    @objc func reloadDataforce(notfication: NSNotification) {
        
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE || keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE ) {
             needReloadDigital = true
             self.collectionView.reloadData()
        }
       
    }
  
    @objc func refreshNewData(notfication: NSNotification) {
        
        if selectedMenuIdx < menus.count, let cell = collectionView.cellForItem(at: IndexPath(row: selectedMenuIdx, section: 0)) as? BoardCollectionCell {
            cell.refresh(nil)
        }
        
    }
    
    func setupDropdownMenu() {
        subMenuButton.isHidden = menus.isEmpty
        if let name = menus.first?.name?.uppercased() {
            self.subMenuButton.setTitle("\(name)   ▼", for: .normal)
        } else {
            self.subMenuButton.setTitle("--   ▼", for: .normal)
        }
        
        
        taskDropDown.anchorView = subMenuButton
        taskDropDown.width = 250
        taskDropDown.bottomOffset = CGPoint(x: 0, y: subMenuButton.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            
            let menuCode = self.menus[index]
            if ( menuCode.code != nil && menuCode.code! == "MeetingRoom_Report" )
            {
                self.viewReportBookingMeetingRoom()
            }
            else if ( menuCode.code != nil &&  menuCode.code! == "MeetingRoom_Statistic"  )
            {
                self.viewChartBookingMeetingRoom()
            }
            else if ( menuCode.code != nil &&  menuCode.code! == "Vehicle_Report" )
            {
                self.viewReportBookingVehicel()
            }
            else if ( menuCode.code != nil &&  menuCode.code! == "Vehicle_Statistic" )
            {
                self.viewReportBookingVehicel()
            }
            else
            {
                self.subMenuButton.setTitle("\(item.uppercased())   ▼", for: .normal)
                self.collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: false)
                self.pageControl.currentPage = index
                self.selectedMenuIdx = index

                NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICATION_MENU_SELECT_PARENT), object: ["IndexChild":index], userInfo: nil)

            }
            
        }
        
        // dropdown datasource

        taskDropDown.dataSource = menus.map {
            var count = $0.count ?? 0
            let codeMenu = $0.code
            if let uName: String = $0.name?.uppercased() {
                
                let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
                if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE && $0.code != nil )
                {
                    let navDigitalObject:[NavDigitalObject] = DataManager.shared.navigDigitalObject.filter({ $0.Code!.uppercased() == codeMenu!.uppercased() })
                    if navDigitalObject != nil && navDigitalObject.count >  0
                    {
                        count  = navDigitalObject[0].Total!
                    }
                    return count > 0 ? uName + " [\(count)]" : uName
                }
                else if ( keyValue == Constants.MEETING_ROOM_CODE && $0.id != $0.parentId )
                {
                    return count > 0 ? uName + " [\(count)]" : uName
                }
                else
                {
                    return count > 0 ? uName + " [\(count)]" : uName
                }
            } else {
                return "Chưa xác định"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if needReload {
            collectionView.reloadData()
            needReload = false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func selectMenuDropDown(_ sender: Any) {
        taskDropDown.show()
    }
    
    func setupMenus(_ menus: [Menu],_ selectItem: Int, updateMenuOnly: Bool = false) {
        self.menus.removeAll()
        self.menus.append(contentsOf: menus)

        self.setupDropdownMenu()
        guard menus.count > 0 else {
            self.collectionView.isHidden = true
            return
        }
        var index = updateMenuOnly ? selectedMenuIdx : selectItem
        if index >= menus.count {
            index = 0
        }
        let selectedMenu = menus[index]
        let name = selectedMenu.name?.uppercased() ?? "-"
        var count = selectedMenu.count ?? 0
        
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE && selectedMenu.code != nil )
        {
            let navDigitalObject:[NavDigitalObject] = DataManager.shared.navigDigitalObject.filter({ $0.Code!.uppercased() == selectedMenu.code!.uppercased() })
            if navDigitalObject != nil && navDigitalObject.count >  0
            {
                count  = navDigitalObject[0].Total!
            }
            
        }
        
        let title = count > 0 ? "\(name) [\(count)]   ▼" : "\(name)   ▼"
        
        selectedMenuIdx = index
        if updateMenuOnly {
            self.subMenuButton.setTitle(title, for: .normal)
            return
        }
        if(self.collectionView != nil){
            self.collectionView.isHidden = true
            self.subMenuButton.setTitle(title, for: .normal)

            self.collectionView.reloadData {
                self.collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: false)
                self.pageControl.currentPage = selectItem
                self.collectionView.isHidden = false
            }
            self.pageControl.numberOfPages = self.menus.count
            
//            if (  selectedMenu.code! == "Vehicle_Report" )
//            {
//                self.viewReportBookingVehicel()
//            }

            
        }
    }
    
    func setupMenus(_ selectItem: Int) {
        if(self.collectionView != nil){
            self.collectionView.reloadData {
                self.collectionView.scrollToItem(at: IndexPath(row: selectItem, section: 0), at: .centeredHorizontally, animated: false)
                self.pageControl.currentPage = selectItem
            }
            self.pageControl.numberOfPages = self.menus.count
        }
    }
    
}

// MARK: Define extensions
// ---------------------------

extension BoardViewController: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BoardCollectionCell", for: indexPath) as! BoardCollectionCell
        let parentIdx:Int = UserDefaults.standard.integer(forKey: Constants.USER_LAST_CHILD_MENU_KEY) as Int
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        cell.selectedTo = self.selectedTo
        cell.selectedForm = self.selectedForm
        cell.delegate = self
        if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE ) {
            if ( parentIdx == indexPath.row ) {
                self.needReloadDigital = true
                cell.needReloadDigital = self.needReloadDigital
                cell.config(menus[indexPath.row])
                self.needReloadDigital = false
            }
        } else {
            cell.config(menus[indexPath.row])
        }
        cell.indexItem = indexPath
        return cell
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let layout = self.collectionView.collectionViewLayout as? EBCardCollectionViewLayout{
            pageControl.currentPage = layout.currentPage
        }
    }
}

extension BoardViewController: BoardCollectionCellDelegate {
    func callbackGenerateDate(startTime: String, endTime: String) {
        
    }
    
    func viewReportBookingVehicel() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Vehicel", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "VehicelReportVC") as! VehicelReportVC
            self.showViewController(viewController: newViewController, false)
        }
        
    }
    
    func assignVehicelType(arrayMeetingBookingRoomObject: [GetMeetingRoomObject], location: MeetingRoomObject, department: MeetingRoomObject,OrgID: String,departmentUsr:[GetMeetingRoomObject]) {
        
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Vehicel", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "VehicelManagermentVC") as! VehicelManagermentVC
            newViewController.arrayMeetingBookingRoomObject = arrayMeetingBookingRoomObject
            newViewController.Department = department
            newViewController.Location = location
            newViewController.OrgID = OrgID
            newViewController.departmentUser = departmentUsr
            self.showViewController(viewController: newViewController, false)
        }
        
    }
    
    
    func viewHeadTitle(objectHeader: GetMeetingRoomObject) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "BookingMeetingRoom", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PresentRoomDetail") as! PresentRoomDetail
        newViewController.objectGetting = objectHeader
        newViewController.modalPresentationStyle = .overCurrentContext
        newViewController.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
        self.navigationController?.present(newViewController, animated: true, completion: {})
    }
    
    
    // procedure Digital
    func showDetailViewController(procedure: ProdureDigital) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalViewDetail") as! ProcedureDigitalViewDetail
        newViewController.procedureDetail = procedure
        self.navigationController?.pushViewController(newViewController)
        
    }
    func showAttachDetailViewController(fileDocument: [FileDocument], procedureId:String) {
        
        if ( fileDocument.count == 1 )
        {
            let fileName:String = fileDocument[0].name!
            let fileId:String = fileDocument[0].id!
            self.presentAttachmentFileDigital(filename: fileName, fileId: fileId, procedureId: procedureId)
        }
        else
        {
            let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "DialogAttachmentVC") as! DialogAttachmentVC
            mainViewController.delegate = self
            mainViewController.arrayFileName = fileDocument
            mainViewController.procedureId = procedureId
            self.present(mainViewController, animated: false, completion: nil)
        }
    }
    func presentAttachmentFileDigital(filename:String, fileId: String, procedureId: String){
        
        let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "AttachmentDigittalDetailView") as! AttachmentDigittalDetailView
        mainViewController.fileName = filename
        mainViewController.fileId = fileId
        mainViewController.procedureId = procedureId
        self.present(mainViewController, animated: false, completion: nil)
        
    }
    func showAllActionItem(procedure: ProdureDigital) {
        
        let actionSheet = UIAlertController(title: "Chọn", message: nil, preferredStyle: .actionSheet)
            for i in 0..<(procedure.itemAction?.count)! {
                let action: UIAlertAction = UIAlertAction(title: procedure.itemAction![i].Name!, style: .default) { (_) in
                let itemAction = procedure.itemAction![i].Code!
                if ( itemAction == "Delete" )
                {
                    let alert = UIAlertController(title: "Xoá Quy Trình Số", message: "Bạn thực sự muốn xoá quy trình ?", preferredStyle: .alert)
                    
                    let actionOK = UIAlertAction(title: LocalizationString.AGREE_STRING, style: .cancel) { (action) in
                        
                        SyncProvider.deleteProcedureDigital(Id: procedure.ID!, done: {
                            (result) in
                            
                            self.view.makeToast("Cập nhật thành công")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                self.collectionView.reloadData()
                            }
                        })
                        
                        
                    }
                    let actionCancel = UIAlertAction(title: LocalizationString.CANCEL_STRING, style: .default, handler: nil)
                    alert.addAction(actionOK)
                    alert.addAction(actionCancel)
                    self.mainNew()?.present(alert, animated: true, completion: nil)
                }
                    
                else if (itemAction == "Return" ||
                    itemAction == "Reject" || itemAction == "Transfer" || itemAction == "Assign" || itemAction == "Audit")
                {                    
                    let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
                    let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalTaskVC") as! ProcedureDigitalTaskVC
                    mainViewController.procedureDetail = procedure
                    mainViewController.itemAction = procedure.itemAction![i]
                    self.navigationController?.pushViewController(mainViewController)
                }
                else if ( itemAction == "Approve")
                {
                    let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
                    let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalApproveTaskVC") as! ProcedureDigitalApproveTaskVC
                    mainViewController.procedureDetail = procedure
                    mainViewController.itemAction = procedure.itemAction![i]
                    self.navigationController?.pushViewController(mainViewController)
                    
                }
            }
            actionSheet.addAction(action)
            }
            let cancel: UIAlertAction = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    func showSelectedActionItem(procedure: ProdureDigital, itemAction: ItemActions) {
        
        if ( itemAction.Code! == "Assign")
        {
            let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalTaskVC") as! ProcedureDigitalTaskVC
            mainViewController.procedureDetail = procedure
            mainViewController.itemAction = itemAction
            self.navigationController?.pushViewController(mainViewController)
        }
        else if ( itemAction.Code! == "Approve")
        {
            let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalApproveTaskVC") as! ProcedureDigitalApproveTaskVC
            mainViewController.procedureDetail = procedure
            mainViewController.itemAction = itemAction
            self.navigationController?.pushViewController(mainViewController)
        }
        
    }
    
    
    func taskCellDidSelected(document: Document?, itemIndex: IndexPath?, rowIndex: IndexPath?) {
        let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        detailVC.document = document
        detailVC.didFinishDocument = { [weak self] in
            self?.needReload = true
        }
        detailVC.didupdatedDocument = { [weak self] (isFinish) in
            guard let `self` = self else { return }
            guard let index = rowIndex, let cell = self.collectionView.cellForItem(at: IndexPath(row: self.selectedMenuIdx, section: 0)) as? BoardCollectionCell  else { return }
            
            cell.reloadItem(indexPath: index, isFinish: isFinish)
        }
        self.navigationController?.pushViewController(detailVC)
        InstanceDB.default.updateStatusDocument(documentID: document?.id ?? "", done: { (result) in
        })
    }
    
    
    
    func finishTaskProcess(document: Document?, index: IndexPath?) {
        //self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
    }
    
    func showController(controller: UIViewController, isPush: Bool) {
        if isPush {
            self.showViewController(viewController: controller, false)
        } else {
            self.navigationController?.pushViewController(controller)
        }
        
    }
    
    func assignTaskProcess(document: Document?, index: IndexPath?) {
        DispatchQueue.main.async {
            let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            assignVC.document = document
            assignVC.delegate = self
            assignVC.indexPath = index
            assignVC.getAssigners()
            self.showViewController(viewController: assignVC, false)
        }

    }
    func assignTaskProcedure()
    {
        DispatchQueue.main.async {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "NewProcessProcedureDigitalVC") as! NewProcessProcedureDigitalVC
            self.showViewController(viewController: newViewController, false)
            
        }
    }
    func viewChartBookingMeetingRoom() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "BookingMeetingRoom", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MeetingRoomCharVC") as! MeetingRoomCharVC
            self.showViewController(viewController: newViewController, false)
        }

    }
    func viewReportBookingMeetingRoom() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "BookingMeetingRoom", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MeetingRoomReportVC") as! MeetingRoomReportVC
            self.showViewController(viewController: newViewController, false)
        }

    }
 
    func assignBookingMeetingRoom(orgID: String){
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "BookingMeetingRoom", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MeetingRoomManagementVC") as! MeetingRoomManagementVC
            newViewController.OrgID = orgID
            self.showViewController(viewController: newViewController, false)
        }
     }

    func approveTaskProcess(document: Document?, index: IndexPath?) {
        DispatchQueue.main.async {
            let approveVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            approveVC.document = document
            approveVC.indexItem = index
            approveVC.delegate = self
            self.showViewController(viewController: approveVC, false)

        }
    }
    
    func processTaskProcess(document: Document?, index: IndexPath?) {
        DispatchQueue.main.async {
            let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            processVC.delegate = self
            processVC.indexPath = index
            processVC.document = document
            self.showViewController(viewController: processVC, false)

        }
    }
    
    func appriseTaskProcess(document: Document?, index: IndexPath?) {
        DispatchQueue.main.async {
            let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            appriseVC.delegate = self
            appriseVC.indexPath = index
            appriseVC.document = document
            appriseVC.indexPath = index
            self.showViewController(viewController: appriseVC, false)
        }
     }
    
    func emailTaskProcess(document: Document?, index: IndexPath?) {
        let emailVC: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        emailVC.document = document
        emailVC.delegate = self
        self.showViewController(viewController: emailVC, false)
    }
    
    func taskCellAttachSelected(document: Document?, rowIndex: IndexPath?) {
        if(document != nil && document!.fileDocuments != nil && document!.fileDocuments!.count == 1){
            let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
            vc?.fileDoucment = document!.fileDocuments![0]
            vc?.document = document!
            vc?.indexPath = rowIndex
            vc?.delegate = self
            self.present(vc!, animated: true, completion: nil)
        } else {
            let vc: ListFileAttachViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle =  .overCurrentContext
            vc.files = document?.fileDocuments ?? []
            vc.indexPath = rowIndex
            vc.document = document!
            vc.onUpdateDocument = { [weak self] (selectedIndex, action) in
                guard let `self` = self, let cell = self.collectionView.cellForItem(at: IndexPath(row: self.selectedMenuIdx, section: 0)) as? BoardCollectionCell  else { return }
                
                cell.reloadItem(indexPath: selectedIndex, isFinish: action == .finish)
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func actionHistoryProcess(document: Document?) {
        let assginVC: TaskHistoryProcessViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        let nav = UINavigationController(rootViewController: assginVC)
        nav.isNavigationBarHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: false, completion: nil)
    }
    
    func showAllProcess(document: Document?, index: IndexPath?) {
        guard let selectedDocument: Document = document else {
            return
        }
        let actionSheet = UIAlertController(title: "Chọn", message: nil, preferredStyle: .actionSheet)
        for i in 0..<selectedDocument.actions!.count {
            let action: UIAlertAction = UIAlertAction(title: selectedDocument.actions![i].actionName, style: .default) { (_) in
                if let actionType: DocumentAction = selectedDocument.actions![i].actionType {
                    if actionType == .approve {
                        let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        assignVC.document = selectedDocument
                        assignVC.indexItem = index
                        assignVC.delegate = self
                        self.showViewController(viewController: assignVC, false)
                    } else if actionType == .assign {
                        let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        assignVC.delegate = self
                        assignVC.document = selectedDocument
                        assignVC.getAssigners()
                        self.showViewController(viewController: assignVC, false)
                    } else if actionType == .process {
                        let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        processVC.delegate = self
                        self.showViewController(viewController: processVC, false)
                    } else if actionType == .appraise {
                        let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        appriseVC.delegate = self
                        appriseVC.document = document
                        appriseVC.indexPath = index
                        self.showViewController(viewController: appriseVC, false)
                    } else if actionType == .sendMail {
                        let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        sendEmailCV.document = document
                        sendEmailCV.delegate = self
                        self.showViewController(viewController: sendEmailCV, false)
                    } else if actionType == .finish {
                        self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
                    }
                }
            }
            actionSheet.addAction(action)
        }
        let cancel: UIAlertAction = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
}

// MARK: ProcessTaskViewDelegate
extension BoardViewController: ProcessTaskViewDelegate {
    
    func sendProcessWork(status: Bool, message: String?, index: IndexPath?) {
        if status {
            var msg = "Xử lý thành công !"
            if let msgTemp: String = message,
                msgTemp != "" {
                msg = msgTemp
            }
            self.view.makeToast(msg)
            
            guard let index = index, let cell = self.collectionView.cellForItem(at: IndexPath(row: selectedMenuIdx, section: 0)) as? BoardCollectionCell  else { return }
            
            cell.reloadItem(indexPath: index)

        } else {
            self.view.makeToast("Có lỗi xảy ra, chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !")
        }
    }
    
    
    func closeView() {
    }
    
}

// MARK: AppriseTaskViewDelegate
extension BoardViewController: AppriseTaskViewDelegate {
    
    func appriseCloseView() {
    }
    
    func appriseDoneView(status: Bool, message: String?, index: IndexPath?) {
        self.view.makeToast("Xử lý thành công !")
        guard let index = index, let cell = self.collectionView.cellForItem(at: IndexPath(row: selectedMenuIdx, section: 0)) as? BoardCollectionCell  else { return }
        
        cell.reloadItem(indexPath: index)
    }
    
}

// MARK: SendEmailViewDelegate
extension BoardViewController: SendEmailViewDelegate {
    
    func sendEmailCloseView() {
        
    }
    
    func sendEmailDoneView() {
        self.view.makeToast("Gửi mail thành công !")
    }
    
}

// MARK: TaskAssignNewViewDelegate
extension BoardViewController: TaskAssignNewViewDelegate {
    
    func taskAssignCloseView(_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?) {
        guard let index = index, let cell = self.collectionView.cellForItem(at: IndexPath(row: selectedMenuIdx, section: 0)) as? BoardCollectionCell  else { return }
        var defaultMsg: String = "Xử lý thành công !"
        if let confirmMsg: String = approveMsg {
            defaultMsg = confirmMsg
        } else if let backMsg: String = returnMsg {
            defaultMsg = backMsg
        } else if let failMsg: String = errorMsg {
            defaultMsg = failMsg
        }
        self.view.makeToast(defaultMsg)
        cell.reloadItem(indexPath: index)
    }
}

// MARK: TaskAssignNoneApproveDelegate
extension BoardViewController: TaskAssignNoneApproveDelegate {
    
    func onCloseView(status: Bool, indexPath: IndexPath?) {
        guard let index = indexPath, let cell = self.collectionView.cellForItem(at: IndexPath(row: selectedMenuIdx, section: 0)) as? BoardCollectionCell  else { return }
        if status {
            self.view.makeToast("Xử lý thành công !")
            cell.reloadItem(indexPath: index)

        } else {
            self.view.makeToast("Có lỗi xảy ra !")
        }
    }
    
}

// MARK: AttachmentDetailViewDelegate
extension BoardViewController: AttachmentDetailViewDelegate {
    func doneView(rowIndex: IndexPath?, action: DocumentAction) {
        if action != .sendMail, let selectedIndex: IndexPath = rowIndex {
            //self.collectionView.reloadItems(at: [selectedIndex])
            guard let cell = self.collectionView.cellForItem(at: IndexPath(row: selectedMenuIdx, section: 0)) as? BoardCollectionCell  else { return }
            
            cell.reloadItem(indexPath: selectedIndex, isFinish: action == .finish)
        }
    }
    
}

