//
//  DashboardViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwipeCellKit
import Kingfisher


class DashboardViewController: UIViewController {
    
    @IBOutlet fileprivate weak var tableView:UITableView!
    @IBOutlet weak var vwContentView: UIView!
    
    var announcements = [Announcement]() {
        didSet {
             refresh.endRefreshing()
            self.tableView.beginUpdates()
            self.tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.automatic)
            self.tableView.endUpdates()
        }
    }
    var notMore = true
    fileprivate var selectedIndexPath: IndexPath?
    var countApi = 0
    var currentTaskList: [[ItemTask]] = [] {
        didSet {
            if notMore {
                self.tableView.reloadData()
            }
        }
    }
    
    var currentTaskTemp: [[ItemTask]] = []
    
    var listHeader: [String]? {
        didSet {
            self.tableView.reloadData()
            getAllApi()
        }
    }
    var selectedAnnouncement: Announcement?
    let refresh = UIRefreshControl()
    let popUp = FilePopUpView()
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        getActiveModule()
        getAnnouncements()
    }
    
    func getAllApi() {
        guard let listHeader = listHeader else { return }
        for (index, header) in listHeader.enumerated() {
            if index == countApi {
                self.getCurrentTask(moduleCode: header)
            }
        }
    }
    
    func getActiveModule() {
        SyncProvider.getActiveModule { (result) in
            if result != nil {
                self.listHeader = result?.data
            }
        }
    }
    
    func getCurrentTask(moduleCode: String) {
        SyncProvider.getCurrentTask(moduleCode: moduleCode) { (result) in
            self.countApi += 1
             self.getAllApi()
            if let result = result {
                self.currentTaskTemp.append(result.item)
                if result.item.count > 3 {
                    let listItem = result.item[0...2]
                    self.currentTaskList.append(Array(listItem))
                } else {
                    self.currentTaskList.append(result.item)
                }
                print(result)
            } else {
                self.currentTaskList.append([])
                self.currentTaskTemp.append([])
            }
           
        }
    }
    
    func configureTableView() {
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        refresh.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableView.addSubview(refresh)
        tableView.registerCustomCell(HomeCell.self)
        tableView.registerCustomCell(HeaderCell.self)
        tableView.registerCustomCell(FooterCell.self)
        
        //Get New Menu ID
//        InstanceDB.default.getMenuID(code: "CVHT_MOI") { (menu) in
//            self.newMenuID = menu?.id
//        }
        
        // Update Kingfisher cookie by getting cookie from iOS Native
        let cookie = KingfisherManager.shared.downloader.sessionConfiguration
        cookie.httpCookieStorage = HTTPCookieStorage.shared
        
        KingfisherManager.shared.downloader.sessionConfiguration = cookie
        
        //Get Data
        getAnnouncements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let format = selectedAnnouncement?.type == .event ? "dd 'Th'MM, yyyy - HH:mm" : "dd 'Tháng' MM, yyyy"
        let displayDate = selectedAnnouncement?.createdDate?.toFormatDate(format: format)
        if segue.identifier == "ToAnnouncementDetail", let controller = segue.destination as? AnnouncementDetailViewController, let body = selectedAnnouncement?.body {
            let titleAndDateHTMLString = " <p><span style=\"font-size: 18pt; font-family: arial, helvetica, sans-serif;\">\(selectedAnnouncement?.subject ?? "")</span></p><p><span style=\"font-size: 12pt; color: #808080; font-family: arial, helvetica, sans-serif;\">\(displayDate ?? "")</span></p> "
            let fullHTMLpageString = "\(titleAndDateHTMLString) \(body)"
            controller.htmlBody = fullHTMLpageString
        }
    }
    
    func refresh(_ sender:UIRefreshControl?) {
        notMore = true
        countApi = 0
        currentTaskTemp = []
        currentTaskList = []
        getAnnouncements()
        getActiveModule()
    }
    
    func getAnnouncements() {
        SyncProvider.getTopAnnouncements { (announcements) in
            self.announcements = announcements
        }
    }
}

extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentTaskList.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            
            return currentTaskList[section - 1].count + 2
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 350
        } else {
            switch indexPath.row {
            case 0:
                return 80
            case currentTaskList[indexPath.section - 1].count + 1:
                return 40
            default:
                return 150
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncementParentViewCell", for: indexPath) as! AnnouncementParentViewCell
            
            cell.configCell(announcements) { [weak self] (announcement) in
                self?.selectedAnnouncement = announcement
                self?.performSegue(withIdentifier: "ToAnnouncementDetail", sender: nil)
            }
            return cell
        } else {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueCustomCell(HeaderCell.self)
                if let listHeader = self.listHeader {
                    cell.header = listHeader[indexPath.section - 1]
                    cell.lbTotal.text = "\(currentTaskTemp[indexPath.section - 1].count)"
                }
                return cell
                
            case currentTaskList[indexPath.section - 1].count + 1:
                let cell = tableView.dequeueCustomCell(FooterCell.self)
                //--check last array then hide button add
                print("section: \(indexPath.section), row: \(indexPath.row)")
                if currentTaskTemp[indexPath.section - 1].count == 0 {
                    cell.vNoData.isHidden = false
                } else {
                    cell.vNoData.isHidden = true
                }
                if indexPath.row >= (currentTaskTemp[indexPath.section - 1].count + 1) {
                    cell.btnMore.isHidden = true
                } else {
                    cell.btnMore.isHidden = false
                }
                cell.indexPath = indexPath
                cell.delegate = self
                return cell
                
            default:
                let cell = tableView.dequeueCustomCell(HomeCell.self)
                cell.indexPath = indexPath
                cell.delegate = self
                cell.currentTask = currentTaskList[indexPath.section - 1][indexPath.row - 1]
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let listHeader = self.listHeader else { return }
        switch listHeader[indexPath.section - 1] {
        case ActiveModuleName.Document.rawValue,
             ActiveModuleName.Task.rawValue:
            let task = currentTaskList[indexPath.section - 1][indexPath.row - 1]
            let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            detailVC.idDocument = task.mobileObjectId
            self.navigationController?.pushViewController(detailVC)
        case ActiveModuleName.DigitalProcedure.rawValue:
            break
        case ActiveModuleName.TimeSheet.rawValue:
            break
        case ActiveModuleName.BusinessTripRequest.rawValue:
            break
        case ActiveModuleName.Vehicle.rawValue:
            break
        case ActiveModuleName.MeetingRoom.rawValue:
            break
        case ActiveModuleName.Stationery.rawValue:
            break
        case ActiveModuleName.DocumentLibrary.rawValue:
            break
        case ActiveModuleName.HelpDesk.rawValue:
            break
        default:
            break
        }
    }
}

extension DashboardViewController: FooterCellDelegate, HomeCellDelegate {
    func btnMoreTapped(indexPath: IndexPath) {
        notMore = false
        let section = indexPath.section
        let index = indexPath.row
        var dataAdd: [ItemTask] = []
        if (currentTaskTemp[section - 1].count - (index - 1)) >=  3 {
           dataAdd = Array(currentTaskTemp[section - 1][(index - 1)...(index + 1)])
        } else if (currentTaskTemp[section - 1].count - (index - 1)) > 0 && (currentTaskTemp[section - 1].count - (index - 1)) < 3 {
           dataAdd = Array(currentTaskTemp[section - 1][(index - 1)...(currentTaskTemp[section - 1].count - 1)])
        } else {
            return
        }
        
        let firstIndex = currentTaskList[section - 1].count
        currentTaskList[section - 1].append(contentsOf: dataAdd)
        let lastIndex = currentTaskList[section - 1].count
        tableView.beginUpdates()
        let indexPaths = (firstIndex..<lastIndex).map { IndexPath(row: $0, section: section) }
        tableView.insertRows(at: indexPaths, with: .bottom)
        tableView.endUpdates()
        tableView.reloadRows(at: [IndexPath(row: lastIndex + 1, section: indexPath.section)], with: .none)
    }
    
    func didSelectedCell(at indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        let task = currentTaskList[indexPath.section - 1][indexPath.row - 1]
        let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        detailVC.document?.id = task.mobileObjectId
        
        self.navigationController?.pushViewController(detailVC)
    }
    
    func btnAttachTapped(indexPath: IndexPath) {
        let task = currentTaskList[indexPath.section - 1][indexPath.row - 1]
        if let file = task.file {
            if file.count == 1 {
                let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
                vc?.file = file[0]
                self.present(vc!, animated: true, completion: nil)
            } else {
                popUp.listFile = file
                popUp.delegate = self
                popUp.showPopUp()
            }
        }
    }
}

extension DashboardViewController: FilePopUpViewDelegate {
    func chooseFile(file: Files) {
        let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
        vc?.file = file
        self.present(vc!, animated: true, completion: nil)
    }
}
