//
//  ListFileAttachViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/9/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class ListFileAttachViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet fileprivate weak var btExit:UIButton!
    @IBOutlet fileprivate weak var tableView:UITableView!
    
    var files:[FileDocument]!
    var indexPath: IndexPath?
    var document:Document!
    var onUpdateDocument: ((IndexPath, DocumentAction) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle =  .overCurrentContext
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        
        btExit.setImage(#imageLiteral(resourceName: "ic_clear").filled(withColor: UIColor.darkGray), for: .normal)
        
        //let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "CELL")
        //tableView.register(cell.self, forCellReuseIdentifier: "CELL")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.dataSource = self
        tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func pressesExit(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "CELL")
        if(cell == nil){
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "CELL")
        }
        cell?.backgroundColor = UIColor.clear
        cell?.selectionStyle = .none
        
        let ext = files[indexPath.row].ext ?? ""
        if ext == ".pdf"{
             cell?.imageView?.image = #imageLiteral(resourceName: "ic_file_pdf")
        }
        else if ext == ".doc" || ext == ".docx"{
            cell?.imageView?.image = #imageLiteral(resourceName: "ic_file_doc")
        }
        else if ext == ".xls" || ext == ".xlsx"{
            cell?.imageView?.image = #imageLiteral(resourceName: "ic_file_xls")
        }
        else if ext == ".ppt" || ext == ".pptx"{
            cell?.imageView?.image = #imageLiteral(resourceName: "ic_file_ppt")
        }
        else if ext == ".png"{
            cell?.imageView?.image = #imageLiteral(resourceName: "ic_file_png")
        }
        else if ext == ".jpg"{
            cell?.imageView?.image = #imageLiteral(resourceName: "ic_file_jpg")
        }
        else {
            cell?.imageView?.image = #imageLiteral(resourceName: "ic_file_other")
        }
        
        cell?.textLabel?.text = files[indexPath.row].name ?? ""
        cell?.textLabel?.font = UIFont(name: "Muli-Light", size: 16)
        cell?.textLabel?.lineBreakMode = .byTruncatingMiddle
        cell?.textLabel?.textColor = UIColor(hexString: "#696969")
        
        cell?.detailTextLabel?.text = files[indexPath.row].size != nil ? "\(files[indexPath.row].size!) KB" : ""
        cell?.detailTextLabel?.font = UIFont(name: "Muli-Light", size: 14)
        cell?.detailTextLabel?.textColor = UIColor(hexString: "#696969")
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
        vc?.fileDoucment = files[indexPath.row]
        vc?.document = self.document
        vc?.delegate = self
        vc?.indexPath = indexPath
        self.present(vc!, animated: true, completion: nil)
    }
}

// MARK: AttachmentDetailViewDelegate
extension ListFileAttachViewController: AttachmentDetailViewDelegate {
    func doneView(rowIndex: IndexPath?, action: DocumentAction) {
        if action != .sendMail, let selectedIndex: IndexPath = rowIndex {
            self.onUpdateDocument?(selectedIndex, action)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
