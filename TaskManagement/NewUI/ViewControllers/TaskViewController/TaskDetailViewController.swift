//
//  TaskDetailViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/11/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown
import FSPagerView
import NextGrowingTextView
import SVProgressHUD
import Kingfisher
import UserNotifications
import SwipeCellKit

let kLastOptionIndex = "kLastOptionIndex"
let kLastSortOptionIndex = "kLastSortOptionIndex"

enum ListType {
    case progress, history, listReciver
}

struct FilterItem {
    var name = ""
    var value = 0
}

class TaskDetailViewController: UIViewController {
    
    // MARK: - Outlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet fileprivate weak var pagerView:FSPagerView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var moreIconButton: UIButton!
    @IBOutlet weak var topGradientView: TNGradientView!
    @IBOutlet weak var tableHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreInfoViewHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTextView: NextGrowingTextView!
    @IBOutlet weak var commentTextViewHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentViewBottomtLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentViewTopLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var docStatusLabel: UILabel!
    
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var senderNoLabel: UILabel!
    @IBOutlet weak var senderDateLabel: UILabel!
    @IBOutlet weak var reciverNoLabel: UILabel!
    @IBOutlet weak var reciverDateLabel: UILabel!
    @IBOutlet weak var docTypeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var viewFilterButton: UIButton!
    @IBOutlet weak var statusFilterButton: UIButton!
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var pagerHeightConstraint: NSLayoutConstraint!
    
    //@IBOutlet weak var relatedCollectionView: UICollectionView!             // Need to confirm with client this
    //For task
    @IBOutlet weak var assginByHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var assginAvatarImageView: UIImageView!
    @IBOutlet weak var assginNameLabel: UILabel!
    @IBOutlet weak var assginJobTitleLabel: UILabel!
    
    @IBOutlet weak var sttTextLabel: UILabel!
    @IBOutlet weak var reciverDataTextLabel: UILabel!
    @IBOutlet weak var docTypeTextLabel: UILabel!
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var importantStatusButton: UIButton!
    
    @IBOutlet weak var serialTextLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var toTextLabel: UILabel!
    @IBOutlet weak var senderTextLabel: UILabel!
    @IBOutlet weak var statusFilterStackView: UIStackView!
    @IBOutlet weak var relatedTextLabel: UILabel!
    @IBOutlet weak var relatedCollectionHeighttLayoutConstraint: NSLayoutConstraint!
    
    //MARK: - constant, static
    static let MAX_RECEIVERS_IN_LINES: Int = 3
    
    //MARK: - var
    fileprivate var dropDown:DropDown = DropDown()
    fileprivate var moreBarButton: UIBarButtonItem!
    fileprivate var viewType = 1
    fileprivate var actions = [DocumentActionObject]()
    fileprivate var viewFilter: [FilterItem] = [
        FilterItem(name: "Công việc - Tất cả", value: 0),
        FilterItem(name: "Công việc - Phòng ban", value: 1),
        FilterItem(name: "Công việc - Cá nhân", value: 2),
        FilterItem(name: "Danh sách nơi nhận", value: -1),
        FilterItem(name: "Lịch sử xử lý", value: -2),
        ]
    fileprivate var statusFilter = [
        FilterItem(name: "Tất cả", value: -1),
        FilterItem(name: "Mới", value: 0),
        FilterItem(name: "Đang xử lý", value: 1),
        FilterItem(name: "Báo cáo", value: 2),
        FilterItem(name: "Hoãn xử lý", value: 3),
        FilterItem(name: "Kết thúc", value: 4),
        ]
    fileprivate var status = -1
    fileprivate var filter = 0
    fileprivate var lastDepartMentName = ""
    
    //data
    fileprivate var commentItems = [String]()
    fileprivate var expendItems = [Bool]()
    fileprivate var fileItems = [FileDocument]()
    fileprivate var trackingItems = [ProgressTracking]() {
        didSet {
            expendItems = [Bool]()
            for _ in self.trackingItems {
                expendItems.append(false)
            }
        }
    }
    fileprivate var historyItems = [ProgressHistory]()
    fileprivate var tranferItems = [Transfer]()
    fileprivate var assignersTrackings = [AssignTracking]()
    fileprivate var pageTrackingItems = [ProgressTracking]()        //using to keep ProgressTracking on each of page
    var document: Document? {
        didSet{
            if let doc = self.document {
                self.actions = doc.actions?.reversed() ?? []
                self.fileItems = doc.fileDocuments ?? []
                //self.trackingItems = doc.trackingDocuments ?? []
            }
        }
    }
    var listType: ListType = .progress
    var documentFromNotification: DocumentNotification?
    var isSubPage = false
    var didFinishDocument: (() -> Void)?
    var didupdatedDocument: ((_ isFinish: Bool) -> Void)?
    
    var idDocument: String? {
        didSet {
            guard let idDoc = idDocument else { return }
            SVProgressHUD.show()
            InstanceDB.default.getDocumentDetail(documentId: idDoc) { (document) in
                if let doc = document {
                    self.document = doc
                    self.getTrackings()
                    self.initData()
                    self.reload()
                    print(doc.toJSON())
                    self.didupdatedDocument?(false)
                }
                SVProgressHUD.dismiss()
            }
        }
       
    }
    
    //MARK: Init data
    func initData() {
        // default : Tat ca
        //self.selectViewFilterAt(index: 0)
        //self.selectViewStatusAt(index: 0)
        //    if let index = UserDefaults.standard.value(forKey: kLastOptionIndex) as? Int {
        //      self.selectViewFilterAt(index: index)
        //    }
        //    if let index = UserDefaults.standard.value(forKey: kLastSortOptionIndex) as? Int {
        //      self.selectViewStatusAt(index: index)
        //    }
        self.fileNameLabel.text = ""
        moreInfoViewHeightLayoutConstraint.constant = 0
        pagerHeightConstraint.constant = 0
        commentItems = []
        if (document?.relatedDocuments?.count ?? 0) > 0 {
            self.relatedTextLabel.text = "VĂN BẢN LIÊN KẾT"
        } else {
            self.relatedTextLabel.text = " "
        }
        if document?.type == .document {
            nameLabel.text = document?.summary ?? "Chưa xác định"
            senderLabel.text = "Chưa xác định"
            if let to = document?.sender, !to.isEmpty {
                senderLabel.text = to
            }
            senderNoLabel.text = document?.serialNumber ?? "Chưa xác định"
            senderDateLabel.text = Constants.formatDate(document?.docDate) ?? "Chưa xác định"
            statusLabel.text = "Chưa xác định"
            if let signedBy = document?.approvedByName, !signedBy.isEmpty{
                statusLabel.text = signedBy
            }
            reciverNoLabel.text = "\(document?.docNumber ?? 0)"
            docTypeLabel.text = document?.authorName ?? "Chưa xác định"
            
            let receiverDate = document?.receivedDate ?? document?.fromDate
            reciverDateLabel.text = Constants.formatDate(receiverDate) ?? ""
            docStatusLabel.text = "  \(document?.statusDocument?.name ?? "")  "
            if let color = document?.statusDocument?.colorCode {
                docStatusLabel.backgroundColor = UIColor(hexString: color)
            } else {
                docStatusLabel.backgroundColor = .clear
            }
            assginByHeightLayoutConstraint.constant = 0
            
            if let inComing = document?.isInComing, inComing {
                titleTextLabel.text = "NGƯỜI CHỈ ĐẠO"
                reciverDataTextLabel.text = "NGÀY ĐẾN"
            } else {
                titleTextLabel.text = "NGƯỜI KÝ"
                reciverDataTextLabel.text = ""
                reciverDateLabel.text = ""
            }
        } else if document?.type == .task {
            sttTextLabel.text = "NGÀY BẮT ĐẦU"
            startDateLabel.text = "NGÀY KẾT THÚC"
            docTypeTextLabel.text = "SỐ HIỆU"
            reciverDataTextLabel.text = "DỰ ÁN/CÔNG VIỆC"
            nameLabel.text =  document?.summary ?? " "
            senderLabel.text = ""
            senderNoLabel.text = ""
            senderDateLabel.text = ""
            titleTextLabel.text = ""
            statusLabel.text = ""
            //statusLabel.text =  document?.categorizeName ?? "Chưa xác định"
            reciverNoLabel.text = Constants.formatDate(document?.fromDate) ?? "Chưa xác định"
            docTypeLabel.text = document?.serialNumber ?? "Chưa xác định"
            reciverDateLabel.text = ""
            serialTextLabel.text = ""
            senderTextLabel.text = "NGƯỜI GIAO"
            senderDateLabel.text = Constants.formatDate(document?.toDate) ?? "Chưa xác định"
            docStatusLabel.text = "  \(document?.statusDocument?.name ?? "")  "
            if let color = document?.statusDocument?.colorCode {
                docStatusLabel.backgroundColor = UIColor(hexString: color)
            } else {
                docStatusLabel.backgroundColor = .clear
            }
            assginByHeightLayoutConstraint.constant = 50
            assginNameLabel.text = document?.approvedByName ?? ""
            assginJobTitleLabel.text = document?.approvedByJobTitle ?? ""
            if let avatar = document?.approvedByPicture {
                assginAvatarImageView.setImage(url: (URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar))!)
            }
            
            importantStatusButton.setTitle(document?.importantDocument?.name ?? "Bình thường", for: .normal)
        }
        
        //    relatedCollectionView.reloadData {
        //      self.relatedCollectionHeighttLayoutConstraint.constant = self.relatedCollectionView.contentSize.height
        //    }
        
        self.updateFile(index: 0)
    }
    
    // MARK: - Life
    override func viewDidLoad() {
        super.viewDidLoad()
        showBack()
        
        self.commentViewTopLayoutConstraint.constant = 0
        setupRightBarButtonItems()
        //relatedCollectionView.register(withClass: RelatedDocCollectionViewCell.self)
        tableView.register(withClass: DetailHandleTableViewCell.self)
        tableView.register(withClass: LeftDiscussionTableViewCell.self)
        tableView.register(withClass: DetailPersonTableViewCell.self)
        //    tableView.register(withClass: LeftDiscussionImgTableViewCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100//UITableViewAutomaticDimension
        //config FSPagerView
        self.pagerView.transformer = FSPagerViewTransformer(type:.coverFlow)
        self.pagerView.isInfinite = false
        self.pagerView.register(CustomFilePagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.dataSource = self
        self.pagerView.delegate = self
        self.pagerView.itemSize = CGSize(width: 100, height: 130)
        self.initData()
        //    docTypeLabel.text = document?.workFlow ?? ""
        let attributedString = NSMutableAttributedString(string: "Nhập bình luận", attributes: [NSFontAttributeName :  UIFont(name: "Muli-SemiBold", size: 14)!, NSForegroundColorAttributeName:#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)])
        commentTextView.placeholderAttributedText = attributedString
        commentTextView.textView.font = UIFont(name: "Muli-SemiBold", size: 14)!
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        DropDown.startListeningToKeyboard()
        
        SVProgressHUD.show()
        var documentID = self.document?.id ?? ""
        if let documentNoti: DocumentNotification = self.documentFromNotification,
            let itemID = documentNoti.itemID {
            documentID = itemID
            self.updateReadNotificationDocument(itemID: itemID, itemType: documentNoti.itemType!)
        }
        InstanceDB.default.getDocumentDetail(documentId: documentID) { (document) in
            if let doc = document {
                self.document = doc
                self.selectViewFilterAt(index: 0)
                self.selectViewStatusAt(index: 0)
                self.initData()
                self.reload()
                if self.document?.type == .task {
                    self.actionExpendButton(self.moreButton)
                }
                print(doc.toJSON())
            }
            SVProgressHUD.dismiss()
            
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_RELOAD), object: nil, userInfo: nil)
        }

        // Do any additional setup after loading the view.
        getAssignTracking()
    }
    
    func getDocumentDetail() {
        SVProgressHUD.show()
        var documentID = self.document?.id ?? ""
        if let documentNoti: DocumentNotification = self.documentFromNotification,
            let itemID = documentNoti.itemID {
            documentID = itemID
        }
        InstanceDB.default.getDocumentDetail(documentId: documentID) { (document) in
            if let doc = document {
                self.document = doc
                self.getTrackings()
                self.initData()
                self.reload()
                print(doc.toJSON())
                self.didupdatedDocument?(false)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    deinit {
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let obj = object as? UITableView {
            if obj == self.tableView && keyPath == "contentSize" {
                if let newSize = change?[NSKeyValueChangeKey.newKey] as? CGSize {
                    //do stuff here
                    self.tableHeightLayoutConstraint.constant = newSize.height
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func setupRightBarButtonItems() {
        //Add 3buttons
        var remainActions = [DocumentActionObject]()
        var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()
        var index = 0
        for item in self.actions {
            /// setup button
            if self.actions.count < 3 || item.actionType == DocumentAction.finish || item.actionType == DocumentAction.assign || item.actionType == DocumentAction.sendMail {
                let button = UIButton(type: .custom)
                button.tag = index
                button.setTitle(item.actionName, for: .normal)
                button.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
                button.sizeToFit()
                button.tintColor = Theme.default.normalWhiteColor
                button.addTarget(self, action: #selector(didPressMenu(_:)), for: .touchUpInside)
                button.backgroundColor = UIColor(hexString: "#17C209")
                button.cornerRadius = Theme.default.normalCornerRadius
                let rightButton: UIBarButtonItem = UIBarButtonItem(customView: button)
                rightButtons.append(rightButton)
            } else {
                remainActions.append(item)
            }
            
            index += 1
        }
        
        if remainActions.count > 0 {
            //let remainActions = self.actions.filter {$0.actionType != .finish && $0.actionType != .assign && $0.actionType != .sendMail}
            //add more button to nav
            moreBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_nav_more"), style: .plain, target: self, action: #selector(actionMoreButton(_:)))
            moreBarButton.width = 30
            rightButtons.insert(moreBarButton, at: 0)
            
            //More action view config
            //dropDown = DropDown()
            DropDown.appearance().textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            DropDown.appearance().textFont = UIFont(name: "Muli-Regular", size: 12)!
            dropDown.anchorView = moreBarButton
            dropDown.cellNib = UINib(nibName: "ActionPopupDropDownCell", bundle: nil)
            dropDown.width = self.view.widthf
            updateDropdownData(actions: remainActions)
            dropDown.bottomOffset = CGPoint(x: 0, y: 50)
            dropDown.selectionAction = {  (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                if let action: DocumentActionObject = remainActions[safe: index],
                    let types: DocumentAction = action.actionType {
                    switch types {
                    case .approve:
                        let approveVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        approveVC.document = self.document
                        approveVC.assignTrackings = self.assignersTrackings
                        approveVC.delegate = self
                        self.showViewController(viewController: approveVC, false)
                        break
                    case .assign:
                        let assginVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        assginVC.document = self.document
                        assginVC.delegate = self
                        assginVC.assignTrackings = self.assignersTrackings
                        self.showViewController(viewController: assginVC, false)
                    case .process:
                        let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        processVC.delegate = self
                        processVC.document = self.document
                        self.showViewController(viewController: processVC, false)
                    case .appraise:
                        let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        appriseVC.delegate = self
                        appriseVC.document = self.document
                        self.showViewController(viewController: appriseVC, false)
                    case .sendMail:
                        let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        sendEmailCV.document = self.document
                        sendEmailCV.delegate = self
                        self.showViewController(viewController: sendEmailCV, false)
                    case .finish:
                        //self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
                        self.finishDocument()
                        break
                    default:
                        break
                    }
                }
            }
        }
        self.addRightButtonNavigationBar(buttons: rightButtons)
    }
    
    func updateDropdownData(actions: [DocumentActionObject]) {
        var dropdownData = [String]()
        var dropdownIcon = [UIImage]()
        for item in actions {
            if let types: DocumentAction = item.actionType,
                let title: String = item.actionName {
                if let uImg: UIImage = UIImage(named: "ic_action_\(types.rawValue)") {
                    dropdownData.append("\(title)")
                    dropdownIcon.append(uImg.filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)))
                } else {
                    dropdownData.append("Khác")
                    dropdownIcon.append(#imageLiteral(resourceName: "ic_more_orther"))
                }
            }
        }
        
        dropDown.dataSource = dropdownData
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? ActionPopupDropDownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = dropdownIcon[index]
        }
    }
    
    func finishDocument() {
        guard let document = document else {
            return
        }
        SVProgressHUD.show()
        DataManager.finishDocument(document: document, done: { (finish) in
            SVProgressHUD.dismiss()
            if finish {
                self.actions = document.actions?.reversed() ?? []
                //self.updateDropdownData(actions: )
                self.docStatusLabel.text = "  \(document.statusDocument?.name ?? "")  "
                //self.didFinishDocument?()
                self.setupRightBarButtonItems()
                self.didupdatedDocument?(true)
                self.closePages(nil)
                self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
            } else {
                self.view.makeToast("Có lỗi xảy ra, hệ thống sẽ cập nhật cho bạn sớm nhất !")
            }
        })
    }
    
    func didPressMenu(_ sender: UIButton) {
        if let action: DocumentActionObject = self.actions[safe: sender.tag],
            let types: DocumentAction = action.actionType {
            switch types {
            case .approve:
                let approveVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                approveVC.document = self.document
                approveVC.assignTrackings = self.assignersTrackings
                approveVC.delegate = self
                self.showViewController(viewController: approveVC, false)
                break
            case .assign:
                let assginVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                assginVC.document = self.document
                assginVC.delegate = self
                assginVC.assignTrackings = self.assignersTrackings
                self.showViewController(viewController: assginVC, false)
            case .process:
                let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                processVC.delegate = self
                processVC.document = self.document
                self.showViewController(viewController: processVC, false)
            case .appraise:
                let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                appriseVC.delegate = self
                appriseVC.document = self.document
                self.showViewController(viewController: appriseVC, false)
            case .sendMail:
                let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                sendEmailCV.document = document
                sendEmailCV.delegate = self
                self.showViewController(viewController: sendEmailCV, false)
            case .finish:
                self.finishDocument()
                self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
                break
            default:
                break
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        //Set nav bg color
        self.navigationBar?.setColors(background: #colorLiteral(red: 0.1490196078, green: 0.4549019608, blue: 0.937254902, alpha: 1), text: .white)
    }
    
    func updateReadNotificationDocument(itemID: String, itemType: String) {
        SyncProvider.updateNotification(itemID: itemID, itemType: itemType) { (result, error) in
            if let success = result, success == true {
                let badge = UIApplication.shared.applicationIconBadgeNumber
                UIApplication.shared.applicationIconBadgeNumber = badge > 0 ? badge-1 : 0
                NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
            }
            
        }
    }
    
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.commentViewBottomtLayoutConstraint.constant =  0
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.commentViewBottomtLayoutConstraint.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    fileprivate func refreshData() {
        getHistories()
        getTrackings()
        // Do any additional setup after loading the view.
        getAssignTracking()
    }
    
    //MARK: - Funcs
    func scrollToBottom(){
        var offset = self.scrollView.contentOffset
        offset.y = self.scrollView.contentSize.height + self.scrollView.contentInset.bottom - self.scrollView.bounds.size.height
        self.scrollView.setContentOffset(offset, animated: true)
    }
    
    func actionAddButton(_ sender: UIBarButtonItem){
        let assginVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        self.showViewController(viewController: assginVC)
    }
    
    func actionMoreButton(_ sender: UIBarButtonItem){
        dropDown.show()
    }
    
    func reload() {
        print("reload data: %d",fileItems.count)
        pagerHeightConstraint.constant = fileItems.count > 0 ? 150 : 0
        setupRightBarButtonItems()
        self.pagerView.reloadData()
        
        tableView.reloadData {
            self.tableView.layoutIfNeeded()
            print("tableView: \(self.tableView.contentSize)")
            self.commentViewTopLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func actionExpendButton(_ sender: UIButton){
        moreButton.isSelected = !sender.isSelected
        if moreButton.isSelected {

        } else {

        }
        moreIconButton.isSelected = moreButton.isSelected
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
            self.topGradientView.gradientLayer?.frame = self.topGradientView.frame
        }
    }
    
    @IBAction func actionSendCommentButton(_ sender: UIButton){
        let comment = commentTextView.textView.text ?? ""
        if !comment.isEmpty{
            self.commentItems.append(comment)
            self.commentTextView.textView.text = ""
        }
        reload()
        self.endEditor()
    }
    
    fileprivate func selectViewFilterAt(index: Int) {
        let item = viewFilter[index]
        UserDefaults.standard.setValue(index, forKey: kLastOptionIndex)
        UserDefaults.standard.synchronize()
        self.viewFilterButton.setTitle(item.name.uppercased(), for: .normal)
        switch item.value {
        case -1:
            self.statusFilterStackView.isHidden = true
            self.listType = .listReciver
            self.tableView.estimatedRowHeight = 110
            self.getTranfers()
        case -2:
            self.statusFilterStackView.isHidden = true
            self.listType = .history
            self.tableView.estimatedRowHeight = 110
            self.getHistories()
        default:
            self.statusFilterStackView.isHidden = false
            self.listType = .progress
            self.tableView.estimatedRowHeight = 160
            self.filter = item.value
            self.getTrackings()
        }
    }
    
    fileprivate func selectViewStatusAt(index: Int) {
        let item = statusFilter[index]
        UserDefaults.standard.setValue(index, forKey: kLastSortOptionIndex)
        UserDefaults.standard.synchronize()
        self.statusFilterButton.setTitle(item.name, for: .normal)
        self.status = item.value
        self.getTrackings()
    }
    
    @IBAction func actionViewFilterButton(_ sender: UIButton){
        let actionSheet = UIAlertController(title: "Chọn mục tiêu cần hiển thị", message: nil, preferredStyle: .actionSheet)
        for (index, item) in viewFilter.enumerated() {
            let action = UIAlertAction(title: item.name, style: .default) { (_) in
                self.selectViewFilterAt(index: index)
            }
            actionSheet.addAction(action)
        }
        let cancel = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
            
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func actionStatusFilterButton(_ sender: UIButton){
        let actionSheet = UIAlertController(title: "Chọn trạng thái để hiển thị", message: nil, preferredStyle: .actionSheet)
        for (index, item) in statusFilter.enumerated() {
            let action = UIAlertAction(title: item.name, style: .default) { (_) in
                self.selectViewStatusAt(index: index)
            }
            actionSheet.addAction(action)
        }
        let cancel = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
            
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func updateFile(index: Int){
        if !fileItems.isEmpty {
            let item = fileItems[index]
            self.fileNameLabel.text = item.name ?? ""
        } else {
            self.fileNameLabel.text = ""
        }
    }
    
}

extension TaskDetailViewController {
    func getHistories(){
        pageTrackingItems = [ProgressTracking]()
        isSubPage = false
        SyncProvider.getListProcessHistoryDocument(editDeptID: self.document?.editDepartment ?? "", documentID: self.document?.id ?? "") { (results) in
            self.historyItems = results ?? []
            if self.listType == .history {
                self.reload()
            }
            
        }
    }
    
    func getTrackings(){
        //pageTrackingItems = [ProgressTracking]()
        //isSubPage = false
        var deptID = self.document?.editDepartment ?? ""
        var documentID = self.document?.id ?? ""
        isSubPage = false
        if let item = self.pageTrackingItems.last {
            isSubPage = true
            deptID = item.deptID ?? ""
            documentID = item.id ?? ""
        }
        SVProgressHUD.show()
        SyncProvider.getListTrackingDocument(deptID: deptID, status: self.status, viewType: self.filter, documentID: documentID, isChild: isSubPage) { (trackings) in
            SVProgressHUD.dismiss()
            if let item = self.pageTrackingItems.last, self.isSubPage {
                item.childs = trackings
                self.trackingItems = [item]
            } else {
                self.trackingItems = trackings ?? []
            }
            
            if self.listType == .progress {
                self.reload()
            }
            
        }
    }
    
    func getTranfers(){
        pageTrackingItems = [ProgressTracking]()
        isSubPage = false
        SyncProvider.getListTranferDocument(documentID: self.document?.id ?? "") { (list) in
            self.tranferItems = list ?? []
            if self.listType == .listReciver {
                self.reload()
            }
            
        }
    }
    
    func getAssignTracking() {
        if let actions = self.document?.actions,
            let documentID: String = self.document?.id {
            for act in actions {
                if let determineAct = act.actionType, (determineAct == .assign || determineAct == .approve) {
                    SyncProvider.getAssignersByDocument(documentID: documentID, done: { (assigners) in
                        self.assignersTrackings = assigners ?? []
                    })
                    break
                }
            }
        }
    }
}



extension TaskDetailViewController: UITableViewDataSource{
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            //    case 1:
        //      return commentItems.count
        default:
            switch self.listType {
            case .listReciver:
                return tranferItems.count
            case .history:
                return historyItems.count
            default:
                return trackingItems.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        default:
            switch self.listType {
            case .listReciver:
                let cell = tableView.dequeueReusableCell(withClass: DetailPersonTableViewCell.self, for: indexPath)
                let item = tranferItems[indexPath.row]
                cell.nameLabel.text = item.receiverName ?? ""
                cell.titleLabe.text = item.receiverLoginJobTitle ?? ""
                cell.contentLabel.text = ""
                cell.setStatusText(text: item.status)
                if item.departmentName != self.lastDepartMentName {
                    self.lastDepartMentName = item.departmentName ?? ""
                    cell.departmentLabel.text = lastDepartMentName
                } else {
                    cell.departmentLabel.text = ""
                }
                if let date = item.transferTime {
                    cell.timeLabel.text = date.toFormatDate(format: "HH:mm")
                    cell.dateLabel.text = date.toFormatDate(format: "dd 'Th'MM")
                } else {
                    cell.timeLabel.text = "-:-"
                    cell.dateLabel.text = "-:-"
                }
                cell.wrapperDepartmentView.isHidden = cell.departmentLabel.text!.isEmpty
                if let avatar = item.receiverLoginPicture, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar){
                    cell.avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                return cell
            case .history:
                let cell = tableView.dequeueReusableCell(withClass: DetailPersonTableViewCell.self, for: indexPath)
                let item = historyItems[indexPath.row]
                cell.departmentLabel.text = ""
                cell.wrapperDepartmentView.isHidden = cell.departmentLabel.text!.isEmpty
                cell.setActive(active: (item.isHighLight ?? false))
                cell.nameLabel.text = item.authorFullName ?? ""
                cell.titleLabe.text = item.authorJobTitle ?? ""
                cell.contentLabel.text = item.summary ?? ""
                let actionName = item.action ?? ""
                cell.processLabel.text = actionName
                cell.hideProgress(actionName.isEmpty)
                if let date = item.created {
                    cell.timeLabel.text = date.toFormatDate(format: "HH:mm")
                    cell.dateLabel.text = date.toFormatDate(format: "dd 'Th'MM")
                } else {
                    cell.timeLabel.text = "-:-"
                    cell.dateLabel.text = "-:-"
                }
                
                if let avatar = item.authorPicture, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar){
                    cell.avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withClass: DetailHandleTableViewCell.self, for: indexPath)
                cell.delegate = self
                cell.detailHandleDelegate = self
                cell.isExpand = expendItems[indexPath.row]
                cell.index = indexPath.row
                let item = trackingItems[indexPath.row]
                cell.config(data: item, isSubPage: isSubPage)
                
                return cell
            }
            
        }
    }
}

extension TaskDetailViewController: UITableViewDelegate {
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 1:
            //      let header = DetailHeaderTableView.instanceFromNib()
            //      header.titleLabel.text = "THẢO LUẬN"
            //      return header
            return nil
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 45
        default:
            return 0
        }
    }
    
    //  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //    return UITableViewAutomaticDimension
    //  }
    //
    //  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //    return UITableViewAutomaticDimension
    //  }
}

extension TaskDetailViewController: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .right {
            let item = trackingItems[indexPath.row]
            let actions:[DocumentActionObject] = item.actions.reversed()
            var list: [SwipeAction] = []
            //History action
            let hitoryAction = SwipeAction(style: .default, title: "XEM LƯỢC SỬ") { action, indexPath in
                
                if let item = self.trackingItems[safe:indexPath.row] {
                    self.actionHistoryProcess(item: item)
                }
                
            }
            hitoryAction.image = #imageLiteral(resourceName: "ic_action_HistoryTracking").imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
            list.append(hitoryAction)
            
            for i in 0..<actions.count {
                if let actionTypeItem: DocumentAction = actions[i].actionType, actionTypeItem != .history  {
                    let action = SwipeAction(style: .default, title: actions[i].actionName, handler: { (action, indexPath) in
                        debugPrint("Default Board Collection Cell: \(action.debugDescription) - indexPath \(indexPath.row)")
                    })
                    if actionTypeItem == .assign {
                        action.handler = { (action, indexPath) in
                            let assginVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                            assginVC.document = self.document
                            assginVC.progressTracking = item
                            assginVC.delegate = self
                            assginVC.assignTrackings = self.assignersTrackings
                            self.showViewController(viewController: assginVC, false)
                        }
                    } else if actionTypeItem == .approve {
                        action.handler = { (action, indexPath) in
                            let approveVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                            approveVC.document = self.document
                            approveVC.assignTrackings = self.assignersTrackings
                            approveVC.delegate = self
                            self.showViewController(viewController: approveVC, false)
                        }
                    } else if actionTypeItem == .process {
                        action.handler = { (action, indexPath) in
                            let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                            processVC.delegate = self
                            processVC.document = self.document
                            processVC.seletedProgressID = item.id
                            self.showViewController(viewController: processVC, false)
                        }
                    } else if actionTypeItem == .appraise {
                        action.handler = { (action, indexPath) in
                            let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                            appriseVC.delegate = self
                            appriseVC.document = self.document
                            appriseVC.seletedProgressID = item.id
                            self.showViewController(viewController: appriseVC, false)
                        }
                    } else if actionTypeItem == .sendMail {
                        action.handler = { (action, indexPath) in
                            let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                            sendEmailCV.document = self.document
                            sendEmailCV.delegate = self
                            self.showViewController(viewController: sendEmailCV, false)
                        }
                    } else if actionTypeItem == .finish {
                        action.handler = { (action, indexPath) in
                            self.finishDocument()
                        }
                    }

                    action.image = actionTypeItem.image
                    list.append(action)
                }
            }
            
            for item in list {
                item.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                item.textColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                item.font = UIFont(name: "Muli-Bold", size: 10)
            }
            return list
        }
        return []
    }
    
    func tableView (_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        return options
    }
}

extension TaskDetailViewController: FSPagerViewDataSource, FSPagerViewDelegate{
    // MARK: - FSPagerViewDataSource, FSPagerViewDelegate
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return fileItems.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let file = document!.fileDocuments![index]
        let fileType = FileDocumentType(rawValue: file.ext?.lowercased() ?? ".other")
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! CustomFilePagerViewCell
        cell.backgroundColor = UIColor.white
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 5.0
        
        let url = URL(string: Constants.default.domainAddress+(document!.fileDocuments![index].previewImg ?? ""))
        if url != nil{
            let resource = ImageResource(downloadURL: url!)
            cell.imageView!.kf.setImage(with: resource, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, type, url) in
                
                if(image != nil){
                    cell.imageView!.image = image!
                }
                else{
                    cell.imageView?.image = #imageLiteral(resourceName: "img_test")
                }
            })
        }
        else{
            cell.imageView?.image = #imageLiteral(resourceName: "img_test")
        }
        cell.iconImageView?.image = fileType?.imageIcon() ?? #imageLiteral(resourceName: "ic_file_other")
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
        vc?.fileDoucment = document!.fileDocuments![index]
        vc?.document = document
        vc?.delegate = self
        self.navigationController?.present(vc!, animated: true, completion: nil)
    }
    
    func pagerView(_ pagerView: FSPagerView, didHighlightItemAt index: Int) {
        print(index)
        updateFile(index: index)
    }
}


extension TaskDetailViewController: DetailHandleTableViewCellDelegate{
    func expandAll(at: Int) {
        //    self.viewType = 4
        //    tableView.estimatedRowHeight = 100
        expendItems[at] = true
        self.reload()
    }
    
    func collapseAll(at: Int) {
        //    self.viewType = 1
        //    tableView.estimatedRowHeight = 100
        expendItems[at] = false
        self.reload()
    }
    
    func onDetail(item: ProgressTracking) {
        isSubPage = true
        self.pageTrackingItems.append(item)
        //trackingItems = [item]
        //self.reload()
        self.getTrackings()
    }
    
    func backSubPage() {
        self.pageTrackingItems.removeLast()
        if let item = self.pageTrackingItems.last {
            isSubPage = true
            trackingItems = [item]
            self.reload()
        } else {
            isSubPage = false
            self.getTrackings()
        }
    }
    
    func openFile(_ file: FileDocument) {
        let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
        vc?.fileDoucment = file
        //vc?.document = document
        self.navigationController?.present(vc!, animated: true, completion: nil)
    }
    
    func actionHistoryProcess(item: ProgressTracking) {
        let assginVC: TaskHistoryProcessViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        assginVC.item = item
        self.showViewController(viewController: assginVC)
    }
    
    func actionAssignProcess(item: ProgressTracking) {
        let assginVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        assginVC.document = self.document
        assginVC.delegate = self
        assginVC.assignTrackings = self.assignersTrackings
        self.showViewController(viewController: assginVC, false)
    }
    
    func actionProcessProcess(item: ProgressTracking) {
        let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        processVC.delegate = self
        processVC.document = self.document
        self.showViewController(viewController: processVC, false)
    }
    
    func actionApproveProcess(item: ProgressTracking) {
        let approveVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        approveVC.document = self.document
        approveVC.assignTrackings = self.assignersTrackings
        approveVC.delegate = self
        self.showViewController(viewController: approveVC, false)
    }
    
    func actionFinishProcess(item: ProgressTracking) {
        self.finishDocument()
    }
    
    func actionAppraiseProcess(item: ProgressTracking) {
        let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        appriseVC.delegate = self
        appriseVC.document = self.document
        self.showViewController(viewController: appriseVC, false)
    }
    
    func actionSendMailProcess(item: ProgressTracking) {
        let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        sendEmailCV.document = self.document
        sendEmailCV.delegate = self
        self.showViewController(viewController: sendEmailCV, false)
    }
}

//MARK: - ProcessTaskViewDelegate
extension TaskDetailViewController: ProcessTaskViewDelegate {
    
    func sendProcessWork(status: Bool, message: String?, index: IndexPath?) {
        if status {
            //self.getDocumentDetail()
            var msg = "Xử lý thành công !"
            if let msgTemp: String = message,
                msgTemp != "" {
                msg = msgTemp
            }
            self.view.makeToast(msg)
            self.getDocumentDetail()
        } else {
            self.view.makeToast("Có lỗi xảy ra !")
        }
    }
    
    
    func closeView() {
    }
    
}

//MARK: - AppriseTaskViewDelegate
extension TaskDetailViewController: AppriseTaskViewDelegate {
    
    func appriseCloseView() {
    }
    
    func appriseDoneView(status: Bool, message: String?, index: IndexPath?) {
        self.getDocumentDetail()
        self.view.makeToast("Xử lý thành công !")
    }
    
}

//MARK: - SendEmailViewDelegate
extension TaskDetailViewController: SendEmailViewDelegate {
    
    func sendEmailCloseView() {
        
    }
    
    func sendEmailDoneView() {
        self.view.makeToast("Gửi mail thành công !")
    }
    
}

//MARK: - TaskAssignNewViewDelegate
extension TaskDetailViewController: TaskAssignNewViewDelegate {
    
    func taskAssignCloseView(_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?) {
        var defaultMsg: String = "Chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !"
        if let confirmMsg: String = approveMsg {
            defaultMsg = confirmMsg
        } else if let backMsg: String = returnMsg {
            defaultMsg = backMsg
        } else if let failMsg: String = errorMsg {
            defaultMsg = failMsg
        }
        self.view.makeToast(defaultMsg)
        self.getDocumentDetail()
    }
    
}

//MARK: - TaskAssignNoneApproveDelegate
extension TaskDetailViewController: TaskAssignNoneApproveDelegate {
    
    func onCloseView(status: Bool, indexPath: IndexPath?) {
        if status {
            self.view.makeToast("Xử lý thành công !")
            self.refreshData()
        } else {
            self.view.makeToast("Có lỗi xảy ra !")
        }
    }
}

// MARK: - AttachmentDetailViewDelegate
extension TaskDetailViewController: AttachmentDetailViewDelegate {
    
    func doneView(rowIndex: IndexPath?, action: DocumentAction) {
        //self.view.makeToast(action.successMessage)
        if action == .finish {
            self.didupdatedDocument?(true)
            self.closePages(nil)
            return
        }
        if action != .sendMail {
            self.getDocumentDetail()
        }
    }
}
