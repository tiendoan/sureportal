//
//  GlobalSettingViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/30/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class GlobalSettingViewController: UIViewController {

    @IBOutlet fileprivate weak var tfIPAddress:UITextField!
    @IBOutlet fileprivate weak var btSetting:UIButton!
    @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
    
    @IBOutlet fileprivate weak var lbVersion:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbVersion.text = "Sure portal @ 2017 build \(Constants.getVersionName()) (\(Constants.getVersionBuild()))"
        
        tfIPAddress.text = Constants.default.domainAddress//UserDefaults.standard.string(forKey: kSettingDomainAddress) ??
        checkBTSetting()
        registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func checkBTSetting(){
        if(tfIPAddress.text != ""){
            btSetting.isEnabled = true
            btSetting.backgroundColor = UIColor(red: 30, green: 106, blue: 194)
        }
        else{
            btSetting.isEnabled = false
            btSetting.backgroundColor = UIColor(red: 186, green: 186, blue: 186)
        }
    }
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: nil, queue: nil) { (notification) in
            self.checkBTSetting()
        }
    }
    
    func showAlert(_ message:String){
        loading.stopAnimating()
        self.view.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "THÔNG BÁO", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Đồng Ý", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func pressesBTSetting(_ sender: UIButton){
        self.view.endEditing(true)
        UserDefaults.standard.set(tfIPAddress.text!, forKey: kSettingDomainAddress)
        UserDefaults.standard.synchronize()
        
        LoginManager.getEncryptKey { (result, message) in
            if (result != nil) {
                UserDefaults.standard.set(result!, forKey: kSettingSecurityKey)
                UserDefaults.standard.synchronize()
                let vc = self.storyboard?.instantiateViewController(withClass: LoginViewController.self)
                self.present(vc!, animated: true, completion: nil)
            } else {
                self.showAlert(message != "" ? message : "Không thể kết nối đến máy chủ!")
            }
        }

    }
}
