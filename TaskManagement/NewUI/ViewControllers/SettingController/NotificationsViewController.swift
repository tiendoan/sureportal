//
//  NotificationsViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import ObjectMapper
import SwipeCellKit
import SVProgressHUD

class NotificationsViewController: UIViewController {
  
  @IBOutlet weak var notificationsTableView: UITableView!
  
  let PAGE_TITLE: String = "Thông báo"
  let HIDE_KB_TITLE: String = "Ẩn bàn phím"
  let DATA_EMPTY: String = "Không có thông báo nào !"
  
//  var notifications: [NotificationItem] = [NotificationItem] ()
  var notificationsGroup = [String: [NotificationItem]]()
    // imageDictionary have Key is Module code, Value is IconName and Color of that icon
    //
    let  imageDictionary = ["Document"           : ["icn_noti_document" ,"#1153FC"],
                            "Task"               : ["icn_noti_task"     ,"#FFB533"],
                            "DigitalProcedure"   : ["icn_noti_signature","#1153FC"],
                            "MeetingRoom"        : ["icn_noti_meeting"  ,"#FF7B02"],
                            "Vehicle"            : ["icn_noti_vehicle"  ,"#FC76B3"]]
//    var notificationsGroup = [(section: String, listNotification: [NotificationItem])]()
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupComponents()
  }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getNotificationsFromApi()
    }
  
  fileprivate func setupComponents() {
    
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = HIDE_KB_TITLE
    
    self.setTitleNavigationBar(PAGE_TITLE)
    self.setupDefaultNavigationBar()
    
    self.showBack()
    
    self.updateRightButtonNavigationBar()
    
    notificationsTableView.separatorStyle = .none
    notificationsTableView.rowHeight = 120
    notificationsTableView.register(withClass: NotificationTableCell.self)
    
  }
    func updateRightButtonNavigationBar() {
        guard notificationsGroup.count > 0 else {
            self.addRightButtonNavigationBar(buttons: nil)
            return
        }
        var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()
        /// Attach button
        let btnDelete = UIButton(type: .custom)
        btnDelete.setTitle("Xoá hết", for: .normal)
        btnDelete.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
        btnDelete.tintColor = Theme.default.normalWhiteColor
        btnDelete.addTarget(self, action: #selector(deleteAllButtonClicked), for: .touchUpInside)
        btnDelete.backgroundColor = UIColor(hexString: "#FBA706")
        btnDelete.cornerRadius = Theme.default.normalCornerRadius
        let deleteRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnDelete)
        rightButtons.append(deleteRightButton)
        self.addRightButtonNavigationBar(buttons: rightButtons)
    }
    
    func deleteAllButtonClicked() {
        let alert = UIAlertController(title: "Bạn có chắc chắn muốn xoá tất cả thông báo", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Đồng Ý", style: .destructive) { (alert) in
            self.readAllNotifications()
        }
        let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARKS: get notifications from apis
// ---------------------------
extension NotificationsViewController {
  
    fileprivate func readAllNotifications() {
        SVProgressHUD.show()
        var itemIDs = [String]()
        var itemTypes = [String]()
        for notifDictionary in notificationsGroup {
            for notif in notifDictionary.value {
                print(notif)
                if let itemID = notif.itemID, let type = notif.itemType {
                    itemIDs.append(itemID)
                    itemTypes.append(type)
                }
            }
        }
        SyncProvider.updateNotifications(itemIDs: itemIDs, itemTypes: itemTypes) {[weak self] (result, error) in
            SVProgressHUD.dismiss()
            if let success = result, success == true {
                UIApplication.shared.applicationIconBadgeNumber = 0
                NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
                
                self?.notificationsGroup.removeAll()
                self?.notificationsTableView.reloadData()
                self?.updateRightButtonNavigationBar()
            }
            
        }
    }
    
  fileprivate func getNotificationsFromApi() {
    // Trying to load all notification (B.A)
    let notificationsRaw = DataManager.shared.notificationList
    self.notificationsGroup = Dictionary(grouping: notificationsRaw, by: {$0.moduleCode! })
    self.notificationsTableView.reloadData()
    SVProgressHUD.dismiss()
    UIApplication.shared.applicationIconBadgeNumber = notificationsRaw.count
    self.updateRightButtonNavigationBar()
    NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
    

    }
  
}

// MARK: UITableViewDelegate
extension NotificationsViewController: UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if self.notificationsGroup.count == 0 {
      TableViewHelper.EmptyMessage(message: DATA_EMPTY, viewController: self, tableView: self.notificationsTableView)
      return 0
    }
    return self.notificationsGroup.count
  }
}

// MARK: UITableViewDataSource
extension NotificationsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.notificationsTableView.frame.size.width, height: 50))
        let rawTitle    = Array(self.notificationsGroup.keys)[section] // Module code
        let title       = NSLocalizedString(rawTitle, comment: "")     // Get display string by module code
        var image       = UIImage(named: imageDictionary[rawTitle]?[0] ?? "") // Get display image by module code// temp color
        if image != nil {
            image = image!.filled(withColor: UIColor(hexString: imageDictionary[rawTitle]?[1] ?? "")!)
        }
        
        // Label
        let titleLabel   = UILabel()
        titleLabel.text  = title.uppercased()
        titleLabel.frame = CGRect(x: 50, y: 0, width: 100, height: 50)
        titleLabel.font  = UIFont.systemFont(ofSize: 18)
        headerView.addSubview(titleLabel)
        
        // ImageView
        let imageView   = UIImageView()
        imageView.frame = CGRect(x: 15, y: 10, width: 30, height: 30)
        imageView.image =  image
        headerView.addSubview(imageView)

        //Line
        let lineViewX       = 60 + titleLabel.intrinsicContentSize.width
        let lineViewY       = headerView.frame.size.height/2
        let lineViewWidth   = headerView.frame.size.width - lineViewX
        let lineView = UIView()
        lineView.backgroundColor = UIColor(hexString: "#F0F0F0")
        lineView.frame = CGRect(x: lineViewX, y: lineViewY, width: lineViewWidth, height: 1)
        headerView.addSubview(lineView)
        headerView.backgroundColor = UIColor.white
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let key = Array(self.notificationsGroup.keys)[section]
    return (self.notificationsGroup[key]?.count)!
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let key = Array(self.notificationsGroup.keys)[indexPath.section]
    let arrayNotification = self.notificationsGroup[key]
    let notification = arrayNotification![indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell", for: indexPath) as! NotificationTableCell
    cell.delegate = self
    cell.config(notification)
    return cell
  }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let key = Array(self.notificationsGroup.keys)[indexPath.section]
        let arrayNotification = self.notificationsGroup[key]
        guard let notification: NotificationItem = arrayNotification![safe: indexPath.row] else {
            return
        }
        let documentData: DocumentNotification = Mapper<DocumentNotification>().map(JSON:[String : AnyObject]())!
        // Go to detail.
        let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        documentData.itemID = notification.itemID
        documentData.itemType = notification.itemType
        detailVC.documentFromNotification = documentData
        self.navigationController?.pushViewController(detailVC)
    }
}

extension NotificationsViewController: SwipeTableViewCellDelegate {
    
    func updateReadNotificationDocument(indexPath:IndexPath, itemID: String, itemType: String) {
        SVProgressHUD.show()
        SyncProvider.updateNotification(itemID: itemID, itemType: itemType) { (result, error) in
            SVProgressHUD.dismiss()
            if let success = result, success == true {
                let key = Array(self.notificationsGroup.keys)[indexPath.section]
                self.notificationsGroup[key]?.remove(at: indexPath.row)
                self.notificationsTableView.beginUpdates()
                self.notificationsTableView.deleteRows(at: [indexPath], with: .right)
                self.notificationsTableView.endUpdates()
                let badge = UIApplication.shared.applicationIconBadgeNumber
                UIApplication.shared.applicationIconBadgeNumber = badge > 0 ? badge-1 : 0
                NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
            }

        }
    }
    
    func tableView (_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        if orientation == .right {
            let key = Array(self.notificationsGroup.keys)[indexPath.section]
            guard let notification: NotificationItem = self.notificationsGroup[key]?[safe: indexPath.row] else {
                return []
            }

            let action = SwipeAction(style: .destructive, title: NSLocalizedString("Delete notification", comment: ""), handler: { (action, index) in
                self.updateReadNotificationDocument(indexPath: indexPath, itemID: notification.itemID!, itemType: notification.itemType!)
            })
            return [action]
        }
        return []
    }
    
    func tableView (_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return options
    }
    
}

