//
//  SettingViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/9/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Kingfisher
import DropDown


class SettingViewController: UIViewController {

    @IBOutlet fileprivate weak var btExit:UIButton!
    
    @IBOutlet fileprivate weak var imgAvatar:UIImageView!
    @IBOutlet fileprivate weak var lbName:UILabel!
    @IBOutlet fileprivate weak var lbPosition:UILabel!
    @IBOutlet fileprivate weak var lbEmail:UILabel!
    @IBOutlet weak var syncButton: UIButton!
    @IBOutlet weak var timeSyncButton: UIButton!
    @IBOutlet weak var processDateButton: UIButton!
    @IBOutlet fileprivate weak var lbVersion:UILabel!
    
    @IBOutlet weak var createTaskSwitch: UISwitch!
    @IBOutlet weak var attachementSwitch: UISwitch!
    @IBOutlet weak var alertSwitch: UISwitch!
    let SYNC_ARRAY = ["Thủ công", "Tức thời", "15 phút", "30 phút", "1 tiếng", "2 tiếng", "4 tiếng"]
    let TIME_SYNC_ARRAY = ["1 ngày", "3 ngày", "7 ngày", "15 ngày", "1 tháng", "2 tháng", "3 tháng"]
    let DATE_ARRAY = ["1 ngày", "2 ngày", "3 ngày", "4 ngày", "5 ngày"]
    
    let syncDropDown: DropDown = DropDown()
    let timeSyncDropDown: DropDown = DropDown()
    let processDateDropDown: DropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .init(rawValue: 0)
        lbVersion.text = "Sure portal @ 2017 build \(Constants.getVersionName()) (\(Constants.getVersionBuild()))"
        
        setupUser()
        
        
        syncButton.setTitle("\(SYNC_ARRAY.first!)   ▼", for: .normal)
        setupDropdown(syncDropDown, anchorView:syncButton)
        
        timeSyncButton.setTitle("\(TIME_SYNC_ARRAY.first!)   ▼", for: .normal)
        setupDropdown(timeSyncDropDown, anchorView:timeSyncButton)
        
        
        self.initTimerSync()
        self.initDateSync()
        
        var alertSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ALERT_NOTIFICATION)
        if alertSelected == nil {
            
            UserDefaults.standard.set(true, forKey: Constants.SYNC_ALERT_NOTIFICATION)
            alertSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ALERT_NOTIFICATION)
        }
         self.alertSwitch.setOn(alertSelected as! Bool, animated: false)
        
        
        var attachmentSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ATTACHMENT_DOWNLOAD)
        if attachmentSelected == nil {
            
            UserDefaults.standard.set(true, forKey: Constants.SYNC_ATTACHMENT_DOWNLOAD)
            attachmentSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ATTACHMENT_DOWNLOAD)
        }
        self.attachementSwitch.setOn(attachmentSelected as! Bool, animated: false)
        
        var taskJob = UserDefaults.standard.object(forKey: Constants.SYNC_CREATE_TASK)
        if taskJob == nil {
            
            UserDefaults.standard.set(true, forKey: Constants.SYNC_CREATE_TASK)
            taskJob = UserDefaults.standard.object(forKey: Constants.SYNC_CREATE_TASK)
        }
        self.createTaskSwitch.setOn(taskJob as! Bool, animated: false)
        
        
    }
    private func initTimerSync(){
        
        guard let timerRequest = UserDefaults.standard.object(forKey: Constants.SYNC_TIME_SERVER)
            else {
                return
        }
        var indexArr = 0
        switch timerRequest as! Int
        {
        case 0:
            indexArr = 0
            break
            
        case 5:
            indexArr = 1
            break
            
        case 15:
            indexArr = 2
            break
        case 30:
            indexArr = 3
            break
            
        case 60:
            indexArr = 4
            break
            
        case 120:
            indexArr = 5
            break
            
        case 240:
            indexArr = 6
            break
            
        default:
            break
        }
        syncButton.setTitle("\(SYNC_ARRAY[indexArr])   ▼", for: .normal)
        
    }
    private func initDateSync(){
        
        guard let timerRequest = UserDefaults.standard.object(forKey: Constants.SYNC_DATE_SERVER)
            else {
                return
        }
        var indexArr = 0
        switch timerRequest as! Int
        {
        case 1:
            indexArr = 0
            break
            
        case 3:
            indexArr = 1
            break
            
        case 7:
            indexArr = 2
            break
        case 15:
            indexArr = 3
            break
            
        case 30:
            indexArr = 4
            break
            
        case 60:
            indexArr = 5
            break
            
        case 90:
            indexArr = 6
            break
            
        default:
            break
        }
        timeSyncButton.setTitle("\(TIME_SYNC_ARRAY[indexArr])   ▼", for: .normal)
        
        
    }
    
    func setupDropdown(_ dropdown: DropDown, anchorView: UIButton) {
        dropdown.anchorView = anchorView
        dropdown.width = 200
        dropdown.bottomOffset = CGPoint(x: 0, y: anchorView.bounds.height + 10)
        dropdown.isMultipleTouchEnabled = false
        dropdown.textColor = UIColor.gray
        dropdown.textFont = Theme.default.semiBoldFont(size: 13)
        dropdown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        dropdown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        
        dropdown.selectionAction = { [unowned self] (index, item) in
            anchorView.setTitle("\(item)   ▼", for: .normal)
            if anchorView == self.syncButton {
                var timeSync = 0;
                switch index
                {
                    case 0:
                         timeSync = 0
                    break
                    
                    case 1:
                        timeSync = 5
                    break
                    
                    case 2:
                        timeSync = 15
                    break
                    case 3:
                        timeSync = 30
                    break
                    
                    case 4:
                        timeSync = 60
                    break
                    
                    case 5:
                        timeSync = 120
                    break
                    
                    case 6:
                        timeSync = 240
                    break
                    
                    default:
                    break
                }
                UserDefaults.standard.set(timeSync, forKey: Constants.SYNC_TIME_SERVER)
                
            } else if anchorView == self.timeSyncButton {
                
                var timeSync = 1;
                switch index
                {
                case 0:
                    timeSync = 1
                    break
                    
                case 1:
                    timeSync = 3
                    break
                    
                case 2:
                    timeSync = 7
                    break
                case 3:
                    timeSync = 15
                    break
                    
                case 4:
                    timeSync = 30
                    break
                    
                case 5:
                    timeSync = 60
                    break
                    
                case 6:
                    timeSync = 90
                    break
                    
                default:
                    break
                }
                
                
                UserDefaults.standard.set(timeSync, forKey: Constants.SYNC_DATE_SERVER)
              
            }
            DocumentManagement.shared.syncDocument()
        }
        
        // dropdown datasource
        var array = SYNC_ARRAY
        if anchorView == timeSyncButton {
            array = TIME_SYNC_ARRAY
        } else if anchorView == processDateButton {
            array = DATE_ARRAY
        }
        dropdown.dataSource = array
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func setupUser(){
        imgAvatar.contentMode = .scaleAspectFill
        lbName.text = CurrentUser?.fullName ?? ""
        lbPosition.text = CurrentUser?.jobTitle ?? "Chưa xác định"
        lbEmail.text = CurrentUser?.email ?? ""
        
        let domain = UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? "http://sureportal.bioportal.vn"
        let url = URL(string: domain + (CurrentUser?.picture ?? ""))
        if url != nil{
            self.imgAvatar.kf.setImage(with: url!, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        } else {
            imgAvatar.image = #imageLiteral(resourceName: "ic_avatar_empty")
        }
    }
    @IBAction func alertChangeAction(_ sender: Any) {
        let isSelected:Bool = (sender as! UISwitch).isOn
        print("\(isSelected)")
        UserDefaults.standard.set(isSelected, forKey: Constants.SYNC_ALERT_NOTIFICATION)
    }
    @IBAction func attachmentChangeAction(_ sender: Any) {
        let isSelected:Bool = (sender as! UISwitch).isOn
        print("\(isSelected)")
        UserDefaults.standard.set(isSelected, forKey: Constants.SYNC_ATTACHMENT_DOWNLOAD)
    }
    
    @IBAction func createJobAction(_ sender: Any) {
        let isSelected:Bool = (sender as! UISwitch).isOn
        print("\(isSelected)")
        UserDefaults.standard.set(isSelected, forKey: Constants.SYNC_CREATE_TASK)
    }
    @IBAction func removeData(_ sender: Any) {
        
    
        
    }
}

extension SettingViewController{
    @IBAction func dropDownButtonClicked(_ sender: Any) {
        let button = sender as! UIButton
        if button == syncButton {
            syncDropDown.show()
        } else if button == timeSyncButton {
            timeSyncDropDown.show()
        } else {
            processDateDropDown.show()
        }
    }
    
    @IBAction func pressesExit(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func pressesChangePassword(_ sender: UIButton){
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withClass: ChangePasswordViewController.self)
        self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func pressesChangeAvatar(_ sender: UIButton){
        self.view.endEditing(true)
        //chooseImage()
    }
}
extension SettingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func chooseImage(){
        
        let vc = UIImagePickerController()
        vc.delegate = self
        vc.allowsEditing = false
        vc.sourceType = .photoLibrary
        self.present(vc, animated: true) {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgAvatar.image = image
        } else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
