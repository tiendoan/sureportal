//
//  SettingURLViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/7/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD

class SettingURLViewController: UIViewController {

    @IBOutlet fileprivate weak var tfURL:UITextField!
    
    @IBOutlet fileprivate weak var lbVersion:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .init(rawValue: 0)
        lbVersion.text = "Sure portal @ 2017 build \(Constants.getVersionName()) (\(Constants.getVersionBuild()))"
        
        tfURL.text = Constants.default.domainAddress//UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? Constants.defaultDomainAddress
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func showAlert(_ message:String){
        self.view.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "THÔNG BÁO", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Đồng Ý", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func pressesBack(_ sender: UIButton){
        self.view.endEditing(true)
        UserDefaults.standard.set(tfURL.text ?? "", forKey: kSettingDomainAddress)
        UserDefaults.standard.synchronize()
        
        SVProgressHUD.show()
        LoginManager.getEncryptKey { (result, message) in
            SVProgressHUD.dismiss()
            if (result != nil) {
                UserDefaults.standard.set(result!, forKey: kSettingSecurityKey)
                UserDefaults.standard.synchronize()
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showAlert(message != "" ? message : "Không thể kết nối đến máy chủ!")
            }
        }
    }
}
