//
//  NewMainViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Pageboy
import Tabman
import SVProgressHUD
import KYDrawerController

class NewMainViewController: TabmanViewController {
    
    lazy fileprivate var btMenu: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = UIColor.clear
        button.setImage(#imageLiteral(resourceName: "ic_menu").filled(withColor: UIColor.white), for: .normal)
        button.showsTouchWhenHighlighted = true
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        button.addTarget(self, action: #selector(actionMenuButton), for: .touchUpInside)
        return button
    }()
    
    lazy fileprivate var btSearch: UIButton = {
        let button = UIButton(type: .custom)
        button.tintColor = Theme.default.normalWhiteColor
        button.setImage(#imageLiteral(resourceName: "ic_search").filled(withColor: UIColor.white), for: .normal)
        button.showsTouchWhenHighlighted = true
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        button.addTarget(self, action: #selector(actionSearchButton(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy fileprivate var btNotification: SSBadgeButton = {
        let button = SSBadgeButton(type: .custom)
        button.tintColor = Theme.default.normalWhiteColor
        button.setImage(#imageLiteral(resourceName: "ic_newnotification").filled(withColor: UIColor.white), for: .normal)
        button.showsTouchWhenHighlighted = true
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        button.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 5)
        button.addTarget(self, action: #selector(actionNotificationButton), for: .touchUpInside)
        return button
    }()
    
    lazy fileprivate var btTitle: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = UIColor.clear
        button.setTitle("--  ▼", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 225, height: 40)
        button.titleLabel?.font = UIFont(name: FontFamily.Bold.rawValue, size: FontSize.XXLarge.rawValue)!
        button.titleLabel?.minimumScaleFactor = 0.7
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.showsTouchWhenHighlighted = true
        button.sizeToFit()
        button.addTarget(self, action: #selector(actionTitle), for: .touchUpInside)
        return button
    }()
    
    @IBOutlet fileprivate weak var tabbleView: UITableView!
    fileprivate var indexRootMenu = 0
    @IBOutlet fileprivate weak var topMenu:NSLayoutConstraint!
    @IBOutlet fileprivate weak var heightMenu:NSLayoutConstraint!
    @IBOutlet fileprivate weak var viewMenu:UIView!
    var isShowMenu = false
    var useSavedMenu = true
    
    fileprivate var rootMenus:[Menu] = []
    fileprivate var childMenus:[Menu] = []
    fileprivate var childMenuTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .init(rawValue: 0)
        
        self.navigationBar?.shadowImage = UIImage()
        self.navigationBar?.setColors(background: #colorLiteral(red: 0.1490196078, green: 0.4549019608, blue: 0.937254902, alpha: 1), text: UIColor.white)
        
        self.dataSource = self
        self.isScrollEnabled = false
        
        let menuBtn = UIBarButtonItem(customView: btMenu)
        self.navigationItem.leftBarButtonItem = menuBtn
        
        let searchBtn = UIBarButtonItem(customView: btSearch)
        let notificationBtn = UIBarButtonItem(customView: btNotification)
        self.navigationItem.rightBarButtonItems = [searchBtn,notificationBtn];
        
        self.navigationItem.titleView = btTitle
        
        tabbleView.dataSource = self
        tabbleView.delegate = self
        tabbleView.register(UITableViewCell.self, forCellReuseIdentifier: "CELL")
        
        viewMenu.backgroundColor = UIColor(white: 0, alpha: 0)
        topMenu.constant = -200
        self.view.layoutIfNeeded()
        self.viewMenu.isUserInteractionEnabled = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(exitMenu))
        viewMenu.addGestureRecognizer(tap)
        
        if UserDefaults.standard.object(forKey: Constants.USER_LAST_ROOT_MENU_KEY) == nil {
            useSavedMenu = false
        }
        self.getRootMenu()
        
        DataManager.shared.getAllUsersInfo()
        DataManager.shared.getAllEmployees()
        DataManager.shared.getImportantDocuments()
        DataManager.shared.getLeftNavigationCount()
        DataManager.shared.getListNotification()
        DataManager.shared.getBookingRoom()
        DataManager.shared.getLocations()

        registerNotification()
        
        DocumentManagement.shared.syncDocument()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            btNotification.badgeLabel.isHidden = false
            btNotification.setValueForBagde(value: UIApplication.shared.applicationIconBadgeNumber)
        } else {
            btNotification.badgeLabel.isHidden = true
        }
      
    }
    
    
    func actionMenuButton(){
        self.mainNew()?.toggleMenu()
    }
    func actionTitle(){
        self.showMenu(show: !isShowMenu)
    }
    
    @IBAction func actionSearchButton(_ sender: UIButton ) {
        let searchVC: SearchViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        self.showViewController(viewController: searchVC, false)
    }
    
    func actionNotificationButton(){
        let notificationsVC: NotificationsViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        self.showViewController(viewController: notificationsVC, false)
    }
    func exitMenu(){
        self.showMenu(show: !isShowMenu)
    }
    
    func getRootMenu(){
        SVProgressHUD.show()
        InstanceDB.default.getMenu() { (result) in
            SVProgressHUD.dismiss()
            self.rootMenus.removeAll()
            self.rootMenus.append(contentsOf: result)
          
            var height = (self.rootMenus.count+1)*44
            height = max(height, 200)
            height = min(height, 400)
            self.heightMenu.constant = CGFloat(height)
            self.topMenu.constant = -self.heightMenu.constant
            self.view.layoutIfNeeded()
            
            if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
                leftVC.loadRootMenu(self.rootMenus)
            }
            
            if(self.rootMenus.count > 0){
                self.indexRootMenu = UserDefaults.standard.integer(forKey: Constants.USER_LAST_ROOT_MENU_KEY)
                self.selectRootMenuAtIndex(index: self.indexRootMenu)
            }
            
            self.tabbleView.reloadData()
        }
    }
    func getMenuChildNodes(_ index: Int, _ isLoadBoard: Bool = true, updateMenuOnly:Bool = false){
        if let selectedMenu: Menu = self.rootMenus[safe: index], let selectedId: String = selectedMenu.id {
            SVProgressHUD.show()
            InstanceDB.default.getMenu(parentId: selectedId, treeFilterCode: self.rootMenus[index].code ?? "") { (result) in
                SVProgressHUD.dismiss()
                self.childMenus.removeAll()
                self.childMenuTitle = result.count == 0 ? "": result[0].name ?? ""
                var resultWithoutTitle = result
                resultWithoutTitle.removeFirst()
                self.childMenus.append(contentsOf: resultWithoutTitle)
                NSLog(self.rootMenus[index].code ?? "")
                
                UserDefaults.standard.set(self.rootMenus[index].code, forKey: Constants.USER_LEFT_MENU_CODE)
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(self.rootMenus[index].id, forKey: Constants.PARENT_PROCEDURE_ID)
                UserDefaults.standard.synchronize()
                
                UserDefaults.standard.set(self.rootMenus[index].code, forKey: Constants.USER_STORED_LEFT_MENU_CODE)
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(self.rootMenus[index].id, forKey: Constants.PARENT_STORED_PROCEDURE_ID)
                UserDefaults.standard.synchronize()
                
                var parentIdx = self.useSavedMenu ? UserDefaults.standard.integer(forKey: Constants.USER_LAST_CHILD_MENU_KEY) : 0
                //if top menu -> select childMenu : all
                if !updateMenuOnly && !self.useSavedMenu {
                    let parentIndex  = self.childMenus.index(where: { menu in
                        if let code = menu.code, code.contains(MenuChildCode.all.rawValue) {
                            return true
                        }
                        return false
                    })
                    if let parentIndex = parentIndex{
                        parentIdx = parentIndex
                    } else {
                        parentIdx = 0
                    }
                }
                
                
                if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
                    if updateMenuOnly && !self.useSavedMenu {
                        parentIdx = leftVC.parentIndex < 0 ? 0 : leftVC.parentIndex
                    }
                    leftVC.loadMenuChildNotes(self.childMenuTitle,self.childMenus)
                    leftVC.selectRootMenu(index, keepParentIndex: updateMenuOnly)
                    leftVC.selectParentMenu(parentIdx)
                    
                }
                if isLoadBoard{
                    if let vc = self.viewControllers![1] as? BoardViewController{
                        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
                        
                            if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE || keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE ) {
                                parentIdx = 0
                                if ( keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE )
                                {
                                    self.childMenus = self.childMenus.filter({ $0.id != $0.parentId })
                                }
                            }
                            vc.setupMenus(self.childMenus, parentIdx, updateMenuOnly: updateMenuOnly)

                    }
                }
                //save child menu
                self.useSavedMenu = false
                UserDefaults.standard.set(parentIdx, forKey: Constants.USER_LAST_CHILD_MENU_KEY)
                UserDefaults.standard.synchronize()
            }
        }
    }
  
    func showAnalyticsPage() {
        self.scrollToPage(PageboyViewController.PageIndex.at(index: 0), animated: false)
    }
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, queue: nil) { (notification) in
            DataManager.shared.getListNotification()
            if UIApplication.shared.applicationIconBadgeNumber > 0 {
                self.btNotification.badgeLabel.isHidden = false
                self.btNotification.setValueForBagde(value: UIApplication.shared.applicationIconBadgeNumber)
            } else {
                self.btNotification.badgeLabel.isHidden = true
            }
            
            //Update badge on menu
            //let menu = self.rootMenus[self.indexRootMenu]
            if let menu = self.rootMenus[safe: self.indexRootMenu], menu.code?.uppercased().contains(MenuCode.currentTasks.rawValue) == true || menu.code?.uppercased().contains(MenuCode.tasksManagement.rawValue) == true{
                if(self.rootMenus.count > 0){
                    self.getMenuChildNodes(self.indexRootMenu, true, updateMenuOnly: true)
                }
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_SELECT_ROOT), object: nil, queue: nil) { (notification) in
            if let item = notification.object as? Int{
                self.indexRootMenu = item
                self.tabbleView.reloadData()
                self.showMenu(show: false)

                self.selectRootMenuAtIndex(index: item)
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICATION_MENU_SELECT_PARENT), object: nil, queue: nil) { (notification) in
            if let dict = notification.object as? [String: AnyObject]{
                let child = dict["IndexChild"] as! Int
                if let parent = dict["IndexParent"] as? Int{
                    self.indexRootMenu = parent
                    self.btTitle.setTitle((self.rootMenus[parent].name?.uppercased() ?? "") + "  ▼", for: .normal)
                    if let menu = dict["Menus"] {
                        self.childMenus = menu as! [Menu]
                    }
                    UserDefaults.standard.set(self.rootMenus[parent].code, forKey: Constants.USER_LEFT_MENU_CODE)
                    UserDefaults.standard.synchronize()
                    UserDefaults.standard.set(self.rootMenus[parent].id, forKey: Constants.PARENT_PROCEDURE_ID)
                    UserDefaults.standard.synchronize()
                    
                    UserDefaults.standard.set(self.rootMenus[parent].code, forKey: Constants.USER_STORED_LEFT_MENU_CODE)
                    UserDefaults.standard.synchronize()
                    UserDefaults.standard.set(self.rootMenus[parent].id, forKey: Constants.PARENT_STORED_PROCEDURE_ID)
                    UserDefaults.standard.synchronize()
                } else {
                    UserDefaults.standard.set(child, forKey: Constants.USER_LAST_CHILD_MENU_KEY)
                    UserDefaults.standard.synchronize()
                    
                    if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
                        leftVC.selectParentMenu(child)
                    }
                }
                self.tabbleView.reloadData()
                self.btTitle.sizeToFit()
                self.scrollToPage(PageboyViewController.PageIndex.at(index: 1), animated: false)
                if let vc = self.viewControllers![1] as? BoardViewController{
                    vc.setupMenus(self.childMenus, child)
                }
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_SELECT_CHILD), object: nil, queue: nil) { (notification) in
            if let dict = notification.object as? [String:Int]{
                let parent = dict["IndexParent"]
                let child = dict["IndexChild"]
                
                self.scrollToPage(PageboyViewController.PageIndex.at(index: 1), animated: false)
                if let vc = self.viewControllers![1] as? BoardViewController{
                    vc.setupMenus(self.childMenus[parent!].childrens!, child!)
                }
            }
        }
        
        // Finish documents -> update badge number
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_RELOAD), object: nil, queue: nil) { (notification) in
            let menu = self.rootMenus[self.indexRootMenu]
            if menu.code?.uppercased().contains(MenuCode.currentTasks.rawValue) == true || menu.code?.uppercased().contains(MenuCode.tasksManagement.rawValue) == true{
                if(self.rootMenus.count > 0){
                    self.getMenuChildNodes(self.indexRootMenu, true, updateMenuOnly: true)
                }
            }
        }
    }
}

extension NewMainViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rootMenus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabbleView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont(name: FontFamily.Bold.rawValue, size: FontSize.Large.rawValue)!
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.textColor = UIColor.white//UIColor.init(red: 99, green: 103, blue: 137)
        
        cell.textLabel?.text = rootMenus[indexPath.row].name?.uppercased()
        if(indexPath.row == self.indexRootMenu){
            cell.textLabel?.alpha = 1
        }
        else{
            cell.textLabel?.alpha = 0.5
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexRootMenu = indexPath.row
        tableView.reloadData()
        self.showMenu(show: false)
        self.selectRootMenuAtIndex(index: self.indexRootMenu)
        
    }
    
    fileprivate func selectRootMenuAtIndex(index: Int){
        let index = index < rootMenus.count ? index : 0
        self.btTitle.setTitle((rootMenus[index].name?.uppercased() ?? "") + "  ▼", for: .normal)
        var rootIdx = 1
        if self.indexRootMenu == 0 && self.rootMenus.first?.code == MenuCode.analytic.rawValue{
            rootIdx = 0
            self.mainNew()?.toggleMenu(forceClose: true)
        } else {
            self.getMenuChildNodes(index, true)
        }

        self.scrollToPage(PageboyViewController.PageIndex.at(index: rootIdx), animated: false)

        self.btTitle.sizeToFit()
        if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
            leftVC.loadMenuChildNotes("",[])
            if index == 0 && rootMenus.count>0 {
                leftVC.selectRootMenu(1) // Select next Value of table root view
            } else {
                leftVC.selectRootMenu(index)
            }
        }
    }
}


extension NewMainViewController:PageboyViewControllerDataSource{
    func viewControllers(forPageboyViewController pageboyViewController: PageboyViewController) -> [UIViewController]? {
        
        var viewControllers:[Any] = []
        
        let dashboardVC = self.storyboard?.instantiateViewController(withClass: DashboardViewController.self)
        let boardVC = self.storyboard?.instantiateViewController(withClass: BoardViewController.self)
        viewControllers.append(dashboardVC!)
        viewControllers.append(boardVC!)
        return viewControllers as? [UIViewController]
        
    }
    
    func defaultPageIndex(forPageboyViewController pageboyViewController: PageboyViewController) -> PageboyViewController.PageIndex? {
        return nil
    }
}

extension NewMainViewController{
    fileprivate func showMenu(show: Bool){
        if(show){
            UIView.animate(withDuration: 0.3, animations: {
                self.viewMenu.backgroundColor = UIColor(white: 0, alpha: 0.5)
                self.topMenu.constant = 0
                self.view.layoutIfNeeded()
            }, completion: { (success) in
                self.viewMenu.isUserInteractionEnabled = true
                self.isShowMenu = true
            })
        }
        else{
            UIView.animate(withDuration: 0.3, animations: {
                self.viewMenu.backgroundColor = UIColor(white: 0, alpha: 0)
                self.topMenu.constant = -self.heightMenu.constant
                self.view.layoutIfNeeded()
            }, completion: { (success) in
                self.viewMenu.isUserInteractionEnabled = false
                self.isShowMenu = false
            })
        }
    }
    
    
}
