//
//  LeftViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/5/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import CryptoSwift
import Kingfisher
import Firebase
import Pastel

class LeftViewController: UIViewController {
  
    @IBOutlet weak var containerView: PastelView!
    
  @IBOutlet fileprivate weak var btMenu:UIButton!
  @IBOutlet fileprivate weak var lbTitle:UILabel!
  
  @IBOutlet fileprivate weak var tableViewRootMenu:UITableView!
  @IBOutlet fileprivate weak var viewGradientTop:UIView!
  @IBOutlet fileprivate weak var viewGradientBotton:UIView!
  @IBOutlet fileprivate weak var btSetting:UIButton!
  @IBOutlet fileprivate weak var btLogout:UIButton!
  
  @IBOutlet fileprivate weak var tableView:UITableView!
  
    fileprivate var timer = Timer()
  fileprivate var rootMenu:[Menu] = []
  fileprivate var selectRootMenu = 1
  
  fileprivate var menus:[Menu] = []
    var parentIndex = -1
  fileprivate var childIndex = -1

    //
  fileprivate var isSelectedRootMenu = false
  override func viewDidLoad() {
    super.viewDidLoad()
    
//    //Gradient
//    containerView.animationDuration = 3.0
//    // Custom Color
//    containerView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
//                          UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
//                          UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
//                          UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
//                          UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
//                          UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
//                          UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
//    
//    containerView.startAnimation()
    
    UIApplication.shared.statusBarStyle = .lightContent
    btMenu.setImage(#imageLiteral(resourceName: "ic_menu").filled(withColor: UIColor.white), for: .normal)
    
    tableView.register(withClass: MenuTableCell.self)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 200
    tableView.dataSource = self
    tableView.delegate = self
    
    tableViewRootMenu.register(UITableViewCell.self, forCellReuseIdentifier: "CELL")
    tableViewRootMenu.rowHeight = 70
    tableViewRootMenu.dataSource = self
    tableViewRootMenu.delegate = self
    
    let gradientLayerTop:CAGradientLayer = CAGradientLayer()
    gradientLayerTop.frame = self.viewGradientTop.bounds
    gradientLayerTop.colors = [UIColor(red: 24.0/255.0, green: 35.0/255.0, blue: 56.0/255.0, alpha: 1).cgColor,UIColor(red: 24.0/255.0, green: 35.0/255.0, blue: 56.0/255.0, alpha: 0).cgColor]
    gradientLayerTop.startPoint = CGPoint(x: 1.0, y: 0.0)
    gradientLayerTop.endPoint = CGPoint(x: 1.0, y: 1.0)
    self.viewGradientTop.layer.addSublayer(gradientLayerTop)
    
    let gradientLayerBotton:CAGradientLayer = CAGradientLayer()
    gradientLayerBotton.frame = self.viewGradientBotton.bounds
    gradientLayerBotton.colors = [UIColor(red: 24.0/255.0, green: 35.0/255.0, blue: 56.0/255.0, alpha: 1).cgColor,UIColor(red: 24.0/255.0, green: 35.0/255.0, blue: 56.0/255.0, alpha: 0).cgColor]
    gradientLayerBotton.startPoint = CGPoint(x: 1.0, y: 1.0)
    gradientLayerBotton.endPoint = CGPoint(x: 1.0, y: 0.0)
    self.viewGradientBotton.layer.addSublayer(gradientLayerBotton)
    
  }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //containerView.startAnimation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if (self.isSelectedRootMenu == false) {
            let currentUserLeftMenuCode = UserDefaults.standard.object(forKey: Constants.USER_STORED_LEFT_MENU_CODE)
            let currentParentProcedureID = UserDefaults.standard.object(forKey: Constants.PARENT_STORED_PROCEDURE_ID)
            UserDefaults.standard.set(currentUserLeftMenuCode, forKey: Constants.USER_LEFT_MENU_CODE)
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(currentParentProcedureID, forKey: Constants.PARENT_PROCEDURE_ID)
            UserDefaults.standard.synchronize()
        }
    }
    
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func loadRootMenu(_ rootMenu:[Menu]){
    self.rootMenu.removeAll()
    self.rootMenu.append(contentsOf: rootMenu)
    self.tableViewRootMenu.reloadData()
  }
    
    func selectRootMenu(_ index: Int, keepParentIndex: Bool = false){
        self.selectRootMenu = index
        self.isSelectedRootMenu = false
        UserDefaults.standard.set(self.rootMenu[index].code, forKey: Constants.USER_LEFT_MENU_CODE)
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(self.rootMenu[index].id, forKey: Constants.PARENT_PROCEDURE_ID)
        UserDefaults.standard.synchronize()
        if menus.count > 0 && !keepParentIndex {
            self.parentIndex = 0
        }
        if let selectedId: String = self.rootMenu[index].id {
            self.tableViewRootMenu.isUserInteractionEnabled = false
            InstanceDB.default.getMenu(parentId: selectedId, treeFilterCode: self.rootMenu[index].code ?? "") { (result) in
                self.menus.removeAll()
                let childMenuTitle = result.count == 0 ? "": result[0].name ?? ""
                var resultWithoutTitle = result
                resultWithoutTitle.removeFirst()
                self.menus.append(contentsOf: resultWithoutTitle)
                self.loadMenuChildNotes(childMenuTitle,self.menus)
                self.tableViewRootMenu.isUserInteractionEnabled = true
            }
        }
        self.tableViewRootMenu.reloadData()
  }
    
    func selectParentMenu(_ index: Int) {
        if menus.count > index {
            self.parentIndex = index
            self.tableView.reloadData()
        }
    }
    
  func loadMenuChildNotes(_ title:String,_ menus:[Menu]){
    self.lbTitle.text = title.uppercased()
    self.parentIndex = 0
    self.menus.removeAll()
    if (UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) != nil  )
    {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        var tmpMenu = menus
        if ( keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE )
        {
            tmpMenu = menus.filter({ $0.id != $0.parentId })
        }
        self.menus.append(contentsOf: tmpMenu)
        self.tableView.reloadData()
    }
    else
    {
        self.menus.append(contentsOf: menus)
        self.tableView.reloadData()

    }
  }
}

extension LeftViewController{
  @IBAction func pressesBTMenu(_ sender: UIButton){
    self.mainNew()?.toggleMenu()
  }
  @IBAction func pressesBTSetting(_ sender: UIButton){
    let vc = self.storyboard?.instantiateViewController(withClass: SettingViewController.self)
    self.mainNew()?.present(vc!, animated: true, completion: nil)
  }
  @IBAction func pressesBTLogout(_ sender: UIButton){
    
    let alert = UIAlertController(title: LocalizationString.ALERT_TITLE, message: LocalizationString.LOGOUT_TITLE, preferredStyle: .alert)
    let actionOK = UIAlertAction(title: LocalizationString.AGREE_STRING, style: .cancel) { (action) in
      self.logout()
    }
    let actionCancel = UIAlertAction(title: LocalizationString.CANCEL_STRING, style: .default, handler: nil)
    alert.addAction(actionOK)
    alert.addAction(actionCancel)
    self.mainNew()?.present(alert, animated: true, completion: nil)
  }
}
extension LeftViewController {
  
  func logout() {
    SVProgressHUD.show()
    self.removeDeviceInformation()
    LoginManager.logout { (success, message) in
      if success{
        try? dropAndRecreateDatabase(UIApplication.shared)
        self.logoutSuccess(message != "" ? message : LocalizationString.LOGOUT_SUCCESS)
      } else {
        self.logoutError(message != "" ? message : LocalizationString.LOGOUT_FAILSE)
      }
    }
  }
  
  func logoutSuccess(_ message: String) {
    SVProgressHUD.dismiss()
    CurrentUser?.logout()
    self.mainNew()?.dismiss(animated: true, completion: nil)
  }
  
  func logoutError(_ message: String) {
    SVProgressHUD.dismiss()
    let alert = UIAlertController(title: LocalizationString.ALERT_TITLE, message: message, preferredStyle: .alert)
    let actionOK = UIAlertAction(title: LocalizationString.AGREE_STRING, style: .cancel) { (action) in}
    alert.addAction(actionOK)
    self.mainNew()?.present(alert, animated: true, completion: nil)
  }
  
  func removeDeviceInformation() {
    var deviceToken: String = ""
    if let token = Messaging.messaging().fcmToken {
      deviceToken = token
    } else {
      deviceToken = "unknown"
    }
    UserDefaults.standard.removeObject(forKey: Constants.USER_LAST_CHILD_MENU_KEY)
    UserDefaults.standard.removeObject(forKey: Constants.USER_LAST_ROOT_MENU_KEY)
    LoginManager.removeDeviceToken(deviceToken: deviceToken) { (result, msg) in
      debugPrint("removeDeviceToken.status: \(result)")
    }
  }
  
}

extension LeftViewController: UITableViewDelegate, UITableViewDataSource, MenuTableCellDelegate{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.tableView{
      return self.menus.count
    }
    else if tableView == self.tableViewRootMenu{
      return self.rootMenu.count
    }
    return 0
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if tableView == self.tableView{
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as! MenuTableCell
      if(parentIndex == indexPath.row){
        if(childIndex >= 0){
            cell.selectIndex = childIndex
//            Un-comment to show highlighted choosen object
//            cell.lbTitle.font = UIFont(name: FontFamily.Regular.rawValue, size: FontSize.Large.rawValue)!
//        } else {
//            cell.lbTitle.font = UIFont(name: FontFamily.Black.rawValue, size: FontSize.Large.rawValue)!
        }
//      } else {
        
      }
        cell.lbTitle.font = UIFont(name: FontFamily.Regular.rawValue, size: FontSize.Large.rawValue)!
      cell.config(self.menus[indexPath.row])
      cell.tag = indexPath.row
      cell.delegate = self
      return cell
    }
    else if tableView == self.tableViewRootMenu{
      let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)
      cell.selectionStyle = .none
      cell.backgroundColor = UIColor.clear
      
      var img = cell.viewWithTag(1) as? UIImageView
      if img == nil{
        img = UIImageView()
        img?.tag = 1
        cell.addSubview(img!)
        img?.snp.makeConstraints({ (make) in
          make.top.equalTo(10)
          make.left.equalTo(10)
          make.right.equalTo(-10)
          make.bottom.equalTo(-10)
        })
      }
      
      if(indexPath.row == self.selectRootMenu){
        
        if let icon = rootMenu[indexPath.row].icon {
            let url = URL(string: Router.baseURLString + icon)
          let resource = ImageResource(downloadURL: url!, cacheKey: rootMenu[indexPath.row].icon ?? "rootMenu\(indexPath.row)")
          img?.kf.setImage(with: resource, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, url) in
            if(image != nil){
              img?.image = image!.filled(withColor: #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1))
            }
          })
        } else {
            img?.image = #imageLiteral(resourceName: "ic_home_menu").filled(withColor: #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1))
        }
      } else {
        
        if let icon = rootMenu[indexPath.row].icon {
            let url = URL(string: Router.baseURLString + icon)
          let resource = ImageResource(downloadURL: url!, cacheKey: rootMenu[indexPath.row].icon ?? "rootMenu\(indexPath.row)")
          img?.kf.setImage(with: resource)
        } else {
            img?.image = #imageLiteral(resourceName: "ic_home_menu")
        }
      }
      return cell
    }
    else{
      return UITableViewCell()
    }
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if tableView == self.tableView{
      parentIndex = indexPath.row
      childIndex = -1
      self.tableView.reloadData()
      
      self.mainNew()?.toggleMenu()
        self.isSelectedRootMenu = true
        NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICATION_MENU_SELECT_PARENT), object: ["IndexParent":self.selectRootMenu,"IndexChild":indexPath.row, "Menus": self.menus], userInfo: nil)
    }
    else if tableView == self.tableViewRootMenu {
      if(indexPath.row != self.selectRootMenu){
        if (indexPath.row == 0) {
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_SELECT_ROOT), object: indexPath.row, userInfo: nil)
        } else {
            self.loadMenuChildNotes("",[])
            self.selectRootMenu(indexPath.row)
        }
      }
    }
  }
  
  func menuSelected(_ parent:Int,_ child: Int) {
    self.parentIndex = parent
    self.childIndex = child
    self.tableView.reloadData()
    
    self.mainNew()?.toggleMenu()
    NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_SELECT_CHILD), object: ["IndexParent":parent,"IndexChild":child], userInfo: nil)
    
  }
}
