//
//  BookingMeetingInfoVC.swift
//  TaskManagement
//
//  Created by Mirum User on 7/30/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import KSTokenView
import IQKeyboardManagerSwift
import Toast_Swift
import SVProgressHUD
import DropDown


class BookingMeetingInfoVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var btnDepartment: UIButton!
    @IBOutlet weak var sgScope: UISegmentedControl!
    @IBOutlet weak var viewDate: UIView!
    let timePicker = UIDatePicker()
    weak var delegate:MeetingRoomEquipmentDelegate!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var btnOutside: UIButton!
    @IBOutlet weak var btnInside: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var toHourLabel: UILabel!
    @IBOutlet weak var fromHourLabel: UILabel!
    @IBOutlet weak var toDayLabel: UILabel!
    @IBOutlet weak var fromDayLabel: UILabel!
    let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
    let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
    var startDate: Date?
    var endDate: Date?
    var selectedFromDateHour:Date!
    var selectedToDateHour:Date!
    var iselectHourFrom:Bool = false
    var selectedRoomID:String = ""
    var selectedDepartment:String = ""
    @IBOutlet weak var btnRooms: UIButton!
    @IBOutlet weak var lblTitleMeeting: UITextField!
    
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var assignersTokenView: KSTokenView!
    @IBOutlet weak var joningRequireTokenView: KSTokenView!
    @IBOutlet weak var joiningOptionTokenView: KSTokenView!
    let ASSIGNEES_TEXT_VIEW_PLACE_HOLDER: String = "người xử lý"
    let CATEGORIES_TOKEN_TAG: Int = 1
    let ASSIGNERS_TOKEN_TAG: Int = 2
    let ASSIGNERS_TOKEN_TAG_REQUIRE: Int = 3
    let ASSIGNERS_TOKEN_TAG_OPTION: Int = 4
    
    var employees: [Assignee] = Dummies.shared.getAllEmployees()
    var flatEmployees: [Assignee] = [Assignee]()
    var authorAcc: [UserProfile] = [UserProfile]()
    var requireAcc: [UserProfile] = [UserProfile]()
    var optionAcc: [UserProfile] = [UserProfile]()
    
    var addTokenFromSearchInput = true
    var arrayRootList:[UserProfile]!
    var OrgID:String!

    let taskDropDown: DropDown = DropDown()
    var arrayMeetingBookingRoomObject: [GetMeetingRoomObject] = [] {
        didSet {
            if !arrayMeetingBookingRoomObject.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayMeetingBookingRoomObject.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    let taskDropDownDepartment: DropDown = DropDown()
    var departmentArray: [GetMeetingRoomObject] = [] {
        didSet {
            if !departmentArray.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = departmentArray.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.arrayRootList = DataManager.shared.allUsers
        self.txtComment.isHidden = true
        self.txtComment.delegate = self
        self.lblTitleMeeting.delegate = self
        self.txtNumber.delegate = self
        // Do any additional setup after loading the view.
        let currentDate: Date = Constants.currentDate
        self.startDate = currentDate
        self.endDate = currentDate
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(BookingMeetingInfoVC.actionStartTimeButton))
        fromDayLabel.isUserInteractionEnabled = true
        fromDayLabel.addGestureRecognizer(tap)
        
        let tapTo = UITapGestureRecognizer(target: self, action: #selector(BookingMeetingInfoVC.actionEndTimeButton))
        toDayLabel.isUserInteractionEnabled = true
        toDayLabel.addGestureRecognizer(tapTo)
        
        let tapSelctedTime = UITapGestureRecognizer(target: self, action: #selector(BookingMeetingInfoVC.selectedTimeFrom))
        fromHourLabel.isUserInteractionEnabled = true
        fromHourLabel.addGestureRecognizer(tapSelctedTime)
        
        let tapSelectedTimeTo = UITapGestureRecognizer(target: self, action: #selector(BookingMeetingInfoVC.selectedTimeTo))
        toHourLabel.isUserInteractionEnabled = true
        toHourLabel.addGestureRecognizer(tapSelectedTimeTo)
        
        
        initUI()
        
        ////        load current department
        SyncProvider.getCurrentDepartment(done: {
            (resultData) in
            if ( resultData?.data != nil && (resultData?.data?.count)! > 0 )
            {
                self.departmentArray = (resultData?.data)!
                self.btnDepartment.setTitle(self.departmentArray[0].Name!, for: .normal)
                self.selectedDepartment = self.departmentArray[0].ID!
                self.updateCallbackData()
                self.loadDataObject()
            }
        })
        
        let parentIdx:Int = UserDefaults.standard.integer(forKey: Constants.USER_LAST_CHILD_MENU_KEY) as Int

        if ( DataManager.shared.arrayMeetingRoomObject.count > 0 && DataManager.shared.arrayMeetingRoomObject.count > parentIdx )
        {
            let object:MeetingRoomObject = DataManager.shared.arrayMeetingRoomObject[parentIdx]
            SyncProvider.getListDepartmentMeetingRoom(DepartmentID: object.ID!, RoomID: "0", done: {
                (result) in
                if ( result?.data != nil  )
                {
                    self.arrayMeetingBookingRoomObject = (result?.data)!
                    self.btnRooms.setTitle(self.arrayMeetingBookingRoomObject[0].Name!, for: .normal)
                    self.selectedRoomID = self.arrayMeetingBookingRoomObject[0].ID!
                    self.updateCallbackData()
                    if (( self.delegate ) != nil)
                    {
                        self.delegate.selectedRoomEquipment(equipment: self.arrayMeetingBookingRoomObject[0])
                    }
                }
            })
        }
        
        self.loadStartEndDays(Date(), Date())
        
    }
    func loadDataObject(){
        
        if ( !self.OrgID.isEmpty) {
            SVProgressHUD.show()
            SyncProvider.getDetailBookingRoom(OrgID: self.OrgID, done: {
                ( result ) in
                SVProgressHUD.dismiss()
                if ( result != nil  )
                {
                    let bookingObjectDetail: CreateBookingObject = (result?.data)!
                    self.txtNumber.text = String(format: "%d", bookingObjectDetail.ParticipantNumber!)
                    self.lblTitleMeeting.text = bookingObjectDetail.Title
                    let fromDate:Date = (bookingObjectDetail.FromDate?.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
                    self.fromDayLabel.text = fromDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
                    self.txtComment.isHidden = false
                    let toDate:Date = (bookingObjectDetail.ToDate?.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
                    self.toDayLabel.text = toDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
                    
                    self.startDate = fromDate
                    self.endDate = toDate
                    self.selectedToDateHour = toDate
                    self.selectedFromDateHour = fromDate
                    
                    self.fromHourLabel.text = fromDate.toFormatHour24NonSecond()
                    self.toHourLabel.text = toDate.toFormatHour24NonSecond()
                    self.sgScope.selectedSegmentIndex = bookingObjectDetail.Scope!
                    
                    let objectSelected:[GetMeetingRoomObject] = self.arrayMeetingBookingRoomObject.filter({ $0.ID == bookingObjectDetail.RoomID })
                    if ( objectSelected.count > 0  )
                    {
                        self.btnRooms.setTitle(objectSelected[0].Name!, for: .normal)
                        self.selectedRoomID = objectSelected[0].ID!
                    }
                    else
                    {
                        if (self.arrayMeetingBookingRoomObject.count > 0)
                        {
                            self.btnRooms.setTitle(self.arrayMeetingBookingRoomObject[0].Name!, for: .normal)
                            self.selectedRoomID = self.arrayMeetingBookingRoomObject[0].ID!
                        }
                    }
                    if ( bookingObjectDetail.Owner != nil &&  self.arrayRootList != nil )
                    {
                        let arrayAcc:ApproversObject = bookingObjectDetail.Owner!
                        let ownerAssignee:[UserProfile] = self.arrayRootList.filter({ $0.fullName == arrayAcc.Name || $0.loginName == arrayAcc.UserName })
                        if ( ownerAssignee != nil && ownerAssignee.count > 0 )
                        {
                            self.authorAcc.append(ownerAssignee[0])
                            self.assignersTokenView.addTokenWithTitle(ownerAssignee[0].fullName!)
                            self.assignersTokenView.layoutIfNeeded()
                            self.assignersTokenView.layoutSubviews()
                            
                        }
                    }
                    
                    if ( bookingObjectDetail.RequiredParticipants != nil )
                    {
                        let requireAss:[ApproversObject] = bookingObjectDetail.RequiredParticipants!
                        for objectAssignee in requireAss
                        {
                            let requireOption:[UserProfile] = self.arrayRootList.filter({ $0.fullName == objectAssignee.Name || $0.loginName == objectAssignee.UserName })
                            if ( requireOption != nil && requireOption.count > 0 )
                            {
                                self.requireAcc.append(requireOption[0])
                                self.joningRequireTokenView.addTokenWithTitle(requireOption[0].fullName!)
                                self.joningRequireTokenView.layoutIfNeeded()
                                self.joningRequireTokenView.layoutSubviews()
                            }
                        }
                    }
                    
                    
                    if ( bookingObjectDetail.Participants != nil )
                    {
                        let optionalAccc:[ApproversObject] = bookingObjectDetail.Participants!
                        if ( optionalAccc.count > 0 )
                        {
                            for objectAssignee in optionalAccc
                            {
                                let requireOption:[UserProfile] = self.arrayRootList.filter({  $0.fullName == objectAssignee.Name || $0.loginName == objectAssignee.UserName })
                                if ( requireOption != nil && requireOption.count > 0 )
                                {
                                    self.optionAcc.append(requireOption[0])
                                    self.joiningOptionTokenView.addTokenWithTitle(requireOption[0].fullName!)
                                    self.joiningOptionTokenView.layoutIfNeeded()
                                    self.joiningOptionTokenView.layoutSubviews()
                                }
                            }
                        }
                    }
                    
                    
                    if ( self.delegate != nil )
                    {
                        
                        self.delegate.updateAddingMore(Assistances: bookingObjectDetail.Assistances!, Equipments: bookingObjectDetail.Equipments!, description: bookingObjectDetail.Description!)
                        self.delegate.updateActiontype(actionType: (bookingObjectDetail.CurrentTask?.itemAction)!)
                        self.delegate.updateHistoryView(history: bookingObjectDetail.Histories!)
                        self.updateCallbackData()
                        
                    }
                    
                    
                }
            })
        }
    }
    
    func initUI(){
        
        assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        assignersTokenView.promptText = ""
        assignersTokenView.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        assignersTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        assignersTokenView.delegate = self
        assignersTokenView.searchResultHeight = 120
        assignersTokenView.tag = ASSIGNERS_TOKEN_TAG
        assignersTokenView.returnKeyType(type: .done)
        assignersTokenView.shouldDisplayAlreadyTokenized = true
        assignersTokenView.maxTokenLimit = -1
        assignersTokenView.shouldSortResultsAlphabatically = false
        assignersTokenView.removesTokensOnEndEditing = false
        assignersTokenView.tokenizingCharacters = []
       
        
        
        joningRequireTokenView.addSubviews(joningRequireTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        joningRequireTokenView.promptText = ""
        joningRequireTokenView.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        joningRequireTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        joningRequireTokenView.delegate = self
        joningRequireTokenView.searchResultHeight = 120
        joningRequireTokenView.tag = ASSIGNERS_TOKEN_TAG_REQUIRE
        joningRequireTokenView.returnKeyType(type: .done)
        joningRequireTokenView.shouldDisplayAlreadyTokenized = true
        joningRequireTokenView.maxTokenLimit = -1
        joningRequireTokenView.shouldSortResultsAlphabatically = false
        joningRequireTokenView.removesTokensOnEndEditing = false
        joningRequireTokenView.tokenizingCharacters = []

        
        
        joiningOptionTokenView.addSubviews(joiningOptionTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        joiningOptionTokenView.promptText = ""
        joiningOptionTokenView.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        joiningOptionTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        joiningOptionTokenView.delegate = self
        joiningOptionTokenView.searchResultHeight = 120
        joiningOptionTokenView.tag = ASSIGNERS_TOKEN_TAG_OPTION
        joiningOptionTokenView.returnKeyType(type: .done)
        joiningOptionTokenView.shouldDisplayAlreadyTokenized = true
        joiningOptionTokenView.maxTokenLimit = -1
        joiningOptionTokenView.shouldSortResultsAlphabatically = false
        joiningOptionTokenView.removesTokensOnEndEditing = false
        joiningOptionTokenView.tokenizingCharacters = []
        
        taskDropDown.anchorView = self.btnRooms
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnRooms.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)

        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            if (( self.delegate ) != nil)
            {
                self.delegate.selectedRoomEquipment(equipment: self.arrayMeetingBookingRoomObject[index])
                self.selectedRoomID = self.arrayMeetingBookingRoomObject[index].ID!
                self.updateCallbackData()
            }
            self.btnRooms.setTitle(item, for: .normal)
           
        }
        
        
        taskDropDownDepartment.anchorView = self.btnDepartment
        taskDropDownDepartment.textColor = UIColor.gray
        taskDropDownDepartment.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDownDepartment.bottomOffset = CGPoint(x: 0, y: self.btnDepartment.bounds.height + 10)
        taskDropDownDepartment.isMultipleTouchEnabled = false
        taskDropDownDepartment.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDownDepartment.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDownDepartment.selectionAction = { [unowned self] (index, item) in
            if (( self.delegate ) != nil)
            {
                self.selectedDepartment = self.departmentArray[index].ID!
                self.updateCallbackData()
            }
            self.btnDepartment.setTitle(item, for: .normal)
            
        }
        
        
        getAllEmployees()
        
        self.viewDate.isHidden = true
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0.0, y: 35, width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        self.viewDate.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(BookingMeetingInfoVC.startTimeDiveChanged), for: UIControlEvents.valueChanged)
        self.selectedFromDateHour = Date()
        self.selectedToDateHour = Date()

    }
    
    @IBAction func confirmTime(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        if ( iselectHourFrom )
        {
            fromHourLabel.text = self.selectedFromDateHour.toFormatHour24NonSecond()
        }
        else
        {
            toHourLabel.text = self.selectedToDateHour.toFormatHour24NonSecond()
        }
        self.updateCallbackData()
         self.viewDate.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
          self.updateCallbackData()
    }
    
    @IBAction func cancelTime(_ sender: Any) {
       self.viewDate.isHidden = true
    }
    func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        if ( iselectHourFrom )
        {
            self.selectedFromDateHour = sender.date
        }
        else
        {
            self.selectedToDateHour =  sender.date
        }
        self.updateCallbackData()
    }
    func getAllEmployees () {
      
        self.employees = DataManager.shared.employees
        self.flatEmployees = DataManager.shared.flatEmployees
      
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var frame = containView.frame
        frame.size.height = 1200
        containView.frame = frame
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1200)
        self.scrollView.layoutIfNeeded()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func selectedTimeFrom (_ sender:UITapGestureRecognizer) {
        self.viewDate.isHidden = false
        iselectHourFrom = true
    }
    @IBAction func selectedTimeTo (_ sender:UITapGestureRecognizer) {
        self.viewDate.isHidden = false
        iselectHourFrom = false
    }
    @IBAction func actionStartTimeButton (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func actionEndTimeButton (_ sender: UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
        fromDayLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        fromHourLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
        toDayLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        toHourLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    }
    @IBAction func changeDepartMent(_ sender: Any) {
        self.taskDropDownDepartment.show()
    }
    @IBAction func changeProcedureType(_ sender: Any) {
        self.taskDropDown.show()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [UserProfile]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? UserProfile, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    func updateCallbackData(){
        if (( self.delegate ) != nil) {
            
            let fromDateString = self.startDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
            let toDateString = self.endDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
            let fromHour = self.selectedFromDateHour.toFormatHour24()
            let toHour = self.selectedToDateHour.toFormatHour24()
            let fromdateHour:String = String(format: "%@T%@Z", fromDateString!,fromHour)
            let todateHour:String = String(format: "%@T%@Z", toDateString!,toHour)
            let numberMeeting:Int = (self.txtNumber.text?.isEmpty)! ? 0 : Int(self.txtNumber.text!)!
            
            self.delegate.updateRoomComponent(FromDate: fromdateHour, ToDateMeeting: todateHour, Owner: self.authorAcc, ParticipantNumber: numberMeeting, Participants: self.optionAcc, RequiredParticipants: self.requireAcc, RoomID: self.selectedRoomID, titleMeeting: self.lblTitleMeeting.text!,scope: self.sgScope.selectedSegmentIndex, comment: self.txtComment.text!,departmentID: self.selectedDepartment)
        }
    }
    @IBAction func segmentControl(_ sender: Any) {
        self.updateCallbackData()
    }
    

}
extension BookingMeetingInfoVC: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
        if let title: String = selector.optionTopPanelTitle {
            if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
                self.startDate = date
                self.loadStartEndDays(date, toDate)
            } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
                self.endDate = date
                self.loadStartEndDays(fromDate, date)
            }
        }
    }
    
}
extension BookingMeetingInfoVC: KSTokenViewDelegate {
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
        {
            joningRequireTokenView.layoutIfNeeded()
            joningRequireTokenView.layoutSubviews()
          
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION
        {
            joiningOptionTokenView.layoutIfNeeded()
            joiningOptionTokenView.layoutSubviews()
           
        }
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            return completion!([])
        }
        var data: Array<AnyObject> = []
        var valueResults = [UserProfile]()
        var textResults = [UserProfile]()
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE || tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION  {
            let locale = Locale(identifier: "vi_VN")
            for item: UserProfile in self.arrayRootList {
                
                if let text = item.fullName, let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    valueResults.append(item)
                }else if let text = item.fullName?.folding(options: .diacriticInsensitive, locale: locale), let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    textResults.append(item)
                }
                data = valueResults.sorted{ ($0.fullName ?? "") < ($1.fullName ?? "") }
                data.append(contentsOf: textResults)
                //data = results
            }
        }
        return completion!(data)
    }
    
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE || tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION  {
            if let assignee = object as? UserProfile,
                let name: String = assignee.fullName {
                return name
            } else {
                return "Chưa xác định"
            }
        }
        return object as! String
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE || tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION {
            if let tokenObject: UserProfile = token.object as? UserProfile {
                token.tokenBackgroundColor = tokenObject.getBackgroundColorToTokenView()
            } else {
                token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
            }
        } else if tokenView.tag == CATEGORIES_TOKEN_TAG {
            token.tokenBackgroundColor = Theme.default.normalOrangeSelectedColor
        }
        return token
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? UserProfile {
            
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                if let index = self.authorAcc.index(of: user) {
                    self.authorAcc.remove(at: index)
                }
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                if let index = self.requireAcc.index(of: user) {
                    self.requireAcc.remove(at: index)
                }
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION
            {
                if let index = self.optionAcc.index(of: user) {
                    self.optionAcc.remove(at: index)
                }
            }
            self.updateCallbackData()
            
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE || tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION, let user = token.object as? UserProfile, addTokenFromSearchInput {
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                self.removeAssignersTokenView(assignersTokenView, [user])
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                self.removeAssignersTokenView(joningRequireTokenView, [user])
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION
            {
                self.removeAssignersTokenView(joiningOptionTokenView, [user])
            }
            
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                self.authorAcc.append(user)
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
               self.requireAcc.append(user)
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION
            {
                self.optionAcc.append(user)
            }
            self.updateCallbackData()
        }
        self.view.endEditing(true)
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE {
            joningRequireTokenView.layoutIfNeeded()
            joningRequireTokenView.layoutSubviews()
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION {
            joiningOptionTokenView.layoutIfNeeded()
            joiningOptionTokenView.layoutSubviews()
        }
        
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE || tokenView.tag == ASSIGNERS_TOKEN_TAG_OPTION , let assignee = object as? UserProfile  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            return cell!
        }
        return UITableViewCell()
    }
    
}
