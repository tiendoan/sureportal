//
//  PresentRoomDetail.swift
//  TaskManagement
//
//  Created by Mirum User on 8/22/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class PresentRoomDetail: UIViewController {

    @IBOutlet weak var lbDescrip: UILabel!
    @IBOutlet weak var lbPosition: UILabel!
    @IBOutlet weak var lbEquip: UILabel!
    @IBOutlet weak var lbDevice: UILabel!
    @IBOutlet weak var lbtotal: UILabel!
    @IBOutlet weak var lbRoomTitle: UILabel!
    @IBOutlet weak var dismissAction: UIButton!
    var objectGetting:GetMeetingRoomObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.lbRoomTitle.text = objectGetting.Name
        self.lbDescrip.text = objectGetting.Description
        self.lbPosition.text = objectGetting.Org?.Name
        self.lbtotal.text = String(format: "%d", objectGetting.Capacity!)
        var devicesRoom:String = ""
        for object in objectGetting.Equipments! {
            devicesRoom = String(format: "%@, %@ ", devicesRoom,object.Name!)
        }
        if !devicesRoom.isEmpty {
            devicesRoom.remove(at: devicesRoom.startIndex)
        }
        self.lbDevice.text = devicesRoom
        
        var equipment:String = ""
        for object in objectGetting.Assistances! {
            equipment = String(format: "%@, %@ ", equipment,object.Name!)
        }
        if !equipment.isEmpty {
            equipment.remove(at: equipment.startIndex)
        }
        self.lbEquip.text = equipment
        
        // Do any additional setup after loading the view.
    }
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
