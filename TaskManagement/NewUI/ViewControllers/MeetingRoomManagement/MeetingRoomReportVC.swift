//
//  MeetingRoomReportVC.swift
//  TaskManagement
//
//  Created by Mirum User on 8/15/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Toast_Swift
import SVProgressHUD
import DropDown

class MeetingRoomReportVC: UIViewController {

    var arrayBookingDic:[String: [GetDetailMeetingRoomObject]] = [String: [GetDetailMeetingRoomObject]]()
    var arrayMeetingBookingRoomObject: [GetMeetingRoomObject] = []
    @IBOutlet weak var tbReportTable: UITableView!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbToDate: UILabel!
    @IBOutlet weak var lbFromDate: UILabel!
    @IBOutlet weak var btnOrg: UIButton!
    var selectedBookingID:String!
    var startDate:Date!
    var endDate:Date!
    let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
    let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
    
    let taskDropDown: DropDown = DropDown()
    var arrayMeetingRoomObject: [MeetingRoomObject] = [] {
        didSet {
            if !arrayMeetingRoomObject.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayMeetingRoomObject.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    @IBAction func departmentAction(_ sender: Any) {
        taskDropDown.show()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        tbReportTable.register(withClass: BookingMeetingRoomCell.self)
        
        let headerNibTitle = UINib.init(nibName: "MeetingTitleView", bundle: Bundle.main)
        tbReportTable.register(headerNibTitle, forHeaderFooterViewReuseIdentifier: "MeetingTitleView")
        tbReportTable.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
        let currentDate: Date = Constants.currentDate
        self.endDate = currentDate
        self.startDate =  Calendar.current.date(byAdding: .month, value: -1, to: Date())
        self.tbReportTable.delegate = self
        self.tbReportTable.dataSource = self

        let tap = UITapGestureRecognizer(target: self, action: #selector(MeetingRoomReportVC.actionStartTimeButton))
        lbFromDate.isUserInteractionEnabled = true
        lbFromDate.addGestureRecognizer(tap)
        
        let tapTo = UITapGestureRecognizer(target: self, action: #selector(MeetingRoomReportVC.actionEndTimeButton))
        lbToDate.isUserInteractionEnabled = true
        lbToDate.addGestureRecognizer(tapTo)
        self.loadStartEndDays(self.startDate, self.endDate)
        self.arrayMeetingRoomObject = DataManager.shared.arrayMeetingRoomObject
        selectedBookingID = self.arrayMeetingRoomObject[0].ID
        self.btnOrg.setTitle(self.arrayMeetingRoomObject[0].Name!, for: .normal)
        self.getBookingRoom()
        
        taskDropDown.anchorView = self.btnOrg
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnOrg.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedBookingID = self.arrayMeetingRoomObject[index].ID!
            self.btnOrg.setTitle(self.arrayMeetingRoomObject[index].Name!, for: .normal)
            self.getBookingRoom()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionStartTimeButton (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func actionEndTimeButton (_ sender: UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
        lbFromDate.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        lbToDate.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension MeetingRoomReportVC: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
        if let title: String = selector.optionTopPanelTitle {
            
            if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
                    
                self.startDate = date
                self.loadStartEndDays(date, toDate)
            } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
                self.endDate = date
                self.loadStartEndDays(fromDate, date)
            }
            self.getBookingRoom()
        }
    }
    func getBookingRoom(){
        
        var fromDate:String = self.startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
        var toDate:String = self.endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
        fromDate = String(format: "%@T00:00:00", fromDate)
        toDate = String(format: "%@T23:50:00", toDate)
        self.arrayMeetingBookingRoomObject.removeAll()
        self.arrayBookingDic.removeAll()
        var object = DataManager.shared.arrayMeetingRoomObject.filter({ $0.ID! == self.selectedBookingID })
        if ( object.count > 0  )
        {
            SyncProvider.getListDepartmentMeetingRoom(DepartmentID: object[0].ID!, RoomID: "0", done: {
                (result) in
                if ( result?.data != nil )
                {
                    let tmp:[GetMeetingRoomObject] = (result?.data)!
                    let dispatchGroup = DispatchGroup()
                    for objectBooking in tmp
                    {
                        dispatchGroup.enter()
                        SyncProvider.getBookingRoom(DepartmentID: object[0].ID!, FromDate: fromDate, Page: "0", PageSize: "0", RoomID: objectBooking.ID!, Status: "-1", ToDate: toDate, done: {
                            (result) in
                            if ( result != nil && result?.data != nil && (result?.data?.count)! > 0 )
                            {
                                self.arrayMeetingBookingRoomObject.append(objectBooking)
                                self.arrayBookingDic[objectBooking.ID!] = result?.data!
                            }
                            dispatchGroup.leave()
                            
                        })
                    }
                    dispatchGroup.notify(queue: DispatchQueue.main) {
                        
                        var totalRecord:Int = 0
                        for objectRoom in self.arrayMeetingBookingRoomObject
                        {
                            let dic = self.arrayBookingDic.filter({ $0.key == objectRoom.ID! })
                            totalRecord += dic[0].value.count
                        }
                        let dictmp =  self.arrayBookingDic.sorted(by: { $0.0.key < $0.1.key })
                        self.arrayBookingDic.removeAll()
                        for object in dictmp
                        {
                            self.arrayBookingDic[object.key] = object.value
                        }
                        self.lbTotal.text = String(format: "%d", totalRecord)
                        
                        self.tbReportTable.reloadData()
                        self.tbReportTable.tableFooterView = nil
                        self.tbReportTable.tableFooterView?.isHidden = true
                    }
                }
                
            })
            
        }
        else
        {
            self.tbReportTable.reloadData()
            self.tbReportTable.tableFooterView = nil
            self.tbReportTable.tableFooterView?.isHidden = true
        }
        
    }
    
    
}
extension MeetingRoomReportVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayMeetingBookingRoomObject.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( self.arrayMeetingBookingRoomObject.count > 0 ) {
            let object = self.arrayMeetingBookingRoomObject[section]
            let dic = self.arrayBookingDic.filter({ $0.key == object.ID! })
            return dic.count > 0 ? dic[0].value.count > 0 ? dic[0].value.count : 1 : 1
        }
        return 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell:BookingMeetingRoomCell = tableView.dequeueReusableCell(withIdentifier: "BookingMeetingRoomCell", for: indexPath) as! BookingMeetingRoomCell
            let object = self.arrayMeetingBookingRoomObject[indexPath.section]
            let dic = self.arrayBookingDic.filter({ $0.key == object.ID! })
            if dic.count > 0 && dic[0].value.count > 0
            {
                let mettingRoom:GetDetailMeetingRoomObject = dic[0].value[indexPath.row]
                
                cell.heightEmpty.constant = 0
                cell.heightBooking.constant = 60
                cell.lbToDate.isHidden = false
                cell.lbFromDate.isHidden = false
                cell.lblRoomMaster.isHidden = false
                cell.lbRoomNumber.isHidden = false
                cell.img.isHidden = false
                if ( mettingRoom.Status == BookingMeetingStatus.WaitingApproval.rawValue )
                {
                    cell.img.image = UIImage(named: "icon_booking_pending")
                }
                else if ( mettingRoom.Status == BookingMeetingStatus.Approved.rawValue )
                {
                    cell.img.image = UIImage(named: "icon_booking")
                }
                cell.lbRoomNumber.text = mettingRoom.Title!
                cell.lblRoomMaster.text = mettingRoom.Owner?.Name!
                cell.lbFromDate.text =  Constants.formatTimeFromDate(mettingRoom.FromDate!)
                cell.lbToDate.text =  Constants.formatTimeFromDate(mettingRoom.ToDate!)
            }
            else
            {
                cell.heightBooking.constant = 0
                cell.heightEmpty.constant = 60
                cell.lbToDate.isHidden = true
                cell.lbFromDate.isHidden = true
                cell.lblRoomMaster.isHidden = true
                cell.lbRoomNumber.isHidden = true
                cell.img.isHidden = true
            }
            return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
            let object = self.arrayMeetingBookingRoomObject[section]
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MeetingTitleView" ) as! MeetingTitleView
            headerView.lbTitle.text = object.Name!
            return headerView
       
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      return 40
    }
}
