//
//  MeetingRoomCharVC.swift
//  TaskManagement
//
//  Created by Mirum User on 8/16/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Toast_Swift
import SVProgressHUD
import DropDown
import Charts

class MeetingRoomCharVC: UIViewController,IValueFormatter,IAxisValueFormatter {
   
    
    @IBOutlet weak var heightPieChart: NSLayoutConstraint!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var heightLineChart: NSLayoutConstraint!
    @IBOutlet weak var heightColumChart: NSLayoutConstraint!
    @IBOutlet weak var horizonBar: HorizontalBarChartView!
    @IBOutlet weak var chartView: LineChartView!
    var arrayBookingDic:Dictionary = [String: [GetDetailMeetingRoomObject]]()
    var arrayMeetingBookingRoomObject: [GetMeetingRoomObject] = []
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbToDate: UILabel!
    @IBOutlet weak var lbFromDate: UILabel!
    @IBOutlet weak var btnOrg: UIButton!
    var selectedBookingID:String!
    var startDate:Date!
    var endDate:Date!
    let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
    let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
    let taskDropDown: DropDown = DropDown()
    var days: [String]!
    let sliderX:Int = 45
    let sliderY:Int = 100
    var lineChart:LineChartResponse!
    var stackColumn:StackedChartResponse!
    var pieChart: PieChartResponse!
    weak var axisFormatDelegate: IAxisValueFormatter?
    var monthsLine:[String]!
    
    var arrayMeetingRoomObject: [MeetingRoomObject] = [] {
        didSet {
            if !arrayMeetingRoomObject.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayMeetingRoomObject.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    @IBAction func derpartmentAction(_ sender: Any) {
        taskDropDown.show()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(MeetingRoomCharVC.actionStartTimeButton))
        lbFromDate.isUserInteractionEnabled = true
        lbFromDate.addGestureRecognizer(tap)
        
        let tapTo = UITapGestureRecognizer(target: self, action: #selector(MeetingRoomCharVC.actionEndTimeButton))
        lbToDate.isUserInteractionEnabled = true
        lbToDate.addGestureRecognizer(tapTo)
        self.endDate = Date()
        self.startDate =  Calendar.current.date(byAdding: .month, value: -1, to: Date())
        self.loadStartEndDays(startDate, endDate)
        
        taskDropDown.anchorView = self.btnOrg
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnOrg.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedBookingID = self.arrayMeetingRoomObject[index].ID!
            self.btnOrg.setTitle(self.arrayMeetingRoomObject[index].Name!, for: .normal)
            self.getBookingRoom()
        }
        
        self.arrayMeetingRoomObject = DataManager.shared.arrayMeetingRoomObject
        selectedBookingID = self.arrayMeetingRoomObject[0].ID
        self.btnOrg.setTitle(self.arrayMeetingRoomObject[0].Name!, for: .normal)
        
        self.getBookingRoom()
//        self.actionDrawChartView()
        // Do any additional setup after loading the view.
    }
    
    func getBookingRoom(){
        
        var fromDate:String = self.startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
        var toDate:String = self.endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
        fromDate = String(format: "%@T00:00:00", fromDate)
        toDate = String(format: "%@T23:50:00", toDate)
        self.arrayMeetingBookingRoomObject.removeAll()
        self.arrayBookingDic.removeAll()
        let object = DataManager.shared.arrayMeetingRoomObject.filter({ $0.ID! == self.selectedBookingID })
        if ( object.count > 0  )
        {
              let dic:[String : Any] = ["OrgID":object[0].ID!,"RoomID":"","DepartmentID":"","Status":-1,"FromDate":fromDate,"ToDate":toDate] as [String : Any]
            
            SyncProvider.getChartMeetingRoom(json: dic, done: {
                 (resultData) in
                if ( resultData != nil  )
                {
                    self.lineChart = resultData?.ChartResponse?.LineChartResponse
                    self.stackColumn = resultData?.ChartResponse?.StackColumnChartReponse
                    self.pieChart = resultData?.ChartResponse?.PieCharReponse
                    self.actionDrawChartView()
                    self.actionDrawColumnView()
                    self.drawPieCharView()
                }
            })
            
        }
       
        
    }
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
         return String(value)
    }

    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if (  Int(value) < monthsLine.count ) {
            return monthsLine[Int(value)]
        }
        return ""
    }
    func actionDrawChartView(){
        
        self.heightLineChart.constant = 250
        axisFormatDelegate = self
        if ( (self.lineChart.data?.count)! > 0 )
        {

            let chartY: [Int] = (self.lineChart.data?.map({
                (lineChart: LineChart) -> Int in
                lineChart.Data!
            }))!
            
            monthsLine = (self.lineChart.data?.map({
                (lineChart: LineChart) -> String in
                lineChart.Label!
            }))!
             // 1 - creating an array of data entries
            var yValues : [ChartDataEntry] = [ChartDataEntry]()
            
            for i in 0 ..< chartY.count {
                yValues.append(ChartDataEntry(x: Double(i + 1), y: Double(chartY[i])))
            }
            
            let data = LineChartData()
            let ds = LineChartDataSet(values: yValues, label: "Tuần")
            data.addDataSet(ds)
            data.dataSets[0].valueFormatter = self
            self.chartView.data = data
            let xAxisValues = self.chartView.xAxis
            xAxisValues.valueFormatter = axisFormatDelegate
            self.chartView.chartDescription?.enabled = false
            self.chartView.xAxis.labelPosition = .bottom

        }
    }
    func actionDrawColumnView(){
        axisFormatDelegate = self
        self.heightColumChart.constant = 250
        monthsLine = (self.stackColumn.data?.map({
            (stackChart: StackedColumnChart) -> String in
            stackChart.Label!
        }))!
        
        let chartWait: [Int] = (self.stackColumn.data?.map({
            (stackChart: StackedColumnChart) -> Int in
            stackChart.Wait!
        }))!
        
        let chartApprove: [Int] = (self.stackColumn.data?.map({
            (stackChart: StackedColumnChart) -> Int in
            stackChart.Approve!
        }))!
    
        let chartDecline: [Int] = (self.stackColumn.data?.map({
            (stackChart: StackedColumnChart) -> Int in
             stackChart.Delete!
        }))!
        
        horizonBar.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        for i in 0..<monthsLine.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(chartWait[i]))
            let dataEntryApprove = BarChartDataEntry(x: Double(i), y: Double(chartApprove[i]))
            let dataEntryDecline = BarChartDataEntry(x: Double(i), y: Double(chartDecline[i]))
            
            dataEntries.append(dataEntry)
            dataEntries.append(dataEntryApprove)
            dataEntries.append(dataEntryDecline)
        }

        let chartData = BarChartData()
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "")
        
        let COLOR_SET : [UIColor]!
        COLOR_SET = [UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1),UIColor.green,UIColor.red]
        chartDataSet.setColors(COLOR_SET, alpha: 1.0)
        chartDataSet.stackLabels = ["Chờ duyệt", "Duyệt","Huỷ"]
        chartData.addDataSet(chartDataSet)
        let xAxisValues = self.horizonBar.xAxis
        xAxisValues.valueFormatter = axisFormatDelegate
        horizonBar.data = chartData
        self.horizonBar.xAxis.labelPosition = .bottomInside
        self.horizonBar.chartDescription?.enabled = false
    }
    func drawPieCharView(){
        self.heightPieChart.constant = 200
        let chartY: [Double] = (self.pieChart.data?.map({
            (lineChart: PieChart) -> Double in
            Double(lineChart.MeetingTotal!)
        }))!
        
        let gross: [String] = (self.pieChart.data?.map({
        (lineChart: PieChart) -> String in
            lineChart.Label!
        }))!
        setChart(days: gross, gross: chartY)
    }
    func setChart(days: [String], gross: [Double]) {
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<days.count {
            let dataEntry = PieChartDataEntry(value : gross[i], label : days[i])
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        pieChartDataSet.sliceSpace = 2.0
        let pieChartData = PieChartData(dataSets: [pieChartDataSet])
        
        var colors: [UIColor] = []
        
        for _ in 0..<days.count {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        
        pieChartDataSet.colors = colors
        pieChartView.chartDescription?.enabled = false
        pieChartView.data = pieChartData

    }
    @IBAction func actionStartTimeButton (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func actionEndTimeButton (_ sender: UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
        lbFromDate.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        lbToDate.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MeetingRoomCharVC: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
        if let title: String = selector.optionTopPanelTitle {
            
            if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
                
                self.startDate = date
                self.loadStartEndDays(date, toDate)
            } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
                self.endDate = date
                self.loadStartEndDays(fromDate, date)
            }
            self.getBookingRoom()
        }
    }
}

