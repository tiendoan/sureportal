//
//  MettingRoomAddMoreInfoVC.swift
//  TaskManagement
//
//  Created by Mirum User on 7/31/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
enum ActionMoreType {
    case Assitants
    case Equipment
}
protocol AddMoreDelegate : class {
    func addMoreActionAssistants(objAssistants: MeetingRoomObject)
    func addMoreActionEquipment(objEquipments: MeetingRoomEquipmentObject)
    func updateNotes(notes: String)
}
class MettingRoomAddMoreInfoVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,AddMoreDelegate {
    func updateNotes(notes: String) {
        self.lblDescription = notes
        if ( self.delegate != nil) {
            
            self.delegate.updateAddingMore(Assistances: self.arrayAssitants, Equipments: self.arrayEquipments, description: self.lblDescription)
            
        }

    }
    
    
    func addMoreActionAssistants(objAssistants: MeetingRoomObject) {
        let indexObj = self.arrayAssitants.firstIndex(where: { $0.ID == objAssistants.ID })
        if ( indexObj != nil)
        {
            self.arrayAssitants.remove(at: indexObj!)
        }
        else
        {
            self.arrayAssitants.append(objAssistants)
        }
        if ( self.delegate != nil) {
            
            self.delegate.updateAddingMore(Assistances: self.arrayAssitants, Equipments: self.arrayEquipments, description: self.lblDescription)
            
        }
    }
    
    func addMoreActionEquipment(objEquipments: MeetingRoomEquipmentObject) {
        let indexObj = self.arrayEquipments.firstIndex(where: { $0.ID == objEquipments.ID })
        if ( indexObj != nil)
        {
            self.arrayEquipments.remove(at: indexObj!)
        }
        else
        {
            objEquipments.isChecked = true
            self.arrayEquipments.append(objEquipments)
        }
        if ( self.delegate != nil) {
            
            self.delegate.updateAddingMore(Assistances: self.arrayAssitants, Equipments: self.arrayEquipments, description: self.lblDescription)

        }
    }

    @IBOutlet weak var tblCell: UICollectionView!
    var selectedGetMeetingRoomObject:GetMeetingRoomObject!
    var arrayAssitants:[MeetingRoomObject] = []
    var arrayEquipments:[MeetingRoomEquipmentObject] = []
    weak var delegate:MeetingRoomEquipmentDelegate!
    var lblDescription:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblCell.delegate = self
        tblCell.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblCell.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if ( indexPath.section == 0)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MeetingRoomSupportCell", for: indexPath) as! MeetingRoomSupportCell
            let deviceObject:MeetingRoomEquipmentObject = self.selectedGetMeetingRoomObject.Equipments![indexPath.row]
            if ( self.arrayAssitants.count > 0  )
            {
                let objectMeeting:[MeetingRoomEquipmentObject] = self.arrayEquipments.filter({ $0.ID == deviceObject.ID})
                if ( objectMeeting.count > 0  )
                {
                    cell.btnCheck.isSelected = true
                }
            }
            cell.actionType = ActionMoreType.Equipment
            cell.lbTitle.text = deviceObject.Name!
            cell.objEquipment = deviceObject
            cell.delegate = self
            return cell
        }
        else if ( indexPath.section == 1 )
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MeetingRoomSupportCell", for: indexPath) as! MeetingRoomSupportCell
            let deviceObject:MeetingRoomObject = self.selectedGetMeetingRoomObject.Assistances![indexPath.row]
            if ( self.arrayAssitants.count > 0  )
            {
                let objectMeeting:[MeetingRoomObject] = self.arrayAssitants.filter({ $0.ID == deviceObject.ID})
                if ( objectMeeting.count > 0  )
                {
                    cell.btnCheck.isSelected = true
                }
                else
                {
                    cell.btnCheck.isSelected = false
                }
            }
            
            cell.actionType = ActionMoreType.Assitants
            cell.lbTitle.text = deviceObject.Name!
            cell.objAssistants = deviceObject
            cell.delegate = self
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MeetingCommentCell", for: indexPath) as! MeetingCommentCell
            cell.txtDescription.text = self.lblDescription
            cell.updateContainLayout()
            cell.delegate = self
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.item + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return selectedGetMeetingRoomObject != nil ? (selectedGetMeetingRoomObject.Equipments?.count)! : 0
        case 1:
            return selectedGetMeetingRoomObject != nil ? (selectedGetMeetingRoomObject.Assistances?.count)!:0
            
        default:
            return 1
          
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderSupportView", for: indexPath) as! HeaderSupportView
        if ( indexPath.section == 0 )
        {
            headerView.iconIMG.image = UIImage(named: "icon_devices")
            headerView.lbTitle.text = "Thiết bị"

        }
        else if ( indexPath.section == 1)
        {
            headerView.iconIMG.image = UIImage(named: "icon_other_require")
            headerView.lbTitle.text = "Yêu cầu khác"
        }
        else
        {
            headerView.iconIMG.image = UIImage(named: "icon_write")
            headerView.lbTitle.text = "Ghi chú"
        }
        return headerView
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        if ( indexPath.section <= 1 ) {
            return CGSize(width: collectionViewSize/2, height: 50)
        }
        return CGSize(width: collectionViewSize, height: 50)

    }

}
