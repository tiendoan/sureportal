//
//  MeetingRoomManagementVC.swift
//  TaskManagement
//
//  Created by Mirum User on 7/30/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
enum BookingMeetingStatus: Int
{
    case WaitingApproval = 0
    case Approved = 1
    case Rejected = 2
    case Deleted = 3
}
enum BookingActionVehicelType: Int
{
    case WaitingApproval = 0
    case Approved = 1
    case Rejected = 2
    case Deleted = 3
}

protocol MeetingRoomEquipmentDelegate: class {
    
    func selectedRoomEquipment(equipment: GetMeetingRoomObject)
    func updateRoomComponent(FromDate:String,ToDateMeeting:String,
                             Owner:[UserProfile],ParticipantNumber:Int,Participants:[UserProfile],
                             RequiredParticipants:[UserProfile],RoomID:String,titleMeeting:String,scope: Int,
                             comment: String,departmentID:String)
    func updateAddingMore(Assistances:[MeetingRoomObject],Equipments:[MeetingRoomEquipmentObject],description:String)
    
    func updateActiontype(actionType:[ItemActions])
    func updateHistoryView(history: [HistoryMeeting])
    
}
class MeetingRoomManagementVC: UIViewController,MeetingRoomEquipmentDelegate {
    func updateHistoryView(history: [HistoryMeeting]) {
        self.arrHistory = history
    }
    
    func updateActiontype(actionType: [ItemActions]) {
        if ( actionType.count > 0 ) {
            self.actionType = actionType
            self.actionSend.setTitle(actionType[0].Name, for: .normal)
            if ( actionType.count > 1) {
                self.otherAction.isHidden = false
                self.otherAction.setTitle(actionType[1].Name, for: .normal)
            }
        }
        else
        {
            self.actionSend.isHidden = true
        }

    }
    
    func updateAddingMore(Assistances: [MeetingRoomObject], Equipments: [MeetingRoomEquipmentObject], description: String) {
        self.Assistances = Assistances
        self.Equipments = Equipments
        self.Description = description
    }
    
  
    
    func updateRoomComponent(FromDate: String, ToDateMeeting: String, Owner: [UserProfile], ParticipantNumber: Int, Participants: [UserProfile], RequiredParticipants: [UserProfile], RoomID: String, titleMeeting: String, scope: Int,comment :String,departmentID:String) {
        
        self.FromDate = FromDate
        self.ToDateMeeting = ToDateMeeting
        self.Owner = Owner
        self.ParticipantNumber = ParticipantNumber
        self.Participants = Participants
        self.RequiredParticipants = RequiredParticipants
        self.titleMeeting = titleMeeting
        self.ParticipantNumber = ParticipantNumber
        self.RoomID = RoomID
        self.Scope = scope
        self.comment = comment
        self.DepartmentID = departmentID
        
    }
    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case threeChildTab = 2
    }
    
    func selectedRoomEquipment(equipment: GetMeetingRoomObject) {
        self.selectedGetMeetingRoomObject = equipment
    }
    var Description:String = ""
    var Assistances:[MeetingRoomObject] = []
    var Equipments:[MeetingRoomEquipmentObject] = []
    var FromDate:String!
    var ToDateMeeting:String!
    var Owner:[UserProfile] = []
    var ParticipantNumber:Int!
    var Participants:[UserProfile] = []
    var RequiredParticipants: [UserProfile] = []
    var RoomID:String!
    var Scope:Int = 0
    var titleMeeting:String = ""
    var OrgID:String!
    var actionType:[ItemActions]!
    var comment:String!
    var DepartmentID:String!
    var arrHistory:[HistoryMeeting]!
    @IBOutlet weak var otherAction: UIButton!
    @IBOutlet weak var actionSend: UIButton!
    var selectedGetMeetingRoomObject:GetMeetingRoomObject!
    @IBOutlet weak var headerView: TNGradientView!
    @IBOutlet weak var avatarAuthor: UIImageView!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var authorDescription: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var segmentCT: UISegmentedControl!

    var currentViewController: UIViewController?
    
    lazy var contentChildTabVC: UIViewController? = {
        let contentChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "BookingMeetingInfoVC")
        return contentChildTabVC
    }()
    lazy var progressChildTabVC : UIViewController? = {
        let progressChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "MettingRoomAddMoreInfoVC")
        
        return progressChildTabVC
    }()
    
    lazy var historyChildTabVC : UIViewController? = {
        let historyChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "MeetingRoomScheduleVC")
        
        return historyChildTabVC
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let users: UserProfile = CurrentUser {
            if let avatarString = users.picture, let url = (URL(string: Constants.default.domainAddress + avatarString)) {
                self.avatarAuthor.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            if let fullName: String = users.fullName {
                authorName.text = fullName.uppercased()
            }
            if let jobTitle: String = users.jobTitle {
                authorDescription.text = jobTitle
            }
        }
         displayCurrentTab(TabIndex.firstChildTab.rawValue)
        if ( self.OrgID.isEmpty )
        {
            segmentCT.removeSegment(at: 2, animated: false)
        }
        self.segmentCT.addUnderlineForSelectedSegment()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
         
            vc = contentChildTabVC
            (vc as! BookingMeetingInfoVC).delegate = self
            (vc as! BookingMeetingInfoVC).OrgID = self.OrgID
            break
        case TabIndex.secondChildTab.rawValue :
          
            vc = ( progressChildTabVC)
           (vc as! MettingRoomAddMoreInfoVC).selectedGetMeetingRoomObject = self.selectedGetMeetingRoomObject
            (vc as! MettingRoomAddMoreInfoVC).delegate = self
            (vc as! MettingRoomAddMoreInfoVC).arrayAssitants = self.Assistances
            (vc as! MettingRoomAddMoreInfoVC).arrayEquipments = self.Equipments
            (vc as! MettingRoomAddMoreInfoVC).lblDescription = self.Description

            break
        case TabIndex.threeChildTab.rawValue:
            vc = historyChildTabVC
            (vc as! MeetingRoomScheduleVC).arrHistory = self.arrHistory
            break

        default:
            return nil
        }
        
        return vc
    }
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentCT.changeUnderlinePosition()
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.containerView.bounds
            self.containerView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    @IBAction func otherActionSender(_ sender: Any) {
        self.view.endEditing(true)
        if ( self.actionType[1].Code == "Delete" )
        {
            self.deleteBooking()
        }
        else if ( self.actionType[1].Code == "Reject" )
        {
            self.rejectbooking()
        }
        else if ( self.actionType[1].Code == "Update" )
        {
            self.createBooking(isUpdate: true)
        }
        else if ( self.actionType[0].Code == "AuthorDelete" )
        {
            self.authorDelete()
        }
        else if ( self.actionType[0].Code == "Return" || (self.actionType[0].Code?.contains("return"))! )
        {
            self.authorReturn()
        }
    }
    
    func rejectbooking(){
        if ( self.comment.isEmpty )
        {
            self.view.makeToast("Bạn chưa nhập nội dung từ chối !")
            return
        }
        let updateBooking:DeleteBookingObject = DeleteBookingObject()
        updateBooking.Comment = self.comment
        let bookingRoom:BookingObject = BookingObject()
        bookingRoom.ID = self.OrgID
        updateBooking.Booking = bookingRoom
        
        let jsonObject:[String:Any] = updateBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.rejectBookingRoom(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xoá phòng họp thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)

                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    func authorDelete(){
        if ( self.comment.isEmpty )
        {
            self.view.makeToast("Bạn chưa nhập nội dung xoá !")
            return
        }
        let updateBooking:DeleteBookingObject = DeleteBookingObject()
        updateBooking.Comment = self.comment
        let bookingRoom:BookingObject = BookingObject()
        bookingRoom.ID = self.OrgID
        updateBooking.Booking = bookingRoom
        
        let jsonObject:[String:Any] = updateBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.authorBookingRoom(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xoá phòng họp thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                                
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    func authorReturn(){
        if ( self.comment.isEmpty )
        {
            self.view.makeToast("Bạn chưa nhập nội dung từ chối !")
            return
        }
        let updateBooking:DeleteBookingObject = DeleteBookingObject()
        updateBooking.Comment = self.comment
        let bookingRoom:BookingObject = BookingObject()
        bookingRoom.ID = self.OrgID
        updateBooking.Booking = bookingRoom
        
        let jsonObject:[String:Any] = updateBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.ReturnBookingMeeting(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xoá phòng họp thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                                
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    func deleteBooking(){
        if ( self.comment.isEmpty )
        {
            self.view.makeToast("Bạn chưa nhập nội dung xoá !")
            return
        }
        let updateBooking:DeleteBookingObject = DeleteBookingObject()
        updateBooking.Comment = self.comment
        let bookingRoom:BookingObject = BookingObject()
        bookingRoom.ID = self.OrgID
        updateBooking.Booking = bookingRoom
        
        let jsonObject:[String:Any] = updateBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.deleteBookingRoom(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xoá phòng họp thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)

                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
        
    }
    func updateBooking(){
        
        var ownerApproject: ApproversObject!
        for object in self.Owner {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.jobTitle
            approveOwener.UserName = object.loginName
            ownerApproject = approveOwener
        }
        var participantApproject: [ApproversObject] = []
        for object in self.Participants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.jobTitle
            approveOwener.UserName = object.loginName
            participantApproject.append(approveOwener)
        }
        
        var requireParticipantObject: [ApproversObject] = []
        for object in self.RequiredParticipants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.jobTitle
            approveOwener.UserName = object.loginName
            requireParticipantObject.append(approveOwener)
        }
        
        let objectBooking:CreateBookingObject = CreateBookingObject()
        objectBooking.Assistances = self.Assistances
        objectBooking.Description = self.Description
        objectBooking.Equipments = self.Equipments
        objectBooking.RoomID = self.RoomID
        objectBooking.Owner = ownerApproject
        objectBooking.FromDate = self.FromDate
        objectBooking.ToDate = self.ToDateMeeting
        objectBooking.Scope = self.Scope
        objectBooking.Title = self.titleMeeting
        objectBooking.ID = self.OrgID
        objectBooking.DepartmentID = self.DepartmentID
        objectBooking.Participants = participantApproject
        objectBooking.ParticipantNumber = self.ParticipantNumber
        objectBooking.RequiredParticipants = requireParticipantObject
        do {
            let jsonObject:[String:Any] = objectBooking.toJSON()
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.createBookingRoom(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Tạo phòng họp thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)

                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    func createBooking(isUpdate:Bool){
        
        var ownerApproject: ApproversObject!
        for object in self.Owner {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.jobTitle
            approveOwener.UserName = object.loginName
            ownerApproject = approveOwener
        }
        var participantApproject: [ApproversObject] = []
        for object in self.Participants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.jobTitle
            approveOwener.UserName = object.loginName
            participantApproject.append(approveOwener)
        }
        
        var requireParticipantObject: [ApproversObject] = []
        for object in self.RequiredParticipants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.jobTitle
            approveOwener.UserName = object.loginName
            requireParticipantObject.append(approveOwener)
        }
        
        let objectBooking:CreateBookingObject = CreateBookingObject()
        objectBooking.Assistances = self.Assistances
        objectBooking.Description = self.Description
        objectBooking.Equipments = self.Equipments
        objectBooking.RoomID = self.RoomID
        objectBooking.Owner = ownerApproject
        objectBooking.FromDate = self.FromDate
        objectBooking.ToDate = self.ToDateMeeting
        objectBooking.Scope = self.Scope
        objectBooking.Title = self.titleMeeting
        objectBooking.ID = isUpdate == true ? self.OrgID : "0"
        objectBooking.DepartmentID = self.DepartmentID
        objectBooking.Participants = participantApproject
        objectBooking.ParticipantNumber = self.ParticipantNumber
        objectBooking.RequiredParticipants = requireParticipantObject
        do {
            let jsonObject:[String:Any] = objectBooking.toJSON()
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                if ( isUpdate )
                {
                    SyncProvider.updateBookingRoom(json: jsonObject, done: {
                        (resullt) in
                        SVProgressHUD.dismiss()
                        
                        if ( resullt != nil )
                        {
                            if ( resullt?.status == 1)
                            {
                                self.view.makeToast("Cập nhật phòng họp thành công !")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                    self.dismiss(animated: false, completion: nil)

                                }
                            }
                            else
                            {
                                self.view.makeToast((resullt?.message)!)
                                
                            }
                        }
                    })
                }
                else
                {
                    SyncProvider.createBookingRoom(json: jsonObject, done: {
                        (resullt) in
                        SVProgressHUD.dismiss()
                        
                        if ( resullt != nil )
                        {
                            if ( resullt?.status == 1)
                            {
                                self.view.makeToast("Tạo phòng họp thành công !")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }
                            else
                            {
                                self.view.makeToast((resullt?.message)!)
                                
                            }
                        }
                    })
                }
                
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    func approveBooking(){
        if ( self.comment.isEmpty )
        {
            self.view.makeToast("Bạn chưa nhập nội dung duyệt!")
            return
        }
        var ownerApproject: ApproversObject!
        for object in self.Owner {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            ownerApproject = approveOwener
        }
        var participantApproject: [ApproversObject] = []
        for object in self.Participants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            participantApproject.append(approveOwener)
        }
        
        var requireParticipantObject: [ApproversObject] = []
        for object in self.RequiredParticipants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            requireParticipantObject.append(approveOwener)
        }
        
        let objectBooking:CreateBookingObject = CreateBookingObject()
        objectBooking.Assistances = self.Assistances
        objectBooking.Description = self.Description
        objectBooking.Equipments = self.Equipments
        objectBooking.RoomID = self.RoomID
        objectBooking.Owner = ownerApproject
        objectBooking.FromDate = self.FromDate
        objectBooking.ToDate = self.ToDateMeeting
        objectBooking.Scope = self.Scope
        objectBooking.Title = self.titleMeeting
        objectBooking.ID = self.OrgID
        objectBooking.DepartmentID = self.DepartmentID
        objectBooking.Participants = participantApproject
        objectBooking.ParticipantNumber = self.ParticipantNumber
        objectBooking.RequiredParticipants = requireParticipantObject
        
        let updateBooking:UpdateBookingObject = UpdateBookingObject()
        updateBooking.Comment = self.comment
        updateBooking.Booking = objectBooking
        let jsonObject:[String:Any] = updateBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.approveBookingRoom(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Duyệt phòng họp thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {

                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    func checkRequireBooking(){
        
        let fromDateTime:Date = self.FromDate.date(withFormat: "yyyy-MM-dd'T'HH:mm:ssZ")!
        let toDateTime:Date = self.ToDateMeeting.date(withFormat: "yyyy-MM-dd'T'HH:mm:ssZ")!
        if ( fromDateTime.compare(toDateTime) == .orderedSame || fromDateTime.compare(toDateTime) == .orderedDescending  ) {
            self.view.makeToast("Thời gian kết thúc phải lớn hơn thời gian bắt đầu !")
            return
        }
        if ( self.titleMeeting.isEmpty )
        {
            self.view.makeToast("Bạn chưa nhập tiêu đề !")
            return
        }
        if ( self.Owner.count == 0 )
        {
            self.view.makeToast("Bạn chưa chọn người chủ trì cuộc họp !")
            return
        }
        if ( self.RequiredParticipants.count == 0 )
        {
            self.view.makeToast("Bạn chưa chọn người tham gia!")
            return
        }
    }
    @IBAction func actionCreate(_ sender: Any) {
        self.view.endEditing(true)
        
        if ( self.actionType != nil && self.actionType.count > 0  ) {
            if ( self.actionType[0].Code == "Approve" )
            {
                self.approveBooking()
            }
            else if ( self.actionType[0].Code == "Delete" )
            {
                self.deleteBooking()
            }
            else if ( self.actionType[0].Code == "Reject" )
            {
                self.rejectbooking()
            }
            else if ( self.actionType[0].Code == "AuthorDelete" )
            {
                self.authorDelete()
            }
            else if ( self.actionType[0].Code == "Return" || (self.actionType[0].Code?.contains("return"))! )
            {
                self.authorReturn()
            }
            else if ( self.actionType[0].Code == "Update" )
            {
                self.createBooking(isUpdate: true)
            }
        }
        else
        {
//            Create Booking Object
            self.createBooking(isUpdate: false)
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}

