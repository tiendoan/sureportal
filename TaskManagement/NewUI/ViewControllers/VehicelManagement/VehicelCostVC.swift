//
//  VehicelCostVC.swift
//  TaskManagement
//
//  Created by Mirum User on 9/19/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
protocol VehicelFeeTypeDelegate:class {
    func updateFeeTypeDelegate(feeObject: CreateFeeUpdateObject)
    
}
class VehicelCostVC: UIViewController {
    
    @IBOutlet weak var viewFee: UIView!
    @IBOutlet weak var tfDistance: UITextField!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbDateTo: UILabel!
    @IBOutlet weak var lbDateFrom: UILabel!
    @IBOutlet weak var tbTableView: UITableView!
    
    @IBOutlet weak var btnSend: UIButton!
    var addFeeObject:[CreateFeeUpdateObject] = []
    var rootFeeObject:[CreateFeeUpdateObject] = []
    var extendBooking:CreateBookingVehicelObject!
    var totalValueCost:Int  = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        let stringDateFrom:String = (extendBooking.StartTime?.replacing("Z", with: ""))!
        let stringDateTo:String = (extendBooking.EndTime?.replacing("Z", with: ""))!
        
        let fromDate:Date = (stringDateFrom.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
        let dateMothFrom = fromDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        let toDate:Date = (stringDateTo.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
        let dateMothTo = fromDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        
        let fromHour = fromDate.toFormatHour24NonSecond()
        let toHour = toDate.toFormatHour24NonSecond()
        
        let fromdateHour:String = String(format: "%@ %@ ", fromHour,dateMothFrom)
        let todateHour:String = String(format: "%@ %@ ", toHour,dateMothTo)
        
        if self.extendBooking.KMEnd != 0  {
            tfDistance.text = String(format: "%d", self.extendBooking.KMEnd!)
        }
        
        self.lbDateFrom.text = fromdateHour
        self.lbDateTo.text = todateHour
        
        tbTableView.register(UINib(nibName: "FeeTypeCell", bundle: nil), forCellReuseIdentifier: "FeeTypeCell")
        tbTableView.delegate = self
        tbTableView.dataSource = self
        tbTableView.separatorStyle = .none
        tbTableView.tableFooterView = UIView()
        
        SyncProvider.getBookingCost(ID: self.extendBooking.ID!, done: {
            (resultData) in
            if ( resultData != nil && (resultData?.Data?.count)! > 0)
            {
                self.viewFee.isHidden = true
                self.addFeeObject = (resultData?.Data)!
                self.rootFeeObject = (resultData?.Data)!
                var totalValues:Int  =  0
                for object in self.addFeeObject {
                    totalValues += Int(object.Total!)
                }
                self.totalValueCost = totalValues
                let formater = NumberFormatter()
                formater.groupingSeparator = "."
                formater.numberStyle = .decimal
                let formattedNumber = formater.string(from: totalValues as NSNumber)
                self.lbTotal.text = String(format: "đ %@", formattedNumber!)
                self.tbTableView.reloadData()
                self.btnSend.setTitle("Cập nhật", for: .normal)
            }
        })

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    @IBAction func addCostMore(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Vehicel", bundle: nil)
        let newVC = storyBoard.instantiateViewController(withIdentifier: "AddCostViewController") as! AddCostViewController
        newVC.modalTransitionStyle = .crossDissolve
        newVC.modalPresentationStyle =  .overCurrentContext
        newVC.extendBooking = self.extendBooking
        newVC.delegate = self
        self.navigationController?.present(newVC, animated: false, completion: {})
        
    }
    
    @IBAction func backVC(_ sender: Any) {
        self.navigationController?.popViewController()
    }

    @IBAction func updateCost(_ sender: Any) {
        if self.addFeeObject.count > 0  {
            var addMore:[CreateFeeUpdateObject] = []
            if ( self.rootFeeObject.count > 0 )
            {
                for object in self.addFeeObject
                {
                    let extis = self.rootFeeObject.filter({ $0.ID == object.ID && $0.Total == object.Total })
                    if ( extis.count == 0  )
                    {
                        addMore.append(object)
                    }
                }
            }
            else
            {
                addMore = self.addFeeObject
            }

            var jsonObject:[[String:Any]] = addMore.toJSON()
            do {
                let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
                if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                    print(jsonString)
                    SVProgressHUD.show()
                    let urlString = Router.baseURLString + "/mobileapis/api/Vehicles/UpdateCosts"
                    let parameters = jsonObject
                    let url = URL(string: urlString)!
                    let session = URLSession.shared
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST" //set http method as POST
                    do {
                        request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                    } catch let error {
                        print(error.localizedDescription)
                    }
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue("application/json", forHTTPHeaderField: "Accept")
                    //create dataTask using the session object to send data to the server
                    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                        if let response = response {
                            let nsHTTPResponse = response as! HTTPURLResponse
                            let statusCode = nsHTTPResponse.statusCode
                            print ("status code = \(statusCode)")
                        }
                        guard error == nil else {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        do {
                            //create json object from data
                            SVProgressHUD.dismiss()
                            self.checkoutBooking()

                        } catch let error {
                            print(error.localizedDescription)
                            SVProgressHUD.dismiss()
                        }
                    })
                    task.resume()
                                        
                }
                
            } catch let error as NSError {
                print(error)
            }
            

        }
        
    }
    func checkoutBooking(){
        
        let objectBooking:CreateBookingVehicelObject =  self.extendBooking
        let kmEnd:Int = (tfDistance.text?.isEmpty)! ? 0 : Int(tfDistance.text!)!
        objectBooking.KMEnd = kmEnd
        objectBooking.Cost = totalValueCost
        let jsonObject:[String:Any] = objectBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.checkinBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
}
extension VehicelCostVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addFeeObject.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeeTypeCell", for: indexPath) as! FeeTypeCell
        cell.lbFeeTitle.text = self.addFeeObject[indexPath.row].FeeType?.Name
        cell.lbFeeNote.text = self.addFeeObject[indexPath.row].Note
        let total:String = String(format: "%d", self.addFeeObject[indexPath.row].Total!)
        
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        let formattedNumber = formater.string(from:  Int(total)! as NSNumber)
        cell.lbFeeTotal.text = String(format: "đ %@", formattedNumber!)
        return cell
        
    }
    
    
}
extension VehicelCostVC:VehicelFeeTypeDelegate
{
    func updateFeeTypeDelegate(feeObject: CreateFeeUpdateObject) {
        
        self.addFeeObject.append(feeObject)
        self.tbTableView.reloadData()
        var totalValues:Int  =  0
        for object in self.addFeeObject {
            totalValues += Int(object.Total!)
        }
        totalValueCost = totalValues
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        let formattedNumber = formater.string(from: totalValues as NSNumber)
        self.lbTotal.text = String(format: "đ %@", formattedNumber!)
        viewFee.isHidden = true
        
    }
}
