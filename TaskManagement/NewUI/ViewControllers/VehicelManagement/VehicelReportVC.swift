//
//  VehicelReportVC.swift
//  TaskManagement
//
//  Created by Mirum User on 10/3/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Toast_Swift
import SVProgressHUD
import DropDown

class VehicelReportVC: UIViewController {
    
    var arrayMeetingBookingRoomObject: [DetailVehicelsRegistrationStatistics] = []
    var arrayBookingDic:[String: [DetailVehicelsRegistrationStatistics]] = [String: [DetailVehicelsRegistrationStatistics]]()
    var arrayBookingDicKey:[String] = [String]()

    @IBOutlet weak var tbReportTable: UITableView!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbToDate: UILabel!
    @IBOutlet weak var lbFromDate: UILabel!
    @IBOutlet weak var btnOrg: UIButton!
    let dispatchGroup = DispatchGroup()
    var selectedBookingID:String = "0"
    var selectedVehicelID:String = "0"
    var selectedMajorID:String = "0"
    var selectedStatus:String = "0"
    var startDate:Date!
    var endDate:Date!
    let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
    let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
    
    let taskDropDown: DropDown = DropDown()
    var arrayMeetingRoomObject: [MeetingRoomObject] = [] {
        didSet {
            if !arrayMeetingRoomObject.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayMeetingRoomObject.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    let taskAllVehicleDropDown: DropDown = DropDown()
    @IBOutlet weak var btnAllVehicle: UIButton!
    var arrrLocationVehicels: [GetMeetingRoomObject] = []
    {
        didSet {
            if !arrrLocationVehicels.isEmpty {
                taskAllVehicleDropDown.dataSource.removeAll()
                taskAllVehicleDropDown.dataSource = arrrLocationVehicels.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    let taskAllMajorDropDown: DropDown = DropDown()
    @IBOutlet weak var btnAllMajor: UIButton!
    var arrMajor: [GetMeetingRoomObject] = []
    {
        didSet {
            if !arrMajor.isEmpty {
                taskAllMajorDropDown.dataSource.removeAll()
                taskAllMajorDropDown.dataSource = arrMajor.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    let taskAllStatusDropDown: DropDown = DropDown()
    @IBOutlet weak var btnAllStatus: UIButton!
    var arrStatus: [String] = []
    {
        didSet {
            if !arrStatus.isEmpty {
                taskAllStatusDropDown.dataSource.removeAll()
                taskAllStatusDropDown.dataSource = arrStatus.map {
                    if let uName: String = ($0).trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    
    @IBAction func departmentAction(_ sender: Any) {
        taskDropDown.show()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbReportTable.register(withClass: RegistrationStatisticsCell.self)
        tbReportTable.separatorStyle = .none
        
        let headerNibTitle = UINib.init(nibName: "HeaderBookingVehicle", bundle: Bundle.main)
        tbReportTable.register(headerNibTitle, forHeaderFooterViewReuseIdentifier: "HeaderBookingVehicle")
        tbReportTable.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
        let currentDate: Date = Constants.currentDate
        self.endDate = currentDate
        self.startDate =  Calendar.current.date(byAdding: .month, value: -1, to: Date())
        self.tbReportTable.delegate = self
        self.tbReportTable.dataSource = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(MeetingRoomReportVC.actionStartTimeButton))
        lbFromDate.isUserInteractionEnabled = true
        lbFromDate.addGestureRecognizer(tap)
        
        let tapTo = UITapGestureRecognizer(target: self, action: #selector(MeetingRoomReportVC.actionEndTimeButton))
        lbToDate.isUserInteractionEnabled = true
        lbToDate.addGestureRecognizer(tapTo)
        self.loadStartEndDays(self.startDate, self.endDate)
        
        self.arrayMeetingRoomObject = DataManager.shared.arrrLocationVehicels
        selectedBookingID = self.arrayMeetingRoomObject[0].ID!
        self.btnOrg.setTitle(self.arrayMeetingRoomObject[0].Name!, for: .normal)
        
        taskDropDown.anchorView = self.btnOrg
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnOrg.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedBookingID = self.arrayMeetingRoomObject[index].ID!
            self.btnOrg.setTitle(self.arrayMeetingRoomObject[index].Name!, for: .normal)
            self.getBookingRoom()
        }
        
   
        taskAllVehicleDropDown.anchorView = self.btnAllVehicle
        taskAllVehicleDropDown.textColor = UIColor.gray
        taskAllVehicleDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskAllVehicleDropDown.bottomOffset = CGPoint(x: 0, y: self.btnAllVehicle.bounds.height + 10)
        taskAllVehicleDropDown.isMultipleTouchEnabled = false
        taskAllVehicleDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskAllVehicleDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskAllVehicleDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedVehicelID = self.arrrLocationVehicels[index].ID!
            self.btnAllVehicle.setTitle(self.arrrLocationVehicels[index].Name!, for: .normal)
             self.getBookingRoom()
        }
       
        
        taskAllMajorDropDown.anchorView = self.btnAllMajor
        taskAllMajorDropDown.textColor = UIColor.gray
        taskAllMajorDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskAllMajorDropDown.bottomOffset = CGPoint(x: 0, y: self.btnAllMajor.bounds.height + 10)
        taskAllMajorDropDown.isMultipleTouchEnabled = false
        taskAllMajorDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskAllMajorDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskAllMajorDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedMajorID = self.arrMajor[index].ID!
            self.btnAllMajor.setTitle(self.arrMajor[index].Name!, for: .normal)
            self.getBookingRoom()
        }
        
        arrStatus = ["Tất cả trạng thái","Chờ xử lý","Đã điều phối","Không phê duyệt","Đã hủy"]
        self.btnAllStatus.setTitle(self.arrStatus[0], for: .normal)
        taskAllStatusDropDown.anchorView = self.btnAllStatus
        taskAllStatusDropDown.textColor = UIColor.gray
        taskAllStatusDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskAllStatusDropDown.bottomOffset = CGPoint(x: 0, y: self.btnAllStatus.bounds.height + 10)
        taskAllStatusDropDown.isMultipleTouchEnabled = false
        taskAllStatusDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskAllStatusDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskAllStatusDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedStatus = String(format: "%d", index)
            self.btnAllStatus.setTitle(self.arrStatus[index], for: .normal)
            self.getBookingRoom()
        }
        
        self.fetchDataFromServer()
    }
    func fetchDataFromServer() {
        
        self.getListVehicleData()
        
        self.getCurrentDeparment()
        
        dispatchGroup.notify(queue: DispatchQueue.main) {
            self.getBookingRoom()
        }
        
    }
    func getListVehicleData()
    {
        dispatchGroup.enter()

        SyncProvider.getListVehicelsType(ID: self.selectedBookingID, done: {
            (result) in
            if ( result?.data != nil )
            {
                let allList: GetMeetingRoomObject  = GetMeetingRoomObject()
                allList.Name = "Tất cả loại xe"
                allList.ID = "0"
                self.arrrLocationVehicels.append(allList)
                self.arrrLocationVehicels += (result?.data)!
                if  self.arrrLocationVehicels.count > 0  {
                    self.btnAllVehicle.setTitle(self.arrrLocationVehicels[0].Name!, for: .normal)
                }
            }
            self.dispatchGroup.leave()

       })
    }
    func getCurrentDeparment()
    {
        dispatchGroup.enter()

        SyncProvider.getCurrentDepartmentVehicle(done: {
            (resultData) in
            if ( resultData?.data != nil && (resultData?.data?.count)! > 0 )
            {
               let allList: GetMeetingRoomObject  = GetMeetingRoomObject()
               allList.Name = "Tất cả phòng ban"
               allList.ID = "0"
               self.arrMajor.append(allList)
               self.arrMajor += (resultData?.data)!
                if  self.arrMajor.count > 0  {
                    self.btnAllMajor.setTitle(self.arrMajor[0].Name!, for: .normal)
                }
            }
            self.dispatchGroup.leave()

        })
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionStartTimeButton (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func actionEndTimeButton (_ sender: UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
        lbFromDate.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        lbToDate.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        
    }
    @IBAction func actionAllVehicle(_ sender: Any) {
        taskAllVehicleDropDown.show()
    }
    
    @IBAction func actionAllMajor(_ sender: Any) {
        taskAllMajorDropDown.show()
    }
    
    @IBAction func actionStatus(_ sender: Any) {
        taskAllStatusDropDown.show()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension VehicelReportVC: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
        if let title: String = selector.optionTopPanelTitle {
            
            if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
                
                self.startDate = date
                self.loadStartEndDays(date, toDate)
            } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
                self.endDate = date
                self.loadStartEndDays(fromDate, date)
            }
            self.getBookingRoom()
        }
    }
    func getBookingRoom(){
        
        var fromDate:String = self.startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
        var toDate:String = self.endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
        fromDate = String(format: "%@T00:00:00", fromDate)
        toDate = String(format: "%@T23:50:00", toDate)
        
        self.arrayMeetingBookingRoomObject.removeAll()
        self.arrayBookingDic.removeAll()
        self.arrayBookingDicKey.removeAll()

        SVProgressHUD.show()
        SyncProvider.getVehicleRegistrationStatistics(StartTime: fromDate, EndTime: toDate, LocationId: selectedBookingID, VehicleTypeId: selectedVehicelID, DepartmentId: selectedMajorID, Status: selectedStatus, done: {
            (result) in
            SVProgressHUD.dismiss()
            if ( result?.data != nil && ((result?.data)?.count)! > 0  )
            {
                for objectbooking in ((result?.data)!)
                {
                    objectbooking.dateGroup = Constants.formatDateString(objectbooking.StartTime)?.uppercased()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    let date = dateFormatter.date(from:objectbooking.StartTime!)
                    objectbooking.dateGroupOrder = date

                    self.arrayMeetingBookingRoomObject.append(objectbooking)
                    self.lbTotal.text = String(format: "%d", self.arrayMeetingBookingRoomObject.count)
                }
                self.arrayMeetingBookingRoomObject.sort(by: { $0.dateGroupOrder!.compare($1.dateGroupOrder!) == ComparisonResult.orderedAscending})

                for meetingBookingVehicle in self.arrayMeetingBookingRoomObject
                {
                    let dicKey:String  = meetingBookingVehicle.dateGroup!
                    if !self.arrayBookingDicKey.contains(dicKey)
                    {
                        self.arrayBookingDicKey.append(dicKey)
                    }

                    if var valArr = self.arrayBookingDic[dicKey] {
                        valArr.append(meetingBookingVehicle)
                        self.arrayBookingDic[dicKey] = valArr
                    }
                    else
                    {
                        var arrayTemp:[DetailVehicelsRegistrationStatistics] = []
                        arrayTemp.append(meetingBookingVehicle)
                        self.arrayBookingDic[dicKey] = arrayTemp
                    }
                }
            }
            self.tbReportTable.reloadData()
        })
    }
    
    
}
extension VehicelReportVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayBookingDicKey.count

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ( self.arrayBookingDicKey.count > 0 ) {
            let keyString = self.arrayBookingDicKey[section]
            if self.arrayBookingDic[keyString] != nil
            {
                let dictObject:[DetailVehicelsRegistrationStatistics] = self.arrayBookingDic[keyString]!
                return dictObject.count
            }
            return 0
            
        }

        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:RegistrationStatisticsCell = tableView.dequeueReusableCell(withIdentifier: "RegistrationStatisticsCell", for: indexPath) as! RegistrationStatisticsCell
        let keyString = self.arrayBookingDicKey[indexPath.section]
        let dictObject:[DetailVehicelsRegistrationStatistics] = self.arrayBookingDic[keyString]!

        let object = dictObject[indexPath.row]
        cell.lbTitleBooking.text = object.Title
        cell.lbAuthor.text = object.Author
        cell.lbStartDate.text =  Constants.formatTimeFromDate(object.StartTime!)
        cell.lbEndDate.text =  Constants.formatTimeFromDate(object.EndTime!)
        if ( object.ColorStatus != nil && object.ColorStatus != "" ) {
            cell.lbStatus.backgroundColor = UIColor(hexString: object.ColorStatus!)!
        }
        cell.lbVehicle.text = object.VehicleType
        cell.lbDepature.text = object.Departure
        cell.lbDestination.text = object.Destination
        
        if let stringKM = object.KMEnd
        {
            cell.lbDistance.text = String(format: "%@ km",stringKM)
        }
        else
        {
            cell.lbDistance.text = "0 km"
        }
        
        if let stringKM = object.Cost
        {
            let formater = NumberFormatter()
            formater.groupingSeparator = "."
            formater.numberStyle = .decimal
            let formattedNumber = formater.string(from: Int(stringKM)! as NSNumber )
            cell.lbCoin.text = String(format: "%@ đ",formattedNumber!)
        }
        else
        {
            cell.lbCoin.text = "0 đ"
        }
        if object.Driver != nil && object.Driver != "" {
            cell.lbDriver.text = object.Driver
            cell.lbVehi.text = object.Vehicle
            cell.lbVehi.layer.borderColor = UIColor.gray.cgColor
            cell.lbVehi.layer.cornerRadius = 3
            cell.lbVehi.layer.borderWidth = 1
        }
        else
        {
            cell.heighLable.constant = 0
            cell.imgDriver.isHidden = true
            cell.heightDriver.constant = 0
            cell.lbDriver.isHidden = true
            cell.lbVehi.isHidden = true
        }

        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let keyString = self.arrayBookingDicKey[section]
        let dictObject:String = keyString

        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderBookingVehicle" ) as! HeaderBookingVehicle
        headerView.lbTitle.text = dictObject
        headerView.backgroundColor = UIColor.white
        return headerView

    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
   
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
}
