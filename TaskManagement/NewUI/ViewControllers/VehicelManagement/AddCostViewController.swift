//
//  AddCostViewController.swift
//  TaskManagement
//
//  Created by Mirum User on 9/19/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown

class AddCostViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    var extendBooking:CreateBookingVehicelObject!
    @IBOutlet weak var tfValues: UITextField!
    @IBOutlet weak var tfComment: UITextView!
    @IBOutlet weak var btnMajor: UIButton!
    var selectedFee:FeeTypeObject!
    let taskDropDown: DropDown = DropDown()
    weak var delegate:VehicelFeeTypeDelegate!
    
    var arrayMeetingBookingRoomObject: [FeeTypeObject] = [] {
        didSet {
            if !arrayMeetingBookingRoomObject.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayMeetingBookingRoomObject.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        tfComment.delegate = self
        taskDropDown.anchorView = self.btnMajor
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedFee = self.arrayMeetingBookingRoomObject[index]
            self.btnMajor.setTitle(self.selectedFee.Name!, for: .normal)
        }
    
        SyncProvider.getFeeVehicelType(done: {
            (result ) in
            if ( result != nil )
            {
                self.arrayMeetingBookingRoomObject = (result?.data!)!
                self.selectedFee = self.arrayMeetingBookingRoomObject[0]
                self.btnMajor.setTitle(self.arrayMeetingBookingRoomObject[0].Name, for: .normal)
            }
        })
 
        tfValues.delegate = self
        tfComment.delegate = self
        tfComment.textColor = UIColor.lightGray;
        tfValues.addTarget(self, action: #selector(AddCostViewController.textFieldDidChange(_:)),
                              for: UIControlEvents.editingChanged)

        
        // Do any additional setup after loading the view.
    }
    @IBAction func selectedMajor(_ sender: Any) {
        taskDropDown.show()
    }
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        var stringValues = textField.text!
        if stringValues != ""
        {
            stringValues = stringValues.replacing(".", with: "")
            stringValues = stringValues.replacing(",", with: "")
            let formater = NumberFormatter()
            formater.groupingSeparator = "."
            formater.numberStyle = .decimal
            let formattedNumber = formater.string(from: Int(stringValues)! as NSNumber )
            stringValues = formattedNumber!
        }
        textField.text = String(format: "%@ đ", stringValues)

    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        let stringValues = textView.text!
        if ( stringValues == "Ghi chú" ) {
            textView.text = ""
        }
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Ghi chú"
            textView.textColor = UIColor.lightGray
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
          var stringValues = textField.text!
          stringValues = stringValues.replacing("đ", with: "")
          stringValues = stringValues.trimmingCharacters(in: .whitespaces)
          textField.text = stringValues
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        var stringValues = textField.text!
        if stringValues != ""
        {
            stringValues = stringValues.replacing(".", with: "")
            stringValues = stringValues.replacing(",", with: "")
            let formater = NumberFormatter()
            formater.groupingSeparator = "."
            formater.numberStyle = .decimal
            let formattedNumber = formater.string(from: Int(stringValues)! as NSNumber )
            textField.text = formattedNumber
            
        }
    }
    @IBAction func extendAction(_ sender: Any) {
        if ( tfValues.text?.isEmpty )!
        {
            self.view.makeToast("Bạn chưa nhập giá tiền")
            return
        }
        var valuesTotal:String = tfValues.text!
        valuesTotal = valuesTotal.replacing(".", with: "")
        valuesTotal = valuesTotal.replacing(",", with: "")
        valuesTotal = valuesTotal.replacing("đ", with: "")
        valuesTotal = valuesTotal.trimmingCharacters(in: .whitespaces)
        
         var valuesComment:String = tfComment.text!
         valuesComment = valuesComment.replacing("Ghi chú", with: "")
        let feeObject: CreateFeeUpdateObject = CreateFeeUpdateObject()
        feeObject.BookingID = self.extendBooking.ID
        feeObject.FeeType = self.selectedFee
        feeObject.Note = valuesComment
        feeObject.Total = Int(valuesTotal)
        if ( self.delegate != nil )
        {
            self.delegate.updateFeeTypeDelegate(feeObject: feeObject)
        }
        self.dismiss(animated: false, completion: nil)
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
