//
//  VehicelManagermentVC.swift
//  TaskManagement
//
//  Created by Mirum User on 9/3/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
import DropDown


enum TabIndex : Int {
    case firstChildTab = 0
    case secondChildTab = 1
    case thirstChildTab = 2
}
protocol VehicelDelegate: class {
    func updateVehicelInfo(vehicelType: GetMeetingRoomObject, title: String, fromDate: String, toDate: String, fromPlace:String,
                           toPlace:String, number:Int,CommentVehicel:String,secrectary: [UserProfile],vehicel: GetMeetingRoomObject,driver: GetMeetingRoomObject,isSerectaryObject:Bool,isSelectedVehicel:Bool,isSelectedDriver:Bool,deparment:MeetingRoomObject,location: MeetingRoomObject,kmEnd:Int)
    func updateOptionVehicel(contactUser: [UserProfile],ParticipantsUser: [UserProfile],externalParticipants: String,notes: String )
    func updateOptionVehicelArranger(contactUser: ArrangersObject,ParticipantsUser: [ArrangersObject],externalParticipants: String,notes: String,arrHistory:[HistoryMeeting],ID:String)
   func updateActiontype(actionType:[ItemActions])
    
   func calbackDelegate()
    
}
class VehicelManagermentVC: UIViewController,VehicelDelegate {
    
    func calbackDelegate() {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
        self.dismiss(animated: false, completion: nil)

    }
    
   
    func updateOptionVehicelArranger(contactUser: ArrangersObject, ParticipantsUser: [ArrangersObject], externalParticipants: String, notes: String, arrHistory: [HistoryMeeting],ID:String) {
        
        self.contactInit = contactUser
        self.partiInit = ParticipantsUser
        self.partEx = externalParticipants
        self.notesInit = notes;
        self.arrHistory = arrHistory
        self.VehicelId = ID
        
    }
    
    func updateOptionVehicel(contactUser: [UserProfile], ParticipantsUser: [UserProfile], externalParticipants: String,notes: String
                             ) {
        self.Contact = contactUser
        self.Participants = ParticipantsUser
        self.ExternalParticipants = externalParticipants
        self.Note = notes
    }
    
    func updateVehicelInfo(vehicelType: GetMeetingRoomObject, title: String, fromDate: String, toDate: String, fromPlace: String, toPlace: String, number: Int,CommentVehicel:String,secrectary: [UserProfile],vehicel: GetMeetingRoomObject,driver: GetMeetingRoomObject,isSerectaryObject:Bool,isSelectedVehicel:Bool,isSelectedDriver:Bool,deparment:MeetingRoomObject,location: MeetingRoomObject,kmEnd:Int) {
        
        self.Description = title
        self.TitleVehicel = title
        self.selectedVehicelType = vehicelType
        self.Departure = fromPlace
        self.Destination = toPlace
        self.StartTime = fromDate
        self.EndTime = toDate
        self.ParticipantCount = number
        self.Comment = CommentVehicel
        self.selectedVehicel = vehicel
        self.selectedDrive = driver
        self.Secretary = secrectary
        self.isSerectary = isSerectaryObject
        self.Department = deparment
        self.Location = location
        self.KMEnd = kmEnd
    }
    
    func updateActiontype(actionType: [ItemActions]) {
        if ( actionType.count > 0 ) {
            self.actionType = actionType.filter({ $0.Code != "Display" })
            if ( actionType.count >= 1) {
                for index in 0..<self.actionType.count {
                    switch index
                    {
                        case 0:
                            var titleName:String = actionType[0].Name!
                            let titleCode:String = actionType[0].Code!
                            if ( titleCode == "CheckOut")
                            {
                                titleName = "Xuất phát"
                            }
                            else if (titleCode == "CheckIn")
                            {
                                titleName = "Kết thúc"
                            }
                            else if (titleCode == "Edit")
                            {
                                titleName = "Cập nhật"
                            }
                            else if (titleCode == "ExternalCheckOut")
                            {
                                titleName = "Đặt Grab"
                            }
                            else if (titleCode == "ExternalCheckIn")
                            {
                                titleName = "Kết thúc"
                            }
                            
                          
                            self.btnActionSend.setTitle(titleName, for: .normal)
                            self.btnActionSend.tag = 0
                            self.btnActionSend.isHidden = false
                        case 1:
                            var titleName:String = actionType[1].Name!
                            let titleCode:String = actionType[1].Code!
                            if ( titleCode == "CheckOut")
                            {
                                titleName = "Xuất phát"
                            }
                            else if (titleCode == "CheckIn")
                            {
                                titleName = "Kết thúc"
                            }
                            else if (titleCode == "Edit")
                            {
                                titleName = "Cập nhật"
                            }
                            else if (titleCode == "ExternalCheckOut")
                            {
                                titleName = "Đặt Grab"
                            }
                            else if (titleCode == "ExternalCheckIn")
                            {
                                titleName = "Kết thúc"
                            }
                            
                            self.otherAction.setTitle(titleName, for: .normal)
                            self.otherAction.tag = 1
                            self.otherAction.isHidden = false
                        case 2:
                            var titleName:String = actionType[2].Name!
                            let titleCode:String = actionType[2].Code!
                            if ( titleCode == "CheckOut")
                            {
                                titleName = "Xuất phát"
                            }
                            else if (titleCode == "CheckIn")
                            {
                                titleName = "Kết thúc"
                            }
                            else if (titleCode == "Edit")
                            {
                                titleName = "Cập nhật"
                            }
                            else if (titleCode == "ExternalCheckOut")
                            {
                                titleName = "Đặt Grab"
                            }
                            else if (titleCode == "ExternalCheckIn")
                            {
                                titleName = "Kết thúc"
                            }
                            self.otherActionSecond.setTitle(titleName, for: .normal)
                            self.otherActionSecond.tag = 2
                            self.otherActionSecond.isHidden = false

                        break
                        default:
                        break
                    }
                    
                }
            }
            else
            {
                self.btnActionSend.isHidden = true
            }
        }
        else
        {
            self.btnActionSend.isHidden = true
        }
        
    }
    
    
    let taskDropDown: DropDown = DropDown()
    var departmentUser: [GetMeetingRoomObject] = [] {
        didSet {
            if !departmentUser.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = departmentUser.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var supperDeparment: UIView!
    @IBOutlet weak var btnDepartment: UIButton!
    
    var selectedVehicel:GetMeetingRoomObject!
    var selectedDrive:GetMeetingRoomObject!
    var isSerectary:Bool!
    var selectedVehicelType:GetMeetingRoomObject!
    var Description:String!
    var StartTime:String!
    var EndTime:String!
    var Destination:String!
    var Departure:String!
    var ParticipantCount:Int!
    var Contact: [UserProfile] = [UserProfile]()
    var Participants: [UserProfile] = [UserProfile]()
    var Secretary: [UserProfile] = [UserProfile]()
    
    var ExternalParticipants:String!
    var Note:String!
    var Comment:String!
    var KMEnd:Int = 0
    var Cost:Int = 0
    var TitleVehicel:String!
    var Location: MeetingRoomObject?
    var Department: MeetingRoomObject?
    var OrgID:String!
    var actionType:[ItemActions]!
    var contactInit:ArrangersObject!
    var partiInit: [ArrangersObject]!
    var partEx:String!
    var notesInit:String!
    var arrHistory:[HistoryMeeting]!
    var VehicelId:String = "0"
    var isSelectedDriver:Bool!
    var isSelectedVehicel:Bool!
    
    @IBOutlet weak var otherActionSecond: UIButton!
    @IBOutlet weak var otherAction: UIButton!
    @IBOutlet weak var btnActionSend: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lbAuthorDescription: UILabel!
    @IBOutlet weak var lbAuthorName: UILabel!
    @IBOutlet weak var imgAuthor: UIImageView!
    @IBOutlet weak var segmentCT: UISegmentedControl!
    var arrayMeetingBookingRoomObject: [GetMeetingRoomObject]!
    
    var currentViewController: UIViewController?
    
    lazy var contentChildTabVC: UIViewController? = {
        let contentChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "VehicalInfoVC")
        return contentChildTabVC
    }()
    lazy var progressChildTabVC : UIViewController? = {
        let progressChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "VehicelOptionVC")
        
        return progressChildTabVC
    }()
    
    lazy var progressThreeTabVC : UIViewController? = {
        let progressThreeTabVC = self.storyboard?.instantiateViewController(withIdentifier: "VehicelHistoryVC")
        
        return progressThreeTabVC
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

  
        if let users: UserProfile = CurrentUser {
            if let avatarString = users.picture, let url = (URL(string: Constants.default.domainAddress + avatarString)) {
                self.imgAuthor.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            if let fullName: String = users.fullName {
                lbAuthorName.text = fullName.uppercased()
            }
            if let jobTitle: String = users.jobTitle {
                lbAuthorDescription.text = jobTitle
            }
        }
        displayCurrentTab(TabIndex.firstChildTab.rawValue)

        if ( self.OrgID.isEmpty )
        {
            segmentCT.removeSegment(at: 2, animated: false)
        }
        self.segmentCT.addUnderlineForSelectedSegment()

        
        taskDropDown.anchorView = self.btnDepartment
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnDepartment.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.btnDepartment.setTitle("\(self.departmentUser[index].Name!)   ▼", for: .normal)
            let deparmentObjUser = MeetingRoomObject()
            deparmentObjUser.ID = self.departmentUser[index].ID!
            deparmentObjUser.Name = self.departmentUser[index].Name!
            self.Department = deparmentObjUser
        }
        if self.departmentUser.count <= 1 {
            supperDeparment.isHidden = true
        }
        else
        {
            self.btnDepartment.setTitle("\(self.departmentUser[0].Name!)   ▼", for: .normal)
            let deparmentObjUser = MeetingRoomObject()
            deparmentObjUser.ID = self.departmentUser[0].ID!
            deparmentObjUser.Name = self.departmentUser[0].Name!
            self.Department = deparmentObjUser
        }
        
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        self.adjustSegmentLayout(index: sender.selectedSegmentIndex)
    }
    func adjustSegmentLayout(index: NSInteger)
    {
        DispatchQueue.main.async {
            self.segmentCT.changeUnderlinePosition()
            self.currentViewController!.view.removeFromSuperview()
            self.currentViewController!.removeFromParentViewController()
            self.displayCurrentTab(index)
        }
    }
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = contentChildTabVC
           (vc as! VehicalInfoVC).arrayMeetingBookingRoomObject = arrayMeetingBookingRoomObject
           (vc as! VehicalInfoVC).delegate = self
           (vc as! VehicalInfoVC).OrgID = self.OrgID
            (vc as! VehicalInfoVC).LocationObject = self.Location
            (vc as! VehicalInfoVC).Department = self.Department!
            break
        case TabIndex.secondChildTab.rawValue :
            vc = ( progressChildTabVC)
            (vc as! VehicelOptionVC).delegate = self
            
            (vc as! VehicelOptionVC).contactInit = self.contactInit
            (vc as! VehicelOptionVC).partiInit = self.partiInit
            (vc as! VehicelOptionVC).partEx = self.partEx
            (vc as! VehicelOptionVC).notesInit = self.notesInit
            
            break
            
        case TabIndex.thirstChildTab.rawValue:
            vc = ( progressThreeTabVC)
            (vc as! VehicelHistoryVC).arrHistory = self.arrHistory
            break
        default:
            return nil
        }
        
        return vc
    }
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentCT.changeUnderlinePosition()
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.containerView.bounds
            self.containerView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func departmentOption(_ sender: Any) {
        self.taskDropDown.show()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func sendVehicelAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if self.OrgID.isEmpty {
            
            let fromDateTime:Date = self.StartTime.date(withFormat: "yyyy-MM-dd'T'HH:mm:ssZ")!
            let toDateTime:Date = self.EndTime.date(withFormat: "yyyy-MM-dd'T'HH:mm:ssZ")!
            if ( fromDateTime.compare(toDateTime) == .orderedSame || fromDateTime.compare(toDateTime) == .orderedDescending  ) {
                self.view.makeToast("Thời gian kết thúc phải lớn hơn thời gian bắt đầu !")
                return
            }
            
            if ( self.TitleVehicel.isEmpty )
            {
                self.view.makeToast("Bạn chưa nhập tiêu đề ")
                return
            }
                
            else if ( self.Destination.isEmpty )
            {
                self.view.makeToast("Bạn chưa nơi đến")
                return
            }
                
            else if ( self.Departure.isEmpty )
            {
                self.view.makeToast("Bạn chưa nhập nơi đi ")
                return
            }
                
            var jsonObject:[String:Any] = [:]
            if ( self.isSerectary )
            {
                let objectBooking:CreateBookingSecre = self.createNewBookingSecrectVehicel()
                jsonObject = objectBooking.toJSON()
            }
            else
            {
                let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
                jsonObject = objectBooking.toJSON()
//                if ( self.isSelectedDriver == false && self.isSelectedVehicel == false  )
//                {
//
//                }
//                if ( self.isSelectedVehicel == true && self.isSelectedDriver == false)
//                {
//                    let objectBooking:CreateBookingVehicel =  self.createNewBookingVehicelNum()
//                    jsonObject = objectBooking.toJSON()
//
//                }
//                else
//                {
//                    let objectBooking:CreateBookingDriver =  self.createNewBookingDriver()
//                    jsonObject = objectBooking.toJSON()
//                }
            }
            
            
            do {
                let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
                if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                    print(jsonString)
                    SVProgressHUD.show()
                    SyncProvider.createBookingVehicel(json: jsonObject, done: {
                        (resullt) in
                        SVProgressHUD.dismiss()
                        
                        if ( resullt != nil )
                        {
                            if ( resullt?.status == 1)
                            {
                                self.view.makeToast("Xử lý thành công !")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                    self.dismiss(animated: false, completion: nil)
                                }
                            }
                            else
                            {
                                self.view.makeToast((resullt?.message)!)
                                
                            }
                        }
                    })
                    
                }
                
            } catch let error as NSError {
                print(error)
            }
            
        }
        else
        {
            let tag:Int = (sender as! UIButton).tag
            let actionType:String = self.actionType[tag].Code!

            if ( actionType == "Reject" )
            {
               if ( self.Comment.isEmpty )
                {
                    self.view.makeToast("Bạn chưa nhập ý kiến")
                    self.showCommentForcus()
                    return
                }
                self.rejectBooking()
            }
            else if ( actionType == "Delete")
            {
                
                if ( self.Comment.isEmpty )
                {
                    self.view.makeToast("Bạn chưa nhập ý kiến")
                    self.showCommentForcus()
                    return
                }
                self.deleteBooking()
            }
            else if ( actionType == "Extend")
            {
                let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
                let storyBoard: UIStoryboard = UIStoryboard(name: "Vehicel", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "ExtendVC") as! ExtendVC
                newViewController.modalTransitionStyle = .crossDissolve
                newViewController.modalPresentationStyle =  .overCurrentContext
                newViewController.extendBooking = objectBooking
                newViewController.delegate = self
                self.navigationController?.present(newViewController, animated: false, completion: {})
               
            }
            else if ( actionType == "ExternalCheckOut")
            {
                
//                if ( self.Comment.isEmpty )
//                {
//                    self.view.makeToast("Bạn chưa nhập ý kiến")
//                    self.showCommentForcus()
//                    return
//                }
                self.externalBooking()
            }
            else if ( actionType == "ExternalCheckIn")
            {
                
//                if ( self.Comment.isEmpty )
//                {
//                    self.view.makeToast("Bạn chưa nhập ý kiến")
//                    self.showCommentForcus()
//                    return
//                }
                self.externalCheckinBooking()
            }

                
            else if ( actionType == "Arranger")
            {
                
                if ( self.Comment.isEmpty )
                {
                    self.view.makeToast("Bạn chưa nhập ý kiến")
                    self.showCommentForcus()
                    return
                }
                self.arrangerBooking()
            }
            else if ( actionType == "CheckOut")
            {
                self.checkoutBooking()
            }
            else if ( actionType == "CheckIn" || actionType == "Cost")
            {
                
                if let users: UserProfile = CurrentUser {
                    if ( users.loginName?.contains("taixe") )!
                    {
                        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Vehicel", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "VehicelCostVC") as! VehicelCostVC
                        newViewController.extendBooking = objectBooking
                        self.navigationController?.pushViewController(newViewController)
                    }
                    else
                    {
                        self.checkInBooking()
                    }

                }
                else
                {
                    self.checkInBooking()
                }
            }
            else if ( actionType == "Cost")
            {
                let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
                let storyBoard: UIStoryboard = UIStoryboard(name: "Vehicel", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "VehicelCostVC") as! VehicelCostVC
                newViewController.extendBooking = objectBooking
                self.navigationController?.pushViewController(newViewController)
            }
            else if ( actionType == "Edit")
            {
                
                if ( self.Comment.isEmpty )
                {
                    self.view.makeToast("Bạn chưa nhập ý kiến")
                      self.showCommentForcus()
                    return
                }
                self.updateBooking()
            }
            else if ( actionType == "Approve")
            {
                
                if ( self.Comment.isEmpty )
                {
                    self.view.makeToast("Bạn chưa nhập ý kiến")
                      self.showCommentForcus()
                    return
                }
                
                var jsonObject:[String:Any] = [:]
                if ( self.isSerectary )
                {
                    let objectBooking:CreateBookingSecre = self.createNewBookingSecrectVehicel()
                    jsonObject = objectBooking.toJSON()
                }
                else
                {
                    
                    if ( self.isSelectedDriver == false && self.isSelectedVehicel == false  )
                    {
                        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
                        jsonObject = objectBooking.toJSON()
                    }
                    if ( self.isSelectedVehicel == true && self.isSelectedDriver == false)
                    {
                        let objectBooking:CreateBookingVehicel =  self.createNewBookingVehicelNum()
                        jsonObject = objectBooking.toJSON()
                        
                    }
                    else
                    {
                        let objectBooking:CreateBookingDriver =  self.createNewBookingDriver()
                        jsonObject = objectBooking.toJSON()
                    }
                }
                
                
                do {
                    let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
                    if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                        print(jsonString)
                        SVProgressHUD.show()
                        SyncProvider.approveBookingVehicel(json: jsonObject, done: {
                            (resullt) in
                            SVProgressHUD.dismiss()
                            
                            if ( resullt != nil )
                            {
                                if ( resullt?.status == 1)
                                {
                                    self.view.makeToast("Xử lý thành công !")
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                        
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                        self.dismiss(animated: false, completion: nil)
                                    }
                                }
                                else
                                {
                                    self.view.makeToast((resullt?.message)!)
                                    
                                }
                            }
                        })
                        
                    }
                    
                } catch let error as NSError {
                    print(error)
                }
            }
            
            
        }
        
        
    }
    func rejectBooking(){
        
        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
        
        let jsonObject:[String:Any] = objectBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.rejectBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func deleteBooking(){
        
        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
        
        let jsonObject:[String:Any] = objectBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.deleteBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func externalBooking(){
        
        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
        
        let jsonObject:[String:Any] = objectBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.externalBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func externalCheckinBooking(){
        
        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
        
        let jsonObject:[String:Any] = objectBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.externalCheckinBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
  
    func updateBooking(){
        
        var jsonObject:[String:Any] = [:]
        if ( self.isSerectary )
        {
            let objectBooking:CreateBookingSecre = self.createNewBookingSecrectVehicel()
            jsonObject = objectBooking.toJSON()
        }
        else
        {
            
            if ( self.isSelectedDriver == false && self.isSelectedVehicel == false  )
            {
                let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
                jsonObject = objectBooking.toJSON()
            }
            if ( self.isSelectedVehicel == true && self.isSelectedDriver == false)
            {
                let objectBooking:CreateBookingVehicel =  self.createNewBookingVehicelNum()
                jsonObject = objectBooking.toJSON()

            }
            else
            {
                let objectBooking:CreateBookingDriver =  self.createNewBookingDriver()
                jsonObject = objectBooking.toJSON()
            }
        }
        
        
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.updateBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func arrangerBooking(){
        
        
        var jsonObject:[String:Any] = [:]
        if ( self.isSerectary )
        {
            let objectBooking:CreateBookingSecre = self.createNewBookingSecrectVehicel()
            jsonObject = objectBooking.toJSON()
        }
        else
        {

            if ( self.isSelectedDriver == false && self.isSelectedVehicel == false  )
            {
                let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
                jsonObject = objectBooking.toJSON()
            }
            if ( self.isSelectedVehicel == true && self.isSelectedDriver == false)
            {
                let objectBooking:CreateBookingVehicel =  self.createNewBookingVehicelNum()
                jsonObject = objectBooking.toJSON()

            }
            else
            {
                let objectBooking:CreateBookingDriver =  self.createNewBookingDriver()
                jsonObject = objectBooking.toJSON()
            }
        }
        

        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.arrangerBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func checkoutBooking(){
        
        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
        
        let jsonObject:[String:Any] = objectBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.checkoutBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func checkInBooking(){
        
        let objectBooking:CreateBookingVehicelObject =  self.createNewBookingVehicel()
        
        let jsonObject:[String:Any] = objectBooking.toJSON()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.checkinBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: nil)
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func createNewBookingVehicel() -> CreateBookingVehicelObject{
        
        var ownerApproject: ApproversObject!
        for object in self.Contact {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            ownerApproject = approveOwener
        }
        var participantApproject: [ApproversObject] = []
        for object in self.Participants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            participantApproject.append(approveOwener)
        }
        
        let vehicalObject:MeetingRoomObject = MeetingRoomObject()
        vehicalObject.ID = self.selectedVehicelType.ID
        vehicalObject.Name = self.selectedVehicelType.Name
        
        let vehicalObjectNum:MeetingRoomObject = MeetingRoomObject()
        if ( self.selectedVehicel != nil ) {
            vehicalObjectNum.ID = self.selectedVehicel.ID
            vehicalObjectNum.Name = self.selectedVehicel.Name
        }
        
        let driverObject:MeetingRoomObject = MeetingRoomObject()
        if ( self.selectedVehicel != nil ) {
            driverObject.ID = self.selectedDrive.ID
            driverObject.Name = self.selectedDrive.Name
        }
        
        var secrecApproject: ApproversObject!
        for object in self.Secretary {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            secrecApproject = approveOwener
        }
        
        
        let objectBooking:CreateBookingVehicelObject = CreateBookingVehicelObject()
        objectBooking.ID = self.VehicelId
        objectBooking.Location = self.Location
        objectBooking.Department = self.Department
        objectBooking.Description = self.Description
        objectBooking.StartTime = self.StartTime
        objectBooking.EndTime = self.EndTime
        objectBooking.VehicleType = vehicalObject
        objectBooking.Destination = self.Destination
        objectBooking.Departure = self.Departure
        objectBooking.Contact = ownerApproject
        objectBooking.Participants = participantApproject

        objectBooking.ParticipantCount = self.ParticipantCount
        objectBooking.Note = self.Note
        objectBooking.KMEnd = self.KMEnd
        objectBooking.Cost = self.Cost
        objectBooking.Title = self.TitleVehicel
        objectBooking.ExternalParticipants = self.ExternalParticipants
        objectBooking.Comment = self.Comment
        return objectBooking
    }
    
    func createNewBookingVehicelNum() -> CreateBookingVehicel{
        
        var ownerApproject: ApproversObject!
        for object in self.Contact {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            ownerApproject = approveOwener
        }
        var participantApproject: [ApproversObject] = []
        for object in self.Participants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            participantApproject.append(approveOwener)
        }
        
        let vehicalObject:MeetingRoomObject = MeetingRoomObject()
        vehicalObject.ID = self.selectedVehicelType.ID
        vehicalObject.Name = self.selectedVehicelType.Name
        
        let vehicalObjectNum:MeetingRoomObject = MeetingRoomObject()
        if ( self.selectedVehicel != nil ) {
            vehicalObjectNum.ID = self.selectedVehicel.ID
            vehicalObjectNum.Name = self.selectedVehicel.Name
        }
        
        let driverObject:MeetingRoomObject = MeetingRoomObject()
        if ( self.selectedVehicel != nil ) {
            driverObject.ID = self.selectedDrive.ID
            driverObject.Name = self.selectedDrive.Name
        }
        
        var secrecApproject: ApproversObject!
        for object in self.Secretary {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            secrecApproject = approveOwener
        }
        
        
        let objectBooking:CreateBookingVehicel = CreateBookingVehicel()
        objectBooking.ID = self.VehicelId
        objectBooking.Location = self.Location
        objectBooking.Department = self.Department
        objectBooking.Description = self.Description
        objectBooking.StartTime = self.StartTime
        objectBooking.EndTime = self.EndTime
        objectBooking.VehicleType = vehicalObject
        objectBooking.Destination = self.Destination
        objectBooking.Departure = self.Departure
        objectBooking.Contact = ownerApproject
        objectBooking.Participants = participantApproject
        objectBooking.Vehicle = vehicalObjectNum
        objectBooking.ParticipantCount = self.ParticipantCount
        objectBooking.Note = self.Note
        objectBooking.KMEnd = self.KMEnd
        objectBooking.Cost = self.Cost
        objectBooking.Title = self.TitleVehicel
        objectBooking.ExternalParticipants = self.ExternalParticipants
        objectBooking.Comment = self.Comment
        return objectBooking
    }
    
    
    func createNewBookingDriver() -> CreateBookingDriver{
        
        var ownerApproject: ApproversObject!
        for object in self.Contact {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            ownerApproject = approveOwener
        }
        var participantApproject: [ApproversObject] = []
        for object in self.Participants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            participantApproject.append(approveOwener)
        }
        
        let vehicalObject:MeetingRoomObject = MeetingRoomObject()
        vehicalObject.ID = self.selectedVehicelType.ID
        vehicalObject.Name = self.selectedVehicelType.Name
        
        let vehicalObjectNum:MeetingRoomObject = MeetingRoomObject()
        if ( self.selectedVehicel != nil ) {
            vehicalObjectNum.ID = self.selectedVehicel.ID
            vehicalObjectNum.Name = self.selectedVehicel.Name
        }
        
        let driverObject:ApproversObject = ApproversObject()
        if ( self.selectedVehicel != nil ) {
            driverObject.Name = self.selectedDrive.User?.Name
            driverObject.UserName = self.selectedDrive.User?.UserName
        }
        
       
        let objectBooking:CreateBookingDriver = CreateBookingDriver()
        objectBooking.ID = self.VehicelId
        objectBooking.Location = self.Location
        objectBooking.Department = self.Department
        objectBooking.Description = self.Description
        objectBooking.StartTime = self.StartTime
        objectBooking.EndTime = self.EndTime
        objectBooking.VehicleType = vehicalObject
        objectBooking.Destination = self.Destination
        objectBooking.Departure = self.Departure
        objectBooking.Contact = ownerApproject
        objectBooking.Participants = participantApproject
        objectBooking.Vehicle = vehicalObjectNum
        objectBooking.Driver = driverObject
        objectBooking.ParticipantCount = self.ParticipantCount
        objectBooking.Note = self.Note
        objectBooking.KMEnd = self.KMEnd
        objectBooking.Cost = self.Cost
        objectBooking.Title = self.TitleVehicel
        objectBooking.ExternalParticipants = self.ExternalParticipants
        objectBooking.Comment = self.Comment
        return objectBooking
    }
    
    
    func createNewBookingSecrectVehicel() -> CreateBookingSecre{
        
        var ownerApproject: ApproversObject!
        for object in self.Contact {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            ownerApproject = approveOwener
        }
        var participantApproject: [ApproversObject] = []
        for object in self.Participants {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            participantApproject.append(approveOwener)
        }
        
        let vehicalObject:MeetingRoomObject = MeetingRoomObject()
        vehicalObject.ID = self.selectedVehicelType.ID
        vehicalObject.Name = self.selectedVehicelType.Name
        
        let vehicalObjectNum:MeetingRoomObject = MeetingRoomObject()
        if ( self.selectedVehicel != nil ) {
            vehicalObjectNum.ID = self.selectedVehicel.ID
            vehicalObjectNum.Name = self.selectedVehicel.Name
        }
        
        let driverObject:MeetingRoomObject = MeetingRoomObject()
        if ( self.selectedVehicel != nil ) {
            driverObject.ID = self.selectedDrive.ID
            driverObject.Name = self.selectedDrive.Name
        }
        
        var secrecApproject: ApproversObject!
        for object in self.Secretary {
            let approveOwener:ApproversObject = ApproversObject()
            approveOwener.Name = object.fullName
            approveOwener.UserName = object.loginName
            secrecApproject = approveOwener
        }
        
        
        let objectBooking:CreateBookingSecre = CreateBookingSecre()
        objectBooking.ID = self.VehicelId
        objectBooking.Location = self.Location
        objectBooking.Department = self.Department
        objectBooking.Description = self.Description
        objectBooking.StartTime = self.StartTime
        objectBooking.EndTime = self.EndTime
        objectBooking.VehicleType = vehicalObject
        objectBooking.Secretary = secrecApproject
        objectBooking.Destination = self.Destination
        objectBooking.Departure = self.Departure
        objectBooking.Contact = ownerApproject
        objectBooking.Participants = participantApproject
        
        objectBooking.ParticipantCount = self.ParticipantCount
        objectBooking.Note = self.Note
        objectBooking.KMEnd = self.KMEnd
        objectBooking.Cost = self.Cost
        objectBooking.Title = self.TitleVehicel
        objectBooking.ExternalParticipants = self.ExternalParticipants
        objectBooking.Comment = self.Comment
        return objectBooking
    }
    func showCommentForcus(){
        DispatchQueue.main.async {
            self.segmentCT.selectedSegmentIndex = 0
            self.adjustSegmentLayout(index: 0)
            DispatchQueue.main.async {
                (self.contentChildTabVC as! VehicalInfoVC).tfComment.becomeFirstResponder()
            }
        }
    }
}
