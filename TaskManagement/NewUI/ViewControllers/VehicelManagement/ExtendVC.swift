//
//  ExtendVC.swift
//  TaskManagement
//
//  Created by Mirum User on 9/19/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
class ExtendVC: UIViewController {
    @IBOutlet weak var lbFromDate: UILabel!
    @IBOutlet weak var lbDistance: UILabel!
    @IBOutlet weak var lbToDate: UILabel!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var lbHourExtend: UILabel!
    @IBOutlet weak var lbDayExtend: UILabel!
    var selectedFromDateHour:Date!
    var startDate:Date!
    var startFromDate:Date!
    var startToDate:Date!
    var extendDateShowing:Date!
    var extendDateTime:String!
    var extendBooking:CreateBookingVehicelObject!
    weak var delegate:VehicelDelegate!
    
    @IBOutlet weak var tfComment: UITextView!
    let timePicker = UIDatePicker()
    let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
    let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
    let START_HOUR_OPTION_TITLE: String = "Chọn giờ bắt đầu"

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        
        let stringDateFrom:String = (extendBooking.StartTime?.replacing("Z", with: ""))!
        let stringDateTo:String = (extendBooking.EndTime?.replacing("Z", with: ""))!
        
        let fromDate:Date = (stringDateFrom.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
        let dateMothFrom = fromDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        let toDate:Date = (stringDateTo.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
        let dateMothTo = fromDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_REPORT)
        
        let fromHour = fromDate.toFormatHour24NonSecond()
        let toHour = toDate.toFormatHour24NonSecond()
        
        let fromdateHour:String = String(format: "%@ %@ ", fromHour,dateMothFrom)
        let todateHour:String = String(format: "%@ %@ ", toHour,dateMothTo)
        
        self.startFromDate = fromDate
        self.startToDate = toDate
        
        self.startDate = toDate
        self.selectedFromDateHour = toDate
        lbFromDate.text = fromdateHour
        lbToDate.text = todateHour
        
        
        self.startDate = startToDate
        self.selectedFromDateHour = startToDate
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(VehicalInfoVC.actionStartTimeButton))
        lbDayExtend.isUserInteractionEnabled = true
        lbDayExtend.addGestureRecognizer(tap)
        
        let tapSelctedTime = UITapGestureRecognizer(target: self, action: #selector(VehicalInfoVC.selectedTimeFrom))
        lbHourExtend.isUserInteractionEnabled = true
        lbHourExtend.addGestureRecognizer(tapSelctedTime)
        
        self.viewDate.isHidden = true
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0.0, y: 35, width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        self.viewDate.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(ExtendVC.startTimeDiveChanged), for: UIControlEvents.valueChanged)
        
        self.lbDayExtend.text = self.startDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        self.lbHourExtend.text = self.selectedFromDateHour.toFormatHour24NonSecond()
        self.updateUI()
        
        // Do any additional setup after loading the view.
    }
    func updateUI() {
        
        let fromDateString = self.startDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
        let fromHour = self.selectedFromDateHour.toFormatHour24()
        let fromdateHour:String = String(format: "%@T%@", fromDateString!,fromHour)
        self.extendDateTime = String(format: "%@T%@Z", fromDateString!,fromHour)
        self.extendDateShowing = fromdateHour.dateFromFormat(Constants.COMMON_DATE_TIME_FORMAT)
        self.lbDistance.text = self.extendDateShowing?.offsetFrom(date: self.startToDate as! NSDate)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismissExtendVC(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func startTimeDiveChanged(sender: UIDatePicker) {
        
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_HOUR_OPTION_TITLE
        selector.optionSelectionType = .single
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        self.present(selector, animated: true, completion: nil)

    }
    @IBAction func selectedTimeFrom (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_HOUR_OPTION_TITLE
        selector.optionSelectionType = .single
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        self.present(selector, animated: true, completion: nil)
    }
   
    @IBAction func actionStartTimeButton (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func actionEndTimeButton (_ sender: UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    fileprivate func loadStartEndDays (_ startDate: Date) {
        
        lbDayExtend.text = self.startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        lbHourExtend.text = self.selectedFromDateHour.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
        self.updateUI()
        
    }
    
    @IBAction func extendAction(_ sender: Any) {
        
      
        if ( self.extendDateShowing.compare(self.startToDate) == .orderedSame || self.extendDateShowing.compare(self.startToDate) == .orderedAscending  ) {
            self.view.makeToast("Thời gian không hợp lệ")
            return
        }
        
        if ( self.tfComment.text.isEmpty )
        {
            self.view.makeToast("Bạn chưa nhập ý kiến")
            return
        }
        self.extendBooking.Comment = tfComment.text        
        self.extendBooking.EndTime = extendDateTime
        
        var jsonObject:[String:Any] = [:]
        do {
            jsonObject = self.extendBooking.toJSON()
            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                print(jsonString)
                SVProgressHUD.show()
                SyncProvider.extendBookingVehicel(json: jsonObject, done: {
                    (resullt) in
                    SVProgressHUD.dismiss()
                    
                    if ( resullt != nil )
                    {
                        if ( resullt?.status == 1)
                        {
                            self.view.makeToast("Xử lý thành công !")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                               
                              
                                self.dismiss(animated: false, completion: {
                                    self.delegate.calbackDelegate()
                                })
                            }
                        }
                        else
                        {
                            self.view.makeToast((resullt?.message)!)
                            
                        }
                    }
                })
                
            }
            
        } catch let error as NSError {
            print(error)
        }
        
        
    }
    @IBAction func confirmTime(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        lbHourExtend.text = self.selectedFromDateHour.toFormatHour24NonSecond()
        self.viewDate.isHidden = true
        self.updateUI()
    }
}
extension ExtendVC: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
        if let title: String = selector.optionTopPanelTitle {
            if title == START_DATE_OPTION_TITLE{ // Start day option.
                self.startDate = date
                self.loadStartEndDays(date)
            }
            else if title == START_HOUR_OPTION_TITLE
            {
               if ( title == START_HOUR_OPTION_TITLE )
                {
                    self.selectedFromDateHour = date
                    lbHourExtend.text = self.selectedFromDateHour.toFormatHour24NonSecond()
                    self.updateUI()
                }
               
            }
        }
    }
    
}
