//
//  VehicelHistoryVC.swift
//  TaskManagement
//
//  Created by Mirum User on 9/5/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class VehicelHistoryVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tbView: UITableView!
    var arrHistory:[HistoryMeeting]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbView.dataSource = self
        self.tbView.delegate = self
        let nib = UINib.init(nibName: "HistoryProcedureCell", bundle: nil)
        self.tbView.register(nib, forCellReuseIdentifier: "HistoryProcedureCell")
        self.tbView.separatorStyle = .none
        self.tbView.estimatedRowHeight = 88.0
        self.tbView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistory != nil ? arrHistory.count : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryProcedureCell", for: indexPath) as! HistoryProcedureCell
        
        let taskWork = self.arrHistory[indexPath.row]
        cell.lblName.text = taskWork.userOwner?.Name
        cell.lblComment.text = taskWork.Comment
        let createDate:String = Constants.formatDateFromString(taskWork.Occurred)!
        cell.lblDate.text = createDate
        cell.lblAction.text = taskWork.Action?.Name
        let backgrounColor:String = taskWork.Action?.Color != nil ? (taskWork.Action?.Color)! : "#c4c4c4"
        cell.lblAction.backgroundColor = UIColor(hexString: backgrounColor)
        cell.lblAction.layer.cornerRadius = 5
        cell.lblAction.borderWidth = 0
        return cell
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
