//
//  VehicalInfoVC.swift
//  TaskManagement
//
//  Created by Mirum User on 9/3/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown
import KSTokenView

class VehicalInfoVC: UIViewController,UITextFieldDelegate {
    let timePicker = UIDatePicker()
   
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var tfEndPlace: UITextField!
    @IBOutlet weak var tfStartPlace: UITextField!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var btnMajor: UIButton!
    var isSelectedDriver:Bool = false
    var isSelectedVehicel:Bool = false
    @IBOutlet weak var tfComment: UITextField!
    @IBOutlet weak var toHourLabel: UILabel!
    @IBOutlet weak var fromHourLabel: UILabel!
    @IBOutlet weak var toDayLabel: UILabel!
    @IBOutlet weak var fromDayLabel: UILabel!
    let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
    let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
    let START_HOUR_OPTION_TITLE: String = "Chọn giờ bắt đầu"
    let END_HOUR_OPTION_TITLE: String = "Chọn giờ kết thúc"
    
    let ASSIGNEES_TEXT_VIEW_PLACE_HOLDER: String = "người xử lý"
    let ASSIGNERS_TOKEN_TAG: Int = 2
    let ASSIGNERS_TOKEN_TAG_REQUIRE: Int = 3
    
    
    @IBOutlet weak var lbDistance: UILabel!
    
    var startDate: Date?
    var endDate: Date?
    var selectedFromDateHour:Date!
    var selectedToDateHour:Date!
    var startDateShowing: Date?
    var endDateShowing: Date?
    
    var iselectHourFrom:Bool = false
    var selectedVehicel:GetMeetingRoomObject!
    var selectedVehicelNum:GetMeetingRoomObject = GetMeetingRoomObject()
    var selectedDriver:GetMeetingRoomObject! = GetMeetingRoomObject()
    var bookingObjectDetail: DetailVehicelsBookingObject!
    
    weak var delegate:VehicelDelegate!
    @IBOutlet weak var tbScroll: UIScrollView!
    @IBOutlet weak var heightVehicel: NSLayoutConstraint!    
    @IBOutlet weak var optionSecrecture: KSTokenView!
    @IBOutlet weak var btnVehicel: UIButton!
    @IBOutlet weak var heightDriver: NSLayoutConstraint!
    @IBOutlet weak var btnDriver: UIButton!
    @IBOutlet weak var heightSecre: NSLayoutConstraint!
    @IBOutlet weak var supperVehicel: UIView!
    @IBOutlet weak var supperDriver: UIView!
    @IBOutlet weak var taskSecre: UIStackView!
    var employees: [Assignee] = Dummies.shared.getAllEmployees()
    var authorAcc: [UserProfile] = [UserProfile]()
    var requireAcc: [UserProfile] = [UserProfile]()
    var arrayRootList:[UserProfile]!
    var LocationObject: MeetingRoomObject?
    var kmEnd:Int!
    var Department: MeetingRoomObject  = MeetingRoomObject()
    var actionType:[ItemActions] = []
    var addTokenFromSearchInput = true
    var OrgID:String!
    var isSelectaryObject:Bool = false
    
    let taskDropDown: DropDown = DropDown()
    var arrayMeetingBookingRoomObject: [GetMeetingRoomObject] = [] {
        didSet {
            if !arrayMeetingBookingRoomObject.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = arrayMeetingBookingRoomObject.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    let taskVehicalDropDown: DropDown = DropDown()
    var arrayVehicel: [GetMeetingRoomObject] = [] {
        didSet {
            if !arrayVehicel.isEmpty {
                taskVehicalDropDown.dataSource.removeAll()
                taskVehicalDropDown.dataSource = arrayVehicel.map {
                    if let uName: String = ($0.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    
    let taskDriverDropDown: DropDown = DropDown()
    var arrayDriver: [GetMeetingRoomObject] = [] {
        didSet {
            if !arrayDriver.isEmpty {
                taskDriverDropDown.dataSource.removeAll()
                taskDriverDropDown.dataSource = arrayDriver.map {
                    if let uName: String = ($0.User?.Name)!.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return ""
                    }
                }
            }
        }
    }
    
    func updateUI() {

        self.lbDistance.text = self.endDateShowing?.offsetFrom(date: self.startDateShowing as! NSDate)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.arrayRootList = DataManager.shared.allUsers
        let currentDate: Date = Constants.currentDate
        self.startDate = currentDate
        self.endDate = currentDate
        self.selectedFromDateHour = currentDate
        self.selectedToDateHour = currentDate
        self.startDateShowing = currentDate
        self.endDateShowing = currentDate
        self.kmEnd = 0
        
        tfNumber.delegate = self
        tfEndPlace.delegate = self
        tfStartPlace.delegate = self
        tfTitle.delegate = self
        tfComment.delegate = self
        
        taskDropDown.anchorView = self.btnMajor
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedVehicel = self.arrayMeetingBookingRoomObject[index]
            self.btnMajor.setTitle(self.selectedVehicel.Name!, for: .normal)
            self.updateUISelection()
            self.updateCallbackData()
        }
        
        taskVehicalDropDown.anchorView = self.btnVehicel
        taskVehicalDropDown.textColor = UIColor.gray
        taskVehicalDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskVehicalDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskVehicalDropDown.isMultipleTouchEnabled = false
        taskVehicalDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskVehicalDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskVehicalDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedVehicelNum = self.arrayVehicel[index]
            self.btnVehicel.setTitle(self.selectedVehicelNum.Name!, for: .normal)
            self.initDriverAction()
            self.updateCallbackData()
        }
        
        
        
        taskDriverDropDown.anchorView = self.btnDriver
        taskDriverDropDown.textColor = UIColor.gray
        taskDriverDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDriverDropDown.bottomOffset = CGPoint(x: 0, y: self.btnMajor.bounds.height + 10)
        taskDriverDropDown.isMultipleTouchEnabled = false
        taskDriverDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDriverDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        taskDriverDropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedDriver = self.arrayDriver[index]
            self.btnDriver.setTitle(self.selectedDriver.User?.Name!, for: .normal)
            self.updateCallbackData()
        }
        
        if ( arrayMeetingBookingRoomObject != nil  && arrayMeetingBookingRoomObject.count > 0 )
        {
            self.btnMajor.setTitle(self.arrayMeetingBookingRoomObject[0].Name, for: .normal)
            self.selectedVehicel = self.arrayMeetingBookingRoomObject[0]
            self.updateCallbackData()
            self.updateUISelection()
        }
        
        // Do any additional setup after loading the view.

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(VehicalInfoVC.actionStartTimeButton))
        fromDayLabel.isUserInteractionEnabled = true
        fromDayLabel.addGestureRecognizer(tap)
        
        let tapTo = UITapGestureRecognizer(target: self, action: #selector(VehicalInfoVC.actionEndTimeButton))
        toDayLabel.isUserInteractionEnabled = true
        toDayLabel.addGestureRecognizer(tapTo)
        
        let tapSelctedTime = UITapGestureRecognizer(target: self, action: #selector(VehicalInfoVC.selectedTimeFrom))
        fromHourLabel.isUserInteractionEnabled = true
        fromHourLabel.addGestureRecognizer(tapSelctedTime)
        
        let tapSelectedTimeTo = UITapGestureRecognizer(target: self, action: #selector(VehicalInfoVC.selectedTimeTo))
        toHourLabel.isUserInteractionEnabled = true
        toHourLabel.addGestureRecognizer(tapSelectedTimeTo)
        
   
        self.fromDayLabel.text = self.startDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        self.toDayLabel.text = self.endDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        
        self.fromHourLabel.text = self.selectedFromDateHour.toFormatHour24NonSecond()
        self.toHourLabel.text = self.selectedToDateHour.toFormatHour24NonSecond()
        
        self.initUIOption()
        self.loadDataObject()
        
        // Do any additional setup after loading the view.
    }
    func initUIOption(){
        self.updateUI()
        if ( self.OrgID.isEmpty ) {
            
            self.heightVehicel.constant = 0
            self.heightSecre.constant = 5
            self.heightDriver.constant = 0
            self.supperDriver.isHidden = true
            self.supperVehicel.isHidden = true
            self.taskSecre.isHidden = true
        }
        
        optionSecrecture.addSubviews(optionSecrecture.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        optionSecrecture.promptText = ""
        optionSecrecture.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        optionSecrecture.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        optionSecrecture.delegate = self
        optionSecrecture.searchResultHeight = 120
        optionSecrecture.tag =  ASSIGNERS_TOKEN_TAG
        optionSecrecture.returnKeyType(type: .done)
        optionSecrecture.shouldDisplayAlreadyTokenized = true
        optionSecrecture.maxTokenLimit = -1
        optionSecrecture.shouldSortResultsAlphabatically = false
        optionSecrecture.removesTokensOnEndEditing = false
        optionSecrecture.tokenizingCharacters = []
        
    }
    func updateUISelection(){
        
        if ( (self.selectedVehicel.Name?.contains("grab"))! || (self.selectedVehicel.Name?.contains("Grab"))! )
        {
            self.taskSecre.isHidden = false
            self.heightSecre.constant = 100
            self.isSelectaryObject = true
        }
        else
        {
            self.taskSecre.isHidden = true
            self.heightSecre.constant = 5
            self.isSelectaryObject = false
        }
        self.initVehicelAction()
        
        
    }
    func initDriverAction()
    {
        if (self.selectedVehicel.Capacity != 0 ) {
            SyncProvider.getDriversByVehicleType(ID: self.selectedVehicel.ID!, done: {
                (result ) in
                if ( result != nil && (result?.data?.count)! > 0 )
                {

                    self.heightDriver.constant = 45
                    self.supperDriver.isHidden = false
                    self.arrayDriver = (result?.data!)!
                    let objectMeeting:GetMeetingRoomObject = (result?.data![0])!
                    self.btnDriver.setTitle(objectMeeting.User?.Name, for: .normal)
                    self.selectedDriver = self.arrayDriver[0]
                    
                    if ( self.bookingObjectDetail.Driver != nil )
                    {
                        let driverObject:VehiceTypeObject = self.bookingObjectDetail.Driver!
                        let objectSelected:[GetMeetingRoomObject] = self.arrayDriver.filter({ $0.User?.Name == driverObject.Name})
                        if ( objectSelected.count > 0  )
                        {
                            let objectMeeting:GetMeetingRoomObject = objectSelected[0]
                            self.btnDriver.setTitle(objectMeeting.User?.Name, for: .normal)
                            self.selectedDriver = objectSelected[0]
                            
                        }
                    }
                    else
                    {
                        let objectMeeting:GetMeetingRoomObject = (result?.data![0])!
                        self.btnDriver.setTitle(objectMeeting.User?.Name, for: .normal)
                        self.selectedDriver = self.arrayDriver[0]
                        
                    }
                    
                    self.isSelectedVehicel = true
                    self.isSelectedDriver = true

                    self.updateCallbackData()
                    
                }
            })
        }
        else
        {
            self.heightDriver.constant = 0
            self.supperDriver.isHidden = true
            self.isSelectedVehicel = true
            self.isSelectedDriver = false
            self.updateCallbackData()

        }
       
    }
    func initVehicelAction()
    {
        if ( self.OrgID.isEmpty || self.Department.ID == nil )
        {
            return
        }
        SyncProvider.getVehiclesByType(DeparmentID: (self.Department.ID!), ID: self.selectedVehicel.ID!, done: {
            (result ) in
            if ( result != nil && (result?.data?.count)! > 0 )
            {
                
                let filterArranger = self.actionType.filter({ $0.Code == "Arranger" })
                if ( filterArranger != nil && filterArranger.count > 0 )
                {
                    self.isSelectedVehicel = true
                    self.isSelectedDriver = false
                    self.heightVehicel.constant = 45
                    self.supperVehicel.isHidden = false
                    self.arrayVehicel = (result?.data!)!
                    
                    if ( self.bookingObjectDetail.Vehicle != nil )
                    {
                        let vehicelObject:VehiceTypeObject = self.bookingObjectDetail.Vehicle!
                        let objectSelected:[GetMeetingRoomObject] = self.arrayVehicel.filter({ $0.Name == vehicelObject.Name})
                        if ( objectSelected.count > 0  )
                        {
                            let objectMeeting:GetMeetingRoomObject = objectSelected[0]
                            self.btnVehicel.setTitle(objectMeeting.Name, for: .normal)
                            self.selectedVehicelNum = objectSelected[0]
                            
                        }
                    }
                    else
                    {
                        self.btnVehicel.setTitle(self.arrayVehicel[0].Name, for: .normal)
                        self.selectedVehicelNum = self.arrayVehicel[0]
                        
                    }
                    
                    
                    self.initDriverAction()
                    self.updateCallbackData()
                }
                else
                {
                    self.heightDriver.constant = 0
                    self.supperDriver.isHidden = true
                    
                    self.heightVehicel.constant = 0
                    self.supperVehicel.isHidden = true
                    
                    self.isSelectedVehicel = false
                    self.isSelectedDriver = false
                    self.updateCallbackData()
                }
            }
            else
            {
                self.heightDriver.constant = 0
                self.supperDriver.isHidden = true
                
                self.heightVehicel.constant = 0
                self.supperVehicel.isHidden = true
                
                self.isSelectedVehicel = false
                self.isSelectedDriver = false
                self.updateCallbackData()

            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "userSegue" {
            if let destinationVC = segue.destination as? CustomListUserVC {
//                destinationVC.arrayRootList = self.arrayRootList  
                destinationVC.delegate = self
            }
        }
    }
    @IBAction func selectedUser(_ sender: Any) {
        self.performSegue(withIdentifier: "userSegue", sender: self)
    }
    
    func loadDataObject(){
        
        if ( !self.OrgID.isEmpty) {
            SyncProvider.getDetailBookingVehicel(OrgID: self.OrgID, done: {
                ( result ) in
              
                if ( result != nil && result?.data != nil )
                {
                    self.bookingObjectDetail = (result?.data)!
                    self.tfNumber.text = String(format: "%d", self.bookingObjectDetail.ParticipantCount!)
                    self.tfTitle.text = self.bookingObjectDetail.Title
                    let fromDate:Date = (self.bookingObjectDetail.FromDate?.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
                    self.fromDayLabel.text = fromDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
                    let toDate:Date = (self.bookingObjectDetail.ToDate?.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss"))!
                    self.toDayLabel.text = toDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
                    self.kmEnd = self.bookingObjectDetail.KMEnd!
                    self.startDate = fromDate
                    self.endDate = toDate
                    self.selectedToDateHour = toDate
                    self.selectedFromDateHour = fromDate

                    self.fromHourLabel.text = fromDate.toFormatHour24NonSecond()
                    self.toHourLabel.text = toDate.toFormatHour24NonSecond()
                    self.tfEndPlace.text = self.bookingObjectDetail.Destination
                    self.tfStartPlace.text = self.bookingObjectDetail.Departure
                    self.Department = self.bookingObjectDetail.Department!
                    self.LocationObject = self.bookingObjectDetail.Location
                    
                    
                    if ( self.bookingObjectDetail.Secretary != nil)
                    {
                        
                        let ownerAssignee:[UserProfile] = self.arrayRootList.filter({ $0.fullName == self.bookingObjectDetail.Secretary!.Name || $0.loginName == self.bookingObjectDetail.Secretary!.UserName })
                        if ( ownerAssignee != nil && ownerAssignee.count > 0 )
                        {
                            self.authorAcc.append(ownerAssignee[0])
                            self.optionSecrecture.addTokenWithTitle(ownerAssignee[0].fullName!)
                            self.optionSecrecture.layoutIfNeeded()
                            self.optionSecrecture.layoutSubviews()
                            
                        }
                    }
                    
                    
                    let objectSelected:[GetMeetingRoomObject] = self.arrayMeetingBookingRoomObject.filter({ $0.ID == self.bookingObjectDetail.VehicleType?.ID })
                    if ( objectSelected.count > 0  )
                    {
                        self.btnMajor.setTitle(objectSelected[0].Name!, for: .normal)
                        self.selectedVehicel = objectSelected[0]
                        self.updateUISelection()
                    }

                    self.updateCallbackData()
                    if ( self.delegate != nil )
                    {
                        let contact:ArrangersObject =  self.bookingObjectDetail.Contact != nil ?  self.bookingObjectDetail.Contact! : ArrangersObject()
                        let participant:[ArrangersObject] = self.bookingObjectDetail.Participants != nil ? self.bookingObjectDetail.Participants! : []
                        let arrHist:[HistoryMeeting] = self.bookingObjectDetail.Histories != nil ? self.bookingObjectDetail.Histories! : []
                        let notes = self.bookingObjectDetail.notes != nil ? self.bookingObjectDetail.notes! : ""
                        let expart = self.bookingObjectDetail.ExternalParticipants != nil ? self.bookingObjectDetail.ExternalParticipants! : ""
                        
                        self.delegate.updateOptionVehicelArranger(contactUser: contact, ParticipantsUser: participant, externalParticipants: expart, notes: notes, arrHistory: arrHist, ID: self.bookingObjectDetail.ID!)

                        self.actionType = self.bookingObjectDetail.Action!.filter({ $0.Code != "Display" })
                        if ( self.actionType.count > 0 )
                        {
                            self.tfComment.isHidden = false
                            var filterArranger = self.bookingObjectDetail.Action?.filter({ $0.Code == "Arranger" })
                            if ( filterArranger == nil || filterArranger?.count == 0 )
                            {
                                self.heightDriver.constant = 0
                                self.supperDriver.isHidden = true
                                
                                self.heightVehicel.constant = 0
                                self.supperVehicel.isHidden = true
                                
                                self.isSelectedVehicel = false
                                self.isSelectedDriver = false
                                self.btnMajor.isUserInteractionEnabled = false
                                self.btnDriver.isUserInteractionEnabled = false
                                self.btnVehicel.isUserInteractionEnabled = false
                                self.fromDayLabel.isUserInteractionEnabled = false
                                self.toDayLabel.isUserInteractionEnabled = false
                                self.fromHourLabel.isUserInteractionEnabled = false
                                self.toHourLabel.isUserInteractionEnabled = false
                                self.tfTitle.isUserInteractionEnabled = false
                                self.tfStartPlace.isUserInteractionEnabled = false
                                self.tfEndPlace.isUserInteractionEnabled = false
                                self.tfNumber.isUserInteractionEnabled = false


                                
                            }
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = false

                            let filterArranger = self.bookingObjectDetail.Action?.filter({ $0.Code == "ExternalCheckOut" || $0.Code == "ExternalCheckIn" })
                            if ( (filterArranger?.count)! > 0  )
                            {
                                
                            }
                        }
                        
                        self.delegate.updateActiontype(actionType: self.actionType)
                      
                    }
                }
                
                    
            })
        }
    }
    
    func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        if ( iselectHourFrom )
        {
            self.selectedFromDateHour = sender.date
        }
        else
        {
            self.selectedToDateHour =  sender.date
        }
        self.updateCallbackData()
    }
    @IBAction func selectedTimeFrom (_ sender:UITapGestureRecognizer) {

        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_HOUR_OPTION_TITLE
        selector.optionSelectionType = .single
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
         selector.optionStyles.showTime(true)
        self.present(selector, animated: true, completion: nil)
    }
    @IBAction func selectedTimeTo (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_HOUR_OPTION_TITLE
        selector.optionSelectionType = .single
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        self.present(selector, animated: true, completion: nil)
    }
    @IBAction func actionStartTimeButton (_ sender:UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    
    @IBAction func actionEndTimeButton (_ sender: UITapGestureRecognizer) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
        selector.optionSelectionType = .single
        self.present(selector, animated: true, completion: nil)
    }
    fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
        toDayLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
        toHourLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
        self.updateCallbackData()
    }
    
    @IBAction func selectedMajor(_ sender: Any) {
        
        taskDropDown.show()
    }
    @IBAction func selectedVehicelAction(_ sender: Any) {
        taskVehicalDropDown.show()
    }
    @IBAction func selectedDriverAction(_ sender: Any) {
        taskDriverDropDown.show()
    }
    @IBAction func confirmTime(_ sender: Any) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateCallbackData()
    }
    
    @IBAction func cancelTime(_ sender: Any) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func updateCallbackData(){
        if (( self.delegate ) != nil) {
            
            let fromDateString = self.startDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
            let toDateString = self.endDate?.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT_SERVER)
            let fromHour = self.selectedFromDateHour.toFormatHour24()
            let toHour = self.selectedToDateHour.toFormatHour24()
            let fromdateHour:String = String(format: "%@T%@Z", fromDateString!,fromHour)
            let todateHour:String = String(format: "%@T%@Z", toDateString!,toHour)
            
            self.startDateShowing = fromdateHour.dateFromFormat(Constants.COMMON_DATE_TIME_FORMAT_ZTIME)
            self.endDateShowing = todateHour.dateFromFormat(Constants.COMMON_DATE_TIME_FORMAT_ZTIME)
            self.updateUI()
            
            
            let numberPerson:Int = tfNumber.text != "" ? Int(tfNumber.text!)! : 0
            
            self.delegate.updateVehicelInfo(vehicelType: self.selectedVehicel, title: self.tfTitle.text!, fromDate: fromdateHour, toDate: todateHour, fromPlace: self.tfStartPlace.text!, toPlace: self.tfEndPlace.text!, number: numberPerson,CommentVehicel: self.tfComment.text!,secrectary: self.authorAcc, vehicel: self.selectedVehicelNum,driver: self.selectedDriver, isSerectaryObject: self.isSelectaryObject, isSelectedVehicel: self.isSelectedVehicel,isSelectedDriver: self.isSelectedDriver,deparment: self.Department, location: self.LocationObject!,kmEnd: self.kmEnd)
        }
    }
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [UserProfile]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? UserProfile, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension VehicalInfoVC: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
        if let title: String = selector.optionTopPanelTitle {
            if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
                self.startDate = date
                self.loadStartEndDays(date, toDate)
            } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
                self.endDate = date
                self.loadStartEndDays(fromDate, date)
            }
            else if title == START_HOUR_OPTION_TITLE || title == END_HOUR_OPTION_TITLE
            {
                let formatter = DateFormatter()
                formatter.timeStyle = .short
                if ( title == START_HOUR_OPTION_TITLE )
                {
                    self.selectedFromDateHour = date
                    fromHourLabel.text = self.selectedFromDateHour.toFormatHour24NonSecond()
                }
                else
                {
                    self.selectedToDateHour = date
                    toHourLabel.text = self.selectedToDateHour.toFormatHour24NonSecond()
                }
                self.updateCallbackData()
            }
        }
    }
    
}
extension VehicalInfoVC: KSTokenViewDelegate {
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            optionSecrecture.layoutIfNeeded()
            optionSecrecture.layoutSubviews()
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
        {
            optionSecrecture.layoutIfNeeded()
            optionSecrecture.layoutSubviews()
            
        }
        
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            return completion!([])
        }
        var data: Array<AnyObject> = []
        var valueResults = [UserProfile]()
        var textResults = [UserProfile]()
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE  {
            let locale = Locale(identifier: "vi_VN")
            for item: UserProfile in self.arrayRootList {
                
                if let text = item.fullName, let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    valueResults.append(item)
                }else if let text = item.fullName?.folding(options: .diacriticInsensitive, locale: locale), let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    textResults.append(item)
                }
                data = valueResults.sorted{ ($0.fullName ?? "") < ($1.fullName ?? "") }
                data.append(contentsOf: textResults)
                //data = results
            }
        }
        return completion!(data)
    }
    
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE {
            if let assignee = object as? UserProfile,
                let name: String = assignee.fullName {
                return name
            } else {
                return "Chưa xác định"
            }
        }
        return object as! String
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE  {
            if let tokenObject: UserProfile = token.object as? UserProfile {
                token.tokenBackgroundColor = tokenObject.getBackgroundColorToTokenView()
            } else {
                token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
            }
        }
        return token
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? UserProfile {
            
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                if let index = self.authorAcc.index(of: user) {
                    self.authorAcc.remove(at: index)
                }
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                if let index = self.requireAcc.index(of: user) {
                    self.requireAcc.remove(at: index)
                }
            }
            self.updateCallbackData()
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE , let user = token.object as? UserProfile, addTokenFromSearchInput {
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                self.removeAssignersTokenView(optionSecrecture, [user])
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                self.removeAssignersTokenView(optionSecrecture, [user])
            }
            
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                self.authorAcc.append(user)
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                self.requireAcc.append(user)
            }
            
            self.updateCallbackData()
        }
        self.view.endEditing(true)
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            optionSecrecture.layoutIfNeeded()
            optionSecrecture.layoutSubviews()
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE {
            optionSecrecture.layoutIfNeeded()
            optionSecrecture.layoutSubviews()
        }
        
        
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE , let assignee = object as? UserProfile  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            return cell!
        }
        return UITableViewCell()
    }
    
}
extension VehicalInfoVC:CustomUserDelegate
{
    func replaceByCustomUserOjbect(userObject: UserProfile, isMore: Bool) {
        let ownerAssignee:[UserProfile] = [userObject]
        if ( ownerAssignee != nil && ownerAssignee.count > 0 )
        {
            self.authorAcc.removeAll()
            self.optionSecrecture.deleteAllTokens()
            self.authorAcc.append(ownerAssignee[0])
            self.optionSecrecture.addTokenWithTitle(ownerAssignee[0].fullName!)
            self.optionSecrecture.layoutIfNeeded()
            self.optionSecrecture.layoutSubviews()
            self.updateCallbackData()
            
        }
    }
    
   
}
