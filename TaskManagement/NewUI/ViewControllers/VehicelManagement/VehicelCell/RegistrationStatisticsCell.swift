//
//  RegistrationStatisticsCell.swift
//  TaskManagement
//
//  Created by Mirum User on 10/10/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class RegistrationStatisticsCell: UITableViewCell {

    @IBOutlet weak var lbVehicle: UILabel!
    @IBOutlet weak var lbStatus: UIView!
    @IBOutlet weak var lbEndDate: UILabel!
    @IBOutlet weak var lbStartDate: UILabel!
    @IBOutlet weak var lbAuthor: UILabel!
    @IBOutlet weak var lbTitleBooking: UILabel!
    @IBOutlet weak var lbDepature: UILabel!
    @IBOutlet weak var lbDestination: UILabel!
    @IBOutlet weak var lbDistance: UILabel!
    @IBOutlet weak var lbCoin: UILabel!
    
    @IBOutlet weak var heighLable: NSLayoutConstraint!
    @IBOutlet weak var lbVehi: UILabel!
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var lbDriver: UILabel!
    @IBOutlet weak var heightDriver: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
