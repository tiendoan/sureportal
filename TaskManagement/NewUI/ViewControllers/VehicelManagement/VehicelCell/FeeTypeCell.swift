//
//  FeeTypeCell.swift
//  TaskManagement
//
//  Created by Mirum User on 9/20/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class FeeTypeCell: UITableViewCell {
    @IBOutlet weak var lbFeeTitle: UILabel!
    @IBOutlet weak var lbFeeNote: UILabel!
    @IBOutlet weak var lbFeeTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
