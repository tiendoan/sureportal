//
//  CustomUserCell.swift
//  TaskManagement
//
//  Created by Mirum User on 9/18/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class CustomUserCell: UITableViewCell {

    @IBOutlet weak var lbUserJob: UILabel!
    @IBOutlet weak var lbUserTittle: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
