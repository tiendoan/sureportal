//
//  HeaderUserCell.swift
//  TaskManagement
//
//  Created by Mirum User on 9/19/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class HeaderUserCell: UITableViewCell {

    @IBOutlet weak var btnLabel: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
