//
//  CustomListUserVC.swift
//  TaskManagement
//
//  Created by Mirum User on 9/18/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

protocol CustomUserDelegate:class {
    func replaceByCustomUserOjbect(userObject: UserProfile,isMore:Bool)
    
}
class CustomListUserVC: UIViewController {
    @IBOutlet weak var searchField: UITextField!
    var arrayList:[Assignee] = []
    var arrayItemList:[Assignee] = []
    var arrayRootList:[Assignee] = []
    var arrayRootListUser:[UserProfile]!
    @IBOutlet weak var tblView: UITableView!
    var optionSelectUser:Bool = false
    weak var delegate:CustomUserDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.arrayRootListUser = DataManager.shared.allUsers
        
        tblView.register(UINib(nibName: "CustomUserCell", bundle: nil), forCellReuseIdentifier: "CustomUserCell")
        tblView.register(UINib(nibName: "HeaderUserCell", bundle: nil), forCellReuseIdentifier: "HeaderUserCell")
        searchField.addTarget(self, action: #selector(CustomListUserVC.textFieldDidChange(_:)),
                              for: UIControlEvents.editingChanged)
        searchField.delegate = self
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.tableFooterView = UIView()
        SyncProvider.getAllUsersInAllDepartments(done: { (resultData) in
            
            if ( resultData != nil && (resultData?.count)! > 0 )
            {
               var arrRemain:[Assignee] = []
                let array:[Assignee] = resultData!
                for  objectData in array[0].childrens as! [Assignee]
                {
                    if ( objectData.childrens != nil && (objectData.childrens?.count)! > 0  )
                    {
                        for  itemData in objectData.childrens!
                        {
                            if ( itemData.childrens != nil && (itemData.childrens?.count)! > 0  )
                            {
                                objectData.childrens?.removeAll(itemData)
                                arrRemain.append(itemData)
                            }
                        }
                        
                        for objectRemain in arrRemain
                        {
                            let filterObject = self.arrayRootList.filter({ $0.value == objectRemain.value})
                             if filterObject.count == 0
                             {
                                 self.arrayRootList.append(objectRemain)
                            }
                        }
                       
                        if (objectData.childrens?.count)! > 0
                        {
                            self.arrayRootList.append(objectData)
                        }
                      
                    }
                    else
                    {
                        self.arrayRootList.append(objectData)
                    }
                }
                self.arrayList.append(contentsOf: self.arrayRootList)
                self.arrayItemList.append(contentsOf: self.arrayRootList)
                self.tblView.reloadData()
            }
            
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.title = " "
    }
 
    @IBAction func dismissVC(_ sender: Any) {
        if self.navigationController != nil {
            self.navigationController?.popViewController()
        }
        else
        {
            self.dismiss(animated: false, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CustomListUserVC:UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if ( self.arrayList != nil )
        {
            return self.arrayList.count
        }
        return 0;
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell:HeaderUserCell = tableView.dequeueReusableCell(withIdentifier: "HeaderUserCell") as! HeaderUserCell
        let array = self.arrayList[section]
        cell.btnLabel.setTitle(array.text, for: .normal)
        return cell;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.arrayList != nil)
        {
            let array = self.arrayList[section]
            return array.childrens != nil ? array.childrens!.count : 0

        }
        return 0
            
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CustomUserCell = tableView.dequeueReusableCell(withIdentifier: "CustomUserCell", for: indexPath) as! CustomUserCell
        let array = self.arrayList[indexPath.section]
        let userObject = array.childrens![indexPath.row]
        cell.lbUserTittle.text = (userObject.text)?.uppercased()
        cell.lbUserJob.text = userObject.jobTitle
        if let avatarString = userObject.picture, let url = (URL(string: Constants.default.domainAddress + avatarString)) {
            cell.userAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        return cell
        
    }
}
extension CustomListUserVC:UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        let stringValues = textField.text!
        if stringValues == "" {
            self.arrayList.removeAll()
            self.arrayItemList.removeAll()
            self.arrayList.append(contentsOf: self.arrayRootList)
            self.arrayItemList.append(contentsOf: self.arrayRootList)
        }
        else
        {
            self.arrayList.removeAll()
            for objectItem in self.arrayItemList {
                let object = objectItem.childrens?.filter({ ($0.text?.uppercased().contains(stringValues.uppercased()))! || ($0.jobTitle?.uppercased().contains(stringValues.uppercased()))! })
                if object != nil && (object?.count)! > 0
                {
                    let arrayItemObj = Assignee()
                    arrayItemObj.parentID  = objectItem.parentID
                    arrayItemObj.jobTitle = objectItem.jobTitle
                    arrayItemObj.parentName = objectItem.parentName
                    arrayItemObj.text = objectItem.text
                    arrayItemObj.childrens = object
                    self.arrayList.append(objectItem)
                }
            }
            
        }
        self.tblView.reloadData()
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let stringValues = textField.text!
        if stringValues == "" {
            self.arrayList.removeAll()
            self.arrayItemList.removeAll()
            self.arrayList.append(contentsOf: self.arrayRootList)
            self.arrayItemList.append(contentsOf: self.arrayRootList)
        }
        else
        {
            self.arrayList.removeAll()
            for objectItem in self.arrayItemList {
                let object = objectItem.childrens?.filter({ ($0.text?.uppercased().contains(stringValues.uppercased()))! || ($0.jobTitle?.uppercased().contains(stringValues.uppercased()))! })
                if object != nil && (object?.count)! > 0
                {
                    let arrayItemObj = Assignee()
                    arrayItemObj.parentID  = objectItem.parentID
                    arrayItemObj.jobTitle = objectItem.jobTitle
                    arrayItemObj.parentName = objectItem.parentName
                    arrayItemObj.text = objectItem.text
                    arrayItemObj.childrens = object
                    self.arrayList.append(arrayItemObj)
                }
            }

        }
        self.tblView.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (( self.delegate ) != nil)
        {
            
            let array = self.arrayList[indexPath.section]
            let userObject = array.childrens![indexPath.row]
            
            let objectAssignee = userObject
            
            let requireOption:[UserProfile] = self.arrayRootListUser.filter({ $0.fullName == objectAssignee.text || $0.loginName == objectAssignee.value })
            if ( requireOption.count > 0  )
            {
                self.delegate.replaceByCustomUserOjbect(userObject: requireOption[0], isMore: self.optionSelectUser)

            }
            self.navigationController?.popViewController()
        }

    }
    
}
