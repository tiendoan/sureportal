//
//  VehicelOptionVC.swift
//  TaskManagement
//
//  Created by Mirum User on 9/3/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import KSTokenView

class VehicelOptionVC: UIViewController,UITextFieldDelegate {
    
    var addTokenFromSearchInput = true

    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var tfOther: UITextField!
    @IBOutlet weak var assignOption: KSTokenView!
    @IBOutlet weak var assignContact: KSTokenView!
      var arrayRootList:[UserProfile]!
    let ASSIGNEES_TEXT_VIEW_PLACE_HOLDER: String = "người xử lý"
    let ASSIGNERS_TOKEN_TAG: Int = 2
    let ASSIGNERS_TOKEN_TAG_REQUIRE: Int = 3
    
    var contactInit:ArrangersObject!
    var partiInit: [ArrangersObject]!
    var partEx:String!
    var notesInit:String!

    weak var delegate:VehicelDelegate!
    var employees: [Assignee] = Dummies.shared.getAllEmployees()
    var authorAcc: [UserProfile] = [UserProfile]()
    var requireAcc: [UserProfile] = [UserProfile]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.arrayRootList = DataManager.shared.allUsers
        tfOther.delegate = self
        tfNotes.delegate = self
        
        assignOption.addSubviews(assignOption.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        assignOption.promptText = ""
        assignOption.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        assignOption.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        assignOption.delegate = self
        assignOption.searchResultHeight = 120
        assignOption.tag = ASSIGNERS_TOKEN_TAG_REQUIRE
        assignOption.returnKeyType(type: .done)
        assignOption.shouldDisplayAlreadyTokenized = true
        assignOption.maxTokenLimit = -1
        assignOption.shouldSortResultsAlphabatically = false
        assignOption.removesTokensOnEndEditing = false
        assignOption.tokenizingCharacters = []
        
        
        assignContact.addSubviews(assignContact.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
        assignContact.promptText = ""
        assignContact.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
        assignContact.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
        assignContact.delegate = self
        assignContact.searchResultHeight = 120
        assignContact.tag =  ASSIGNERS_TOKEN_TAG
        assignContact.returnKeyType(type: .done)
        assignContact.shouldDisplayAlreadyTokenized = true
        assignContact.maxTokenLimit = -1
        assignContact.shouldSortResultsAlphabatically = false
        assignContact.removesTokensOnEndEditing = false
        assignContact.tokenizingCharacters = []
        
        
        if ( self.contactInit != nil)
        {
         
            let ownerAssignee:[UserProfile] = self.arrayRootList.filter({ $0.fullName == self.contactInit.Name || $0.loginName == self.contactInit.UserName })
            if ( ownerAssignee != nil && ownerAssignee.count > 0 && ownerAssignee[0].fullName != nil )
            {
                self.authorAcc.append(ownerAssignee[0])
                self.assignContact.addTokenWithTitle(ownerAssignee[0].fullName!)
                self.assignContact.layoutIfNeeded()
                self.assignContact.layoutSubviews()

            }
        }
        
        if ( self.partiInit != nil)
        {
            
            for objectAssignee in self.partiInit
            {
                let requireOption:[UserProfile] = self.arrayRootList.filter({ $0.fullName == objectAssignee.Name || $0.loginName == objectAssignee.UserName })
                if ( requireOption != nil && requireOption.count > 0 )
                {
                    self.requireAcc.append(requireOption[0])
                    self.assignOption.addTokenWithTitle(requireOption[0].fullName!)
                    self.assignOption.layoutIfNeeded()
                    self.assignOption.layoutSubviews()
                }
            }
        }
        self.tfOther.text = self.partEx
        self.tfNotes.text = self.notesInit


        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func selectedUser(_ sender: Any) {
        self.performSegue(withIdentifier: "userSegue", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [UserProfile]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? UserProfile, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateCallbackData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func showCustomView(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Vehicel", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CustomListUserVC") as! CustomListUserVC
        newViewController.delegate = self
        newViewController.optionSelectUser = true
        self.navigationController?.pushViewController(newViewController)

    }
    func updateCallbackData()
    {
        self.delegate.updateOptionVehicel(contactUser: self.authorAcc, ParticipantsUser: self.requireAcc, externalParticipants: self.tfOther.text!, notes: self.tfNotes.text!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "userSegue" {
            if let destinationVC = segue.destination as? CustomListUserVC {
                destinationVC.delegate = self
            }
        }
    }
}
extension VehicelOptionVC: KSTokenViewDelegate {
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            assignContact.layoutIfNeeded()
            assignContact.layoutSubviews()
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
        {
            assignOption.layoutIfNeeded()
            assignOption.layoutSubviews()
            
        }
        
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            return completion!([])
        }
        var data: Array<AnyObject> = []
        var valueResults = [UserProfile]()
        var textResults = [UserProfile]()
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE  {
            let locale = Locale(identifier: "vi_VN")
            for item: UserProfile in self.arrayRootList {
                
                if let text = item.fullName, let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    valueResults.append(item)
                }else if let text = item.fullName?.folding(options: .diacriticInsensitive, locale: locale), let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
                    textResults.append(item)
                }
                data = valueResults.sorted{ ($0.fullName ?? "") < ($1.fullName ?? "") }
                data.append(contentsOf: textResults)
                //data = results
            }
        }
        return completion!(data)
    }
    
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE {
            if let assignee = object as? UserProfile,
                let name: String = assignee.fullName {
                return name
            } else {
                return "Chưa xác định"
            }
        }
        return object as! String
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE  {
            if let tokenObject: UserProfile = token.object as? UserProfile {
                token.tokenBackgroundColor = tokenObject.getBackgroundColorToTokenView()
            } else {
                token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
            }
        }
        return token
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? UserProfile {
            
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                if let index = self.authorAcc.index(of: user) {
                    self.authorAcc.remove(at: index)
                }
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                if let index = self.requireAcc.index(of: user) {
                    self.requireAcc.remove(at: index)
                }
            }
            self.updateCallbackData()
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE , let user = token.object as? UserProfile, addTokenFromSearchInput {
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                self.removeAssignersTokenView(assignContact, [user])
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                self.removeAssignersTokenView(assignOption, [user])
            }
            
            if tokenView.tag == ASSIGNERS_TOKEN_TAG
            {
                self.authorAcc.append(user)
            }
            else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE
            {
                self.requireAcc.append(user)
            }
            
            self.updateCallbackData()
        }
        self.view.endEditing(true)
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            assignContact.layoutIfNeeded()
            assignContact.layoutSubviews()
        }
        else if tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE {
            assignOption.layoutIfNeeded()
            assignOption.layoutSubviews()
        }
        
        
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG || tokenView.tag == ASSIGNERS_TOKEN_TAG_REQUIRE , let assignee = object as? UserProfile  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            return cell!
        }
        return UITableViewCell()
    }
    
}
extension VehicelOptionVC:CustomUserDelegate
{
    func replaceByCustomUserOjbect(userObject: UserProfile, isMore: Bool) {
        if ( !isMore ) {
            let ownerAssignee:[UserProfile] = [userObject]
            if ( ownerAssignee != nil && ownerAssignee.count > 0 )
            {
                self.authorAcc.removeAll()
                self.assignContact.deleteAllTokens()
                self.authorAcc.append(ownerAssignee[0])
                self.assignContact.addTokenWithTitle(ownerAssignee[0].fullName!)
                self.assignContact.layoutIfNeeded()
                self.assignContact.layoutSubviews()
                self.updateCallbackData()
            }
        }
        else
        {
            let ownerAssignee:[UserProfile] = [userObject]
            self.requireAcc.append(ownerAssignee[0])
            self.assignOption.addTokenWithTitle(ownerAssignee[0].fullName!)
            self.assignOption.layoutIfNeeded()
            self.assignOption.layoutSubviews()
            self.updateCallbackData()
        }
    }
    
   
}

