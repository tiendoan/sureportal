//
//  TaskAssignNoneApproveViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 11/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import KSTokenView
import IQKeyboardManagerSwift
import Toast_Swift
import ObjectMapper
import SVProgressHUD
import DropDown
import Kingfisher

protocol TaskAssignNoneApproveDelegate {
  func onCloseView(status: Bool, indexPath: IndexPath?)
}

class TaskAssignNoneApproveViewController: UIViewController {
  
  //MARK: - Init variables, iboutlets, constant.
  //--------------------------------------------------------
    @IBOutlet weak var imvAvatar: UIImageView!
    @IBOutlet weak var assignersTokenView: KSTokenView!
  @IBOutlet weak var jobCategoriesTokenView: KSTokenView!
  @IBOutlet weak var opinionTextView: UITextView!
  @IBOutlet weak var fromToDayView: UIView!
  @IBOutlet weak var totalCountLabel: LVLabel!
  @IBOutlet weak var publishedDateLabel: LVLabel!
  @IBOutlet weak var assignerFullNameLabel: UILabel!
  @IBOutlet weak var assignerJobTitleLabel: UILabel!
  @IBOutlet weak var fromDayLabel: UILabel!
  @IBOutlet weak var fromHourLabel: UILabel!
  @IBOutlet weak var toDayLabel: UILabel!
  @IBOutlet weak var toHourLabel: UILabel!
  @IBOutlet weak var attachmentsStackView: UIStackView!
  @IBOutlet weak var reportSwitcher: UISwitch!
  @IBOutlet weak var emailSwitcher: UISwitch!
  @IBOutlet weak var smsSwitcher: UISwitch!
    @IBOutlet weak var replySwitcher: UISwitch!
    @IBOutlet weak var noticeSwitcher: UISwitch!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var importantButton: UIButton!
    
    @IBOutlet weak var attachFilesContainerView: UIStackView!
    @IBOutlet weak var textviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var assignJobTypeButton: UIButton!
    @IBOutlet weak var assignByLabel: UILabel!
    @IBOutlet weak var assignByView: UIStackView!
    @IBOutlet weak var assignByDropdownImageView: UIImageView!
    
  let CATEGORIES_TOKEN_TAG: Int = 1 
  let ASSIGNERS_TOKEN_TAG: Int = 2
  let OPINION_TEXT_VIEW_TAG: Int = 1
  let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
  let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
  let PAGE_TITLE: String = "Giao việc"
  let OPINION_TEXT_VIEW_PLACE_HOLDER: String = "Nội dung giao việc"
  let ASSIGNEES_TEXT_VIEW_PLACE_HOLDER: String = "người xử lý"
  let CATEGORIES_TEXT_VIEW_PLACE_HOLDER: String = "loại công việc"
  let HIDE_KB_TITLE: String = "Ẩn bàn phím"
  
    var progressTracking:ProgressTracking?
    var selectedProcessTracking:ProcessTracking?
    var processTrackings: [ProcessTracking] = [ProcessTracking.defaultProcessTracking()] {
        didSet {
            if !processTrackings.isEmpty {
                taskTypeDropDown.dataSource.removeAll()
                taskTypeDropDown.dataSource = processTrackings.map {
                    let userName = $0.assignByName ?? "Chưa xác định"
                    let content:String = $0.name?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "Chưa xác định"
                    if $0.ID == Constants.default.DEFAULT_PARENT_ID {
                        return content
                    }
                    return "\(userName) : \(content)"
                }
                
                self.selectedProcessTracking = processTrackings.first
                let userName = selectedProcessTracking?.assignByName ?? "Chưa xác định"
                let content:String = selectedProcessTracking!.name?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "Chưa xác định"
                
                if self.selectedProcessTracking?.ID == Constants.default.DEFAULT_PARENT_ID {
                     self.assignJobTypeButton.setTitle(content, for: .normal)
                } else {
                    self.assignJobTypeButton.setTitle("\(userName) : \(content)", for: .normal)
                }
                
            } else {
                //self.loadDefaultState()
                self.assignByLabel.isHidden = true
                self.assignByView.isHidden = true
                self.assignByDropdownImageView.isHidden = true
            }
        }
    }
    
  var delegate: TaskAssignNoneApproveDelegate?
  var indexPath: IndexPath?
  var addTokenFromSearchInput = true
  weak var document: Document? {
    didSet {
      // Reload data in views.
    }
  }
  
  var assignTrackings: [AssignTracking]? {
    didSet {
      // Reload data in views.
    }
  }
    let taskDropDown: DropDown = DropDown()
    let taskTypeDropDown: DropDown = DropDown()
    var seletedImportantIndex = 0
    var importantDocuments: [ImportantDocument] = [ImportantDocument]() {
        didSet {
            if !importantDocuments.isEmpty {
                taskDropDown.dataSource.removeAll()
                taskDropDown.dataSource = importantDocuments.map {
                    if let uName: String = $0.name?.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return "Bình thường"
                    }
                }
            }
        }
    }
    
  var categories: [CategoryTracking] = [CategoryTracking]()
  var employees: [Assignee] = [Assignee]()
  var flatEmployees: [Assignee] = [Assignee]()
  var processors: [Assignee] = [Assignee]()
  var collaborators: [Assignee] = [Assignee]()
  var acknownledges: [Assignee] = [Assignee]()
    var attachFiles:[TrackingFileDocument] = [TrackingFileDocument]()
  
  var startDate: Date?
  var endDate: Date?
  
  //MARK: - Setup view components, init state of view controller.
  //----------------------------------------------
  
  override func viewDidLoad () {
    super.viewDidLoad()
    getAllEmployees()
    getCategories()
    setupComponents()
    loadData()
  }
  
  fileprivate func setupComponents () {
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = HIDE_KB_TITLE
    
    //self.setTitleNavigationBar(PAGE_TITLE)
    self.setupDefaultNavigationBar()
    self.showBack()
    var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()

    /// Approve button
    let btnApprove = UIButton(type: .custom)
    btnApprove.setTitle("Giao việc", for: .normal)
    btnApprove.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnApprove.tintColor = Theme.default.normalWhiteColor
    btnApprove.addTarget(self, action: #selector(actionAssginButton(_:)), for: .touchUpInside)
    btnApprove.backgroundColor = UIColor(hexString: "#17C209")
    btnApprove.cornerRadius = Theme.default.normalCornerRadius
    let approveRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnApprove)
    rightButtons.append(approveRightButton)

    /// Attach button
    let btnAttach = UIButton(type: .custom)
    btnAttach.setTitle("Đ.Kèm", for: .normal)
    btnAttach.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnAttach.tintColor = Theme.default.normalWhiteColor
    btnAttach.addTarget(self, action: #selector(attachFilesButton(_:)), for: .touchUpInside)
    btnAttach.backgroundColor = UIColor(hexString: "#17C209")
    btnAttach.cornerRadius = Theme.default.normalCornerRadius
    let attachRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnAttach)
    rightButtons.append(attachRightButton)
    self.addRightButtonNavigationBar(buttons: rightButtons)
    
    assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    assignersTokenView.promptText = ""
    assignersTokenView.descriptionText = ASSIGNEES_TEXT_VIEW_PLACE_HOLDER
    assignersTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
    assignersTokenView.delegate = self
    assignersTokenView.searchResultHeight = 120
    assignersTokenView.tag = ASSIGNERS_TOKEN_TAG
    assignersTokenView.returnKeyType(type: .done)
    assignersTokenView.shouldDisplayAlreadyTokenized = true
    assignersTokenView.maxTokenLimit = -1
    assignersTokenView.shouldSortResultsAlphabatically = false
    assignersTokenView.removesTokensOnEndEditing = false
    assignersTokenView.tokenizingCharacters = []
    _ = assignersTokenView.becomeFirstResponder()
    
    jobCategoriesTokenView.addSubviews(jobCategoriesTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    jobCategoriesTokenView.promptText = ""
    jobCategoriesTokenView.descriptionText = CATEGORIES_TEXT_VIEW_PLACE_HOLDER
    jobCategoriesTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
    jobCategoriesTokenView.delegate = self
    jobCategoriesTokenView.tag = CATEGORIES_TOKEN_TAG
    jobCategoriesTokenView.returnKeyType(type: .done)
    jobCategoriesTokenView.shouldDisplayAlreadyTokenized = true
    jobCategoriesTokenView.maxTokenLimit = -1
    
    opinionTextView.borderColor = Theme.default.normalBlackBorderColor
    opinionTextView.delegate = self
    opinionTextView.tag = OPINION_TEXT_VIEW_TAG
    opinionTextView.font = Theme.default.italicFont(size: opinionTextView.font!.pointSize)
    opinionTextView.textColor = UIColor.lightGray
    
    fromToDayView.borderColor = Theme.default.normalBlackBorderColor

    imvAvatar.layer.cornerRadius = imvAvatar.widthf/2
    imvAvatar.layer.masksToBounds = true
    
    attachmentsStackView.isHidden = true
    
    //IMportant document status
    importantButton.setupArrowDropdownInRight()
    taskDropDown.anchorView = importantButton
    taskDropDown.textColor = UIColor.gray
    taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
    taskDropDown.bottomOffset = CGPoint(x: 0, y: importantButton.bounds.height + 10)
    taskDropDown.isMultipleTouchEnabled = false
    taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
    
    taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        guard let cell = cell as? CustomDropdownCell else { return }
        
        // Setup your custom UI components
        cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
    }
    
    taskDropDown.selectionAction = { [unowned self] (index, item) in
        self.seletedImportantIndex = index
        self.importantButton.setTitle(item, for: .normal)
    }
    
    ///Task type
    taskTypeDropDown.anchorView = assignJobTypeButton
    taskTypeDropDown.textColor = UIColor.gray
    taskTypeDropDown.textFont = Theme.default.semiBoldFont(size: 13)
    taskTypeDropDown.bottomOffset = CGPoint(x: 0, y: assignJobTypeButton.bounds.height + 10)
    taskTypeDropDown.isMultipleTouchEnabled = false
    taskTypeDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
    
    taskTypeDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        guard let cell = cell as? CustomDropdownCell else { return }
        
        // Setup your custom UI components
        cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
    }
    
    taskTypeDropDown.selectionAction = { [unowned self] (index, item) in
        //self.seletedImportantIndex = index
        self.selectedProcessTracking = self.processTrackings[index]
        self.assignJobTypeButton.setTitle(item, for: .normal)
    }
  }
  
  fileprivate func loadData () {
    self.getListTrackingForUser()
    self.importantDocuments = DataManager.shared.importantDocuments
    
    if let users: UserProfile = CurrentUser {
        if let avatarString = users.picture, let url = (URL(string: Constants.default.domainAddress + avatarString)) {
            self.imvAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
      if let fullName: String = users.fullName {
        assignerFullNameLabel.text = fullName.uppercased()
      }
      if let jobTitle: String = users.jobTitle {
        assignerJobTitleLabel.text = jobTitle
      } else if let fName: String = users.fullName {
        assignerJobTitleLabel.text = fName
      }
      publishedDateLabel.text = Constants.currentDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
      let startDate: Date = Constants.currentDate
      guard let endDate: Date = Calendar.current.date(byAdding: .day, value: Constants.DEFAULT_NEXT_DATE, to: Constants.currentDate) else {
        debugPrint("TaskAssignNoneApproveViewController.loadData.error")
        return
      }
      self.startDate = startDate
      self.endDate = endDate
      loadStartEndDays(startDate, endDate)
    }
  }
  
  fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
    fromDayLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    fromHourLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    toDayLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    toHourLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    let diffComponents = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
    if let totalDay: Int = diffComponents.day {
      totalCountLabel.text = "\(totalDay) ngày"
    }
  }
    
    fileprivate func getListTrackingForUser() {
        if let objectDocument = self.document,
            let documentID = objectDocument.id {
            SVProgressHUD.show()
            SyncProvider.getListTrackingForUser(documentID: documentID) { (result, error) in
                SVProgressHUD.dismiss()
                if let lsTracking: [ProcessTracking] = result {
                    self.processTrackings = [ProcessTracking.defaultProcessTracking()] + lsTracking
                } else {
                    debugPrint("ProcessTaskViewController.getListTrackingForUser: empty data.")
                    self.processTrackings = [ProcessTracking.defaultProcessTracking()]
                }
            }
        } else {
            self.processTrackings = []
        }
    }
  
  fileprivate func loadAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee], _ backgroundColor: UIColor) {
    if assigners.isEmpty {
      return
    }

    for assigner in assigners {
      let assignToken: KSToken = KSToken(title: "")
      if let title: String = assigner.text {
        assignToken.title = title
      }
      assignToken.object = assigner
      assignToken.tokenBackgroundColor = backgroundColor
      owner.addToken(assignToken)
    }
  }
    
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? Assignee, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    
    //MARK: - AttachFiles
    func updateAttchFileView() {
        attachmentsStackView.isHidden = self.attachFiles.isEmpty
        contentView.setNeedsLayout()
        contentView.setNeedsUpdateConstraints()
        contentView.layoutIfNeeded()
    }
    
    func deleteAttachFile(fileView: AttachFileView) {
        if let file = fileView.file, let index = self.attachFiles.index(where: {$0.id == file.id}) {
            self.attachFiles.remove(at: index)
        }
        fileView.removeSubviews()
        self.updateAttchFileView()
    }
    
    func addAttachFilesView(file:TrackingFileDocument) {
        let fileView = AttachFileView.instanceFromNib()
        fileView.delegate = self
        fileView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        fileView.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        fileView.updateFileInfo(f: file)
        self.attachFilesContainerView.addArrangedSubview(fileView)
        self.attachFiles.append(file)
        self.updateAttchFileView()
    }
  //MARK: - Handle actions
  //----------------------------------------------
  
    @IBAction func taskTypeButtonClicked(_ sender: Any) {
        taskTypeDropDown.show()
    }
    
    @IBAction func importantButtonClicked(_ sender: Any) {
        taskDropDown.show()
    }
    
    @IBAction func attachFilesButton(_ sender: Any) {
    debugPrint("TaskAssignNoneApproveViewController.attachFilesButton")
    let importMenu = UIDocumentMenuViewController(documentTypes: ["public.content","public.item"], in: .import)
    importMenu.delegate = self
    importMenu.modalPresentationStyle = .formSheet
    self.present(importMenu, animated: true, completion: nil)
  }
  
  @IBAction func addPersonsButton(_ sender: Any) {
    self.view.endEditing(true)
    let assignPerson: AssignPersonViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    assignPerson.assignees = self.employees//self.flatEmployees
    assignPerson.delegate = self
    self.present(assignPerson, animated: true, completion: nil)
  }
  
  @IBAction func actionStartTimeButton (_ sender: Any) {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func actionEndTimeButton (_ sender: Any) {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func actionCancelButton(_ sender: Any){
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func actionAssginButton(_ sender: Any) {

    let isCreateTask = self.document == nil
    if let assignTracking: AssignTrackingRequest = AssignTrackingRequest(JSON: [:]) {
      if let documentID: String = document?.id {
        assignTracking.documentID = documentID
      }
      var assignTrackings: [AssignTracking] = [AssignTracking] ()
      if let assignTrackingItem: AssignTracking = AssignTracking(JSON: [:]) {
        if let description: String = opinionTextView.text, !description.isEmpty, description != OPINION_TEXT_VIEW_PLACE_HOLDER  {
          assignTrackingItem.description = description
        }
        if let assignersToken: [KSToken] = assignersTokenView.tokens(),
           !assignersToken.isEmpty {
          debugPrint("actionAssginButton.count: \(assignersToken.count)")
          var primaryAssignees: [String] = [String]()
          var collaborateAssignees: [String] = [String]()
          var acknownledgeAssignees: [String] = [String]()
          for assignerToken in assignersToken {
            if let assign: Assignee = assignerToken.object as? Assignee {
              if let isProcessRole: Bool = assign.isProcessRoleInTask, isProcessRole {
                if let value: String = assign.value, let parentId: String = assign.parentID {
                  primaryAssignees.append("\(value)|\(parentId)")
                }
              } else if let isCollaborateRole: Bool = assign.isCollaborateRoleInTask, isCollaborateRole {
                if let value: String = assign.value, let parentId: String = assign.parentID {
                  collaborateAssignees.append("\(value)|\(parentId)")
                }
              } else if let isAcknownledgeRole: Bool = assign.isAcknownledgeRoleInTask, isAcknownledgeRole {
                if let value: String = assign.value, let parentId: String = assign.parentID {
                  acknownledgeAssignees.append("\(value)|\(parentId)")
                }
              }
            }
          }
          if !primaryAssignees.isEmpty {
            assignTrackingItem.primaryAssignTo = primaryAssignees
          }
          if !collaborateAssignees.isEmpty {
            assignTrackingItem.supportAssignTo = collaborateAssignees
          }
          if !acknownledgeAssignees.isEmpty {
            assignTrackingItem.readOnlyAssignTo = acknownledgeAssignees
          }
        } else {
            self.view.makeToast("Bạn chưa chọn người xử lý!")
            return
        }
        assignTrackingItem.isReport = reportSwitcher.isOn
        assignTrackingItem.isSendSMS = smsSwitcher.isOn
        assignTrackingItem.isSendMail = emailSwitcher.isOn
        assignTrackingItem.isNotice = noticeSwitcher.isOn
        assignTrackingItem.isReply = replySwitcher.isOn
        if let fromDate: Date = self.startDate {
          assignTrackingItem.fromDate = fromDate
        } else {
          assignTrackingItem.fromDate = Constants.currentDate
        }
        if let toDate: Date = self.endDate {
          assignTrackingItem.toDate = toDate
        } else {
          assignTrackingItem.toDate = Constants.currentDate
        }
        
        if let progressTracking = self.progressTracking, let id = progressTracking.id {
            assignTrackingItem.parentID = id
        } else if let assignTrackings: [AssignTracking] = self.assignTrackings,
           !assignTrackings.isEmpty,
           let firstTracking: AssignTracking = assignTrackings[safe: 0],
           let parentID: String = firstTracking.parentID {
            assignTrackingItem.parentID = parentID
        } else {
          assignTrackingItem.parentID = Constants.default.DEFAULT_PARENT_ID
        }
        
        if let progressTracking = selectedProcessTracking, let parentID: String = progressTracking.ID {
            assignTrackingItem.parentID = parentID
        }
        //important status
        assignTrackingItem.important = self.importantDocuments[safe: self.seletedImportantIndex]
        
        assignTrackings.append(assignTrackingItem)
      }
      assignTracking.assignTrackings = assignTrackings
        assignTracking.files = attachFiles
        
        SVProgressHUD.show()
        SyncProvider.assignTrackingDocument(params: assignTracking, isCreateTask: isCreateTask, done: {
        debugPrint("actionAssginButton.assignTrackingDocument.done")
        SVProgressHUD.dismiss()
        return self.dismiss(animated: true, completion: {
            if let docStatus = self.document?.statusDocument, docStatus.isNew {
                NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_RELOAD), object: nil, userInfo: nil)
            }
            
          self.delegate?.onCloseView(status: true, indexPath: self.indexPath)
        })
      })
    } else {
      self.dismiss(animated: true, completion: {
        self.delegate?.onCloseView(status: false, indexPath: self.indexPath)
      })
    }
  }
}

//MARK: - Extension actions
//----------------------------------------------

extension TaskAssignNoneApproveViewController {
  
  func getAssigners () {
    if let documentObj = self.document,
       let documentID = documentObj.id {
      SyncProvider.getAssignersByDocument(documentID: documentID, done: { (assigners) in
        self.assignTrackings = assigners ?? []
      })
    }
  }
  
  func getAllEmployees () {
    self.employees = DataManager.shared.employees
    self.flatEmployees = Constants.default.recursiveFlatmap(list: employees, true).sorted{ ($0.value ?? "") < ($1.value ?? "") }
    _ = self.flatEmployees.map ({ assignee in
        assignee.isCollaborateRoleInTask = false
        assignee.isAcknownledgeRoleInTask = false
        assignee.isProcessRoleInTask = false
    })
//    SVProgressHUD.show()
//    SyncProvider.getAllUsersInDepartments { (assigners) in
//      if let lAssigners: [Assignee] = assigners {
//        self.employees = lAssigners
//        self.flatEmployees = Constants.default.recursiveFlatmap(list: lAssigners, true).sorted{ ($0.value ?? "") < ($1.value ?? "") }
//      } else {
//        self.employees = Dummies.shared.getAllEmployees()
//        self.flatEmployees = Dummies.shared.getAllEmployees()
//      }
//        SVProgressHUD.dismiss()
//    }
  }
  
  func getCategories () {
    SyncProvider.getCategoryTracking(keyWord: "") { (categories) in
      if let lCategories: [CategoryTracking] = categories {
        self.categories = lCategories
      } else {
        self.categories = Dummies.shared.getAllCategories()
      }
    }
  }
  
}

extension TaskAssignNoneApproveViewController: KSTokenViewDelegate {
    
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
  
  func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
    if (string.isEmpty) {
      return completion!([])
    }
    var data: Array<AnyObject> = []
    var valueResults = [Assignee]()
    var textResults = [Assignee]()
    if tokenView.tag == CATEGORIES_TOKEN_TAG {
      
      for item: CategoryTracking in self.categories {
        if let categoryName: String = item.name,
          categoryName.lowercased().range(of: string.lowercased()) != nil {
          data.append(categoryName as AnyObject)
        }
      }
    } else if tokenView.tag == ASSIGNERS_TOKEN_TAG {
        let locale = Locale(identifier: "vi_VN")
        for item: Assignee in self.flatEmployees {
        guard item.isDept == false else {
            continue
        }
        if let text = item.value, let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
            valueResults.append(item)
        }else if let text = item.text?.folding(options: .diacriticInsensitive, locale: locale), let _ = item.jobTitle, text.contains(string.folding(options: .diacriticInsensitive, locale: locale), caseSensitive: false) {
            textResults.append(item)
        }
            data = valueResults.sorted{ ($0.value ?? "") < ($1.value ?? "") }
            data.append(contentsOf: textResults)
            //data = results
      }
    }
    return completion!(data)
  }
  
  func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
    if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
      if let assignee = object as? Assignee,
        let name: String = assignee.text {
        return name
      } else {
        return "Chưa xác định"
      }
    }
    return object as! String
  }
  
  func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
    if tokenView.tag == ASSIGNERS_TOKEN_TAG {
      if let tokenObject: Assignee = token.object as? Assignee {
        token.tokenBackgroundColor = tokenObject.getBackgroundColorToTokenView()
      } else {
        token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
      }
    } else if tokenView.tag == CATEGORIES_TOKEN_TAG {
      token.tokenBackgroundColor = Theme.default.normalOrangeSelectedColor
    }
    return token
  }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? Assignee {
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = false
            user.isCollaborateRoleInTask = false
            
            if let index = self.processors.index(of: user) {
                self.processors.remove(at: index)
            }
            
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let user = token.object as? Assignee, addTokenFromSearchInput {
            self.removeAssignersTokenView(assignersTokenView, [user])
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
            
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = true
            user.isCollaborateRoleInTask = false
            
            self.processors.append(user)
        }
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
  
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let assignee = object as? Assignee  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            return cell!
        }
        return UITableViewCell()
    }
}

extension TaskAssignNoneApproveViewController: WWCalendarTimeSelectorProtocol {
  
  func WWCalendarTimeSelectorDone (_ selector: WWCalendarTimeSelector, date: Date) {
    if let title: String = selector.optionTopPanelTitle {
      if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
        self.startDate = date
        self.loadStartEndDays(date, toDate)
      } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
        self.endDate = date
        self.loadStartEndDays(fromDate, date)
      }
    }
  }
  
}

extension TaskAssignNoneApproveViewController: AssignPersonProtocol {
  
  func onCloseView (status: Bool, processors: [Assignee]?, collaborators: [Assignee]?, acknownledges: [Assignee]?) {
    debugPrint("TaskAssignNoneApproveViewController.onCloseView.data: processors.count(\(String(describing: processors?.count))), collaborators.count(\(String(describing: collaborators?.count))), acknownledges.count(\(String(describing: acknownledges?.count)))")
    addTokenFromSearchInput = false

    self.assignersTokenView.deleteAllTokens()
    if let localProcessors: [Assignee] = processors {
        self.loadAssignersTokenView(assignersTokenView, localProcessors, Theme.default.normalGreenSelectedColor)
        self.processors = localProcessors
    }
    
    if let localCollaborators: [Assignee] = collaborators {
        self.loadAssignersTokenView(assignersTokenView, localCollaborators, Theme.default.normalBlueSelectedColor)
        self.collaborators = localCollaborators
    }
    
    if let localAcknownledges: [Assignee] = acknownledges {
        self.loadAssignersTokenView(assignersTokenView, localAcknownledges, Theme.default.normalOrangeSelectedColor)
        self.acknownledges = localAcknownledges
    }
    self.assignersTokenView.layoutSubviews()
    _ = assignersTokenView.becomeFirstResponder()
    addTokenFromSearchInput = true
    debugPrint("TaskAssignNoneApproveViewController.onCloseView.data: total token in view \(String(describing: assignersTokenView.tokens()?.count)).")
  }
  
}

extension TaskAssignNoneApproveViewController: UITextViewDelegate {
  
    func updateHeight(_ text: String) {

        let tmpTextView = UITextView(frame: self.opinionTextView.frame)
        tmpTextView.font = self.opinionTextView.font
        tmpTextView.isScrollEnabled = true
        tmpTextView.text = text
        tmpTextView.sizeToFit()
        
        
        var height = tmpTextView.frame.size.height
        if height <= 50 {
            height = 50
        }
        self.textviewHeightConstraint.constant = height
    }
    
  func textViewDidBeginEditing(_ textView: UITextView) {
    textView.font = Theme.default.semiBoldFont(size: textView.font!.pointSize)
    if textView.tag == OPINION_TEXT_VIEW_TAG {
        textView.textColor = UIColor.init(hexString: "363636")
        textView.text = nil
        textView.borderColor = Theme.default.normalGreenSelectedColor
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.tag == OPINION_TEXT_VIEW_TAG && textView.text.isEmpty {
        textView.text = OPINION_TEXT_VIEW_PLACE_HOLDER
        textView.textColor = UIColor.lightGray
        textView.font = Theme.default.italicFont(size: textView.font!.pointSize)
    } else {
        textView.font = Theme.default.semiBoldFont(size: textView.font!.pointSize)
    }
    textView.borderColor = Theme.default.normalBlackBorderColor
  }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let viewText = textView.text {
            let newString = (viewText as NSString).replacingCharacters(in: range, with: text)
            self.updateHeight(newString)
        }
        return true
    }
}

extension TaskAssignNoneApproveViewController: UIDocumentPickerDelegate {
  
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    let cico = url as URL
    debugPrint("The Url is : \(cico)")

    let attachment = Mapper<TrackingFileDocument>().map(JSON:[:])!
    attachment.name = url.lastPathComponent
    attachment.ext = url.pathExtension
    if let fileData = try? Data(contentsOf: URL(fileURLWithPath: url.path)) {
        attachment.size = fileData.count
        self.view.makeToast("Đang quá trình upload.")
        SVProgressHUD.show()
        UploadManager.uploadFile(fileData, type: url.pathExtension, updateHandler: { (percent) in
            print("Percent: \(percent)")
        }) { ( fileID, error) in
            if let id = fileID?.split(separator: ".").first {
                attachment.id = String(id)
                self.addAttachFilesView(file: attachment)
                SVProgressHUD.dismiss()
                self.view.makeToast("Upload thành công, bạn có thể tiếp tục bổ sung thông tin khác.")
            } else {
                self.view.makeToast("Upload thất bại!")
            }
        }
    }
  }
  
  func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
  }
  
}

extension TaskAssignNoneApproveViewController: UIDocumentMenuDelegate {
  
  func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    documentPicker.delegate = self
    self.present(documentPicker, animated: true, completion: nil)
  }
  
  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    
  }
  
}

extension TaskAssignNoneApproveViewController: AttachFileViewDelegate {
    func didDeleteAttachFile(_ fileView: AttachFileView) {
        self.deleteAttachFile(fileView: fileView)
    }
}
