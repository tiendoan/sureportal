//
//  AssignPersonViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/14/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import DropDown

// - MARK: Initlize protocol.
//-----------------------------------------------
protocol AssignPersonProtocol {
  func onCloseView(status: Bool, processors: [Assignee]?, collaborators: [Assignee]?, acknownledges: [Assignee]?)
}

class AssignPersonViewController: UIViewController {
  
  // - MARK: Initlize variables.
  //-----------------------------------------------
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var roomButton: UIButton!  
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var searchbarWidthConstraint: NSLayoutConstraint!
    var onlyOneDepartment = false
    var isSearching = false
    var delegate: AssignPersonProtocol?
    var allAssignees: [Assignee] = [Assignee]()
  var assignees: [Assignee] = [Assignee]() {
    didSet {
      if !assignees.isEmpty {
        departments = self.getDepartments(list: self.assignees)
        allAssignees = Constants.default.recursiveFlatmap(list: self.assignees).sorted{ ($0.value ?? "") < ($1.value ?? "") }
        if selectedAssignee == nil {
            self.selectedAssignee = self.assignees[safe: 0]
        }
      }
    }
  }
    var departments: [Assignee] = [Assignee]()
    var filteredAssignees: [Assignee] = [Assignee]() {
        didSet {
            self.tableView.reloadData()
        }
    }

  let taskDropDown: DropDown = DropDown()
  var selectedAssignee: Assignee?
  
  // - MARK: Override functions.
  //-----------------------------------------------
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    NotificationCenter.default.addObserver(self, selector: #selector(textFieldTextDidChange), name: .UITextFieldTextDidChange, object: tfSearch)
    setupComponents()
    self.loadReadyData()
  }
  
  fileprivate func setupComponents () {
    tableView.register(withClass: DepartmentTableViewCell.self)
    tableView.register(withClass: AssignPersonTableViewCell.self)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 100
    tableView.separatorStyle = .none
    
    taskDropDown.anchorView = roomButton
    taskDropDown.bottomOffset = CGPoint(x: 0, y: roomButton.bounds.height + 10)
    taskDropDown.isMultipleTouchEnabled = false
    taskDropDown.textColor = UIColor.gray
    taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
    taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
    
    taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        guard let cell = cell as? CustomDropdownCell else { return }
        
        // Setup your custom UI components
        cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
    }
    
    // dropdown datasource
    self.roomButton.setTitle(self.selectedAssignee?.text ?? "", for: .normal)
    taskDropDown.dataSource = departments.map {
        if let uName: String = $0.text {
            return uName
        } else {
            return "Chưa xác định"
        }
    }
    
    // selected action
    taskDropDown.selectionAction = { [unowned self] (index, item) in
      self.selectedAssignee = self.departments[safe: index]
      self.roomButton.setTitle(item, for: .normal)
        self.tfSearch.text = ""
        self.loadReadyData()
    }
    
    //searchview
    tfSearch.returnKeyType = .done

  }
    
    
  override func didReceiveMemoryWarning () {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // - MARK: Load data.
  //-----------------------------------------------
    
    func getDepartments(list: [Assignee]) -> [Assignee] {
        var results = [Assignee]()
        for element in list {
            if element.isDept == true {
                results.append(element)
            }
            if let subList: [Assignee] = element.childrens,
                !subList.isEmpty {
                results += getDepartments(list: subList)
            }
        }
        return results
    }
    
  fileprivate func loadReadyData () {
    guard let assignee = selectedAssignee, let assigneeId = assignee.value else {
        self.filteredAssignees = [Assignee]()
        return
    }
    onlyOneDepartment = false
    var filteredAssignees = Constants.default.recursiveFlatmapToNode(nodeId: assigneeId, list: self.assignees, true)
    let indexes = filteredAssignees.indices(where: {$0.isDept == true})
    if indexes == nil {
        onlyOneDepartment = true
        filteredAssignees.insert(selectedAssignee!, at: 0)
    }
    self.filteredAssignees = filteredAssignees
    //self.tableView.reloadData()
  }
    
    func searchAssignee(text:String) {
        guard isSearching else {
            return
        }
        if text.isEmpty {
            self.filteredAssignees = self.allAssignees
            return
        }
        
        self.filteredAssignees = self.allAssignees.filter({ assignee in
            if let value = assignee.value, value.contains(text, caseSensitive: false) {
                return true
            }
            if let name = assignee.text, name.contains(text, caseSensitive: false) {
                return true
            }
            return false
        })
        
        //order by value
        //self.filteredAssignees = self.filteredAssignees.sorted { ($0.text ?? "") < ($1.text ?? "") }
    }
    
    func processRoleAction(assignee:Assignee, role:RolesInTask) {
        guard let nodeId = assignee.value else {
            return
        }
        let assignees = Constants.default.recursiveFlatmapToNode(nodeId: nodeId, list: self.assignees, true)
        for assignee in assignees {
            if assignee.isDept != true{
//                assignee.isProcessRoleInTask = (role == .process && (assignee.isProcessRoleInTask == false || assignee.isProcessRoleInTask == nil))
//                assignee.isCollaborateRoleInTask = (role == .collaborate && (assignee.isCollaborateRoleInTask == false || assignee.isCollaborateRoleInTask == nil))
//                assignee.isAcknownledgeRoleInTask = (role == .acknowledge && (assignee.isAcknownledgeRoleInTask == false || assignee.isAcknownledgeRoleInTask == nil))
                
                assignee.isProcessRoleInTask = role == .process
                assignee.isCollaborateRoleInTask = role == .collaborate
                assignee.isAcknownledgeRoleInTask = role == .acknowledge
            } else {
                assignee.selectedRole = role
            }
            
        }
        
        tableView.reloadData()
    }
  
  // - MARK: Define actions.
  //-----------------------------------------------
    @IBAction func backButtonClicked(_ sender: Any) {
        _ = self.allAssignees.map ({ assignee in
            assignee.isCollaborateRoleInTask = false
            assignee.isAcknownledgeRoleInTask = false
            assignee.isProcessRoleInTask = false
        })
        self.dismiss(animated: true, completion: {
            self.assignees.removeAll()
            self.delegate?.onCloseView(status: true, processors: [Assignee](), collaborators: [Assignee](), acknownledges: [Assignee]())
        })
    }
    
  @IBAction func actionRoomSelectButton (_ sender: Any) {
    taskDropDown.show()
  }
  
  @IBAction func actionSearchButton (_ sender: Any) {
    
    self.searchbarWidthConstraint.constant = !self.isSearching ? 150 : 0
    UIView.animate(withDuration: 0.2) {
        self.view.layoutIfNeeded()
        self.isSearching = !self.isSearching
    }
  }
  
  @IBAction func actionDoneButton (_ sender: Any) {
    let processors: [Assignee] = filteredAssignees.filtered({ (user) -> Bool in
      if let isProcessRole = user.isProcessRoleInTask, isProcessRole {
        return isProcessRole
      } else {
        return false
      }
    }) { $0 }
    let collaborators: [Assignee] = filteredAssignees.filtered({ (user) -> Bool in
      if let isCollaborateRole = user.isCollaborateRoleInTask, isCollaborateRole {
        return isCollaborateRole
      } else {
        return false
      }
    }) { $0 }
    let acknownledges: [Assignee] = filteredAssignees.filtered({ (user) -> Bool in
      if let isAcknownledgeRole = user.isAcknownledgeRoleInTask, isAcknownledgeRole {
        return isAcknownledgeRole
      } else {
        return false
      }
    }) { $0 }
    self.dismiss(animated: true, completion: {
      self.assignees.removeAll()
      self.delegate?.onCloseView(status: true, processors: processors, collaborators: collaborators, acknownledges: acknownledges)
    })
  }
  
}

// - MARK: Extensions
//-----------------------------------------------

extension AssignPersonViewController: UITableViewDataSource {
  
  func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.filteredAssignees.count
  }
  
  func tableView (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let assignee = self.filteredAssignees[safe: indexPath.row] else {
        return UITableViewCell()
    }
    
    if assignee.isDept == true{
        guard let cell: DepartmentTableViewCell = tableView.dequeueReusableCell(withClass: DepartmentTableViewCell.self) else {
            return UITableViewCell()
        }

        if onlyOneDepartment {
            cell.departmentName.isHidden = true
        } else {
            cell.departmentName.setTitle(assignee.text, for: .normal)
        }
        
        cell.onSelectedRole = { [weak self] (role) in
            if assignee.selectedRole == role {
                assignee.selectedRole = .other
                self?.processRoleAction(assignee: assignee, role: .other)
            } else {
                assignee.selectedRole = role
                self?.processRoleAction(assignee: assignee, role: role)
            }
            
        }
        return cell
    } else {
        guard let cell: AssignPersonTableViewCell = tableView.dequeueReusableCell(withClass: AssignPersonTableViewCell.self) else {
            return UITableViewCell()
        }
        debugPrint("AssignPersonViewController.cellForRowAt: row \(indexPath.row)")
        cell.delegate = self
        if let assignee = self.filteredAssignees[safe: indexPath.row] {
            cell.config(assignee: assignee, indexPath: indexPath)
        }
        return cell
    }
    
  }
   
}

extension AssignPersonViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    debugPrint("AssignPersonViewController.didSelectRowAt.index: \(indexPath.row)")
  }
  
}

extension AssignPersonViewController:UITextFieldDelegate {
    func textFieldTextDidChange() {
        self.searchAssignee(text: tfSearch.text ?? "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if let text = textField.text, let textRange = Range(range, in: text) {
//            let updatedText = text.replacingCharacters(in: textRange,
//                                                       with: string)
//            self.searchAssignee(text: updatedText)
//        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        self.actionSearchButton(tfSearch)
        return true
    }
}

extension AssignPersonViewController: AssignPersonTableViewCellDelegate {
  
  func processAction (sender: UIButton, indexPath: IndexPath, processStatus: RolesInTask) {
    if let assignee = self.filteredAssignees[safe: indexPath.row] {
        assignee.isProcessRoleInTask = false
        assignee.isCollaborateRoleInTask = false
        assignee.isAcknownledgeRoleInTask = false
      if processStatus == .process {
        assignee.isProcessRoleInTask = sender.isSelected
      } else if processStatus == .collaborate {
        assignee.isCollaborateRoleInTask = sender.isSelected
      } else if processStatus == .acknowledge {
        assignee.isAcknownledgeRoleInTask = sender.isSelected
      } else {
        if sender.tag == AssignPersonTableViewCell.PROCESS_BTN {
          assignee.isProcessRoleInTask = false
        } else if sender.tag == AssignPersonTableViewCell.COLLABORATE_BTN {
          assignee.isCollaborateRoleInTask = false
        } else if sender.tag == AssignPersonTableViewCell.ACKNOWNLEDGE_BTN {
          assignee.isAcknownledgeRoleInTask = false
        }
      }
    }
  }
  
}

