//
//  TaskAssignViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import KSTokenView
import IQKeyboardManagerSwift
import SVProgressHUD
import ObjectMapper
import DropDown
protocol TaskAssignNewViewDelegate {
  func taskAssignCloseView (_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?)
}

class TaskAssignNewViewController: UIViewController {
  
  //MARK: - Init variables.
  //----------------------------------------------
  @IBOutlet weak var authorAvatarImage: UIImageView!
  @IBOutlet weak var authorNameLabel: UILabel!
  @IBOutlet weak var authorJobTitleLabel: UILabel!
  @IBOutlet weak var publishedDateLabel: LVLabel!
  @IBOutlet weak var summaryLabel: UILabel!
  
  @IBOutlet weak var currentUserNameLabel: UILabel!
  @IBOutlet weak var currentUserJobTitleLabel: UILabel!
  @IBOutlet weak var currentUserAvatarImage: UIImageView!
  
  @IBOutlet weak var opinionTextView: UITextView!
  @IBOutlet weak var assignersTokenView: KSTokenView!
  @IBOutlet weak var categoriesTokenView: KSTokenView!
    @IBOutlet weak var nextAssignerButton: UIButton!
    
  @IBOutlet weak var totalDayLabel: LVLabel!
  @IBOutlet weak var fromDayLabel: UILabel!
  @IBOutlet weak var fromHoursLabel: UILabel!
  @IBOutlet weak var toDayLabel: UILabel!
  @IBOutlet weak var toHoursLabel: UILabel!
  
  @IBOutlet weak var reportSwitcher: UISwitch!
  @IBOutlet weak var emailSwitcher: UISwitch!
  @IBOutlet weak var smsSwitcher: UISwitch!
  
    @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var attachmentStackView: UIStackView!
  @IBOutlet weak var attachFilesContainerView: UIStackView!
    @IBOutlet weak var nextStepStackView: UIStackView!
    
  fileprivate var moreBarButton: UIBarButtonItem!
    @IBOutlet weak var assignerStackView: UIStackView!
    @IBOutlet weak var totalDayStackView: UIStackView!
    @IBOutlet weak var daysView: UIView!
    @IBOutlet weak var notificationStackView: UIStackView!
    
  let CATEGORIES_TOKEN_TAG: Int = 1
  let ASSIGNERS_TOKEN_TAG: Int = 2
  let CANCEL_ACTION_TAG: Int = 0
  let APPROVE_ACTION_TAG: Int = 1
  let RETURN_ACTION_TAG: Int = 2
  let OPINION_TEXT_VIEW_TAG: Int = 1
  let START_DATE_OPTION_TITLE: String = "Chọn ngày bắt đầu"
  let END_DATE_OPTION_TITLE: String = "Chọn ngày kết thúc"
  let EMPLOYEE_TOKEN_VIEW_DESC: String = "người xử lý"
  let CATEGORY_TOKEN_VIEW_DESC: String = "loại công việc"
  let UNKNOWN_STRING: String = "Chưa xác định"
  let APPROVE_AND_ASSIGN_SUCESS_STRING: String = "Yêu cầu của bạn đã được gửi đi thành công"
  let PAGE_TITLE: String = "Duyệt & Giao Việc"
  let HIDE_KB_TITLE: String = "Ẩn bàn phím"
  let OPINION_TEXT_VIEW_PLACE_HOLDER: String = "Ý kiến"
  
  var document: Document? {
    didSet {
    }
  }
  var startDate: Date?
  var endDate: Date?
  var assignTrackings: [AssignTracking] = [AssignTracking] ()
  var categories: [CategoryTracking] = [CategoryTracking]()
  var employees: [Assignee] = [Assignee] ()
  var flatEmployees: [Assignee] = [Assignee]()
  var processors: [Assignee] = [Assignee]()
  var collaborators: [Assignee] = [Assignee]()
  var acknownledges: [Assignee] = [Assignee]()
    var attachFiles:[TrackingFileDocument] = [TrackingFileDocument]()
    
    var selectedNextStep: GetNextWorkFlowStep?
    var nextWorkFlowSteps:[GetNextWorkFlowStep] = [GetNextWorkFlowStep]() {
        didSet {
            if !nextWorkFlowSteps.isEmpty {
                selectedNextStep = nextWorkFlowSteps.first
                nextAssignerButton.setTitle(selectedNextStep?.objectActionName, for: .normal)
                stepsDropDown.dataSource.removeAll()
                stepsDropDown.dataSource = nextWorkFlowSteps.map {
                    if let uName: String = $0.objectActionName?.trimmingCharacters(in: .whitespacesAndNewlines) {
                        return uName
                    } else {
                        return "Chưa xác định"
                    }
                }
                nextStepStackView.isHidden = false
                nextAssignerButton.isHidden = false
            } else {
                nextStepStackView.isHidden = true
                nextAssignerButton.isHidden = true
            }
        }
    }
    var addTokenFromSearchInput = true
  var indexItem: IndexPath?
  
  var delegate: TaskAssignNewViewDelegate?
  let stepsDropDown: DropDown = DropDown()
    
  // MARK: - Init functions, localize actions
  //----------------------------------------------
  
  override func viewDidLoad () {
    super.viewDidLoad()
    self.setupComponents()
    // MARK: Binding data in views.
    // -------------------------------
    self.loadDocumentSection()
    self.getAssigners()
    self.getAllAssigneesInDept()
    self.getCategories()
    self.getNextWorkFlowStep()
  }
  
  fileprivate func setupComponents () {
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = HIDE_KB_TITLE
    
    //self.setTitleNavigationBar(PAGE_TITLE)
    self.setupDefaultNavigationBar()
    self.showBack()

    var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()
    
    /// Approve button
    let btnApprove = UIButton(type: .custom)
    btnApprove.setTitle("Duyệt", for: .normal)
    btnApprove.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnApprove.tintColor = Theme.default.normalWhiteColor
    btnApprove.addTarget(self, action: #selector(actionAssginButton(_:)), for: .touchUpInside)
    btnApprove.backgroundColor = UIColor(hexString: "#17C209")
    btnApprove.cornerRadius = Theme.default.normalCornerRadius
    let approveRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnApprove)
    rightButtons.append(approveRightButton)
    
    /// Return button
    let btnReturn = UIButton(type: .custom)
    btnReturn.setTitle("Trả lại", for: .normal)
    btnReturn.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnReturn.tintColor = Theme.default.normalWhiteColor
    btnReturn.addTarget(self, action: #selector(returnDocumentButton(_:)), for: .touchUpInside)
    btnReturn.backgroundColor = UIColor(hexString: "#17C209")
    btnReturn.cornerRadius = Theme.default.normalCornerRadius
    let returnRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnReturn)
    rightButtons.append(returnRightButton)
    
    /// Attach button
    let btnAttach = UIButton(type: .custom)
    btnAttach.setTitle("Đ.Kèm", for: .normal)
    btnAttach.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    btnAttach.tintColor = Theme.default.normalWhiteColor
    btnAttach.addTarget(self, action: #selector(attachFilesButton(_:)), for: .touchUpInside)
    btnAttach.backgroundColor = UIColor(hexString: "#17C209")
    btnAttach.cornerRadius = Theme.default.normalCornerRadius
    let attachRightButton: UIBarButtonItem = UIBarButtonItem(customView: btnAttach)
    rightButtons.append(attachRightButton)
    self.addRightButtonNavigationBar(buttons: rightButtons)
    

    assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    assignersTokenView.promptText = ""
    assignersTokenView.descriptionText = EMPLOYEE_TOKEN_VIEW_DESC
    assignersTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
    assignersTokenView.delegate = self
    assignersTokenView.searchResultHeight = 120
    assignersTokenView.tag = ASSIGNERS_TOKEN_TAG
    assignersTokenView.returnKeyType(type: .done)
    assignersTokenView.shouldDisplayAlreadyTokenized = true
    assignersTokenView.maxTokenLimit = -1
    assignersTokenView.removesTokensOnEndEditing = false
    assignersTokenView.tokenizingCharacters = []
    _ = assignersTokenView.becomeFirstResponder()
    
    categoriesTokenView.addSubviews(categoriesTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
    categoriesTokenView.promptText = ""
    categoriesTokenView.descriptionText = CATEGORY_TOKEN_VIEW_DESC
    categoriesTokenView.font = Theme.default.lightFont(size: Theme.default.middleFontSize)
    categoriesTokenView.delegate = self
    categoriesTokenView.tag = CATEGORIES_TOKEN_TAG
    categoriesTokenView.returnKeyType(type: .done)
    categoriesTokenView.maxTokenLimit = -1
    
    opinionTextView.borderColor = Theme.default.normalBlackBorderColor
    opinionTextView.delegate = self
    opinionTextView.tag = OPINION_TEXT_VIEW_TAG
    
    totalDayLabel.borderColor = Theme.default.normalBlackBorderColor
    totalDayLabel.borderWidth = Theme.default.normalBorderWidth
    totalDayLabel.cornerRadius = Theme.default.normalCornerRadius
    
    attachmentStackView.isHidden = true
    
    stepsDropDown.anchorView = nextAssignerButton
    stepsDropDown.textColor = UIColor.gray
    stepsDropDown.textFont = Theme.default.semiBoldFont(size: 13)
    stepsDropDown.bottomOffset = CGPoint(x: 0, y: nextAssignerButton.bounds.height + 10)
    stepsDropDown.isMultipleTouchEnabled = false
    stepsDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
    
    stepsDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        guard let cell = cell as? CustomDropdownCell else { return }
        
        // Setup your custom UI components
        cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
    }
    
    stepsDropDown.selectionAction = { [unowned self] (index, item) in
        self.selectedNextStep = self.nextWorkFlowSteps[safe: index]
        self.nextAssignerButton.setTitle(item, for: .normal)
    }
    
    if document?.isInComing == false {  //Van ban di -> hide all
        self.assignerStackView.isHidden = true
        self.assignerStackView.arrangedSubviews.forEach({$0.isHidden = true})
        self.totalDayStackView.isHidden = true
        self.totalDayStackView.arrangedSubviews.forEach({$0.isHidden = true})
        self.daysView.isHidden = true
        self.attachmentStackView.isHidden = true
        self.attachmentStackView.arrangedSubviews.forEach({$0.isHidden = true})
        self.notificationStackView.isHidden = true
        self.notificationStackView.arrangedSubviews.forEach({$0.isHidden = true})
        
    }
  }
  
  fileprivate func loadStartEndDays (_ startDate: Date, _ endDate: Date) {
    fromDayLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    fromHoursLabel.text = startDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    toDayLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
    toHoursLabel.text = endDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
    let diffComponents = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
    if let totalDay: Int = diffComponents.day {
      totalDayLabel.text = "\(totalDay) ngày"
    }
  }
  
  fileprivate func loadAssignTrackingSection () {
    if !self.assignTrackings.isEmpty {
      if let assigner: AssignTracking = self.assignTrackings[safe: 0] {
        if let desc: String = assigner.description {
          opinionTextView.text = desc
        }
        
        // Calculate total day.
        if let fromDay: Date = assigner.fromDate {
          fromDayLabel.text = fromDay.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
          fromHoursLabel.text = fromDay.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
          self.startDate = fromDay
        } else {
          let currentDate: Date = Constants.currentDate
          fromDayLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
          fromHoursLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
          self.startDate = currentDate
        }
        
        if let toDay: Date = assigner.toDate {
          toDayLabel.text = toDay.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
          toHoursLabel.text = toDay.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
          var diffComponents = Calendar.current.dateComponents([.day], from: Constants.currentDate, to: toDay)
          if let fromDay: Date = assigner.fromDate {
            diffComponents = Calendar.current.dateComponents([.day], from: fromDay, to: toDay)
          }
          if let totalDay: Int = diffComponents.day {
            totalDayLabel.text = "\(totalDay)"
          }
          self.endDate = toDay
        } else if let nextDate: Date = Calendar.current.date(byAdding: .day, value: Constants.DEFAULT_NEXT_DATE, to: Constants.currentDate) {
          toDayLabel.text = nextDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
          toHoursLabel.text = nextDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
          totalDayLabel.text = "\(Constants.DEFAULT_NEXT_DATE) ngày"
          self.endDate = nextDate
        } else {
          let currentDate: Date = Constants.currentDate
          toDayLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
          toHoursLabel.text = currentDate.toFormatDate(format: Constants.SIMPLE_TIME_FORMAT)
          self.endDate = currentDate
        }
        
        if let isReport: Bool = assigner.isReport, isReport {
          reportSwitcher.isOn = isReport
        }
        if let isSendEmail: Bool = assigner.isSendMail, isSendEmail {
          emailSwitcher.isOn = isSendEmail
        }
        if let isSendSMS: Bool = assigner.isSendSMS, isSendSMS {
          smsSwitcher.isOn = isSendSMS
        }
      }
    }
  }
  
  fileprivate func loadDocumentSection () {
    if let documentObject: Document = document {
        if let authorAvatar = documentObject.authorPicture, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(authorAvatar){
            self.authorAvatarImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
      if let authorName: String = documentObject.authorName {
        authorNameLabel.text = "\(authorName)"
      }
      if let authorJobTile: String = documentObject.authorJobTitle {
        authorJobTitleLabel.text = "\(authorJobTile)"
      }
      if let createdDate: Date = documentObject.created {
        publishedDateLabel.text = createdDate.toFormatDate(format: Constants.SIMPLE_DATETIME_FORMAT)
      }
        
        if let status = document?.statusDocument?.name {
            publishedDateLabel.text = " \(status) "
        }
      if let summary: String = documentObject.summary {
        summaryLabel.text = "\(summary)"
      }
      if let user: UserProfile = CurrentUser {
        if let currentUserName: String = user.fullName {
          currentUserNameLabel.text = currentUserName
        }
        if let currentJobTitle: String = user.jobTitle {
          currentUserJobTitleLabel.text = currentJobTitle
        } else if let currentUserName : String = user.fullName {
          currentUserJobTitleLabel.text = currentUserName
        }
        
        if let currentUserAvatar = user.avartar, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(currentUserAvatar){
            self.currentUserAvatarImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
      }
    }
  }
  
  fileprivate func loadAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee], _ backgroundColor: UIColor) {
    if assigners.isEmpty {
      return
    }
    for assigner in assigners {
      let assignToken: KSToken = KSToken(title: "")
      if let title: String = assigner.text {
        assignToken.title = title
      }
      assignToken.object = assigner
      assignToken.tokenBackgroundColor = backgroundColor
      owner.addToken(assignToken)
    }
  }
    
    fileprivate func removeAssignersTokenView (_ owner: KSTokenView, _ assigners: [Assignee]) {
        guard let tokens = owner.tokens(), !assigners.isEmpty else {
            return
        }
        for token in tokens {
            if let assigner = token.object as? Assignee, assigners.contains(assigner) {
                owner.deleteToken(token)
            }
        }
    }
    
    func updateAttchFileView() {
        attachmentStackView.isHidden = self.attachFiles.isEmpty
        contentView.setNeedsLayout()
        contentView.setNeedsUpdateConstraints()
        contentView.layoutIfNeeded()
    }
    
    func deleteAttachFile(fileView: AttachFileView) {
        if let file = fileView.file, let index = self.attachFiles.index(where: {$0.id == file.id}) {
            self.attachFiles.remove(at: index)
        }
        fileView.removeSubviews()
        self.updateAttchFileView()
    }
    
    func addAttachFilesView(file:TrackingFileDocument) {
        let fileView = AttachFileView.instanceFromNib()
        fileView.delegate = self
        fileView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        fileView.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        fileView.updateFileInfo(f: file)
        self.attachFilesContainerView.addArrangedSubview(fileView)
        self.attachFiles.append(file)
        self.updateAttchFileView()
    }
  
  //MARK: - Define functions actions
  //----------------------------------------------
    @IBAction func nextAssignerButtonClicked(_ sender: Any) {
        stepsDropDown.show()
    }
    
  @IBAction func attachFilesButton(_ sender: Any) {
    debugPrint("TaskAssignNoneApproveViewController.attachFilesButton")
    let importMenu = UIDocumentMenuViewController(documentTypes: ["public.content","public.item"], in: .import)
    importMenu.delegate = self
    importMenu.modalPresentationStyle = .formSheet
    self.present(importMenu, animated: true, completion: nil)
  }
  
  @IBAction func addPersonsButton (_ sender: Any) {
    self.view.endEditing(true)
    let assignPerson: AssignPersonViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    assignPerson.assignees = self.employees//self.flatEmployees
    assignPerson.delegate = self
    self.present(assignPerson, animated: true, completion: nil)
  }
  
  @IBAction func returnDocumentButton(_ sender: Any) {
    if let documentID: String = self.document?.id,
      let comment: String = opinionTextView.text {
      self.returnDocument(comment, documentID, done: { (result) in
        return self.dismiss(animated: true, completion: {
          self.delegate?.taskAssignCloseView(self.indexItem, nil, result, nil)
        })
      }, error: { (error) in
        return self.dismiss(animated: true, completion: {
          self.delegate?.taskAssignCloseView(self.indexItem, nil, nil, error)
        })
      })
    }
  }
  
  @IBAction func actionAssginButton(_ sender: Any) {
    if let documentObject: Document = self.document {
      guard let documentID: String = documentObject.id else {
        return self.dismiss(animated: true, completion: {
          self.delegate?.taskAssignCloseView(self.indexItem, nil, nil, "Document ID invalid")
        })
      }
      var opinonStr = ""
      if let opinon: String = opinionTextView.text {
        opinonStr = opinon
      }
      var workFlowStepIDStr = ""
      if let workFlowStepID: String = documentObject.workFlowStepID {
        workFlowStepIDStr = workFlowStepID
      }
      self.approveDocument(documentID, workFlowStepIDStr, opinonStr, done: { (result) in
      })

      if let assignRequest: AssignTrackingRequest = AssignTrackingRequest(JSON: [:]), documentObject.isInComing == true {
        assignRequest.documentID = documentID
        var assignTrackings: [AssignTracking] = [AssignTracking] ()
        if let assignTrackingItem: AssignTracking = AssignTracking(JSON: [:]) {
          assignTrackingItem.description = opinonStr
          if let assignersToken: [KSToken] = assignersTokenView.tokens(),
            !assignersToken.isEmpty {
            var primaryAssignees: [String] = [String]()
            var collaborateAssignees: [String] = [String]()
            var acknownledgeAssignees: [String] = [String]()
            for assignerToken in assignersToken {
              if let assign: Assignee = assignerToken.object as? Assignee {
                if let isProcessRole: Bool = assign.isProcessRoleInTask, isProcessRole {
                  if let value: String = assign.value, let parentId: String = assign.parentID {
                    primaryAssignees.append("\(value)|\(parentId)")
                  }
                } else if let isCollaborateRole: Bool = assign.isCollaborateRoleInTask, isCollaborateRole {
                  if let value: String = assign.value, let parentId: String = assign.parentID {
                    collaborateAssignees.append("\(value)|\(parentId)")
                  }
                } else if let isAcknownledgeRole: Bool = assign.isAcknownledgeRoleInTask, isAcknownledgeRole {
                  if let value: String = assign.value, let parentId: String = assign.parentID {
                    acknownledgeAssignees.append("\(value)|\(parentId)")
                  }
                }
              }
            }
            if !primaryAssignees.isEmpty {
              assignTrackingItem.primaryAssignTo = primaryAssignees
            }
            if !collaborateAssignees.isEmpty {
              assignTrackingItem.supportAssignTo = collaborateAssignees
            }
            if !acknownledgeAssignees.isEmpty {
              assignTrackingItem.readOnlyAssignTo = acknownledgeAssignees
            }
          }
          assignTrackingItem.isReport = reportSwitcher.isOn
          assignTrackingItem.isSendSMS = smsSwitcher.isOn
          assignTrackingItem.isSendMail = emailSwitcher.isOn
          if let fromDate: Date = self.startDate {
            assignTrackingItem.fromDate = fromDate
          } else {
            assignTrackingItem.fromDate = Constants.currentDate
          }
          if let toDate: Date = self.endDate {
            assignTrackingItem.toDate = toDate
          } else {
            assignTrackingItem.toDate = Constants.currentDate
          }
          if !self.assignTrackings.isEmpty,
            let firstTracking: AssignTracking = assignTrackings[safe: 0],
            let parentID: String = firstTracking.parentID {
            assignTrackingItem.parentID = parentID
          } else {
            assignTrackingItem.parentID = Constants.default.DEFAULT_PARENT_ID
          }
          assignTrackings.append(assignTrackingItem)
        }
        assignRequest.assignTrackings = assignTrackings
        assignRequest.files = attachFiles
        
        self.assignDocument(assignRequest, done: { (result) in
          return self.dismiss(animated: true, completion: {
            self.delegate?.taskAssignCloseView(self.indexItem, self.APPROVE_AND_ASSIGN_SUCESS_STRING, nil, nil)
          })
        })
      } else {
        self.delegate?.taskAssignCloseView(self.indexItem, self.APPROVE_AND_ASSIGN_SUCESS_STRING, nil, nil)
    }
    }
  }
  
  @IBAction func actionStartTimeButton (_ sender: Any) {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = START_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
  
  @IBAction func actionEndTimeButton (_ sender: Any) {
    let selector = WWCalendarTimeSelector.instantiate()
    selector.delegate = self
    selector.optionTopPanelTitle = END_DATE_OPTION_TITLE
    selector.optionSelectionType = .single
    self.present(selector, animated: true, completion: nil)
  }
}


//MARK: - Define get datas
//----------------------------------------------

extension TaskAssignNewViewController {
  
  fileprivate func getAssigners () {
    if let documentObj = self.document,
      let documentID = documentObj.id {
      SyncProvider.getAssignersByDocument(documentID: documentID, done: { (assigners) in
        self.assignTrackings = assigners ?? []
        self.loadAssignTrackingSection()
      })
    }
  }
  
  fileprivate func getAllAssigneesInDept () {
    SyncProvider.getAllUsersInDepartments { (assigners) in
      if let lAssigners: [Assignee] = assigners {
        self.employees = lAssigners
        self.flatEmployees = Constants.default.recursiveFlatmap(list: lAssigners, true)
      } else {
        self.employees = Dummies.shared.getAllEmployees()
        self.flatEmployees = Dummies.shared.getAllEmployees()
      }
    }
  }
  
  fileprivate func getCategories () {
    SyncProvider.getCategoryTracking(keyWord: "") { (categories) in
      if let lCategories: [CategoryTracking] = categories {
        self.categories = lCategories
      } else {
        self.categories = Dummies.shared.getAllCategories()
      }
    }
  }
    
    fileprivate func getNextWorkFlowStep () {
        guard let documentObj = self.document, let documentID = documentObj.id else { return }
        SyncProvider.getNextWorkFlowSteps(docID: documentID) { (steps) in
            if let steps: [GetNextWorkFlowStep] = steps {
                self.nextWorkFlowSteps = steps
            } else {
                self.nextWorkFlowSteps = []
            }
        }
    }
  
  fileprivate func returnDocument (_ comment: String, _ documentID: String, done: @escaping((_ message: String) -> ()), error: @escaping((_ reason: String) -> ())) {
    if comment == "" {
      return error("Comment parameter is invalid.")
    }
    SyncProvider.returnDocument(documentID: documentID, comment: comment, done: {
      let sucessMsg: String = "Return document \(documentID) success"
      return done(sucessMsg)
    })
  }
  
  fileprivate func approveDocument (_ documentID: String, _ workFlowStepID: String, _ opinon: String, done: @escaping((_ message: String) -> ())) {
    SyncProvider.approveDocument(documentID: documentID, workFlowStepID: workFlowStepID, contentObj: opinon, nextWFStep:selectedNextStep, done: {
      done("actionAssginButton.approveDocument: \(documentID) success")
    })
  }
  
  fileprivate func assignDocument (_ assignTracking: AssignTrackingRequest, done: @escaping((_ message: String) -> ())){
    SyncProvider.assignTrackingDocument(params: assignTracking, isCreateTask: false, done: {
      done("actionAssginButton.assignTrackingDocument.done")
    })
  }
  
}

//MARK: - Define extensions
//----------------------------------------------

extension TaskAssignNewViewController: KSTokenViewDelegate {
  
//  func tokenViewDidBeginEditing(_ tokenView: KSTokenView) {
//    if tokenView.tag == CATEGORIES_TOKEN_TAG {
//      categoriesTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalGreenSelectedColor))
//    } else if tokenView.tag == ASSIGNERS_TOKEN_TAG {
//      assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalGreenSelectedColor))
//    }
//  }
//  
//  func tokenViewDidEndEditing(_ tokenView: KSTokenView) {
//    if tokenView.tag == CATEGORIES_TOKEN_TAG {
//      categoriesTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
//    } else if tokenView.tag == ASSIGNERS_TOKEN_TAG {
//      assignersTokenView.addSubviews(assignersTokenView.addBorder(edges: .bottom, color: Theme.default.normalBlackBorderColor))
//    }
//  }
  
    func tokenViewDidHideSearchResults(_ tokenView: KSTokenView) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
    
    func tokenView (_ tokenView: KSTokenView, performSearchWithString string: String, completion: ((Array<AnyObject>) -> Void)?) {
        if (string.isEmpty) {
            return completion!([])
        }
        var data: Array<AnyObject> = []
        if tokenView.tag == CATEGORIES_TOKEN_TAG {
            
            for item: CategoryTracking in self.categories {
                if let categoryName: String = item.name,
                    categoryName.lowercased().range(of: string.lowercased()) != nil {
                    data.append(categoryName as AnyObject)
                }
            }
        } else if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            for item: Assignee in self.flatEmployees {
                guard item.isDept == false else {
                    continue
                }
                if let value = item.value, value.contains(string, caseSensitive: false) {
                    data.append(item as AnyObject)
                } else if let text = item.text, text.contains(string, caseSensitive: false) {
                    data.append(item as AnyObject)
                }
                
                //order by value
                //data = data.sorted { ($0.value ?? "") < ($1.value ?? "") }
            }
        }
        return completion!(data)
    }
  
    func tokenView (_ tokenView: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG  {
            if let assignee = object as? Assignee,
                let name: String = assignee.text {
                return name
            } else {
                return "Chưa xác định"
            }
        }
        return object as! String
    }
    
    func tokenView (_ tokenView: KSTokenView, shouldChangeAppearanceForToken token: KSToken) -> KSToken? {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            if let tokenObject: Assignee = token.object as? Assignee {
                token.tokenBackgroundColor = tokenObject.getBackgroundColorToTokenView()
            } else {
                token.tokenBackgroundColor = Theme.default.normalGreenSelectedColor
            }
        } else if tokenView.tag == CATEGORIES_TOKEN_TAG {
            token.tokenBackgroundColor = Theme.default.normalOrangeSelectedColor
        }
        return token
    }
    
    func tokenView(_ tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if let user = token.object as? Assignee {
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = false
            user.isCollaborateRoleInTask = false
            
            if let index = self.processors.index(of: user) {
                self.processors.remove(at: index)
            }
            
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
        }
    }
    
    func tokenViewDidDeleteAllToken(_ tokenView: KSTokenView) {
        
    }
    
    func tokenView(_ tokenView: KSTokenView, willAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let user = token.object as? Assignee, addTokenFromSearchInput {
            self.removeAssignersTokenView(assignersTokenView, [user])
            if let index = self.collaborators.index(of: user) {
                self.collaborators.remove(at: index)
            }
            
            if let index = self.acknownledges.index(of: user) {
                self.acknownledges.remove(at: index)
            }
            
            user.isAcknownledgeRoleInTask = false
            user.isProcessRoleInTask = true
            user.isCollaborateRoleInTask = false
            
            self.processors.append(user)
        }
    }
    
    func tokenView(_ tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG {
            assignersTokenView.layoutIfNeeded()
            assignersTokenView.layoutSubviews()
        }
        
    }
    
    func tokenView(_ tokenView: KSTokenView, withObject object: AnyObject, tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tokenView.tag == ASSIGNERS_TOKEN_TAG, let assignee = object as? Assignee  {
            // For registering nib files
            let cellIdentifier = "UserTableViewCell"
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserTableViewCell?
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell!.setUser(assignee)
            
            return cell!
        }
        return UITableViewCell()
    }
}

extension TaskAssignNewViewController: WWCalendarTimeSelectorProtocol {
  
  func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
    if let title: String = selector.optionTopPanelTitle {
      if title == START_DATE_OPTION_TITLE, let toDate: Date = self.endDate { // Start day option.
        self.startDate = date
        self.loadStartEndDays(date, toDate)
      } else if title == END_DATE_OPTION_TITLE, let fromDate: Date = self.startDate { // End day option.
        self.endDate = date
        self.loadStartEndDays(fromDate, date)
      }
    }
  }
  
}

extension TaskAssignNewViewController: AssignPersonProtocol {
  
    func onCloseView (status: Bool, processors: [Assignee]?, collaborators: [Assignee]?, acknownledges: [Assignee]?) {
        debugPrint("TaskAssignNoneApproveViewController.onCloseView.data: processors.count(\(String(describing: processors?.count))), collaborators.count(\(String(describing: collaborators?.count))), acknownledges.count(\(String(describing: acknownledges?.count)))")
        addTokenFromSearchInput = false
        
        self.assignersTokenView.deleteAllTokens()
        if let localProcessors: [Assignee] = processors {
            self.loadAssignersTokenView(assignersTokenView, localProcessors, Theme.default.normalGreenSelectedColor)
            self.processors = localProcessors
        }
        
        if let localCollaborators: [Assignee] = collaborators {
            self.loadAssignersTokenView(assignersTokenView, localCollaborators, Theme.default.normalBlueSelectedColor)
            self.collaborators = localCollaborators
        }
        
        if let localAcknownledges: [Assignee] = acknownledges {
            self.loadAssignersTokenView(assignersTokenView, localAcknownledges, Theme.default.normalOrangeSelectedColor)
            self.acknownledges = localAcknownledges
        }
        self.assignersTokenView.layoutSubviews()
        _ = assignersTokenView.becomeFirstResponder()
        addTokenFromSearchInput = true
        debugPrint("TaskAssignNoneApproveViewController.onCloseView.data: total token in view \(String(describing: assignersTokenView.tokens()?.count)).")
    }
  
}

extension TaskAssignNewViewController: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.tag == OPINION_TEXT_VIEW_TAG {
      if textView.text == OPINION_TEXT_VIEW_PLACE_HOLDER {
        textView.text = nil
      }
      textView.borderColor = Theme.default.normalGreenSelectedColor
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.tag == OPINION_TEXT_VIEW_TAG {
      if textView.text.isEmpty {
        textView.text = OPINION_TEXT_VIEW_PLACE_HOLDER
      }
      textView.borderColor = Theme.default.normalBlackBorderColor
    }
  }
}

extension TaskAssignNewViewController: UIDocumentPickerDelegate {
  
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        debugPrint("The Url is : \(cico)")
        
        let attachment = Mapper<TrackingFileDocument>().map(JSON:[:])!
        attachment.name = url.lastPathComponent
        attachment.ext = url.pathExtension
        if let fileData = try? Data(contentsOf: URL(fileURLWithPath: url.path)) {
            attachment.size = fileData.count
            self.view.makeToast("Đang quá trình upload.")
            SVProgressHUD.show()
            UploadManager.uploadFile(fileData, type: url.pathExtension, updateHandler: { (percent) in
                print("Percent: \(percent)")
            }) { ( fileID, error) in
                if let id = fileID?.split(separator: ".").first {
                    attachment.id = String(id)
                    self.addAttachFilesView(file: attachment)
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Upload thành công, bạn có thể tiếp tục bổ sung thông tin khác.")
                } else {
                    self.view.makeToast("Upload thất bại!")
                }
            }
        }
    }
  
  func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
  }
  
}

extension TaskAssignNewViewController: UIDocumentMenuDelegate {
  
  func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    documentPicker.delegate = self
    self.present(documentPicker, animated: true, completion: nil)
  }
  
  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    
  }
  
}

extension TaskAssignNewViewController: AttachFileViewDelegate {
    func didDeleteAttachFile(_ fileView: AttachFileView) {
        self.deleteAttachFile(fileView: fileView)
    }
}
