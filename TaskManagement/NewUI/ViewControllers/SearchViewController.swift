//
//  SearchViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/19/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import SwipeCellKit
import SVProgressHUD

protocol AdvanceSearchDelegate {
    func getParamAdvanceSearch(parm:[String:Any],isAdvance:Bool)
}

class SearchViewController: UIViewController,viewAttachmentFileDelegate,AdvanceSearchDelegate {
    func getParamAdvanceSearch(parm: [String : Any], isAdvance: Bool) {
        
        self.paramAdvanceSearch = parm
        self.isAdvanceSearch = isAdvance
        self.search()
    }
    
    
    func viewAttachmentfile(fileDocument: FileDocument, procedureId: String) {
        
        let fileName:String = fileDocument.name!
        let fileId:String = fileDocument.id!
        self.presentAttachmentFileDigital(filename: fileName, fileId: fileId, procedureId: procedureId)
        
    }
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchResultTableView: UITableView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var deleteFilterButton: UIButton!
    @IBOutlet weak var filterView: UIView!
    var paramAdvanceSearch:[String:Any] = [:]
    var isAdvanceSearch:Bool!
    //Advance search setting
    var prevSearch:String = ""
    var haveFilter = false
    var searchType = SearchType.all
    var docType = DocType.go
    var department:Assignee?
    var startDate = Date(month: 1, day: 1)!
    var endDate = Date(month: 12, day: 31)!
    
    let PAGE_TITLE: String = "Tìm kiếm"
    let HIDE_KB_TITLE: String = "Ẩn bàn phím"
    let SEARCH_TEXTFIELD_PLACE_HOLDER: String = "Nhập từ khoá văn bản để tìm kiếm"
    let SEARCH_RESULT_EMPTY: String = "Không có dữ liệu, vui lòng nhập từ khoá mới !"
    var currentPage: Int = 1
    var canLoadMore = true
    var produreItem:[ProdureDigital] = []
    
    var searchDocuments: [Document] = [Document]() {
        didSet {
            self.searchResultTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupComponents()
        updateFilterView()
    }
    
    func updateFilterView() {
        filterLabel.isHidden = haveFilter ? false : true
        deleteFilterButton.isHidden = haveFilter ? false : true
        filterView.isHidden = haveFilter ? false : true
        
        var filter = "Loại: \(searchType.name)"
        filter += ", Loại văn bản: \(docType.name)"
        if let department = self.department, let name = department.text {
            filter += ", Đơn vị: \(name)"
        }
        
        filter += ", Từ ngày: \(startDate.toFormatDate(format: "dd/MM/yyyy"))"
        filter += ", Đến ngày: \(endDate.toFormatDate(format: "dd/MM/yyyy"))"
        
        filterLabel.text = filter
        
        filterView.setNeedsLayout()
        self.filterView.layoutSubviews()
    }
    
    fileprivate func setupComponents() {
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = HIDE_KB_TITLE
        
        self.setTitleNavigationBar(PAGE_TITLE)
        self.setupDefaultNavigationBar()
        self.navigationBar?.barTintColor = UIColor.init(hexString: "2674EF")
        
        self.showBack()
        
        //set right button
        var rightButtons: [UIBarButtonItem] = [UIBarButtonItem]()
        
        let btnOptions = UIButton(type: .custom)
        btnOptions.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        btnOptions.setImage(#imageLiteral(resourceName: "ic_nav_more"), for: .normal)
        btnOptions.addTarget(self, action: #selector(touchUpInsideMore(_:)), for: .touchUpInside)
        let rightButton: UIBarButtonItem = UIBarButtonItem(customView: btnOptions)
        rightButtons.append(rightButton)
        self.addRightButtonNavigationBar(buttons: rightButtons)
        
        //    let icon = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
        //    icon.contentMode = .center
        //    icon.image = #imageLiteral(resourceName: "ic_search")
        //
        let searchButton = UIButton(type: .custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchButton.addTarget(self, action: #selector(searchButtonClicked), for: .touchUpInside)
        searchButton.setImage(#imageLiteral(resourceName: "ic_search"), for: .normal)
        
        searchTextField.rightView = searchButton
        searchTextField.rightViewMode = .always
        searchTextField.autocapitalizationType = .none
        searchTextField.tintColor = Theme.default.normalBlackColor
        searchTextField.setPlaceHolderTextColor(Theme.default.normalBlackColor)
        searchTextField.cornerRadius = 15
        searchTextField.borderStyle = .roundedRect
        searchTextField.borderColor = Theme.default.normalBlackBorderColor
        searchTextField.borderWidth = Theme.default.normalBorderWidth
        searchTextField.clipsToBounds = true
        searchTextField.delegate = self
        searchTextField.clearButtonMode = .whileEditing
        searchTextField.returnKeyType = .search
        searchTextField.placeholder = SEARCH_TEXTFIELD_PLACE_HOLDER
        
        searchResultTableView.separatorStyle = .none
        searchResultTableView.rowHeight = UITableViewAutomaticDimension
        searchResultTableView.register(withClass: TaskTableCell.self)
        
        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor.white
        refresh.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        searchResultTableView.addSubview(refresh)
    }
    
    @IBAction func touchUpInsideMore(_ sender: Any) {
//        
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE )
        {
            
            self.view.endEditing(true)
            let settingVC: AdvanceSearchSettingViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            settingVC.selectedType = searchType
            settingVC.selectedDepartment = department
            settingVC.fromDate = startDate
            settingVC.toDate = endDate
            settingVC.searchText = searchTextField.text ?? ""
            settingVC.onSetAdvanceSearch = { [unowned self] (text, type, docType, department, startDate, endDate) in
                self.haveFilter = true
                self.searchTextField.text = text
                self.searchType = type
                self.docType = docType
                self.department = department
                self.startDate = startDate
                self.endDate = endDate
                self.updateFilterView()
                self.searchDocumentByKeywords()
            }
            self.showViewController(viewController: settingVC, true, UIModalTransitionStyle.crossDissolve)
        }
        else
        {
            let settingVC: AdvanceSearchVC = UIStoryboard(storyboard: .newMain).instantiateViewController()
            settingVC.delegate = self
            self.showViewController(viewController: settingVC, true, UIModalTransitionStyle.crossDissolve)
        }
     
    }
    
    @IBAction func deleteFilterButtonClicked(_ sender: Any) {
        haveFilter = false
        department = nil
        searchType = .all
        startDate = Date(month: 1, day: 1)!
        endDate = Date(month: 12, day: 31)!
        updateFilterView()
    }
    
    @IBAction func refreshData(_ sender: Any) {
        
    }
    
    func searchButtonClicked() {
        searchTextField.resignFirstResponder()
        self.searchDocumentByKeywords()
    }
    
    @IBAction func pressesBTAttach(_ sender: UIButton) {
        if (self.searchDocuments[sender.tag].fileDocuments != nil && searchDocuments[sender.tag].fileDocuments!.count == 1) {
            let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
            vc?.fileDoucment = searchDocuments[sender.tag].fileDocuments?[0]
            vc?.document = searchDocuments[sender.tag]
            vc?.delegate = self
            self.present(vc!, animated: true, completion: nil)
        } else {
            let vc: ListFileAttachViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle =  .overCurrentContext
            vc.files = searchDocuments[sender.tag].fileDocuments ?? []
            vc.document = searchDocuments[sender.tag]
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
}

// MARKS: Searching api
// ----------------------

extension SearchViewController {
    
    func resetSearch() {
        self.currentPage = 1
        self.searchDocuments = []
        canLoadMore = true
    }
    
    fileprivate func searchDocumentByKeywords() {
        resetSearch()
        search()
    }
    
    func loadNextPage() {
        self.currentPage += 1
        search()
    }
    
    fileprivate func search() {
        if let keyword: String = searchTextField.text {
            if self.currentPage == 1 {
                SVProgressHUD.show()
            }
            
            let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
            if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE ) {
             
                let searchType = self.searchType.value
                let docType = self.docType.rawValue
                let org = "OrgID|\(self.department?.value ?? "")"
                let startDate = self.startDate.toFormatDate(format: "yyyy-MM-dd")
                let endDate = self.endDate.toFormatDate(format: "yyyy-MM-dd")
                let advancedParams = [org, "DocDateFrom|\(startDate)", "DocDateTo|\(endDate)", "Category|\(searchType)", "DocType|\(docType)"]
                //["OrgID|", "DocDateFrom|2018-01-01", "DocDateTo|2018-12-31", "Category|All"]
                SyncProvider.advancedSearchDocument(keyword: keyword, advancedParams: advancedParams, page: self.currentPage, done: { (result) in
                    SVProgressHUD.dismiss()
                    if result.isEmpty {
                        self.canLoadMore = false
                    }
                    self.searchDocuments.append(contentsOf: result)
                    self.searchResultTableView.reloadData()
                })
            }
            else
            {
                self.produreItem.removeAll()
                if ( self.isAdvanceSearch )
                {
                    
                    InstanceDB.default.advanceSearchDigital(keyParam: self.paramAdvanceSearch) { (result) in
                        
                        self.prevSearch = keyword
                        SVProgressHUD.dismiss()
                        if result == nil {
                            self.canLoadMore = false
                        }
                        if ( result != nil && (result as! DigitalResponse).status == 1 )
                        {
                            let tmpArray:[ProdureDigital] = ((((result as! DigitalResponse).data)!.procedureData)?.Items)!
                            self.produreItem.append(contentsOf: tmpArray)
                        }
                        self.searchResultTableView.reloadData()
                    }
                    
                }
                else
                {
                    
                    InstanceDB.default.getProdureDigitalPage(keySearch: keyword, page: self.currentPage, status: 100) { (result) in
                        
                        self.prevSearch = keyword
                        SVProgressHUD.dismiss()
                        if result == nil {
                            self.canLoadMore = false
                        }
                        if ( result != nil && (result as! DigitalResponse).status == 1 )
                        {
                            let tmpArray:[ProdureDigital] = ((((result as! DigitalResponse).data)!.procedureData)?.Items)!
                            self.produreItem.append(contentsOf: tmpArray)
                        }
                        self.searchResultTableView.reloadData()
                    }
                    
                }

            }
          
        }
    }
    
}

extension SearchViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.searchDocumentByKeywords()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //    let key = textField.text ?? ""
        ////    if !key.isEmpty {
        ////      self.searchDocumentByKeywords()
        ////      return false
        ////    }
//        self.searchDocumentByKeywords()
        return true
    }
}

extension SearchViewController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE )
        {
            if !self.searchDocuments.isEmpty {
                TableViewHelper.EmptyMessage(message: "", viewController: self, tableView: self.searchResultTableView)
                return 1
            } else {
                TableViewHelper.EmptyMessage(message: SEARCH_RESULT_EMPTY, viewController: self, tableView: self.searchResultTableView)
                return 0
            }
        }
        else
        {
            if !self.produreItem.isEmpty {
                TableViewHelper.EmptyMessage(message: "", viewController: self, tableView: self.searchResultTableView)
                return 1
            } else {
                TableViewHelper.EmptyMessage(message: SEARCH_RESULT_EMPTY, viewController: self, tableView: self.searchResultTableView)
                return 0
            }
        }
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE )
        {
            guard let searchDocument: Document = self.searchDocuments[safe: indexPath.row] else {
                return
            }
            let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            detailVC.document = searchDocument
            self.navigationController?.pushViewController(detailVC)
            if let documentID: String = searchDocument.id {
                InstanceDB.default.updateStatusDocument(documentID: documentID, done: { (result) in
                })
            }
            
        }
        else
        {
            let procedureDigital:ProdureDigital = self.produreItem[indexPath.row]
            let storyBoard: UIStoryboard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProcedureDigitalViewDetail") as! ProcedureDigitalViewDetail
            newViewController.procedureDetail = procedureDigital
            self.navigationController?.pushViewController(newViewController)
        }
      
    }
    
}

extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == searchDocuments.count - 2) && canLoadMore {
            loadNextPage()
        }
        
        let lastRowIndex = searchDocuments.count - 1
        if indexPath.row == lastRowIndex && canLoadMore {
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self.searchResultTableView.tableFooterView = spinner
            self.searchResultTableView.tableFooterView?.isHidden = false
        } else {
            self.searchResultTableView.tableFooterView = nil
            self.searchResultTableView.tableFooterView?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE )
        {
            return searchDocuments.count
        }
        return produreItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskTableCell", for: indexPath) as! TaskTableCell
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE )
        {
            guard let searchDocument: Document = self.searchDocuments[safe: indexPath.row] else {
                return UITableViewCell()
            }

            cell.delegate = self
            cell.config(searchDocument, false, indexPath.row, searchText: searchTextField.text ?? "")
            cell.btAttach.tag = indexPath.row
            cell.btAttach.addTarget(self, action: #selector(pressesBTAttach(_:)), for: .touchUpInside)
        }
        
      
        else
        {
            guard let documentItem: ProdureDigital = produreItem[safe: indexPath.row] else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.configDigital(documentItem, false, indexPath.row, searchText: "")
            cell.onOpenAttachFile = {
                let fileDocument:String = (documentItem.document?.count)! > 0 ? (documentItem.document![0]).name! : ""
                if !fileDocument.isEmpty
                {
                    let procedueId:String = documentItem.ID!
                    self.showAttachDetailViewController(fileDocument: documentItem.document!, procedureId: procedueId)
                }
            }
        }
        return cell
    }
    func showAttachDetailViewController(fileDocument: [FileDocument], procedureId:String) {
        
        if ( fileDocument.count == 1 )
        {
            let fileName:String = fileDocument[0].name!
            let fileId:String = fileDocument[0].id!
            self.presentAttachmentFileDigital(filename: fileName, fileId: fileId, procedureId: procedureId)
        }
        else
        {
            let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "DialogAttachmentVC") as! DialogAttachmentVC
            mainViewController.delegate = self
            mainViewController.arrayFileName = fileDocument
            mainViewController.procedureId = procedureId
            self.present(mainViewController, animated: false, completion: nil)
        }
    }
    func presentAttachmentFileDigital(filename:String, fileId: String, procedureId: String){
        
        let storyBoard = UIStoryboard(name: "ProcedureDigital", bundle: nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "AttachmentDigittalDetailView") as! AttachmentDigittalDetailView
        mainViewController.fileName = filename
        mainViewController.fileId = fileId
        mainViewController.procedureId = procedureId
        self.present(mainViewController, animated: false, completion: nil)
        
    }
    
}

extension SearchViewController: SwipeTableViewCellDelegate {
    
    func tableView (_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if (indexPath.section == 0) {
            if orientation == .right {
                var list: [SwipeAction] = []
                if let document = self.searchDocuments[safe: indexPath.row],
                    document.actions != nil {
                    for i in 0..<document.actions!.count {
                        if (i == 3) {
                            let action = SwipeAction(style: .default, title: "Xem thêm", handler: { (action, indexPath) in
                                let actionSheet = UIAlertController(title: "Chọn", message: nil, preferredStyle: .actionSheet)
                                for i in 0..<document.actions!.count {
                                    let action: UIAlertAction = UIAlertAction(title: document.actions![i].actionName, style: .default) { (_) in
                                        if let actionType: DocumentAction = document.actions![i].actionType {
                                            if actionType == .approve {
                                                let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                                assignVC.document = document
                                                assignVC.indexItem = indexPath
                                                assignVC.delegate = self
                                                self.showViewController(viewController: assignVC, false)
                                            } else if actionType == .assign {
                                                let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                                assignVC.delegate = self
                                                assignVC.document = document
                                                assignVC.getAssigners()
                                                self.showViewController(viewController: assignVC, false)
                                            } else if actionType == .process {
                                                let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                                processVC.delegate = self
                                                processVC.document = document
                                                self.showViewController(viewController: processVC, false)
                                            } else if actionType == .appraise {
                                                let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                                appriseVC.delegate = self
                                                appriseVC.document = document
                                                appriseVC.indexPath = indexPath
                                                self.showViewController(viewController: appriseVC, false)
                                            } else if actionType == .sendMail {
                                                let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                                sendEmailCV.document = document
                                                sendEmailCV.delegate = self
                                                self.showViewController(viewController: sendEmailCV, false)
                                            } else if actionType == .finish {
                                                self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
                                            }
                                        }
                                    }
                                    actionSheet.addAction(action)
                                }
                                let cancel: UIAlertAction = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
                                }
                                actionSheet.addAction(cancel)
                                self.present(actionSheet, animated: true, completion: nil)
                            })
                            action.image = #imageLiteral(resourceName: "multiSelect_more").filled(withColor: Theme.default.normalGreenSelectedColor)
                            list.append(action)
                            break
                        } else {
                            if let actionTypeItem: DocumentAction = document.actions![i].actionType {
                                let action = SwipeAction(style: .default, title: document.actions![i].actionName, handler: { (action, indexPath) in
                                })
                                if actionTypeItem == .approve {
                                    action.handler = { (action, indexPath) in
                                        let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                        assignVC.document = document
                                        assignVC.indexItem = indexPath
                                        assignVC.delegate = self
                                        self.showViewController(viewController: assignVC, false)
                                    }
                                } else if actionTypeItem == .assign {
                                    action.handler = { (action, indexPath) in
                                        let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                        assignVC.delegate = self
                                        assignVC.document = document
                                        assignVC.getAssigners()
                                        self.showViewController(viewController: assignVC, false)
                                    }
                                } else if actionTypeItem == .process {
                                    action.handler = { (action, indexPath) in
                                        let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                        processVC.delegate = self
                                        processVC.document = document
                                        self.showViewController(viewController: processVC, false)
                                    }
                                } else if actionTypeItem == .appraise {
                                    action.handler = { (action, indexPath) in
                                        let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                        appriseVC.delegate = self
                                        appriseVC.document = document
                                        self.showViewController(viewController: appriseVC, false)
                                    }
                                } else if actionTypeItem == .sendMail {
                                    action.handler = { (action, indexPath) in
                                        let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                                        sendEmailCV.delegate = self
                                        sendEmailCV.document = document
                                        self.showViewController(viewController: sendEmailCV, false)
                                    }
                                } else if actionTypeItem == .finish {
                                    action.handler = { (action, indexPath) in
                                        self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
                                    }
                                }
                                let imgAction: String = "ic_action_\(actionTypeItem.rawValue)"
                                if let imgObj = UIImage(named: imgAction) {
                                    action.image = imgObj.imageResize(sizeChange: CGSize(width: 25, height: 20)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                                } else {
                                    action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                                }
                                list.append(action)
                            }
                        }
                    }
                }
                for item in list {
                    item.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.9058823529, blue: 0.9411764706, alpha: 1)
                    item.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    item.font = Theme.default.boldFont(size: 12)
                }
                return list
            }
        }
        return []
    }
    
    func tableView (_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return options
    }
    
}

extension SearchViewController: TaskAssignNewViewDelegate {
    
    func taskAssignCloseView(_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?) {
        var defaultMsg: String = "Chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !"
        if let confirmMsg: String = approveMsg {
            defaultMsg = confirmMsg
        } else if let backMsg: String = returnMsg {
            defaultMsg = backMsg
        } else if let failMsg: String = errorMsg {
            defaultMsg = failMsg
        }
        self.view.makeToast(defaultMsg)
        if let selectedIndex: IndexPath = index {
            self.searchResultTableView.reloadRows(at: [selectedIndex], with: UITableViewRowAnimation.automatic)
        }
    }
    
}

extension SearchViewController: TaskAssignNoneApproveDelegate {
    
    func onCloseView(status: Bool, indexPath: IndexPath?) {
        if status {
            self.view.makeToast("Xử lý thành công !")
        } else {
            self.view.makeToast("Có lỗi xảy ra !")
        }
    }
    
}

extension SearchViewController: ProcessTaskViewDelegate {
    
    func closeView() {
        
    }
    
    func sendProcessWork(status: Bool, message: String?, index: IndexPath?) {
        if status {
            var msg = "Gửi yêu cầu xử lý thành công."
            if let msgTemp: String = message,
                msgTemp != "" {
                msg = msgTemp
            }
            self.view.makeToast(msg)
        } else {
            self.view.makeToast("Có lỗi xảy ra, chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !")
        }
    }
    
    
}

extension SearchViewController: AppriseTaskViewDelegate {
    
    func appriseDoneView(status: Bool, message: String?, index: IndexPath?) {
        self.view.makeToast("Gửi yêu cầu xử lý thành công")
    }
    
    
    func appriseCloseView() {
    }
    
}

extension SearchViewController: SendEmailViewDelegate {
    
    func sendEmailCloseView() {
    }
    
    func sendEmailDoneView() {
        self.view.makeToast("Gửi mail thành công !")
    }
    
}

class TableViewHelper {
    
    class func EmptyMessage(message:String, viewController: UIViewController, tableView: UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.view.bounds.size.width, height: viewController.view.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = Theme.default.normalBlackColor
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = Theme.default.semiBoldFont(size: 16)
        messageLabel.sizeToFit()
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = .none;
    }
    
}

// MARK: - AttachmentDetailViewDelegate
extension SearchViewController: AttachmentDetailViewDelegate {
    
    func doneView(rowIndex: IndexPath?, action: DocumentAction) {
        //self.view.makeToast(action.successMessage)
        if action != .sendMail, let selectedIndex = rowIndex {
            self.searchResultTableView.reloadRows(at: [selectedIndex], with: UITableViewRowAnimation.automatic)
        }
    }
}
