//
//  CustomFilePagerViewCell.swift
//  TaskManagement
//
//  Created by Scor Doan on 3/26/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import FSPagerView

open class CustomFilePagerViewCell : FSPagerViewCell {

    fileprivate weak var _iconImageView: UIImageView?

    open var iconImageView: UIImageView? {
        if let _ = _iconImageView {
            return _iconImageView
        }
        let imageView = UIImageView(frame: .zero)
        self.contentView.addSubview(imageView)
        _iconImageView = imageView
        return imageView
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
    
        if let imageView = _iconImageView {
            let size = self.contentView.bounds.size
            imageView.frame = CGRect(x: 5, y: size.height-32, width: 20, height: 27)
        }
    }
}
