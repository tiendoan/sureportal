//
//  DepartmentTableViewCell.swift
//  TaskManagement
//
//  Created by Scor Doan on 3/13/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

class DepartmentTableViewCell: UITableViewCell {

    @IBOutlet weak var departmentName: UIButton!
    
    var onSelectedRole: ((RolesInTask) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        onSelectedRole = nil
        departmentName.isHidden = false
    }
    
    @IBAction func taskButtonClicked(_ sender: Any) {
        let tag = (sender as! UIButton).tag
        onSelectedRole?(RolesInTask(rawValue: tag)!)
    }
}
