//
//  AttachFileView.swift
//  TaskManagement
//
//  Created by Scor Doan on 3/14/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

protocol AttachFileViewDelegate {
    func didDeleteAttachFile(_ fileView:AttachFileView)
}

class AttachFileView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    var delegate:AttachFileViewDelegate?
    var file:TrackingFileDocument?
    
    class func instanceFromNib() -> AttachFileView {
        return UINib(nibName: "AttachFileView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AttachFileView
    }
    
    func updateFileInfo(f:TrackingFileDocument) {
        self.file = f
        self.titleLabel.text = self.file!.name
        self.sizeLabel.text = "(\(getFileSize()))"
    }
    
    @IBAction func closeButtonClicked() {
        self.delegate?.didDeleteAttachFile(self)
    }
    
    func getFileSize() -> String {
        if let fileSize = file?.size {
            // bytes
            if fileSize < 1023 {
                return String(format: "%lu bytes", CUnsignedLong(fileSize))
            }
            // KB
            var floatSize = Float(fileSize / 1024)
            if floatSize < 1023 {
                return String(format: "%.1f KB", floatSize)
            }
            // MB
            floatSize = floatSize / 1024
            if floatSize < 1023 {
                return String(format: "%.1f MB", floatSize)
            }
            // GB
            floatSize = floatSize / 1024
            return String(format: "%.1f GB", floatSize)
        }
        
        return "NA"
    }
}
