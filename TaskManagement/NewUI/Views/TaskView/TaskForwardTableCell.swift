//
//  TaskForwardTableCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/17/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

enum TaskForwardTableCellType {
    case Top
    case Middle
    case Botton
}

class TaskForwardTableCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var btShow:UIButton!
    @IBOutlet fileprivate weak var lbTitle:UILabel!
    @IBOutlet fileprivate weak var lineTop:UIView!
    @IBOutlet fileprivate weak var lineBotton:UIView!
    
    @IBOutlet fileprivate weak var tableView:UITableView!
    @IBOutlet fileprivate weak var heightTable:NSLayoutConstraint!
    
    fileprivate var array:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        self.btShow.setTitle("+", for: .normal)
        
        tableView.contentInset = UIEdgeInsets(top: -8, left: 0, bottom: 0, right: 0)
        tableView.register(withClass: TaskForwardTableCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 47
        tableView.dataSource = self
        tableView.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func config(_ title:String, _ type: TaskForwardTableCellType, _ dataChild:[String]?, _ show:Bool){
        lbTitle.text = title
        switch type {
        case .Top:
            lineTop.isHidden = true
            lineBotton.isHidden = false
        case .Middle:
            lineTop.isHidden = false
            lineBotton.isHidden = false
        case .Botton:
            lineTop.isHidden = false
            lineBotton.isHidden = true
        }
        array.removeAll()
        if(dataChild != nil){
            array.append(contentsOf: dataChild!)
        }
        
        heightTable.constant = CGFloat(47*array.count)-16
        self.layoutIfNeeded()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskForwardTableCell", for: indexPath) as! TaskForwardTableCell
        if indexPath.row == 0{
            cell.config(array[indexPath.row], .Top, nil, false)
        }
        else if indexPath.row == array.count - 1{
            cell.config(array[indexPath.row], .Botton, nil, false)
        }
        else{
            cell.config(array[indexPath.row], .Middle, nil, false)
        }
        return cell
    }
}
