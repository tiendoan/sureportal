//
//  AssignPersonTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/16/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Kingfisher

protocol AssignPersonTableViewCellDelegate {
  func processAction (sender: UIButton, indexPath: IndexPath, processStatus: RolesInTask)
}

class AssignPersonTableViewCell: UITableViewCell {
  
  //MARK: - Init variables, override functions.
  //----------------------------------------------
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var processButton: UIButton!
  @IBOutlet weak var collaborateButton: UIButton!
  @IBOutlet weak var acknownledgeButton: UIButton!

  @IBOutlet weak var actionsStackView: UIStackView!
  
  static let PROCESS_BTN: Int = 1
  static let COLLABORATE_BTN: Int = 2
  static let ACKNOWNLEDGE_BTN: Int = 3
  static let DEFAULT_WIDTH_IMG_SIZE: Int = 50
  static let DEFAULT_HEIGHT_IMG_SIZE: Int = 50
  var actionTags: [Int] = [Int] ()
  
  var assigner: Assignee?
  var indexPath: IndexPath?
  var delegate: AssignPersonTableViewCellDelegate?
  var isActiveSelected: Bool = false
  
  override func awakeFromNib () {
    super.awakeFromNib()
    // Initialization code
    setupComponents()
  }
  
  fileprivate func updateStateForButtons (_ button: UIButton, _ roleInTask: RolesInTask) {
    button.setTitleColor(Theme.default.normalWhiteColor, for: .normal)
    button.isSelected = true
    switch roleInTask {
    case .process:
      button.backgroundColor = Theme.default.normalGreenSelectedColor
      break
    case .collaborate:
      button.backgroundColor = Theme.default.normalBlueSelectedColor
      break
    case .acknowledge:
      button.backgroundColor = Theme.default.normalOrangeSelectedColor
      break
    case .other:
        button.isSelected = false
      button.backgroundColor = Theme.default.normalGrayBorderColor
      button.setTitleColor(Theme.default.normalBlackColor, for: .normal)
      break
    }
  }
  
  fileprivate func setupComponents () {
    self.selectionStyle = .none
    
    processButton.tag = RolesInTask.process.rawValue
    processButton.backgroundColor = Theme.default.normalBlackBorderColor
    processButton.setTitleColor(Theme.default.normalBlackColor, for: .normal)
    
    collaborateButton.tag = RolesInTask.collaborate.rawValue
    collaborateButton.backgroundColor = Theme.default.normalBlackBorderColor
    collaborateButton.setTitleColor(Theme.default.normalBlackColor, for: .normal)

    acknownledgeButton.tag = RolesInTask.acknowledge.rawValue
    acknownledgeButton.backgroundColor = Theme.default.normalBlackBorderColor
    acknownledgeButton.setTitleColor(Theme.default.normalBlackColor, for: .normal)
    
    avatarImageView.layer.cornerRadius = avatarImageView.widthf/2
    avatarImageView.layer.masksToBounds = true
  }
  
  //MARK: - Configuration, Binding data in views
  //----------------------------------------------  
  
  func config (assignee: Assignee, indexPath: IndexPath) {
    self.indexPath = indexPath
    self.assigner = assignee
    if let name: String = assignee.text {
      nameLabel.text = name
    }
    if let title: String = assignee.jobTitle {
      titleLabel.text = title
    }
    if let avatarURLStr: String = assignee.picture,
      let avatarURL: URL = URL(string: Constants.default.domainAddress + avatarURLStr) {
        self.avatarImageView.kf.setImage(with: avatarURL, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    if let isProcessRoleInTask: Bool = assignee.isProcessRoleInTask, isProcessRoleInTask {
      self.updateStateForButtons(processButton, RolesInTask.process)
    } else {
      self.updateStateForButtons(processButton, RolesInTask.other)
    }
    if let isCollaborateRoleInTask: Bool = assignee.isCollaborateRoleInTask, isCollaborateRoleInTask {
      self.updateStateForButtons(collaborateButton, RolesInTask.collaborate)
    } else {
      self.updateStateForButtons(collaborateButton, RolesInTask.other)
    }
    if let isAcknownledgeRoleInTask: Bool = assignee.isAcknownledgeRoleInTask, isAcknownledgeRoleInTask {
      self.updateStateForButtons(acknownledgeButton, RolesInTask.acknowledge)
    } else {
      self.updateStateForButtons(acknownledgeButton, RolesInTask.other)
    }
    
    avatarImageView.isHidden = false
    nameLabel.isHidden = false
    titleLabel.isHidden = false
    actionsStackView.isHidden = false
  }
  
  //MARK: - Define actions
  //----------------------------------------------
  
    func unactiveAllButtons() {
        processButton.isSelected = false
        self.updateStateForButtons(processButton, RolesInTask.other)
        collaborateButton.isSelected = false
        self.updateStateForButtons(collaborateButton, RolesInTask.other)
        acknownledgeButton.isSelected = false
        self.updateStateForButtons(acknownledgeButton, RolesInTask.other)
    }
    
  @IBAction func buttonToolsAction (_ sender: UIButton) {
    //if !isActiveSelected {
    if !sender.isSelected {
        unactiveAllButtons()
    }
    
      isActiveSelected = !isActiveSelected
      sender.isSelected = !sender.isSelected
      var localRoleInTask: RolesInTask = RolesInTask.other
      if sender.isSelected {
        switch sender.tag {
        case AssignPersonTableViewCell.PROCESS_BTN:
          localRoleInTask = RolesInTask.process
          break
        case AssignPersonTableViewCell.COLLABORATE_BTN:
          localRoleInTask = RolesInTask.collaborate
          break
        case AssignPersonTableViewCell.ACKNOWNLEDGE_BTN:
          localRoleInTask = RolesInTask.acknowledge
          break
        default:
          break
        }
      }
      self.updateStateForButtons(sender, localRoleInTask)
      if let selectedIndex: IndexPath = self.indexPath {
        self.delegate?.processAction(sender: sender, indexPath: selectedIndex, processStatus: localRoleInTask)
      }
    //}
  }
  
}
