//
//  DetailProcessItemCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/18/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SwipeCellKit

enum ProcessStatus {
  case new, done, cancel, report, inProgress
}

class DetailProcessItemCell: SwipeTableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  //@IBOutlet weak var workingLabel: UILabel!
  @IBOutlet weak var endTimeLabel: UILabel!
  @IBOutlet weak var btnMoreUsers: UIButton!
    @IBOutlet weak var btnDown: UIButton!
    @IBOutlet weak var btnFiles: UIButton!
    
//  @IBOutlet weak var progessView: UIProgressView!
  @IBOutlet weak var statusButton: UIButton!
  @IBOutlet weak var percenLabel: UILabel!
    @IBOutlet var vwUsers: [UIView]!
    @IBOutlet var imvAvatars: [UIImageView]!
    @IBOutlet weak var sumaryLabel: UILabel!
    
    var onDetailTapped: (() -> Void)?
    var onOpenFiles: ((FileDocument) -> Void)?
    var progressItem:ProgressTracking?
  var color: UIColor = #colorLiteral(red: 0.6823529412, green: 0.6823529412, blue: 0.6823529412, alpha: 1)
  var statusTitle = "MỚI"
  var status: ProcessStatus = .new {
    didSet{
//      self.progessView.progressTintColor = color
      self.statusButton.backgroundColor = color
      self.percenLabel.textColor = color
    }
  }
  
  func config(data: ProgressTracking?){
    progressItem = data
    if let avatar = data?.assignToPicture, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar){
        self.avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    avatarImageView.layer.borderColor = data?.typeColor.cgColor
    
    nameLabel.text = data?.assignToName ?? ""
    titleLabel.text = data?.assignToJobTitle ?? ""

    let startTime = Constants.formatDate(data?.fromDate, "dd 'Th'MM") ?? ""
    let endTime = Constants.formatDate(data?.toDate, "dd 'Th'MM") ?? ""
    endTimeLabel.text = startTime + " - " + endTime
    //workingLabel.text = data?.name ?? ""
    
    sumaryLabel.text = data?.lastResult//data?.name
    
    if let status = data?.statusDocument {
      self.statusButton.backgroundColor = UIColor(hexString: status.colorCode ?? "AEAEAE")
      self.statusButton.setTitle(status.name ?? "Chưa xác định", for: .normal)
    }
    
    if let percent = data?.percentFinish, percent > 0 {
        percenLabel.isHidden = false
        percenLabel.text = "\(percent)%"
        self.percenLabel.textColor = self.statusButton.backgroundColor
    }
    
    if let files = data?.fileTrackingDocuments, !files.isEmpty {
        btnFiles.isHidden = false
    }
    
    if let persons = data?.recentChilds{
        btnDown.isHidden = false
        var index = 0
        while index < 3 && index < persons.count {
            let view = vwUsers[index]
            view.isHidden = false
            let imageView = imvAvatars[index]
            let user = persons[index]
            if let personPicture: String = user.assignToPicture,
                let personURL = URL(string: Constants.default.domainAddress) {
                imageView.kf.setImage(with: personURL.appendingPathComponent(personPicture), placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
            } else {
                imageView.image = #imageLiteral(resourceName: "ic_avatar_empty")
            }
            
            index += 1
        }
        
        if persons.count > 3 {
            btnMoreUsers.setTitle("+\(persons.count-3)", for: .normal)
            let view = vwUsers[3]
            view.isHidden = false
        }
    }
    
    //setNeedsUpdateConstraints()
    setNeedsLayout()
    layoutIfNeeded()
  }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.selectionStyle = .none
    // Initialization code
    
    btnFiles.isHidden = true
    percenLabel.isHidden = true
    btnDown.isHidden = true
    vwUsers.forEach({ view in
        view.isHidden = true
        view.layer.cornerRadius = view.widthf/2
        view.layer.masksToBounds = true
    })
    
    imvAvatars.forEach({ imv in
        imv.layer.cornerRadius = imv.widthf/2
        imv.layer.masksToBounds = true
    })
    
    avatarImageView.layer.cornerRadius = avatarImageView.size.width/2
    avatarImageView.layer.borderColor = UIColor.clear.cgColor
    avatarImageView.layer.borderWidth = 2
  }
  
    override func prepareForReuse() {
        super.prepareForReuse()
        vwUsers.forEach{$0.isHidden = true}
        btnDown.isHidden = true
        percenLabel.isHidden = true
        percenLabel.text = ""
        btnFiles.isHidden = true
        self.onOpenFiles = nil
        self.onDetailTapped = nil
        progressItem = nil
        avatarImageView.layer.borderColor = UIColor.clear.cgColor
    }
    
    @IBAction func btnFilesClicked(_ sender: Any) {
        if let file = progressItem?.fileTrackingDocuments?.first {
            self.onOpenFiles?(file)
        }
        
    }
    
    @IBAction func btnUserClicked(_ sender: Any) {
        self.onDetailTapped?()
    }
}
