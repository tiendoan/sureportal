//
//  DetailAssignPersonTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/18/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class DetailAssignPersonTableViewCell: UITableViewCell {

  
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var filesButton: UIButton!
    @IBOutlet weak var btnMoreUsers: UIButton!
    @IBOutlet weak var btnExpanded: UIButton!
    
    @IBOutlet var vwUsers: [UIView]!
    @IBOutlet var imvAvatars: [UIImageView]!
    
    var personsData: [ProgressTracking]?
    var attachmentData: [FileDocument]?
    var onExpanded:((Bool) -> Void)?
    var onDetailTapped: (() -> Void)?
    var onOpenFiles: ((FileDocument) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
        
        btnExpanded.layer.cornerRadius = btnExpanded.widthf/2
        btnExpanded.layer.masksToBounds = true
        
        vwUsers.forEach({ btn in
            btn.isHidden = true
            btn.layer.cornerRadius = btn.widthf/2
            btn.layer.masksToBounds = true
        })
        
        imvAvatars.forEach({ imv in
            imv.layer.cornerRadius = imv.widthf/2
            imv.layer.masksToBounds = true
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        vwUsers.forEach{$0.isHidden = true}
        onExpanded = nil
        filesButton.isHidden = true
        onOpenFiles = nil
        onDetailTapped = nil
    }
      
//  override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
//    return collectionView.contentSize
//  }
  
  func config() {
    
    layoutIfNeeded()
  }
  
    func config(person: [ProgressTracking], files: [FileDocument], expanded: Bool, onExpanded:((Bool) -> Void)? = nil) {
        self.personsData = person
        self.attachmentData = files
        self.onExpanded = onExpanded
 
        filesButton.isHidden = files.isEmpty
        self.btnExpanded.isSelected = expanded
        var index = 0
        while index < 3 && index < person.count {
            let view = vwUsers[index]
            view.isHidden = false
            let imageView = imvAvatars[index]
            let user = person[index]
            if let personPicture: String = user.assignToPicture,
                let personURL = URL(string: Constants.default.domainAddress) {
                imageView.kf.setImage(with: personURL.appendingPathComponent(personPicture), placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
            } else {
                imageView.image = #imageLiteral(resourceName: "ic_avatar_empty")
            }
            
            index += 1
        }
        
        if person.count > 3 {
            btnMoreUsers.setTitle("+\(person.count-3)", for: .normal)
            let view = vwUsers[3]
            view.isHidden = false
        }
        
        layoutIfNeeded()
  }

    @IBAction func btnExpandedClicked(_ sender: Any) {
        self.btnExpanded.isSelected = !self.btnExpanded.isSelected
        self.onExpanded?(self.btnExpanded.isSelected)
    }
    
    @IBAction func actionAttachButton(_ sender: UIButton){
        if let file = attachmentData?.first {
            self.onOpenFiles?(file)
        }
    }
  
    @IBAction func btnUserClicked(_ sender: Any) {
        //self.onDetailTapped?()
    }
}
