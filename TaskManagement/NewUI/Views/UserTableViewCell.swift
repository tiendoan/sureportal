//
//  UserTableViewCell.swift
//  TaskManagement
//
//  Created by Scor Doan on 3/14/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        iconImageView.layer.cornerRadius = iconImageView.widthf/2
        iconImageView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUser(_ user: Assignee) {
        self.titleLabel.text = user.text
        self.descLabel.text = user.value
        if let avatarURLStr: String = user.picture,
            let avatarURL: URL = URL(string: Constants.default.domainAddress + avatarURLStr) {
            self.iconImageView.kf.setImage(with: avatarURL, placeholder: UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
    func setUser(_ user: UserProfile) {
        self.titleLabel.text =  user.fullName
        self.descLabel.text = user.loginName
        if let avatarURLStr: String = user.picture,
            let avatarURL: URL = URL(string: Constants.default.domainAddress + avatarURLStr) {
            self.iconImageView.kf.setImage(with: avatarURL, placeholder: UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}
