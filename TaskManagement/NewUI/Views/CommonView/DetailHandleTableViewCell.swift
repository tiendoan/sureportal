//
//  DetailHandleTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/13/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SwipeCellKit

//enum DetailHandleStyle: Int {
//  case attachAndPerson = 1, list = 2, onlyAttach = 3, full = 4
//}

protocol DetailHandleTableViewCellDelegate {
    func expandAll(at: Int)
    func collapseAll(at: Int)
    func onDetail(item: ProgressTracking)
    func backSubPage()
    func openFile(_ file: FileDocument)
    
    func actionHistoryProcess(item: ProgressTracking)
    func actionAssignProcess(item: ProgressTracking)
    func actionProcessProcess(item: ProgressTracking)
    func actionAppraiseProcess(item: ProgressTracking)
    func actionSendMailProcess(item: ProgressTracking)
    func actionFinishProcess(item: ProgressTracking)
    func actionApproveProcess(item: ProgressTracking)
}

class DetailHandleTableViewCell: SwipeTableViewCell {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var sumaryLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var leftLineView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusDocumentLabel: UILabel!
    @IBOutlet weak var tableHeightLayoutConstraint: NSLayoutConstraint!
    //@IBOutlet weak var percentLabel: UILabel!
    
    var detailHandleDelegate: DetailHandleTableViewCellDelegate?
    var index = 0
    var isExpand = false
    //  var style: DetailHandleStyle = .list {
    //    didSet{
    //      self.tableView.reloadData()
    //    }
    //  }
    var data: ProgressTracking?
    var arrChilds = [ProgressTracking]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        tableView.register(withClass: DetailHandleTableViewCell.self)
        tableView.register(withClass: DetailAttachTableViewCell.self)
        tableView.register(withClass: DetailAssignPersonTableViewCell.self)
        tableView.register(withClass: DetailProcessItemCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 95
        tableView.separatorStyle = .none
        // Initialization code
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        avatarImageView.layer.cornerRadius = avatarImageView.size.width/2
        avatarImageView.layer.borderColor = UIColor.clear.cgColor
        avatarImageView.layer.borderWidth = 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = #imageLiteral(resourceName: "ic_avatar_empty")
        arrChilds = [ProgressTracking]()
        //self.tableHeightLayoutConstraint.constant = 0
        self.btnBack.isHidden = true
        avatarImageView.layer.borderColor = UIColor.clear.cgColor
    }
    
    deinit {
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //  override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
    //    var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
    //    size.height += tableView.contentSize.height
    //    return size
    //  }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let obj = object as? UITableView {
            if obj == self.tableView && keyPath == "contentSize" {
                if let newSize = change?[NSKeyValueChangeKey.newKey] as? CGSize {
                    //do stuff here
                    self.tableHeightLayoutConstraint.constant = newSize.height
                    self.layoutIfNeeded()
                }
            }
        }
    }
    
    func config(data: ProgressTracking?, isSubPage:Bool = false){
        self.btnBack.isHidden = !isSubPage
        self.data = data
        self.arrChilds = data?.recentChilds ?? [ProgressTracking]()
        tableView.isHidden = false
        if let avatar = data?.assignToPicture, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar){
            self.avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        self.avatarImageView.layer.borderColor = data?.typeColor.cgColor
        
        nameLabel.text = data?.assignToName ?? ""
        jobTitleLabel.text = data?.assignToJobTitle ?? ""
        let sumary = isSubPage ? data?.lastResult : data?.name
        sumaryLabel.text = sumary ?? ""
        let startTime = Constants.formatDate(data?.fromDate, "dd 'Th'MM") ?? ""
        let endTime = Constants.formatDate(data?.toDate, "dd 'Th'MM") ?? ""
        endTimeLabel.text = startTime + " - " + endTime
        
        tableView.reloadData()
        if let status = data?.statusDocument {
            self.statusView.backgroundColor = UIColor(hexString: status.colorCode ?? "AEAEAE")
            self.statusDocumentLabel.text = status.name ?? "Chưa xác định"
        }
        //    if let percent: Int = data?.percentFinish {
        //      percentLabel.text = "\(percent.string)%"
        //    }
        
        setNeedsLayout()
        layoutIfNeeded()
        self.tableHeightLayoutConstraint.constant = self.tableView.contentSize.height
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.detailHandleDelegate?.backSubPage()
    }
    
}

extension DetailHandleTableViewCell: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if !arrChilds.isEmpty {
            return isExpand ? 2 : 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return arrChilds.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withClass: DetailAssignPersonTableViewCell.self, for: indexPath)
            
            if let persons: [ProgressTracking] = data?.recentChilds,
                let attachments: [FileDocument] = data?.fileTrackingDocuments {
                cell.config(person: persons, files: attachments, expanded:isExpand) { [weak self] (expanded) in
                    guard let sself = self else {
                        return
                    }
                    sself.isExpand = expanded
                    if !expanded {
                        sself.detailHandleDelegate?.collapseAll(at: sself.index)
                    } else {
                        sself.detailHandleDelegate?.expandAll(at: sself.index)
                    }
                }
                
                cell.onOpenFiles = { [weak self] (file) in
                    self?.detailHandleDelegate?.openFile(file)
                }
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withClass: DetailProcessItemCell.self, for: indexPath)
            cell.delegate = self
            if let item = data?.childs?[indexPath.row] {
                cell.config(data: item)
                cell.onDetailTapped = { [weak self] in
                    self?.detailHandleDelegate?.onDetail(item:item)
                }
                cell.onOpenFiles = { [weak self] (file) in
                    self?.detailHandleDelegate?.openFile(file)
                }
            }
            return cell
        }
    }
}

extension DetailHandleTableViewCell: UITableViewDelegate {
    func tableView (_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 48
        }
        
        return UITableViewAutomaticDimension
    }
}

extension DetailHandleTableViewCell: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .right, let item = data?.childs?[indexPath.row]{
            let actions:[DocumentActionObject] = item.actions.reversed()
            var list: [SwipeAction] = []
            
            //History action
            let hitoryAction = SwipeAction(style: .default, title: "XEM LƯỢC SỬ") { action, indexPath in
                self.detailHandleDelegate?.actionHistoryProcess(item: item)
                
            }
            hitoryAction.image = #imageLiteral(resourceName: "ic_action_HistoryTracking").imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
            list.append(hitoryAction)
            
            for i in 0..<actions.count {
                if let actionTypeItem: DocumentAction = actions[i].actionType, actionTypeItem != .history  {
                    let action = SwipeAction(style: .default, title: actions[i].actionName, handler: { (action, indexPath) in
                        debugPrint("Default Board Collection Cell: \(action.debugDescription) - indexPath \(indexPath.row)")
                    })
                    if actionTypeItem == .assign {
                        action.handler = { (action, indexPath) in
                            self.detailHandleDelegate?.actionAssignProcess(item: item)
                        }
                    } else if actionTypeItem == .approve {
                        action.handler = { (action, indexPath) in
                            self.detailHandleDelegate?.actionApproveProcess(item: item)
                        }
                    } else if actionTypeItem == .process {
                        action.handler = { (action, indexPath) in
                            self.detailHandleDelegate?.actionProcessProcess(item: item)
                        }
                    } else if actionTypeItem == .appraise {
                        action.handler = { (action, indexPath) in
                            self.detailHandleDelegate?.actionProcessProcess(item: item)
                        }
                    } else if actionTypeItem == .sendMail {
                        action.handler = { (action, indexPath) in
                            self.detailHandleDelegate?.actionSendMailProcess(item: item)
                        }
                    } else if actionTypeItem == .finish {
                        action.handler = { (action, indexPath) in
                            self.detailHandleDelegate?.actionFinishProcess(item: item)
                        }
                    }

                    action.image = actionTypeItem.image
                    list.append(action)
                }
            }
            
            for item in list {
                item.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                item.textColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                item.font = UIFont(name: "Muli-Bold", size: 10)
            }
            return list
        }
        else{
            return []
        }
    }
    
    func tableView (_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        return options
    }
}
