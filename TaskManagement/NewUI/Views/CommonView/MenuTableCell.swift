//
//  MenuTableCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/9/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

protocol MenuTableCellDelegate {
    func menuSelected(_ parent:Int,_ selectIndex: Int)
}

class MenuTableCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lbTitle:UILabel!
    @IBOutlet fileprivate weak var lbNumber:UILabel!
    @IBOutlet fileprivate weak var tableView:UITableView!
    @IBOutlet fileprivate weak var heightTable:NSLayoutConstraint!
    @IBOutlet fileprivate weak var widthLBCount:NSLayoutConstraint!
    
    fileprivate var menu:Menu? = nil
    
    var delegate: MenuTableCellDelegate?
    var selectIndex = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        
        lbTitle.textColor = UIColor.white
        lbNumber.textColor = UIColor.white
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CELL")
        tableView.rowHeight = 30
        tableView.dataSource = self
        tableView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func config(_ menu:Menu){
        
        lbTitle.text = menu.name?.uppercased() ?? ""
    
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as? String ?? ""

        if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE && menu.code != nil )
        {
            var count:Int = 0
            let navDigitalObject:[NavDigitalObject] = DataManager.shared.navigDigitalObject.filter({ $0.Code!.uppercased() == menu.code!.uppercased() })
            if navDigitalObject != nil && navDigitalObject.count >  0
            {
                count  = navDigitalObject[0].Total!
            }
            lbNumber.text = menu.count != nil ? ( count == 0 ? "" : "\(count)") : ""
        }
        else
        {
             lbNumber.text = menu.count != nil ? ( menu.count! == 0 ? "" : "\(menu.count!)") : ""
        }
       
        if lbNumber.text != ""{
            lbNumber.backgroundColor = UIColor.orange
//            widthLBCount.constant = 30
            lbNumber.sizeToFit()
        }
        else{
            lbNumber.backgroundColor = UIColor.clear
//            widthLBCount.constant = 0
        }
        heightTable.constant = CGFloat((menu.childrens?.count ?? 0) * 30 )
        self.layoutIfNeeded()
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menu?.childrens?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name: "Muli-Light", size: 14)!
        
        let menu = self.menu?.childrens?[indexPath.row] ?? nil
        cell.textLabel?.text = "  " + (menu?.name ?? "")

        if selectIndex == indexPath.row{
            cell.textLabel?.font = UIFont(name: "Muli-Black", size: 14)!
        }
        else{
            cell.textLabel?.font = UIFont(name: "Muli-Light", size: 14)!
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.menuSelected(self.tag ,indexPath.row)
    }
}
