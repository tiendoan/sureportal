//
//  EmployeeCollectionViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/14/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class EmployeeCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var imageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    layoutIfNeeded()
//    imageView.cornerRadius = imageView.height/2
    // Initialization code
  }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
  
}
