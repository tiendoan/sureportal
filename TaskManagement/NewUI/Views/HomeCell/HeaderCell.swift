//
//  HeaderCell.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/7/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTotal: UILabel!
    
    var header: String = "" {
        didSet {
            self.setData()
        }
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    func setData() {
        switch header {
        case ActiveModuleName.Document.rawValue:
            lbTitle.text = LocalizationString.Document
        case ActiveModuleName.Task.rawValue:
            lbTitle.text = LocalizationString.Task
        case ActiveModuleName.DigitalProcedure.rawValue:
            lbTitle.text = LocalizationString.DigitalProcedure
        case ActiveModuleName.TimeSheet.rawValue:
            lbTitle.text = LocalizationString.TimeSheet
        case ActiveModuleName.BusinessTripRequest.rawValue:
            lbTitle.text = LocalizationString.BusinessTripRequest
        case ActiveModuleName.Vehicle.rawValue:
            lbTitle.text = LocalizationString.Vehicle
        case ActiveModuleName.MeetingRoom.rawValue:
            lbTitle.text = LocalizationString.MeetingRoom
        case ActiveModuleName.Stationery.rawValue:
            lbTitle.text = LocalizationString.Stationery
        case ActiveModuleName.DocumentLibrary.rawValue:
            lbTitle.text = LocalizationString.DocumentLibrary
        case ActiveModuleName.HelpDesk.rawValue:
            lbTitle.text = LocalizationString.HelpDesk
        default:
            break
        }
    }
    
}
