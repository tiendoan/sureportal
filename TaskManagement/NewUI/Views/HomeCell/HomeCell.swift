//
//  HomeCell.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/7/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

protocol HomeCellDelegate: class {
    func btnAttachTapped(indexPath: IndexPath)
}

class HomeCell: UITableViewCell {
    
    @IBOutlet weak var lbContent    : UILabel!
    @IBOutlet weak var lbTitle      : UILabel!
    @IBOutlet weak var lbDate       : UILabel!
    @IBOutlet weak var lbStatus     : UILabel!
    @IBOutlet weak var vStatus      : UIView!
    @IBOutlet weak var imgAttach    : UIImageView!
    @IBOutlet weak var btnAttach    : UIButton!
    @IBOutlet weak var lbFileCount : UILabel!
    
    weak var delegate: HomeCellDelegate?
    var currentTask: ItemTask? {
        didSet {
            self.setData()
        }
    }
    
    var indexPath: IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        imgAttach.isHidden = true
    }
    
    override func draw(_ rect: CGRect) {
        vStatus.layer.cornerRadius = vStatus.bounds.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    func setData() {
        guard let task = currentTask else { return }
        lbContent.text = task.summary
        lbTitle.text = task.title
        lbDate.text = task.timeString
        lbStatus.text = task.status
        if let file = task.file {
            if file.count == 0 {
                hideAttach()
            } else if file.count == 1 {
                showAttach()
                lbFileCount.isHidden = true
            } else {
                showAttach()
                lbFileCount.text = "\(file.count)"
            }
            
        } else {
            hideAttach()
        }
    }
    
    func hideAttach() {
        imgAttach.isHidden = true
        btnAttach.isHidden = true
        lbFileCount.isHidden = true
    }
    
    func showAttach() {
        imgAttach.isHidden = false
        btnAttach.isHidden = false
        lbFileCount.isHidden = false
    }
    
    @IBAction func btnAttachTapped() {
        guard let index = indexPath else { return }
        delegate?.btnAttachTapped(indexPath: index)
    }
}
