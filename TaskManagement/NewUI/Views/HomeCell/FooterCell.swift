//
//  FooterCell.swift
//  TaskManagement
//
//  Created by DINH VAN TIEN on 11/7/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

protocol FooterCellDelegate: class {
    func btnMoreTapped(indexPath: IndexPath)
}

class FooterCell: UITableViewCell {

    @IBOutlet weak var vNoData: UIView!
    @IBOutlet weak var btnMore: UIButton!
    
    weak var delegate: FooterCellDelegate?
    var indexPath: IndexPath = IndexPath(item: 0, section: 0)
    var hideFooter = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    @IBAction func btnMoreTapped() {
        delegate?.btnMoreTapped(indexPath: indexPath)
    }
    
}
