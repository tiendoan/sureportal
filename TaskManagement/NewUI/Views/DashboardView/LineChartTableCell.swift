//
//  LineChartTableCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/7/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Charts
import DropDown

enum ViewType: String {
    case week = "week", month = "month", year = "year"
}

protocol LineChartCellDelegate {
    func lineChartCellSelectViewType(viewType: ViewType)
}

class LineChartTableCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet fileprivate weak var btViewType:UIButton!
    @IBOutlet fileprivate weak var viewChart:LineChartView!
    @IBOutlet fileprivate weak var collectionView:UICollectionView!
    
    fileprivate var dropDown = DropDown()
    fileprivate var viewType:ViewType = .month
    var delegate:LineChartCellDelegate?
    
    fileprivate var titles:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
        self.selectionStyle = .none
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CELL")
        //collectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        
        setupDropDown()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setupDropDown(){
        DropDown.startListeningToKeyboard()
        
        DropDown.appearance().textColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
        DropDown.appearance().textFont = UIFont(name: "Muli-Regular", size: 19)!
        //dropDown.anchorView = self.navigationItem.rightBarButtonItem
        dropDown.dataSource = ["Tuần", "Tháng", "Năm"]
        dropDown.cellNib = UINib(nibName: "ActionPopupDropDownCell", bundle: nil)
        dropDown.width = self.widthf - 20
        dropDown.bottomOffset = CGPoint(x: 50, y: 50)
        dropDown.selectionAction = {  (index: Int, item: String) in
            switch index {
            case 0:
                self.viewType = .week
            case 1:
                self.viewType = .month
            case 2:
                self.viewType = .year
            default:
                break
            }
            if self.viewType == .month{
                self.btViewType.setTitle("Tháng", for: .normal)
            }
            else if self.viewType == .year{
                self.btViewType.setTitle("Năm", for: .normal)
            }
            else{
                self.btViewType.setTitle("Tuần", for: .normal)
            }
            
            self.delegate?.lineChartCellSelectViewType(viewType: self.viewType)
        }
    }
    @IBAction func pressesViewType(_ sender: UIButton){
        dropDown.show()
    }
    
    
    func setupChart(_ charts:[Chart],_ viewType:ViewType){
        
        self.titles.removeAll()
        let maxValue = charts.max(by: { (a, b) -> Bool in
            return (a.data ?? 0) < (b.data ?? 0)
        })?.data ?? 200
        
        self.viewType = viewType
        if self.viewType == .month{
            self.btViewType.setTitle("Tháng", for: .normal)
        }
        else if self.viewType == .year{
            self.btViewType.setTitle("Năm", for: .normal)
        }
        else{
            self.btViewType.setTitle("Tuần", for: .normal)
        }
        
        viewChart.chartDescription?.enabled = false
        viewChart.dragEnabled = true
        viewChart.setScaleEnabled(false)
        viewChart.pinchZoomEnabled = false
        viewChart.drawGridBackgroundEnabled = false
        
        
        
        let llXAxis = ChartLimitLine(limit: 10.0, label: "Index 10")
        llXAxis.lineWidth = 4.0
        llXAxis.lineDashLengths = [10, 10, 0]
        llXAxis.labelPosition = .rightBottom
        llXAxis.valueFont = UIFont.systemFont(ofSize: 10)
        
        viewChart.xAxis.gridLineDashLengths = [10,10]
        viewChart.xAxis.gridLineDashPhase = 0
        viewChart.xAxis.enabled = true
        viewChart.xAxis.labelPosition = .bottom
        viewChart.xAxis.labelTextColor = .clear
        
        let ll1 = ChartLimitLine(limit: 150, label: "Upper Limit")
        ll1.lineWidth = 4.0
        ll1.lineDashLengths = [5,5]
        ll1.labelPosition = .rightTop
        ll1.valueFont = UIFont.systemFont(ofSize: 10)
        
        let ll2 = ChartLimitLine(limit: 0, label: "")
        ll2.lineWidth = 0.5
        ll2.lineDashLengths = [5,0]
        ll2.labelPosition = .rightBottom
        ll2.lineColor = UIColor.black
        ll2.valueFont = UIFont.systemFont(ofSize: 10)
        
        let leftAxis = viewChart.leftAxis
        leftAxis.removeAllLimitLines()
        //leftAxis.addLimitLine(ll1)
        leftAxis.addLimitLine(ll2)
        
        leftAxis.axisMaximum = Double(maxValue) + Double(maxValue/5)
        leftAxis.axisMinimum = 0
        leftAxis.gridLineDashLengths = [5,5]
        leftAxis.drawZeroLineEnabled = false
        leftAxis.drawLimitLinesBehindDataEnabled = true
        
        //viewChart.leftAxis.enabled = false
        viewChart.rightAxis.enabled = false
        viewChart.legend.form = .line
        
        setData(charts)
        viewChart.animate(xAxisDuration: 0)
        
    }
    
    func setData(_ charts:[Chart]){
        
        
        var values1:[ChartDataEntry] = []
        for i in 0..<charts.count {
            let val = charts[i].data ?? 0
            let data  = ChartDataEntry(x: Double(i), y: Double(val))
            values1.append(data)
            titles.append(charts[i].name ?? "")
        }
        var set1:LineChartDataSet? = nil
        set1 = LineChartDataSet(values: values1, label: nil)
        set1?.drawIconsEnabled = false
        set1?.lineDashLengths = [5, 0]
        set1?.highlightLineDashLengths = [5, 2.5]
        set1?.highlightEnabled = false
        set1?.setColor(UIColor(red: 28, green: 179, blue: 246))
        set1?.setCircleColor(UIColor(red: 113, green: 169, blue: 255))
        set1?.lineWidth = 2.0
        set1?.circleRadius = 4.0
        set1?.drawCircleHoleEnabled = false
        set1?.valueFont = UIFont.systemFont(ofSize: 9)
        set1?.formLineDashLengths = [0, 0]
        set1?.formLineWidth = 0
        set1?.formSize = 15.0
        set1?.drawValuesEnabled = true
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.locale = Locale.current
        let valuesNumberFormatter = ChartValueFormatter(numberFormatter: numberFormatter)
        set1?.valueFormatter = valuesNumberFormatter
        
        let dataSets:[LineChartDataSet] = [set1!]
        let data = LineChartData(dataSets: dataSets)
        viewChart.data = data;

        collectionView.reloadData()
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/CGFloat(titles.count), height: collectionView.bounds.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath)
        
        var lbTitle = cell.viewWithTag(1) as? UILabel
        if lbTitle == nil{
            lbTitle = UILabel()
            lbTitle?.tag = 1
            lbTitle?.font = UIFont(name: "Muli-Light", size: 12.0)!
            lbTitle?.backgroundColor = UIColor.clear
            cell.addSubview(lbTitle!)
            lbTitle?.snp.makeConstraints({ (make) in
                //make.top.equalTo(0)
                //make.left.equalTo(5)
                //make.right.equalTo(-5)
                //make.height.equalTo(20)
                make.top.left.right.bottom.equalToSuperview()
            })
        }
        if indexPath.row < titles.count/3{
            lbTitle?.textAlignment = .left
        }
        else if indexPath.row < (titles.count/3)*2{
            lbTitle?.textAlignment = .center
        }
        else{
            lbTitle?.textAlignment = .right
        }
        lbTitle?.text = titles[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class ChartValueFormatter: NSObject, IValueFormatter {
    fileprivate var numberFormatter: NumberFormatter?
    
    convenience init(numberFormatter: NumberFormatter) {
        self.init()
        self.numberFormatter = numberFormatter
    }
    
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        guard let numberFormatter = numberFormatter
            else {
                return ""
        }
        return numberFormatter.string(for: value)!
    }
}
