//
//  CountDownView.swift
//  TaskManagement
//
//  Created by Scor Doan on 8/15/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit


class CountDownView: UIView {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    var contentView: UIView!
    var timer: Timer?
    var seconds: TimeInterval = 0
    var isRunning = false
    var countDownDate: Date?
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CountDownView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    var nibName: String {
        return String(describing: type(of: self))
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInit()
    }
    
    func setupInit() {
        contentView = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as! UIView
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    func stop() {
        timer?.invalidate()
        timer = nil
        isRunning = false
        countDownDate = nil
        resetCountDown()
    }
    
    func updateUI() {
        guard let date = countDownDate else {
            resetCountDown()
            return
        }
        self.seconds = date.timeIntervalSince1970 - Date().timeIntervalSince1970
        let days = Int(seconds) / 3600 / 24
        let hours = Int(seconds) / 3600 % 24
        let minutes = Int(seconds) / 60 % 60
        let second =  Int(seconds) % 60
        
        
        dayLabel.text = String(format: "%02d", days)
        hourLabel.text = String(format: "%02d", hours)
        minuteLabel.text = String(format: "%02d", minutes)
        secondLabel.text = String(format: "%02d", second)
    }
    
    func startCountDown(with date: Date) {
        self.stop()
        self.countDownDate = date
        self.updateUI()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (_) in
            self.updateUI()
        })
        
    }
    
    func resetCountDown() {
        dayLabel.text = "00"
        hourLabel.text = "00"
        minuteLabel.text = "00"
        secondLabel.text = "00"
    }
}
