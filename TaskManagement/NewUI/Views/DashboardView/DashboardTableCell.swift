//
//  DashboardTableCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/7/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

protocol DashboardTableCellDelegate {
  func taskCellDidSelected()
  func taskCellAttachSelected(_ index: IndexPath)
}

class DashboardTableCell: UITableViewCell,UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet fileprivate weak var lbTitle:UILabel!
  @IBOutlet fileprivate weak var tableView:UITableView!
  @IBOutlet fileprivate weak var btViewMore:UIButton!
  
  var delegate: DashboardTableCellDelegate?
  
  var listTaskBoard:[TaskBoard] = []
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.white
    self.selectionStyle = .none
    
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(withClass: TaskTableCell.self)
  }
  
  
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func config(_ listTaskBoard:[TaskBoard]){
    self.listTaskBoard.removeAll()
    self.listTaskBoard.append(contentsOf: listTaskBoard)
    tableView.reloadData()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return listTaskBoard.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 150
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TaskTableCell", for: indexPath) as! TaskTableCell
    cell.btAttach.tag = indexPath.row
    cell.btAttach.addTarget(self, action: #selector(self.pressesBTAttach(_:indexPath:)), for: .touchUpInside)
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.delegate?.taskCellDidSelected()
  }
  
  func pressesBTAttach(_ sender: UIButton, indexPath: IndexPath){
    self.delegate?.taskCellAttachSelected(indexPath)
  }
}
