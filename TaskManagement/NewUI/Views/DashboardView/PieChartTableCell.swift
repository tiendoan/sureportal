//
//  PieChartTableCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Charts

class PieChartTableCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
  
    @IBOutlet fileprivate weak var lbTitle:UILabel!
    @IBOutlet fileprivate weak var viewChart:PieChartView!
    @IBOutlet fileprivate weak var lbTotal:UILabel!
    @IBOutlet fileprivate weak var lbTitleTotal:UILabel!
    @IBOutlet fileprivate weak var collectionView:UICollectionView!

    @IBOutlet weak var lbDate: UILabel!
    fileprivate var colors:[UIColor] = []
    fileprivate var value:[Int] = []
    fileprivate var strTitle:[String] = []
  
    var onReload:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
        self.selectionStyle = .none

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CELL")
    }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func config(_ charts:[Chart]){
    colors.removeAll()
    value.removeAll()
    strTitle.removeAll()
    setupDate()
    setupPieChart(charts)
  }
    
    @IBAction func reloadButtonClicked(_ sender: Any) {
        self.onReload?()
    }
    
    func setupDate() {
        let currDate = Date()
        let lastDate = currDate.adding(.month, value: -6)
        let currDateString = currDate.stringFromFormat("dd/MM/yyyy")
        let lastDateString = lastDate.stringFromFormat("dd/MM/yyyy")
        let dateString = "Từ ngày \(lastDateString) đến ngày \(currDateString)"
        let attributedString = NSMutableAttributedString(string: dateString)
        
        let currDateRange = (dateString as NSString).range(of: currDateString)
        let lastDateRange = (dateString as NSString).range(of: lastDateString)
        
        
        attributedString.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName:UIColor.black.withAlphaComponent(0.7)], range: NSMakeRange(0, dateString.length))
        attributedString.addAttributes([NSForegroundColorAttributeName:UIColor.blue], range: currDateRange)
        attributedString.addAttributes([NSForegroundColorAttributeName:UIColor.blue], range: lastDateRange)
        
        self.lbDate.attributedText = attributedString
    }
  
  func setupPieChart(_ charts:[Chart]){
    
    self.viewChart.usePercentValuesEnabled = true
    self.viewChart.drawEntryLabelsEnabled = false
    self.viewChart.drawSlicesUnderHoleEnabled = false
    self.viewChart.holeRadiusPercent = 0.8
    self.viewChart.transparentCircleRadiusPercent = 0
    self.viewChart.chartDescription?.enabled = false
    self.viewChart.drawCenterTextEnabled = false
    
    self.viewChart.drawHoleEnabled = true
    self.viewChart.rotationAngle = 0.0
    self.viewChart.rotationEnabled = false
    self.viewChart.highlightPerTapEnabled = false
    
    let l = self.viewChart.legend
    l.horizontalAlignment = .left
    l.verticalAlignment = .bottom
    l.orientation = .horizontal
    l.form = .line
    l.drawInside = false
    l.xEntrySpace = 10.0
    l.yEntrySpace = 20.0
    l.formLineWidth = 20
    l.font = UIFont(name: "Muli-Light", size: 14.0)!
    l.enabled = false
    setupDataChart(charts)
    self.viewChart.animate(xAxisDuration: 0, easingOption: .easeOutBack)
  }
  
  func setupDataChart(_ charts:[Chart]) {
    var values:[PieChartDataEntry] = []
    for i in 0..<charts.count{
      let name = charts[i].name ?? ""
      if name.uppercased() == "TOTAL"{
        lbTotal.text = charts[i].data != nil ? "\(charts[i].data!)" : "0"
        lbTitleTotal.text = "văn bản"
      }
      else{
        let val = charts[i].data ?? 0
        let pie:PieChartDataEntry = PieChartDataEntry(value: Double(val), label: "\(val) " + name)
        values.append(pie)
        colors.append(UIColor(hexString: charts[i].color ?? "#555555") ?? UIColor(hexString:"#555555")!)
        value.append(val)
        strTitle.append(name)
      }
    }
    let dataSet = PieChartDataSet(values: values, label: "")
    dataSet.values = values
    dataSet.drawIconsEnabled = false
    dataSet.sliceSpace = 0.0
    dataSet.iconsOffset = CGPoint(x: 0, y: 40)
    dataSet.colors = colors
    let data = PieChartData(dataSet: dataSet)
    let pFormatter = NumberFormatter()
    pFormatter.numberStyle = .percent
    pFormatter.maximumFractionDigits = 1
    pFormatter.multiplier = NSNumber(floatLiteral: 1.0)
    pFormatter.percentSymbol = "%"
    let a = DefaultValueFormatter(formatter: pFormatter)
    data.setValueFormatter(a)
    data.setValueFont(UIFont.systemFont(ofSize: 15))
    data.setValueTextColor(UIColor.clear)
    self.viewChart.data = data;
    self.viewChart.highlightValue(nil)
    self.collectionView.reloadData()
  }

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return colors.count
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.bounds.size.width/CGFloat(colors.count), height: 50)
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath)
    
    var view = cell.viewWithTag(1)
    if (view == nil) {
      view = UIView()
      view?.tag = 1
      cell.addSubview(view!)
      view?.snp.makeConstraints({ (make) in
        make.top.equalTo(5)
        make.left.equalTo(5)
        make.right.equalTo(-5)
        make.height.equalTo(7)
      })
    }
    view?.backgroundColor = colors[indexPath.row]
    var lbCount = cell.viewWithTag(2) as? UILabel
    if lbCount == nil {
      lbCount = UILabel()
      lbCount?.tag = 2
      lbCount?.font = UIFont(name: "Muli-Bold", size: 14.0)!
      lbCount?.textAlignment = .center
      cell.addSubview(lbCount!)
      lbCount?.snp.makeConstraints({ (make) in
        make.top.equalTo(15)
        make.left.equalTo(5)
        make.right.equalTo(-5)
        make.height.equalTo(15)
      })
    }
    lbCount?.text = "\(value[indexPath.row])"
    var lbTitle = cell.viewWithTag(3) as? UILabel
    if lbTitle == nil {
      lbTitle = UILabel()
      lbTitle?.tag = 3
      lbTitle?.font = UIFont(name: "Muli-Light", size: 14.0)!
      lbTitle?.textAlignment = .center
      cell.addSubview(lbTitle!)
      lbTitle?.snp.makeConstraints({ (make) in
        make.top.equalTo(30)
        make.left.equalTo(5)
        make.right.equalTo(-5)
        make.height.equalTo(20)
      })
    }
    lbTitle?.text = strTitle[indexPath.row]    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}
