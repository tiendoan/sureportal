//
//  AnnouncementViewCell.swift
//  TaskManagement
//
//  Created by Scor Doan on 8/10/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class AnnouncementViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var countDownView: CountDownView?
    @IBOutlet weak var eventButton: UIButton?
   
    
    var announcement: Announcement?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        coverImage.image = nil
        titleLabel.text = ""
        dateLabel.text = ""
        detailLabel.text = ""
        
        //countDownView?.resetCountDown()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
//        coverImage.image = nil
        titleLabel.text = ""
        dateLabel.text = ""
        detailLabel.text = ""
        
        stopCountDown()
    }
    
    func stopCountDown() {
        countDownView?.stop()
    }
    
    func startCountDown() {
        if let startDate = announcement?.startDate, announcement?.type == .event, startDate > Date() {
            countDownView?.startCountDown(with: startDate)
        } else {
            countDownView?.stop()
        }
        
    }
    
    func configCell(_ announcement: Announcement) {
        self.announcement = announcement

        if let thumb = announcement.thumbnail, let url = URL(string: thumb) {
            self.coverImage.setImage(url: url)
        }
        
        titleLabel.text = announcement.subject
        let format = announcement.type == .event ? "dd 'Th'MM, yyyy - HH:mm" : "dd 'Tháng' MM, yyyy"
        
        if announcement.type == .event {
            detailLabel.text = announcement.location
            dateLabel.text = announcement.startDate?.toFormatDate(format: format)
            if let startDate = announcement.startDate, startDate > Date() {
                countDownView?.isHidden = false
                eventButton?.isHidden = true
            } else {
                countDownView?.isHidden = true
                eventButton?.isHidden = false
            }
        } else {
            detailLabel.text = announcement.summary
            dateLabel.text = announcement.createdDate?.toFormatDate(format: format)
        }
        
    }
}
