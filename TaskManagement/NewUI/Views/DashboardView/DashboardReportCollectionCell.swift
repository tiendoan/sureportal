//
//  DashboardReportCollectionCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/7/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class DashboardReportCollectionCell: UICollectionViewCell {

    @IBOutlet fileprivate weak var lbTitle:UILabel!
    @IBOutlet fileprivate weak var lbSubTittle:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
    }
    
    func config(index: Int){
        switch index {
        case 0:
            self.lbTitle.text = "06"
            self.lbSubTittle.text = "Công việc trễ hẹn"
            self.lbSubTittle.textColor = UIColor(red: 208, green: 2, blue: 27)
            self.lbTitle.textColor = UIColor(red: 208, green: 2, blue: 27)
            break
        case 1:
            self.lbTitle.text = "23"
            self.lbSubTittle.text = "Công việc đang làm"
            self.lbSubTittle.textColor = UIColor(red: 74, green: 144, blue: 226)
            self.lbTitle.textColor = UIColor(red: 74, green: 144, blue: 226)
            break
        case 2:
            self.lbTitle.text = "173"
            self.lbSubTittle.text = "Công việc hoàn tất"
            self.lbSubTittle.textColor = UIColor(red: 126, green: 211, blue: 33)
            self.lbTitle.textColor = UIColor(red: 126, green: 211, blue: 33)
            break
        case 3:
            self.lbTitle.text = "11"
            self.lbSubTittle.text = "Chờ phản hồi"
            self.lbSubTittle.textColor = UIColor(red: 245, green: 166, blue: 35)
            self.lbTitle.textColor = UIColor(red: 245, green: 166, blue: 35)
            break
        default:
            self.lbTitle.text = ""
            self.lbSubTittle.text = ""
            break
        }
    }

}
