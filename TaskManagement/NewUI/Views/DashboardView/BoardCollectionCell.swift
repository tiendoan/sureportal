//
//  BoardCollectionCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/7/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwipeCellKit
import DropDown
import FSCalendar
enum ContraintPaddingAnimation : CGFloat {
    case chartType = 195
    case reportType = 135
    case addMeeting = 75
}

protocol BoardCollectionCellDelegate:class {
    
    func taskCellDidSelected(document: Document?, itemIndex:IndexPath?, rowIndex: IndexPath?)
    func taskCellAttachSelected(document: Document?, rowIndex: IndexPath?)
    func actionHistoryProcess(document: Document?)
    func assignTaskProcess(document: Document?, index: IndexPath?)
    func approveTaskProcess(document: Document?, index: IndexPath?)
    func processTaskProcess(document: Document?, index: IndexPath?)
    func appriseTaskProcess(document: Document?, index: IndexPath?)
    func emailTaskProcess(document: Document?, index: IndexPath?)
    func showAllProcess(document: Document?, index: IndexPath?)
    func finishTaskProcess(document: Document?, index: IndexPath?)
    
    func showController(controller: UIViewController, isPush: Bool)
    func showDetailViewController(procedure: ProdureDigital)
    func showAttachDetailViewController(fileDocument: [FileDocument],procedureId:String)
    func assignTaskProcedure()
    func showAllActionItem(procedure:ProdureDigital)
    func showSelectedActionItem(procedure:ProdureDigital, itemAction: ItemActions)
    
    func assignBookingMeetingRoom(orgID: String)
    func viewReportBookingMeetingRoom()
    func viewChartBookingMeetingRoom()
    func viewHeadTitle(objectHeader:GetMeetingRoomObject)
    func assignVehicelType(arrayMeetingBookingRoomObject:[GetMeetingRoomObject],location:MeetingRoomObject,department:MeetingRoomObject,OrgID: String,departmentUsr:[GetMeetingRoomObject])
    
    func callbackGenerateDate(startTime: String, endTime: String)
  
}

protocol colapseExpandDelegate {
    func colapseExpandAction(isColapse:Bool)
}

class BoardCollectionCell: UICollectionViewCell, UITableViewDataSource, UITableViewDelegate,colapseExpandDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet fileprivate weak var tableview:UITableView!
    @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
    @IBOutlet fileprivate weak var btItemCheck:UIButton!
    @IBOutlet fileprivate weak var viewMultiSelect:UIView!
    @IBOutlet fileprivate weak var customView:UIView!
    @IBOutlet weak var addJobButton: UIButton!
    @IBOutlet weak var addMeetingBtn: UIButton!
    @IBOutlet weak var reportBtn: UIButton!
    @IBOutlet weak var chartBtn: UIButton!
//    Properties Constraint
    @IBOutlet weak var paddingButtonChart: NSLayoutConstraint!
    @IBOutlet weak var paddingButtonMore: NSLayoutConstraint!
    @IBOutlet weak var paddingButtonReport: NSLayoutConstraint!
    @IBOutlet weak var paddingButtonAddMetting: NSLayoutConstraint!
    
    
    var currentLocation:MeetingRoomObject = MeetingRoomObject()
    var currentDepartment:MeetingRoomObject = MeetingRoomObject()
    var departmentUser : [GetMeetingRoomObject] = []
    
    fileprivate weak var calendar: FSCalendar!
    fileprivate var headerView: TableSectionHeader!
    var bookingRoomStatus:String!
    weak var delegate: BoardCollectionCellDelegate!
    var indexItem: IndexPath?
    var needReloadDigital:Bool = false
    fileprivate var menu:Menu!
    fileprivate var documents:[Document] = []
    fileprivate var documentData:DocumentData?
    var arrayBookingDic:Dictionary = [String: [GetDetailMeetingRoomObject]]()
    var arrayVehicelDic: Dictionary = [String: [DetailVehicelsBookingObject]]()
    var produreItem:[ProdureDigital] = []
    var totalPageDigital:Int = 1
    var currentPageDigital:Int = 1
    var currentStatusProcedure:Int = 100
    var selectedForm:String!
    var selectedTo:String!
    fileprivate var page = 1
    fileprivate var isLoadMore = false
    fileprivate var canLoadMore = false
    fileprivate var allSelect:[Int] = []
    fileprivate var cellHeightsCache: [IndexPath: CGFloat] = [:]
    var isSelectedAdmore:Bool = true
    var isFirstTimeCalendar:Bool = true
    let taskDropDown: DropDown = DropDown()
   
    func colapseExpandAction(isColapse: Bool) {
        self.isSelectedAdmore = isColapse
        self.tableview.reloadData()
    }

   var arrayMeetingBookingRoomObject: [GetMeetingRoomObject] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        tableview.separatorStyle = .none
        tableview.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        tableview.dataSource = self
        tableview.delegate = self
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = UITableViewAutomaticDimension
        tableview.register(withClass: TaskTableCell.self)
        tableview.register(withClass: BookingMeetingRoomCell.self)

//
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        tableview.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        let headerNibTitle = UINib.init(nibName: "MeetingTitleView", bundle: Bundle.main)
        tableview.register(headerNibTitle, forHeaderFooterViewReuseIdentifier: "MeetingTitleView")
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableview.addSubview(refresh)
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: (#selector(handleLongPress(_:))))
        self.tableview.addGestureRecognizer(longPressGesture)
        self.vwContent.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        allSelect = []
        documents = []
    }
    
    func specialCaseModule(keyValue:String) -> Bool
    {
        if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE && keyValue != Constants.MEETING_ROOM_CODE && keyValue != Constants.VEHICEL_CODE )
        {
            return false
        }
        return true
    }
    
    @IBAction func viewChartForm(_ sender: Any) {
        if ( self.delegate != nil )
        {
            self.delegate?.viewChartBookingMeetingRoom()
        }

    }
    @IBAction func viewReportForm(_ sender: Any) {
        if ( self.delegate != nil )
        {
            let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
            if ( keyValue == Constants.MEETING_ROOM_CODE )
            {
                self.delegate?.viewReportBookingMeetingRoom()
            }
            else
            {

            }
        }

    }
    @IBAction func createNewForm(_ sender: Any) {
        if ( self.delegate != nil )
        {
            let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
            if ( keyValue == Constants.MEETING_ROOM_CODE )
            {
                self.delegate?.assignBookingMeetingRoom(orgID: "")
            }
            else
            {
                self.delegate.assignVehicelType(arrayMeetingBookingRoomObject: self.arrayMeetingBookingRoomObject, location: self.currentLocation, department: self.currentDepartment,OrgID: "",departmentUsr: self.departmentUser)
            }

        }
    }
    @IBAction func addJobButtonClicked(_ sender: Any) {
         let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) )
        {
            if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE ) {
                self.delegate?.assignTaskProcedure()
            }
            else
            {
                let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
                if ( keyValue == Constants.MEETING_ROOM_CODE )
                {
                    self.delegate?.assignBookingMeetingRoom(orgID: "")
                }
                else
                {
                    self.delegate.assignVehicelType(arrayMeetingBookingRoomObject: self.arrayMeetingBookingRoomObject, location: self.currentLocation, department: self.currentDepartment,OrgID: "",departmentUsr: self.departmentUser)
                }
            }
        }
        else
        {
            self.delegate?.assignTaskProcess(document: nil, index: nil)
        }
    }
    
    func animationAddMore(){
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) )
        {
            if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE ) {

                if ( addJobButton.isSelected == false )
                {
                    addMeetingBtn.isHidden = false
                    reportBtn.isHidden = false
                    chartBtn.isHidden = true
                    
                    addJobButton.isSelected = !addJobButton.isSelected
                    self.paddingButtonChart.constant = ContraintPaddingAnimation.chartType.rawValue
                    self.paddingButtonReport.constant = ContraintPaddingAnimation.reportType.rawValue
                    self.paddingButtonAddMetting.constant = ContraintPaddingAnimation.addMeeting.rawValue
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.layoutIfNeeded()
                    })

                 }
                else
                {
                    addMeetingBtn.isHidden = true
                    reportBtn.isHidden = true
                    chartBtn.isHidden = true
                    addJobButton.isSelected = !addJobButton.isSelected
                    self.paddingButtonChart.constant = 15
                    self.paddingButtonReport.constant = 15
                    self.paddingButtonAddMetting.constant = 15

                    UIView.animate(withDuration: 0.5, animations: {
                        self.layoutIfNeeded()
                    })
                    
                }
            }
        }
    }
    func loadViewButtonActionFromView(){
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) )
        {
            if ( keyValue != Constants.DIGITAL_PROCEDURE_CODE ) {
                addMeetingBtn.isHidden = true
                reportBtn.isHidden = true
                chartBtn.isHidden = true
                addJobButton.setBackgroundImage(UIImage(named: "icom_menu_add"), for: .normal)
                addJobButton.setBackgroundImage(UIImage(named: "icom_menu_add"), for: .selected)
            }
            else
            {
                addMeetingBtn.isHidden = true
                reportBtn.isHidden = true
                chartBtn.isHidden = true
                addJobButton.setBackgroundImage(UIImage(named: "icom_menu_add"), for: .normal)
                addJobButton.setBackgroundImage(UIImage(named: "icom_menu_add"), for: .selected)
                
            }
        }
        else
        {
            addMeetingBtn.isHidden = true
            reportBtn.isHidden = true
            chartBtn.isHidden = true
            addJobButton.setBackgroundImage(UIImage(named: "icom_menu_add"), for: .normal)
            addJobButton.setBackgroundImage(UIImage(named: "icom_menu_add"), for: .selected)
            
        }
        
    }
    
    @IBAction func finishSelectedDocuments(_ sender: Any) {
        var docs = [Document]()
        for index in allSelect {
            if let doc = self.documents[safe: index], let _ = doc.actions?.index(where: {$0.actionType == .finish}) {
                docs.append(self.documents[index])
            }
        }
        self.finishDocuments(documents: docs)
    }
    
    @IBAction func closeMultiSelectedMode(_ sender: Any?) {
        allSelect = []
        btItemCheck.isHidden = true
        viewMultiSelect.isHidden = true
        tableview.reloadData()
    }
    
    func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        let point = gestureRecognizer.location(in: self.tableview)
        let indexPath = self.tableview.indexPathForRow(at: point)
        if indexPath != nil {
            if gestureRecognizer.state == .began {
                selectItemAtIndex(indexPath!.row)
                //tableview.reloadRows(at: [indexPath!], with: .automatic)
                 let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
                if ( !self.specialCaseModule(keyValue: keyValue) )
                {
                    let cell = tableview.cellForRow(at: indexPath!) as! TaskTableCell
                    cell.viewCheck.isHidden = !checkSelectAtIndex(indexPath!.row)

                }
            }
        }
    }
    
    func selectItemAtIndex(_ index:Int) {
        var isYes = false
        for i in 0..<allSelect.count {
            if allSelect[i] == index {
                isYes = true
                allSelect.remove(at: i)
                break
            }
        }
        if isYes == false {
            allSelect.append(index)
        }
        
        if allSelect.count > 0 {
            btItemCheck.isHidden = false
            viewMultiSelect.isHidden = false
        } else {
            btItemCheck.isHidden = true
            viewMultiSelect.isHidden = true
        }
    }
    
    func checkSelectAtIndex(_ index: Int) -> Bool {
        var isCheck = false
        if self.allSelect.isEmpty {
            return isCheck
        }
        for i in 0..<allSelect.count {
            if allSelect[i] == index{
                isCheck = true
                break
            }
        }
        return isCheck
    }
    
    func config(_ menu: Menu) {
        bookingRoomStatus = menu.name!
        self.loadViewButtonActionFromView()
//       show bage notification
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
//        B.A Fix here
//        let notifications = DataManager.shared.notificationList.filter({ $0.moduleCode == keyValue })
        let notifications = DataManager.shared.notificationList
        
        UIApplication.shared.applicationIconBadgeNumber = notifications.count
        NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
        
        self.menu = menu
        let str = self.menu?.name?.uppercased() ?? ""
        self.allSelect.removeAll()
        btItemCheck.isHidden = true
        viewMultiSelect.isHidden = true
        if let code = menu.code, code.contains(MenuCode.tasksManagement.rawValue) {
            self.addJobButton.isHidden = false
        } else {
            self.addJobButton.isHidden = true
        }
        self.loadActionDataFromMenu(keyValue: keyValue)
        
    }
    func loadActionDataFromMenu(keyValue:String)
    {
        self.documents.removeAll()
        self.arrayMeetingBookingRoomObject.removeAll()
        self.arrayVehicelDic.removeAll()

        if ( Utils().isModuleApprove(keyValue: keyValue) ) {
            if ( self.specialCaseModule(keyValue: keyValue) == false ) {
                getDocument()
//                self.tableview.reloadData()
            }
            else
            {
                if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE )
                {
                    self.currentPageDigital = 1
                    self.loadProcedureDigital()
                    self.addJobButton.isHidden = false
                    
                }
                else  if ( keyValue == Constants.MEETING_ROOM_CODE )
                {
                    self.getBookingRoom()
                    self.addJobButton.isHidden = false
                }
                else
                {
                    
                    self.addJobButton.isHidden = false
                    self.getVehicels()
                }
            }
        }
        else
        {
            self.tableview.reloadData()
        }
        
    }
    
    func getVehicels(){
        if ( self.delegate != nil )
        {
            self.delegate.callbackGenerateDate(startTime: self.selectedForm, endTime: self.selectedTo)
        }
        self.tableview.separatorStyle = UITableViewCellSeparatorStyle.none
        let fromDate:String = self.selectedForm
        let toDate:String = self.selectedTo
        self.arrayMeetingBookingRoomObject.removeAll()
        self.arrayVehicelDic.removeAll()
        var object = DataManager.shared.arrrLocationVehicels.filter({ $0.Name! == self.bookingRoomStatus })
        if ( object.count == 0 ) {return}
      
        SyncProvider.getCurrentDepartmentVehicle(done: {
            (resultData) in
            if ( resultData?.data != nil && (resultData?.data?.count)! > 0 )
            {
                self.departmentUser = (resultData?.data)!
                self.departmentUser = self.departmentUser.filter({ $0.Name != "Trụ sở chính" })
                self.currentLocation = object[0]
                let deparmentId:String = (resultData?.data![0].ID)!
                self.currentDepartment = MeetingRoomObject()
                self.currentDepartment.ID = deparmentId
                self.currentDepartment.Name = (resultData?.data![0].Name)!
                
            }
        })
        SVProgressHUD.show()
        SyncProvider.getListVehicelsType(ID: object[0].ID!, done: {
            (result) in
            if ( result?.data != nil )
            {
                self.arrayMeetingBookingRoomObject = (result?.data)!
                
                SyncProvider.getVehicelsBooking(LocationID: object[0].ID!, FromDate: fromDate, VehicleID: "0", Status: "", ToDate: toDate,done: {
                    (resultBooking) in
                    SVProgressHUD.dismiss()
                    if ( resultBooking != nil && resultBooking?.data != nil && (resultBooking?.data?.count)! > 0 )
                    {
                        let arrayBookingVehicel:[DetailVehicelsBookingObject] = (resultBooking?.data!)!
                        for objectBooking in self.arrayMeetingBookingRoomObject
                        {
                            let arrayObject = arrayBookingVehicel.filter({
                                $0.VehicleType?.ID == objectBooking.ID
                            })
                            if ( arrayObject != nil && arrayObject.count > 0  )
                            {
                                self.arrayVehicelDic[objectBooking.ID!] = arrayObject
                            }
                        }
                    }
                    self.tableview.reloadData()
                    self.tableview.tableFooterView = nil
                    self.tableview.tableFooterView?.isHidden = true
                    
                })
                
            }
            else
            {
                SVProgressHUD.dismiss()
                self.tableview.reloadData()
            }
        })
       
    }
    func getBookingRoom(){
        let fromDate:String = self.selectedForm
        let toDate:String = self.selectedTo
        self.arrayMeetingBookingRoomObject.removeAll()
        self.arrayBookingDic.removeAll()
        
        SVProgressHUD.show()
        var object = DataManager.shared.arrayMeetingRoomObject.filter({ $0.Name! == self.bookingRoomStatus })
        if object.count == 0
        {
            SyncProvider.getDepartmentMeetingRoom(done: {
                (result) in
                if ( result?.data != nil )
                {
                    DataManager.shared.arrayMeetingRoomObject = (result as! MeetingRoomResponse).data!
                    object = DataManager.shared.arrayMeetingRoomObject.filter({ $0.Name! == self.bookingRoomStatus })
                }
            })
        }
        if ( object.count > 0  )
        {
            SyncProvider.getListDepartmentMeetingRoom(DepartmentID: object[0].ID!, RoomID: "0", done: {
                (result) in
                if ( result?.data != nil )
                {
                    self.arrayMeetingBookingRoomObject = (result?.data)!
                    let dispatchGroup = DispatchGroup()
                    for objectBooking in self.arrayMeetingBookingRoomObject
                    {
                        dispatchGroup.enter()
                        SyncProvider.getBookingRoom(DepartmentID: object[0].ID!, FromDate: fromDate, Page: "0", PageSize: "0", RoomID: objectBooking.ID!, Status: "-1", ToDate: toDate, done: {
                                (result) in
                                if ( result != nil && result?.data != nil )
                                {
                                    self.arrayBookingDic[objectBooking.ID!] = result?.data!
                                }
                                dispatchGroup.leave()
                            
                          })
                    }
                    dispatchGroup.notify(queue: DispatchQueue.main) {
                        SVProgressHUD.dismiss()
                        self.tableview.reloadData()
                        self.tableview.tableFooterView = nil
                        self.tableview.tableFooterView?.isHidden = true
                    }
                }
              
            })
            
        }
        else
        {
            SVProgressHUD.dismiss()
            self.tableview.reloadData()
            self.tableview.tableFooterView = nil
            self.tableview.tableFooterView?.isHidden = true
        }
        
    }
    func loadProcedureDigital(){
        
        self.tableview.isHidden = true
        self.tableview.contentOffset = CGPoint(x: 0, y: -15)
        self.loading.startAnimating()
        let parentIdx:Int = UserDefaults.standard.integer(forKey: Constants.USER_LAST_CHILD_MENU_KEY) as Int
        var intStatus = 100
        
        //         This switch case use to get list by digital procedure code
        //         With: parentIdx: (Int) code of Item
        //        case 0: load all list (DP_ALL)
        //        case 1: load Inprocess (DP_Draft)
        //        case 2: load Need approve list (DP_NeedApprove)
        //        case 3: load In approve process (DP_Processing)
        //        case 4: load Approved list (DP_Approved)
        //        case 5: load rejected list (DP_Rejected)
        switch parentIdx
        {
        case 0:
            intStatus = 100
            break
        case 1:
            intStatus = 0
            break
        case 2:
            intStatus = 1
            break
        case 3:
            intStatus = 2
            break
        case 4:
            intStatus = 3
            break
        case 5:
            intStatus = 4
            break
        default:
            break
            
        }
        if ( self.currentStatusProcedure != intStatus  && ( self.needReloadDigital != nil && self.needReloadDigital ) )
        {
            self.produreItem.removeAll()
        }
        
        InstanceDB.default.getProdureDigitalPage(keySearch: "", page: self.currentPageDigital, status: intStatus) { (result) in
            
            self.currentStatusProcedure = intStatus
            self.loading.stopAnimating()
            self.tableview.isHidden = false
            if ( result != nil && (result as! DigitalResponse).status == 1 )
            {
                let tmpArray:[ProdureDigital] = ((((result as! DigitalResponse).data)!.procedureData)?.Items)!
                self.produreItem.append(contentsOf: tmpArray)
                self.totalPageDigital = ((((result as! DigitalResponse).data)?.procedureData)!.Page?.TotalPages!)!
                self.currentPageDigital = ((((result as! DigitalResponse).data)?.procedureData)!.Page?.CurrentPage!)!
            }
            self.tableview.reloadData()
            self.tableview.tableFooterView = nil
            self.tableview.tableFooterView?.isHidden = true
            
        }
    }
    
    func getDocument() {
      
        self.page = 1
        self.tableview.isHidden = true
        self.tableview.contentOffset = CGPoint(x: 0, y: -15)
        self.loading.startAnimating()
       
        InstanceDB.default.getDocument(treefilterID: menu.id ?? "", page: page) { (result) in
            self.loading.stopAnimating()
            self.tableview.isHidden = false
            self.documentData = result
            self.documents.removeAll()
            self.documents.append(contentsOf: self.documentData?.items ?? [])
            self.tableview.reloadData()
            
            self.tableview.tableFooterView = nil
            self.tableview.tableFooterView?.isHidden = true
          
        }
      
    }
    
    func refresh(_ sender:UIRefreshControl?) {
        self.page = 1
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) == false )
        {
            InstanceDB.default.getDocument(isPull: true,treefilterID: menu.id ?? "", page: page) { (result) in
                sender?.endRefreshing()
                SVProgressHUD.dismiss()
                self.documents.removeAll()
                if(result != nil){
                    self.documentData = result
                    self.documents.append(contentsOf: self.documentData?.items ?? [])
                    self.tableview.reloadData()
                    self.tableview.tableFooterView = nil
                    self.tableview.tableFooterView?.isHidden = true
                }
            }
        }
        else
        {
            if ( self.currentPageDigital < self.totalPageDigital)
            {
                self.currentPageDigital += 1
                self.loadProcedureDigital()
            }
            else
            {
                sender?.endRefreshing()
            }
            
        }
    }
    
    func loadMoreData() {
        if(isLoadMore == false && documentData != nil && documentData!.page != nil && documentData!.page!.totalPages != nil && documentData!.page!.totalPages! > page){
            canLoadMore = true
            self.page = self.page + 1
            InstanceDB.default.getDocument(treefilterID: menu.id ?? "", page: page) { (result) in
                if (result != nil) {
                    self.documentData = result
                    self.documents.insert(contentsOf: result!.items ?? [], at: self.documents.count)
                    self.tableview.reloadData()
                    self.tableview.tableFooterView = nil
                    self.tableview.tableFooterView?.isHidden = true
                }
            }
        } else {
            canLoadMore = false
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE )
        {
            return arrayMeetingBookingRoomObject.count + 1
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) == false )
        {
            return documents.count
        }
        else if  keyValue == Constants.DIGITAL_PROCEDURE_CODE {
            return produreItem.count
        }
        else
        {
            if section == 0
            {
                return 0
            }
            else if keyValue == Constants.MEETING_ROOM_CODE
            {
                let object = self.arrayMeetingBookingRoomObject[section - 1]
                let dic = self.arrayBookingDic.filter({ $0.key == object.ID! })
                return dic.count > 0 ? dic[0].value.count > 0 ? dic[0].value.count : 1 : 1
                
            }
            else
            {
                let object = self.arrayMeetingBookingRoomObject[section - 1]
                let dic = self.arrayVehicelDic.filter({ $0.key == object.ID! })
                return dic.count > 0 ? dic[0].value.count > 0 ? dic[0].value.count : 1 : 1
                
             }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskTableCell", for: indexPath) as! TaskTableCell
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) == false )
        {
            guard let documentItem: Document = documents[safe: indexPath.row] else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.config(documentItem, checkSelectAtIndex(indexPath.row), indexPath.row)
            cell.onOpenAttachFile = {
                if self.allSelect.count == 0 {
                    self.delegate?.taskCellAttachSelected(document: self.documents[indexPath.row], rowIndex: indexPath)
                }
            }
        }
        else
        {
            if ( keyValue  == Constants.DIGITAL_PROCEDURE_CODE )
            {
                guard let documentItem: ProdureDigital = produreItem[safe: indexPath.row] else {
                    return UITableViewCell()
                }
                cell.delegate = self
                cell.configDigital(documentItem, checkSelectAtIndex(indexPath.row), indexPath.row, searchText: "")
                cell.onOpenAttachFile = {
                    let fileDocument:String = (documentItem.document?.count)! > 0 ? (documentItem.document![0]).name! : ""
                    if !fileDocument.isEmpty
                    {
                        let procedueId:String = documentItem.ID!
                        self.delegate?.showAttachDetailViewController(fileDocument: documentItem.document!, procedureId: procedueId)
                    }
                }
            }
            else
            {
                
                let cell:BookingMeetingRoomCell = tableView.dequeueReusableCell(withIdentifier: "BookingMeetingRoomCell", for: indexPath) as! BookingMeetingRoomCell
                cell.statusColor.isHidden = true
                if ( keyValue == Constants.MEETING_ROOM_CODE)
                {
                    let object = self.arrayMeetingBookingRoomObject[indexPath.section - 1]
                    let dic = self.arrayBookingDic.filter({ $0.key == object.ID! })
                    if dic.count > 0 && dic[0].value.count > 0
                    {
                        let mettingRoom:GetDetailMeetingRoomObject = dic[0].value[indexPath.row]
                        cell.heightEmpty.constant = 0
                        cell.heightBooking.constant = 60
                        cell.lbVehicalNumber.isHidden = true
                        cell.lbVehicelType.isHidden = true
                        cell.imgDriver.isHidden = true
                        cell.lbToDate.isHidden = false
                        cell.lbFromDate.isHidden = false
                        cell.lblRoomMaster.isHidden = false
                        cell.lbRoomNumber.isHidden = false
                        cell.img.isHidden = false

                        if ( mettingRoom.Status == BookingMeetingStatus.WaitingApproval.rawValue )
                        {
                            cell.img.image = UIImage(named: "icon_booking_pending")
                        }
                        else if ( mettingRoom.Status == BookingMeetingStatus.Approved.rawValue )
                        {
                            cell.img.image = UIImage(named: "icon_booking")
                        }
                        cell.lbRoomNumber.text = mettingRoom.Title!
                        cell.lblRoomMaster.text = mettingRoom.Owner?.Name!
                        if ( mettingRoom.FromDate != nil  )
                        {
                            cell.lbFromDate.text =  Constants.formatTimeFromDate(mettingRoom.FromDate!)
                        }
                        if ( mettingRoom.ToDate != nil )
                        {
                            cell.lbToDate.text =  Constants.formatTimeFromDate(mettingRoom.ToDate!)
                        }
                        
                    }
                    else
                    {
                         cell.titleEmpty.text = "PHÒNG TRỐNG"
                        cell.lbVehicalNumber.isHidden = true
                        cell.lbVehicelType.isHidden = true
                        cell.imgDriver.isHidden = true
                        cell.heightBooking.constant = 0
                        cell.heightEmpty.constant = 60
                        cell.lbToDate.isHidden = true
                        cell.lbFromDate.isHidden = true
                        cell.lblRoomMaster.isHidden = true
                        cell.lbRoomNumber.isHidden = true
                        cell.img.isHidden = true
                    }
                }
                else
                {
                    let object = self.arrayMeetingBookingRoomObject[indexPath.section - 1]
                    let dic = self.arrayVehicelDic.filter({ $0.key == object.ID! })
                    if dic.count > 0 && dic[0].value.count > 0
                    {
                        let mettingRoom:DetailVehicelsBookingObject = dic[0].value[indexPath.row]
                        
                        cell.heightEmpty.constant = 0
                        cell.heightBooking.constant = 90
                        cell.lbToDate.isHidden = false
                        cell.lbFromDate.isHidden = false
                        cell.lblRoomMaster.isHidden = false
                        cell.lbRoomNumber.isHidden = false
                        cell.lbVehicelType.isHidden = false
                        cell.lbVehicalNumber.isHidden = false
                        cell.img.isHidden = true
                        cell.lblRoomMaster.text = ""
                        cell.lbVehicelType.text = ""
                        cell.statusColor.isHidden = false
                        cell.statusColor.backgroundColor = UIColor(hexString: mettingRoom.ColorStatus!)
                        cell.lbRoomNumber.text = mettingRoom.Title!
                        cell.lblRoomMaster.text = mettingRoom.Author?.Name
                      
                        cell.lbVehicalNumber.text = ""
                        if ( mettingRoom.Driver?.Name != nil && mettingRoom.Driver?.Name != "" )
                        {
                            cell.lbVehicalNumber.layer.cornerRadius = 2
                            cell.lbVehicalNumber.layer.borderWidth = 1
                            cell.lbVehicalNumber.layer.borderColor = UIColor.gray.cgColor
                            cell.lbVehicalNumber.text = String(format: " %@  ", (mettingRoom.Vehicle?.Name)!)
                             cell.lbVehicelType.text = mettingRoom.Driver?.Name
                            cell.heightFooter.constant = 100
                            cell.paddingTop.constant = 10
                            cell.imgDriver.isHidden = false
                        }
                        else
                        {
                            cell.heightFooter.constant = 70
                            cell.paddingTop.constant = 18
                            cell.imgDriver.isHidden = true
                        }
                        if ( mettingRoom.FromDate != nil  )
                        {
                            cell.lbFromDate.text =  Constants.formatTimeFromDate(mettingRoom.FromDate!)
                        }
                        if ( mettingRoom.ToDate != nil )
                        {
                            cell.lbToDate.text =  Constants.formatTimeFromDate(mettingRoom.ToDate!)
                        }
                        
                    }
                    else
                    {
                        cell.titleEmpty.text = "Chưa có lịch"
                        cell.heightBooking.constant = 0
                        cell.heightEmpty.constant = 60
                        cell.lbToDate.isHidden = true
                        cell.lbFromDate.isHidden = true
                        cell.lblRoomMaster.isHidden = true
                        cell.lbRoomNumber.isHidden = true
                        cell.img.isHidden = true
                        cell.imgDriver.isHidden = true
                        cell.lbVehicalNumber.isHidden = true
                        cell.lbVehicelType.isHidden = true
                    }
                }
                
                return cell
            }
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) == false )
        {
            self.cellHeightsCache[indexPath] = cell.frame.size.height
            if (indexPath.row == documents.count - 2) {
                loadMoreData()
            }
            
            let lastRowIndex = documents.count - 1
            if indexPath.row == lastRowIndex && canLoadMore {
                // print("this is the last cell")
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableview.tableFooterView = spinner
                self.tableview.tableFooterView?.isHidden = false
            } else {
                self.tableview.tableFooterView = nil
                self.tableview.tableFooterView?.isHidden = true
            }
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( self.specialCaseModule(keyValue: keyValue) == false )
        {
            if (allSelect.count == 0) {
                self.documents[indexPath.row].unReadColor = "#777B9B"
                self.tableview.reloadRows(at: [indexPath], with: .automatic)
                self.delegate?.taskCellDidSelected(document: documents[indexPath.row], itemIndex: indexItem, rowIndex: indexPath)
            } else {
                selectItemAtIndex(indexPath.row)
                let cell = tableview.cellForRow(at: indexPath) as! TaskTableCell
                cell.viewCheck.isHidden = !checkSelectAtIndex(indexPath.row)
            }
        }
        else
        {
            if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE )
            {
                let procedureDigital:ProdureDigital = self.produreItem[indexPath.row]
                self.delegate?.showDetailViewController(procedure: procedureDigital)
            }
            else
            {
                let object = self.arrayMeetingBookingRoomObject[indexPath.section - 1]
                let dic = self.arrayBookingDic.filter({ $0.key == object.ID! })
                if dic.count > 0 && dic[0].value.count > 0
                {
                    let mettingRoom:GetDetailMeetingRoomObject = dic[0].value[indexPath.row]
                    if ( self.delegate  != nil )
                    {
                        self.delegate?.assignBookingMeetingRoom(orgID: mettingRoom.ID!)
                    }
                }
                else
                {
                    
                    let object = self.arrayMeetingBookingRoomObject[indexPath.section - 1]
                    let dic = self.arrayVehicelDic.filter({ $0.key == object.ID! })
                    if ( dic.count > 0  )
                    {
                        let mettingRoom:DetailVehicelsBookingObject = dic[0].value[indexPath.row]
                        
                        if ( self.delegate  != nil )
                        {
                            self.delegate.assignVehicelType(arrayMeetingBookingRoomObject: self.arrayMeetingBookingRoomObject, location: self.currentLocation, department: self.currentDepartment,OrgID: mettingRoom.ID!,departmentUsr: self.departmentUser)
                        }
                    }
                }
            }
        }
        
    }
   func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE )
        {
            return 0
        }
        else
        {
            if let height =  self.cellHeightsCache[indexPath]{
                return height
            }
            return UITableViewAutomaticDimension

        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            // Dequeue with the reuse identifier
            let headerView = self.tableview.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
            headerView.delegate = self
            headerView.isSelectedAdmore = self.isSelectedAdmore
            if ( self.isSelectedAdmore )
            {
                headerView.fsCalendar.setScope(.month, animated: false)
                headerView.btnCollapse.setTitle("Thu gọn", for: .normal)
            }
            else
            {
                headerView.fsCalendar.setScope(.week, animated: false)
                headerView.btnCollapse.setTitle("Mở rộng", for: .normal)

            }
            self.configHeaderMettingView(headerView: headerView)
            return headerView
        }
        else
        {
            let object = self.arrayMeetingBookingRoomObject[section - 1]
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MeetingTitleView" ) as! MeetingTitleView
            headerView.lbTitle.text = object.Name!
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            headerView.lbTitle.isUserInteractionEnabled = true
            headerView.lbTitle.tag = section - 1
            headerView.lbTitle.addGestureRecognizer(tapGesture)
            let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
            if ( keyValue == Constants.VEHICEL_CODE)
            {
                if ( object.Capacity == 0)
                {
                    headerView.imgPicker.image = UIImage(named: "icon_taxi")
                    headerView.imgPicker.contentMode = .scaleAspectFit
                }
                else
                {
                    headerView.imgPicker.image = UIImage(named: "icon_7slot")
                    headerView.imgPicker.contentMode = .scaleAspectFit
                }
            }
            else
            {
                headerView.imgPicker.image = UIImage(named: "ic_place")
                headerView.imgPicker.contentMode = .scaleAspectFit
            }
            return headerView
        }
      
    }
    func handleTap(sender: UITapGestureRecognizer) {
        
        let commonView = sender.view
        let tagValue:Int = (commonView?.tag)!
        let object:GetMeetingRoomObject = self.arrayMeetingBookingRoomObject[tagValue]
        if ( self.delegate != nil )
        {
            let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
            if ( keyValue == Constants.MEETING_ROOM_CODE )
            {
                self.delegate.viewHeadTitle(objectHeader: object)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE )
        {
            if ( section == 0 )
            {
                if ( self.isSelectedAdmore )
                {
                    return 300
                }
                else
                {
                    return 150
                }
            }
            else
            {
                return 40
            }
        }
        return 0
    }
    func configHeaderMettingView(headerView: TableSectionHeader){
        
        headerView.fsCalendar.appearance.headerDateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM yyyy", options: 0, locale: NSLocale(localeIdentifier: "vi-VN") as Locale)
        headerView.fsCalendar.locale = NSLocale(localeIdentifier: "vi-VN") as Locale
        headerView.fsCalendar.delegate = self
        headerView.fsCalendar.dataSource = self
        headerView.fsCalendar.clipsToBounds = true
        self.calendar = headerView.fsCalendar
        if ( self.isFirstTimeCalendar )
        {
            self.isFirstTimeCalendar = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.isSelectedAdmore = false
                self.tableview.reloadData()
            }
        }
        
    }
    
    
    func finishDocuments(documents: [Document]) {
        SVProgressHUD.show()
        DataManager.finishDocuments(docs: documents, done: { (finish) in
            if finish {
                self.closeMultiSelectedMode(nil)
                //self.makeToast("Gửi yêu cầu kết thúc thành công.")
                if let menuCode = self.menu.code, menuCode.contains(MenuChildCode.new.rawValue) || menuCode.contains(MenuChildCode.processing.rawValue) {
                    self.documents.removeAll(documents)
                    self.tableview.reloadData()
                } else {
                    self.refresh(nil)
                }
                SVProgressHUD.dismiss()
            } else {
                SVProgressHUD.dismiss()
                self.makeToast("Có lỗi xảy ra, hệ thống sẽ cập nhật cho bạn sớm nhất !")
            }
        })
    }
    
    func finishDocument(document: Document, indexPath:IndexPath) {
        SVProgressHUD.show()
        DataManager.finishDocument(document: document, done: { (finish) in
            if finish {
                //self.makeToast("Gửi yêu cầu kết thúc thành công.")
                if let menuCode = self.menu.code, menuCode.contains(MenuChildCode.new.rawValue) || menuCode.contains(MenuChildCode.processing.rawValue) {
                    self.documents.remove(at: indexPath.row)
                    self.tableview.reloadData()
                } else {
                    self.refresh(nil)
                }
                SVProgressHUD.dismiss()
            } else {
                SVProgressHUD.dismiss()
                self.makeToast("Có lỗi xảy ra, hệ thống sẽ cập nhật cho bạn sớm nhất !")
            }
        })
    }
    
    func reloadItem(indexPath: IndexPath, isFinish: Bool = false) {
        if ((isFinish && self.menu.isFinishMenu()) || self.menu.isNewMenu()) && self.documents.count > indexPath.row {
            self.documents.remove(at: indexPath.row)
            self.tableview.reloadData()
            return
        }
        
        getDocumentDetail(indexPath: indexPath)
    }
    
    func getDocumentDetail(indexPath: IndexPath, isFinish: Bool = false) {
        guard let document = self.documents[safe: indexPath.row], let id = document.id else { return }
        SVProgressHUD.show()
        InstanceDB.default.getDocumentDetail(documentId: id) { [weak self] (document) in
            SVProgressHUD.dismiss()
            guard let `self` = self else { return }
            if let doc = document {
                if let statusId = doc.statusDocument?.id, statusId == 4 && self.menu.isFinishOrNewMenu() && self.documents.count > indexPath.row {
                    self.documents.remove(at: indexPath.row)
                } else {
                    self.documents[indexPath.row] = doc
                }
                
                self.tableview.reloadData()
            }
            
        }
    }
}

extension BoardCollectionCell: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {
            if let document = self.documents[safe: indexPath.row],
                let actions = document.actions {
                let filterActions:[DocumentActionObject] = actions.filter {$0.actionType == .finish}
                guard let docAction = filterActions.first else {
                    return []
                }
                let action = SwipeAction(style: .default, title: docAction.actionName, handler: { (action, index) in
                    self.finishDocument(document: document, indexPath: indexPath)
                })
                action.title = "Kết thúc văn bản"
                action.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.9058823529, blue: 0.9411764706, alpha: 1)
                action.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                action.font = Theme.default.boldFont(size: 20)
                
                return [action]
            }
            return nil
        }
        if (indexPath.section == 0) {
            var list: [SwipeAction] = []
            let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
//            for swipe
            if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE && self.produreItem.count > 0) {
                let procedureDigital: ProdureDigital = self.produreItem[indexPath.row]
                var isAddmore = false
                for object in procedureDigital.itemAction!
                {
                    let action = SwipeAction(style: .default, title: object.Name!, handler: { (action, index) in
                        if ( self.delegate  != nil )
                        {
                            let itemCode:ItemActions = ItemActions()
                            itemCode.Name = action.title!
                            itemCode.Code = action.identifier
                            self.delegate?.showSelectedActionItem(procedure: procedureDigital, itemAction: itemCode)
                        }
                        
                    })
                    if ( object.Code! == "Assign" || object.Code! == "Approve" )
                    {
                        let imgAction: String = "ic_action_\(object.Code!)"
                        if let imgObj = UIImage(named: imgAction) {
                            action.image = imgObj.imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                        } else {
                            action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                        }
                        action.identifier = object.Code!
                        list.append(action)
                    }
                    else
                    {
                        isAddmore = true
                    }
                }
                if ( isAddmore )
                {
                    let action = SwipeAction(style: .default, title: "Xem Thêm", handler: { (action, index) in
                        self.delegate?.showAllActionItem(procedure: procedureDigital)
                    })
                    let imgAction: String = "multiSelect_more"
                    if let imgObj = UIImage(named: imgAction) {
                        action.image = imgObj.imageResize(sizeChange: CGSize(width: 42, height: 12)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                    } else {
                        action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                    }
                    list.append(action)
                }
                
                return list
                
            }
            
            if let document = self.documents[safe: indexPath.row],
                let actions = document.actions {
                let filterActions:[DocumentActionObject] = actions.reversed()
                for i in 0..<filterActions.count {
                    if (i == 4) {
                        let action = SwipeAction(style: .default, title: "Xem thêm", handler: { (action, indexPath) in
                            debugPrint("Defaule Board Collection Cell More: \(action.debugDescription) - indexPath \(indexPath.debugDescription)")
                            self.delegate?.showAllProcess(document: document, index: self.indexItem)
                        })
                        action.image = #imageLiteral(resourceName: "multiSelect_more").filled(withColor: Theme.default.normalGreenSelectedColor)
                        list.append(action)
                        break
                    } else {
                        if let actionTypeItem: DocumentAction = filterActions[i].actionType {
                            let action = SwipeAction(style: .default, title: filterActions[i].actionName, handler: { (action, indexPath) in
                                debugPrint("Default Board Collection Cell: \(action.debugDescription) - indexPath \(indexPath.row)")
                            })
                            if actionTypeItem == .assign {
                                action.handler = { (action, indexPath) in
                                    self.delegate?.assignTaskProcess(document: document, index: indexPath)
                                }
                            } else if actionTypeItem == .approve {
                                action.handler = { (action, indexPath) in
                                    self.delegate?.approveTaskProcess(document: document, index: indexPath)
                                }
                            } else if actionTypeItem == .process {
                                action.handler = { (action, indexPath) in
                                    self.delegate?.processTaskProcess(document: document, index: indexPath)
                                }
                            } else if actionTypeItem == .appraise {
                                action.handler = { (action, indexPath) in
                                    self.delegate?.appriseTaskProcess(document: document, index: indexPath)
                                }
                            } else if actionTypeItem == .sendMail {
                                action.handler = { (action, indexPath) in
                                    self.delegate?.emailTaskProcess(document: document, index: indexPath)
                                }
                            } else if actionTypeItem == .finish {
                                action.handler = { (action, indexPath) in
                                    //self.delegate?.finishTaskProcess(document: document, index: self.indexItem)
                                    self.finishDocument(document: document, indexPath: indexPath)
                                }
                            }
                            let imgAction: String = "ic_action_\(actionTypeItem.rawValue)"
                            if let imgObj = UIImage(named: imgAction) {
                                action.image = imgObj.imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                            } else {
                                action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                            }
                            list.append(action)
                        }
                    }
                }
            }
            for item in list {
                item.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.9058823529, blue: 0.9411764706, alpha: 1)
                item.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                item.font = Theme.default.boldFont(size: 12)
            }
            return list
        }
        else {
            
        }
        return []
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = orientation == .left ? .selection : nil
        options.transitionStyle = .drag
        options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return options
    }
}

extension BoardCollectionCell: ProcessTaskViewDelegate {
    
    func sendProcessWork(status: Bool, message: String?, index: IndexPath?) {
        if status {
            var msg = "Xử lý thành công !"
            if let msgTemp: String = message,
                msgTemp != "" {
                msg = msgTemp
            }
            self.makeToast(msg)
            if let selectedIndex: IndexPath = index {
                self.tableview.reloadRows(at: [selectedIndex], with: .none)
            }
        } else {
            self.makeToast("Có lỗi xảy ra, chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !")
        }
    }
    
    
    func closeView() {
    }
    
}

// MARK: TaskAssignNoneApproveDelegate
extension BoardCollectionCell: TaskAssignNoneApproveDelegate {
    
    func onCloseView(status: Bool, indexPath: IndexPath?) {
        guard let index = indexPath else { return }
        if status {
            //self.tableview.reloadData()
            self.getDocumentDetail(indexPath: index)
            self.makeToast("Xử lý thành công !")
            //            if let index = indexPath {
            //                self.tableview.reloadData()
            //            }
        } else {
            self.makeToast("Có lỗi xảy ra !")
        }
    }
    
}

// MARK: AppriseTaskViewDelegate
extension BoardCollectionCell: AppriseTaskViewDelegate {
    
    func appriseCloseView() {
    }
    
    func appriseDoneView(status: Bool, message: String?, index: IndexPath?) {
        self.makeToast("Xử lý thành công !")
    }
    
}

// MARK: TaskAssignNewViewDelegate
extension BoardCollectionCell: TaskAssignNewViewDelegate {
    
    func taskAssignCloseView(_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?) {
        var defaultMsg: String = "Xử lý thành công !"
        if let confirmMsg: String = approveMsg {
            defaultMsg = confirmMsg
        } else if let backMsg: String = returnMsg {
            defaultMsg = backMsg
        } else if let failMsg: String = errorMsg {
            defaultMsg = failMsg
        }
        self.makeToast(defaultMsg)
        if let selectedIndex: IndexPath = index {
            self.tableview.reloadRows(at: [selectedIndex], with: .none)
        }
    }
}

// MARK: SendEmailViewDelegate
extension BoardCollectionCell: SendEmailViewDelegate {
    
    func sendEmailCloseView() {
        
    }
    
    func sendEmailDoneView() {
        self.makeToast("Gửi mail thành công !")
    }
    
}
extension BoardCollectionCell: FSCalendarDataSource,FSCalendarDelegate
{

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // Output Format
        dateFormatter.timeZone = TimeZone.current
        let UTCToCurrentFormat = dateFormatter.string(from: date)
        let fromDate = String(format: "%@T00:00:00+07:00",UTCToCurrentFormat)
        let toDate = String(format: "%@T23:50:00+07:00",UTCToCurrentFormat)
        self.selectedForm = fromDate
        self.selectedTo = toDate
        let keyValue:String = UserDefaults.standard.object(forKey: Constants.USER_LEFT_MENU_CODE) as! String
        if ( keyValue == Constants.MEETING_ROOM_CODE) {
            
            self.getBookingRoom()
            
        }
        else {
            self.getVehicels()
        }
    }

    
}
