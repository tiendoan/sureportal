//
//  AnnouncementParentViewCell.swift
//  TaskManagement
//
//  Created by Scor Doan on 8/14/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

class AnnouncementParentViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var announcements = [Announcement]() {
        didSet {
            collectionView.reloadData()
        }
    }
    var timer: Timer?
    var currentIdx = 0
    var onSelectedAnnouncement:((Announcement) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "AnnouncementViewCell", bundle: nil), forCellWithReuseIdentifier: "NewsAnnouncement")
        collectionView.register(UINib(nibName: "EventAnnouncementViewCell", bundle: nil), forCellWithReuseIdentifier: "EventAnnouncement")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let width = UIScreen.main.bounds.width
        layout.itemSize = CGSize(width: width, height: 350)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        onSelectedAnnouncement = nil
    }
    
    func configCell(_ announcements: [Announcement], _ onSelectedAnnouncement:((Announcement) -> ())?) {
        self.announcements = announcements
        self.onSelectedAnnouncement = onSelectedAnnouncement
        
//        timer = Timer.scheduledTimer(withTimeInterval: 30.0, repeats: true, block: { (_) in
//            self.currentIdx += 1
//            if self.currentIdx == announcements.count {
//                self.currentIdx = 0
//            }
//            self.collectionView.scrollToItem(at: IndexPath(row: self.currentIdx, section: 0), at: .left, animated: true)
//        })
    }
}


extension AnnouncementParentViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return announcements.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let announcement = announcements[indexPath.row]
        let identifierCell = announcement.type == .event ? "EventAnnouncement" : "NewsAnnouncement"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifierCell, for: indexPath) as! AnnouncementViewCell
        
        cell.configCell(announcement)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height-20)
        return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let visibleCells = collectionView.visibleCells as? [AnnouncementViewCell] else {
            return
        }
        visibleCells.forEach{$0.stopCountDown()}
        (cell as? AnnouncementViewCell)?.startCountDown()
        self.pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let announcement = announcements[indexPath.row]
        self.onSelectedAnnouncement?(announcement)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        currentIdx = collectionView.indexPathForItem(at: visiblePoint)?.row ?? 0
    }
}
