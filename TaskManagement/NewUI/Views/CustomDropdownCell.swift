//
//  CustomDropdownCell.swift
//  TaskManagement
//
//  Created by Scor Doan on 3/15/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import DropDown

class CustomDropdownCell: DropDownCell {
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
