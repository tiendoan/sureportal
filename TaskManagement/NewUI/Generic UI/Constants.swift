//
//  Constants.swift
//  TaskManagement
//
//  Created by HaiComet on 10/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

struct NotificationName {
  static let POST_NOTIFICAITON_MENU_SELECT_ROOT          = "POST_NOTIFICAITON_MENU_SELECT_ROOT"
  static let POST_NOTIFICATION_MENU_SELECT_PARENT        = "POST_NOTIFICATION_MENU_SELECT_PARENT"
  static let POST_NOTIFICAITON_MENU_SELECT_CHILD         = "POST_NOTIFICAITON_MENU_SELECT_CHILD"
  static let POST_NOTIFICAITON_MENU_BADGE_UPDATED         = "POST_NOTIFICAITON_MENU_BADGE_UPDATED"
  static let POST_NOTIFICAITON_MENU_RELOAD         = "POST_NOTIFICAITON_MENU_RELOAD"
}

enum FontFamily:String {
    case Regular = "Muli-Regular"
    case Black = "Muli-Black"
    case Bold = "Muli-Bold"
}
enum FontSize:CGFloat {
    case XXLarge = 17
    case XLarge = 16
    case Large = 14
    case Medium = 12
    case Small = 10
}

class Constants {
  static let `default` = Constants()
  
  let DEFAULT_EXPIRED_CACH: Int = 36000
  let DEFAULT_PARENT_ID: String = "00000000-0000-0000-0000-000000000000"
  static let currentDate = Date()
  static let DEFAULT_NEXT_DATE = 1
    static let COMMON_DATE_TIME_FORMAT_ZTIME = "yyyy-MM-dd'T'HH:mm:ssZ"
  static let SIMPLE_FULL_DATETIME_FORMAT = "HH:mm dd 'th'MM, yyyy"
  static let COMMON_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
  static let SIMPLE_DATETIME_FORMAT_REPORT = "dd 'th'MM yyyy"
  static let SIMPLE_DATETIME_FORMAT = "dd 'th'MM, yyyy"
  static let SIMPLE_DATETIME_FORMAT_SERVER = "yyyy-MM-dd"
  static let FULL_DATETIME_FORMAT = "dd/MM/yyyy HH:mm"
  static let SIMPLE_TIME_FORMAT = "HH:mm"
  static let USER_DATA_KEY                       = "USER_DATA_KEY"
  static let USER_LAST_ROOT_MENU_KEY                 = "USER_LAST_ROOT_MENU_KEY"
  static let USER_LAST_CHILD_MENU_KEY                 = "USER_LAST_CHILD_MENU_KEY"
  static let USER_LEFT_MENU_CODE                 = "USER_LEFT_MENU_CODE"
  static let PARENT_PROCEDURE_ID =  "PARENT_PROCEDURE_ID"
    
    // This variable to stored the current state of left menu code (this variable used to rollback value of user_left_menu_code)
    static let USER_STORED_LEFT_MENU_CODE        = "USER_STORED_LEFT_MENU_CODE"
    
    // This variable to stored the current state of left menu code (this variable used to rollback value of user_left_menu_code)
    static let PARENT_STORED_PROCEDURE_ID =  "PARENT_STORED_PROCEDURE_ID"
    
  static let DIGITAL_PROCEDURE_CODE =            "DigitalProcedure"
  static let MEETING_ROOM_CODE =            "MeetingRoom"
  static let VEHICEL_CODE =            "Vehicle"
  static let DOCUMENT_CODE =            "Document"
  static let TASK_CODE = "Task"
  static let SYNC_TIME_SERVER                 = "SYNC_TIME_SERVER"
  static let SYNC_DATE_SERVER                 = "SYNC_DATE_SERVER"
    
  static let SYNC_ALERT_NOTIFICATION          = "SYNC_ALERT_NOTIFICATION"
  static let SYNC_ATTACHMENT_DOWNLOAD         = "SYNC_ATTACHMENT_DOWNLOAD"
  static let SYNC_CREATE_TASK                 = "SYNC_CREATE_TASK"
    
    
    


    
    
  static let FONT_SIZE_LEFT_MENU:Int = 18
  
  //static var domainAddress = UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? "http://sureportal.bioportal.vn"
    static let logoLink = "/mobileapis/images/logo.png"
    var domainAddress:String{
        get {
            return UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? "http://demo.bioportal.vn"//"https://qlcv.pvgas.com.vn"
        }
    } //= UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? "https://qlcv.pvgas.com.vn"
  
    struct UserDefault {
        static let kAppVersion = "kAppVersion"
    }
    
  public static func getVersionBuild() -> String{
    return Bundle.main.infoDictionary!["CFBundleVersion"] as! String
  }
  public static func getVersionName() -> String{
    return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
  }
  
  public static func convertStringToDictionary(text: String) -> [String: Any]? {
    let data = text.data(using: .utf8)
    return convertDataToDictionary(data: data)
  }
  
  public static func convertDataToDictionary(data: Data?) -> [String: Any]? {
    if data != nil{
      do {
        return try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
      } catch {
        print(error.localizedDescription)
      }
    }
    return nil
  }
    public static func formatDateString(_ strDate: String?) -> String? {
        if strDate == nil{
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date = dateFormatter.date(from:strDate!)
        if date == nil{
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
            date = dateFormatter.date(from:strDate!)
            if date == nil
            {
                return nil
            }
        }
        dateFormatter.locale = Locale(identifier: "vi-VN")
        dateFormatter.dateFormat = "dd MMMM,yyyy"
        let str = dateFormatter.string(from: date!)
        return str
    }
  public static func formatDate(_ strDate: String?) -> String? {
    if strDate == nil{
      return nil
    }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    var date = dateFormatter.date(from:strDate!)
    if date == nil{
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
         date = dateFormatter.date(from:strDate!)
        if date == nil
        {
            return nil
        }
    }
    dateFormatter.dateFormat = "dd/MM/yyyy"
    let str = dateFormatter.string(from: date!)
    return str
  }
    
    public static func formatTimeFromDate(_ strDate: String?) -> String? {
        if strDate == nil{
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date = dateFormatter.date(from:strDate!)
        if date == nil{
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
            date = dateFormatter.date(from:strDate!)
            if date == nil
            {
                return nil
            }
        }
        dateFormatter.dateFormat = "HH:mm"
        let str = dateFormatter.string(from: date!)
        return str
    }
    
    public static func formatDateTime(_ strDate: String?) -> String? {
        if strDate == nil{
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date = dateFormatter.date(from:strDate!)
        if date == nil{
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
            date = dateFormatter.date(from:strDate!)
            if date == nil
            {
                return nil
            }
        }
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let str = dateFormatter.string(from: date!)
        return str
    }
    public static func formatDateFromString(_ strDate: String?) -> String? {
        if strDate == nil{
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date = dateFormatter.date(from:strDate!)
        if date == nil{
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
            date = dateFormatter.date(from:strDate!)
            if date == nil
            {
                return nil
            }
        }
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let str = dateFormatter.string(from: date!)
        return str
    }
    
  
  public static func formatDate(_ date: Date?, _ formatString: String = "dd/MM/yyyy") -> String? {
    if date == nil{
      return nil
    }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formatString
    let str = dateFormatter.string(from: date!)
    return str
  }
  
  
  func recursiveFlatmap(list: [Assignee], _ isIncludeParent: Bool = false) -> [Assignee] {
    var results = [Assignee]()    
    for element in list {
      if let subList: [Assignee] = element.childrens,
        !subList.isEmpty {
        if isIncludeParent {
          results.append(element)
        }
        results += recursiveFlatmap(list: subList, isIncludeParent)
      } else {
        results.append(element)
      }
    }
    return results
  }
    
    func recursiveToNode(nodeId: String, list: [Assignee]) -> Assignee? {
        var result:Assignee?
        for element in list {
            if result != nil {
                return result
            }
            if let value = element.value, value == nodeId {
                return element
            } else if let subList: [Assignee] = element.childrens,
                !subList.isEmpty {
                if let founded = recursiveToNode(nodeId: nodeId, list: subList) {
                    result = founded
                }
            }
            
        }
        return result
    }
    
    func recursiveFlatmapToNode(nodeId: String, list: [Assignee], _ isIncludeParent: Bool = false) -> [Assignee] {
        if let node = recursiveToNode(nodeId: nodeId, list: list) {
            if let subList: [Assignee] = node.childrens,
                !subList.isEmpty {
                return recursiveFlatmap(list: subList, isIncludeParent)
            }
        }
        
        return [Assignee]()
    }
}
