//
//  UIExtensions.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/14/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  
  public func setTitleNavigationBar(_ title: String) {
    self.title = title
  }
  
  public func setupDefaultNavigationBar() {
    self.navigationBar?.barTintColor = Theme.default.normalBlueOceanColor
    self.navigationBar?.tintColor = Theme.default.normalBlueOceanColor
    self.navigationBar?.shadowImage = UIImage()
    self.navigationBar?.isTranslucent = false
    self.navigationBar?.titleTextAttributes = [NSForegroundColorAttributeName: Theme.default.normalWhiteColor, NSFontAttributeName: Theme.default.semiBoldFont(size: 22)]
  }
  
  public func generateBackAction() -> UIBarButtonItem {
    let backBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_nav_back"), style: .plain, target: self, action: #selector(closePages(_:)))
    backBarButton.tintColor = Theme.default.normalWhiteColor
    return backBarButton
  }
  
  public func addLeftButtonNavigationBar(buttons: [UIBarButtonItem]?) {
    if buttons == nil || (buttons?.isEmpty)! {
      return
    }
    self.navigationItem.leftBarButtonItems = buttons
  }
  
  public func addRightButtonNavigationBar(buttons: [UIBarButtonItem]?) {
    if buttons == nil || (buttons?.isEmpty)! {
      return
    }
    self.navigationItem.rightBarButtonItems = buttons
  }
  
  public func showBack () {
    self.navigationItem.leftBarButtonItem = generateBackAction()
  }
  
  public func closePages (_ sender: Any?) {
    if let navigationController = navigationController, navigationController.viewControllers.first != self {
      navigationController.popViewController(animated: true)
    } else {
      dismiss(animated: true, completion: nil)
    }
  }
  
  public func addTapEndEdit (view: UIView? = nil) {
    let tap = UITapGestureRecognizer(target: self, action: #selector(endEditor))
    if view == nil{
      self.view.addGestureRecognizer(tap)
    } else {
      view?.addGestureRecognizer(tap)
    }
  }
  
  public func endEditor () {
    self.view.endEditing(true)
  }
  
  func showViewController(viewController: UIViewController, _ isSetHiddenNavigationBar: Bool? = true, _ transitionStyle: UIModalTransitionStyle? = .coverVertical) {
    let nav = UINavigationController(rootViewController: viewController)
    nav.isNavigationBarHidden = isSetHiddenNavigationBar!
    nav.modalPresentationStyle = .overCurrentContext
    nav.modalTransitionStyle = transitionStyle!
    self.present(nav, animated: true, completion: nil)
  }
    
}
