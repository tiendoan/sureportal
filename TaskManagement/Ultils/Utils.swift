//
//  Utils.swift
//  TaskManagement
//
//  Created by Mirum User on 10/10/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit

class Utils: NSObject {

    func isModuleApprove(keyValue: String)->Bool
    {
        if ( keyValue == Constants.DIGITAL_PROCEDURE_CODE || keyValue == Constants.MEETING_ROOM_CODE || keyValue == Constants.VEHICEL_CODE || keyValue == Constants.DOCUMENT_CODE || keyValue == Constants.TASK_CODE ) {
            return true
        }
        return false
    }
}
