//
//  CommonUtils.swift
//  TaskManagement
//
//  Created by Mirum User on 5/28/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation

struct LocalizationString {
    static let ALERT_TITLE          = "THÔNG BÁO"
    static let LOGOUT_TITLE        = "Bạn có muốn đăng xuất không?"
    static let AGREE_STRING         = "Đồng ý"
    static let CANCEL_STRING         = "Hủy"
    static let LOGOUT_SUCCESS         = "Đăng xuất thành công"
    static let LOGOUT_FAILSE         = "Đăng xuất thất bại"
    static let CONNECTION_SERVER_ERROR         = "Không thể kết nối đến máy chủ!"
    static let Document = "Văn bản"
    static let Task = "Công việc"
    static let DigitalProcedure = "Quy trình số"
    static let TimeSheet = "Nghỉ phép"
    static let BusinessTripRequest = "Công tác"
    static let Vehicle = "Đăng ký Xe"
    static let MeetingRoom = "Phòng họp"
    static let Stationery = "Văn phòng phẩm"
    static let DocumentLibrary = "Thư viện tài liệu"
    static let HelpDesk = "Hỗ trợ helpdesk"
    

}

enum ActiveModuleName: String {
    case Document
    case Task
    case DigitalProcedure
    case TimeSheet
    case BusinessTripRequest
    case Vehicle
    case MeetingRoom
    case Stationery
    case DocumentLibrary
    case HelpDesk
}
