//
//  DocumentManagement.swift
//  TaskManagement
//
//  Created by Mirum User on 11/2/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import UIKit
import GRDB

class DocumentManagement: NSObject {
    
    static let shared = DocumentManagement()
    var timerSync:Timer
    var rangeDateSync:Int
    var currentPage:Int
    let defineParentId:String = "61eae503-8d66-4785-b47e-b8c372bf7961"
    var menu = [Menu]()
    override init(){
        timerSync = Timer()
        rangeDateSync = 0
        currentPage = 1
    }
    
    func syncDocument(){
        //Code Process
        var timerRequest = UserDefaults.standard.object(forKey: Constants.SYNC_TIME_SERVER)
        if timerRequest == nil
        {
            UserDefaults.standard.set(0, forKey: Constants.SYNC_TIME_SERVER)
            timerRequest = UserDefaults.standard.object(forKey: Constants.SYNC_TIME_SERVER)
        }

        var rangeDate = UserDefaults.standard.object(forKey: Constants.SYNC_DATE_SERVER)
        if rangeDate == nil
        {
            UserDefaults.standard.set(1, forKey: Constants.SYNC_DATE_SERVER)
            rangeDate = UserDefaults.standard.object(forKey: Constants.SYNC_DATE_SERVER)
        }
        rangeDateSync = rangeDate as! Int
        if (timerRequest as! Int) > 0
        {
            timerSync = Timer.scheduledTimer(timeInterval: (TimeInterval((timerRequest as! Int)*60)), target: self, selector: #selector(loadingDocumentValidDate), userInfo: nil, repeats: true)
            timerSync.fire()
           
        }
    }
    @objc private func loadingDocumentValidDate(){

        SyncProvider.getDocumentSync(page: self.currentPage, done: { (result) in
            if ( result != nil  && ((result)!.items.count) > 0 )
            {
                if self.currentPage < ((result as? DocumentData)!.page?.totalPages)!
                {
                    var treefilterID:String = ""
                    if let items = result?.items {
                        do {
                            try dbQueue.inTransaction { db in
                                for item in items {
                                    let dayDistrance:Int = Date().days(from: item.created!)
                                     if ( dayDistrance < self.rangeDateSync )
                                     {
                                        let arr = item.treeFilterPath?.components(separatedBy: ";")
                                        if (arr?.count)! >  0
                                        {
                                            treefilterID = arr![0]
                                            try item.saveToDB(db)
                                            let ID = treefilterID + "-" + item.id!
                                            try db.execute("INSERT OR REPLACE INTO menu_document (ID, MenuID, DocID, CreatedAt) VALUES (?, ?, ?, ?)", arguments: [ID, treefilterID, item.id, Date()])
                                        }
                                      }
                                }
                                return .commit
                            }
                            
                        } catch let e {
                            print(e.localizedDescription)
                        }
                        
                        let attachmentSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ATTACHMENT_DOWNLOAD)
                        if attachmentSelected != nil && (attachmentSelected as! Bool) == true {
                            
                            items.forEach{
                                let dayDistrance:Int = Date().days(from: $0.created!)
                                if ( dayDistrance < self.rangeDateSync )
                                {
                                    $0.downloadFiles()
                                }
                            }
                        }
                        
                    }
                    let createdDate = (result?.items[(result?.items.count)! - 1].created)
                    let dayDistrance:Int = Date().days(from: createdDate!)
                    if ( dayDistrance < self.rangeDateSync )
                    {
                        self.currentPage += 1
                        self.loadingDocumentValidDate()
                    }
                }
                else
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        self.currentPage = 1;
                    })
                }
            }
        })
        
    }
    func dispose()
    {
        timerSync.invalidate()
    }

}
