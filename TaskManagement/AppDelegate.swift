  //
//  AppDelegate.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/1/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import Messages
import UserNotifications
import SVProgressHUD
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    Fabric.with([Crashlytics.self])
    SVProgressHUD.setDefaultMaskType(.clear)
    IQKeyboardManager.shared.enable = true
    do {
        if WhatsNewManager.shared.updateAppVersion() {
            try dropAndRecreateDatabase(application)
        } else {
            try setupDatabase(application)
        }
      
    } catch let e {
      print(e.localizedDescription)
    }

    // For iOS 10 display notification (sent via APNS)
    UNUserNotificationCenter.current().delegate = self
    
    let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
    UNUserNotificationCenter.current().requestAuthorization(
        options: authOptions,
        completionHandler: {_, _ in })
    //Firebase config
    FirebaseApp.configure()
    Messaging.messaging().delegate = self
    
    application.registerForRemoteNotifications()

    // Override point for customization after application launch.
    
    UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-100, 0), for:UIBarMetrics.default)

    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    DocumentManagement.shared.syncDocument()
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    DocumentManagement.shared.dispose()
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
  }
  
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    var token = NSString(format: "%@", deviceToken as CVarArg)
    token = token.replacingOccurrences(of: "<", with: "") as NSString
    token = token.replacingOccurrences(of: ">", with: "") as NSString
    token = token.replacingOccurrences(of: " ", with: "") as NSString
    print("APNs token retrieved: \(token)")
  }
  
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    debugPrint("didReceiveRemoteNotification: \(userInfo.debugDescription)")
    let attachmentSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ALERT_NOTIFICATION)
    if attachmentSelected == nil && (attachmentSelected as! Bool) == true {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        // TO-DO: show badge text with count number or punch text.
        processReceivedData(data: userInfo)

    }
  }
  
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    let attachmentSelected = UserDefaults.standard.object(forKey: Constants.SYNC_ALERT_NOTIFICATION)
    if attachmentSelected == nil && (attachmentSelected as! Bool) == true {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        debugPrint("didReceiveRemoteNotification.data: \(userInfo.debugDescription)")
        //    processReceivedData(data: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)    }
  }

  
}
  
extension AppDelegate {
  
  static func showPushNotification(title: String, body: String, withSecond: Int, userData: [String:Any]? = nil) {
    let interval = TimeInterval(withSecond)
    if #available(iOS 10.0, *) {
      let trigger = UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: false)
      let content = UNMutableNotificationContent()
      content.title = title
      content.body = body
      content.userInfo = userData ?? [:]
      let req = UNNotificationRequest(identifier: "localPushNotification", content: content, trigger: trigger)
      let center = UNUserNotificationCenter.current()
      center.removeAllDeliveredNotifications()
      center.add(req, withCompletionHandler: nil)
    } else {
      let notification = UILocalNotification()
      notification.alertTitle = title
      notification.alertBody = body
      notification.alertAction = "open"
      notification.fireDate = Date().addingTimeInterval(interval)
      notification.soundName = UILocalNotificationDefaultSoundName
      notification.userInfo = userData
      UIApplication.shared.cancelAllLocalNotifications()
      UIApplication.shared.scheduleLocalNotification(notification)
    }
  }
  
  func processReceivedData(data: [AnyHashable : Any]) {
    if let encryptData: String = data["msg"] as? String,
       let cleanData: String = SPCrypto.default.decryptAES(cleanText: encryptData),
       let documentData: DocumentNotification = DocumentNotification(JSONString: cleanData) {
      debugPrint("AppDelegate.processReceivedData: \(String(describing: documentData.toJSONString()))")
      // Go to detail.
      let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      //documentData.itemID = "c5600e51-875b-402f-8822-a82c5ac1dae4"
      detailVC.documentFromNotification = documentData
      let navController = UINavigationController.init(rootViewController: detailVC)
        DispatchQueue.main.async {
            if let window = self.window,
                let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(navController, animated: true, completion: nil)
            }
        }
      
    } else {
      debugPrint("AppDelegate.processReceivedData.error: invalid data")
    }
  }
  
  func registerNotification(application: UIApplication) {
    if #available(iOS 10.0, *) {
      // For iOS 10 display notification (sent via APNS)
      UNUserNotificationCenter.current().delegate = self
      let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
      UNUserNotificationCenter.current().requestAuthorization(
        options: authOptions,
        completionHandler: {_, _ in })
    } else {
      let settings: UIUserNotificationSettings =
        UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
      application.registerUserNotificationSettings(settings)
    }
    application.registerForRemoteNotifications()
  }
  
}
  
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
  
//MARK :- Notification delegate
  @available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // iOS10+, called when presenting notification in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        completionHandler([.alert])
        if let aps = userInfo["aps"] as? [String:Any], let badge = aps["badge"] as? Int{
            UIApplication.shared.applicationIconBadgeNumber = badge
        }
        NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
    }

    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("didReceiveResponse: \(userInfo)")
        //TODO: Handle background notification
        processReceivedData(data: userInfo)
        completionHandler()
    }
}

